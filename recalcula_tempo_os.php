<?
/**
* Corpo do Manusis - Arquivo geral que administra o fluxo das informa��es
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package engine
*/

// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) {
    die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configura��es
elseif (!require("conf/manusis.conf.php")) {
    die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
    die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
    die ($ling['bd01']);
}
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/delcascata.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/autent.php")) {
    die ($ling['autent01']);
}
elseif (!require("lib/forms.php")) {
    die ($ling['autent01']);
}
// Modulos
elseif (!require("conf/manusis.mod.php")) {
    die ($ling['mod01']);
}
// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) {
    $manusis['tema']="padrao";
}


// Obtem as orden que tiveram apontamento
$sql = "SELECT DISTINCT MID_ORDEM FROM ".ORDEM_MAODEOBRA."";

$rs = $dba[$tdb[ORDEM_MAODEOBRA]['dba']]->execute($sql);

if(!$rs){
   erromsg($dba[0] -> ErrorMsg());
   exit;
}


$totalReg =  $rs->RecordCount();

unset($rs);

// Limite de ordens selecionadas
$limit = 10;


if(isset($_GET['count'])){

    $count = (int) $_GET['count'];

    $rs = $dba[$tdb[ORDEM_MAODEOBRA]['dba']]->selectLimit($sql, $limit, $count);

    if(!$rs){
       erromsg($dba[0] -> ErrorMsg());
       exit;
    }

    while(!$rs->EOF){
        TempoServcoOS($rs->fields['MID_ORDEM']);
        $rs->moveNext();
    }

    echo "Atualizado : " . ($count) . " de ".($totalReg/$limit) ."  paginas ";

    // Roda mais uma ves?
    if($totalReg >= ($count * $limit)){

        $count++;

        echo "<script>
                window.location = 'recalcula_tempo_os.php?count={$count}';
        </script>";

    }
    else{
        echo "<br /><br />Total de {$totalReg} O.S atualizadas";
    }
     exit;
}
 
echo "<a href=\"recalcula_tempo_os.php?count=0\">Click aqui para Iniciar</a>";