<?

// Funções de Estrutura
if (!require_once("lib/mfuncoes.php")) die ("Impossível continuar, arquivo de estrutura não pode ser carregado.");
// Configurações
elseif (!require_once("conf/manusis.conf.php")) die ("Impossível continuar, arquivo de configuração não pode ser carregado.");
// Idioma
elseif (!require_once("lib/idiomas/".$manusis['idioma'][0].".php")) die ("Impossível continuar, arquivo de idioma não pode ser carregado.");
// Biblioteca de abstração de dados
elseif (!require_once("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
if (!require_once("lib/bd.php")) die ($ling['bd01']);



echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"Manusis Padr�o\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "
<script>
// Alerta ao fechar a pagina
window.onbeforeunload = function(e) {
    var e = e || window.event;

    var msg = 'Se fizer isso a atualiza��o pode dar errado!!!!';

    // For IE and Firefox
    if (e) {
        e.returnValue = msg;
    }

    // For Safari
    return msg;
}
</script>
</head>
<body>
<div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>Finalizar a Migra��o de Bases</h1>
</div>
<div id=\"corpo_relatorio\">";


echo "<form method=\"GET\" action=\"\">

<fieldset><legend>Bases de dados</legend>
Base origem: "; 

$carga_sel = (int) $_GET['carga_sel'];

FormSelectD('BASE_ORIGEM', '', 'cargas', $carga_sel, 'carga_sel', 'carga_sel', 'MID', '');

echo "
<input type=\"submit\" name=\"env\" value=\"Finalizar a migra��o\" class=\"botao\" onclick=\"window.onbeforeunload = null;\" />

</fieldset>
</form>
<br><br>";

if ($carga_sel != 0) {

    echo "# COLOCANDO A M�O DE OBRA E O MATERIAL PREVISTO NAS OS... \n";

    // Buscando as OS a serem alteradas
    $sql = "SELECT MID, TIPO, NUMERO FROM " . ORDEM . " WHERE STATUS = 1 AND (TIPO = 1 OR TIPO = 2) AND MID_CARGA = {$carga_sel}";
    if (! $rs = $dba[0] -> Execute ($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
    }
    while (! $rs -> EOF) {
        $cc = $rs -> fields;
        
        echo "<br />\n# Atualizando OS {$cc['NUMERO']} <br />\n";
        
        grava_previsao_os_tmp($cc['MID'], $cc['TIPO']);
        
        $rs -> MoveNext();
    }
    
    echo "<br />\n";
    echo "<br />\n";
    echo "# COLOCANDO OS DADOS DA SOLICITA��O NAS OS... \n";
    
    // Buscando as OS a serem alteradas
    $sql = "SELECT MID, NUMERO, MID_SOLICITACAO FROM " . ORDEM . " WHERE STATUS = 1 AND MID_SOLICITACAO != 0 AND MID_CARGA = {$carga_sel}";
    if (! $rs = $dba[0] -> Execute ($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
    }
    while (! $rs -> EOF) {
        $cc = $rs -> fields;
        
        echo "<br />\n# Atualizando OS {$cc['NUMERO']} <br />\n";
        
        // MONTANDO O TEXTO
        $recomendacoes_texto  = mb_strtoupper(unhtmlentities($tdb[SOLICITACOES]['DESCRICAO'])) . ": " . VoltaValor(SOLICITACOES, 'DESCRICAO', 'MID', $cc['MID_SOLICITACAO']) . "\r\n";
        $recomendacoes_texto .= mb_strtoupper(unhtmlentities($tdb[SOLICITACOES]['TEXTO'])) . ": " . VoltaValor(SOLICITACOES, 'TEXTO', 'MID', $cc['MID_SOLICITACAO']) . "\r\n";
        $recomendacoes_texto .= unhtmlentities($ling['recompensacoes_m']) . ": ";
        
        echo "UPDATE " . ORDEM . " SET TEXTO = CONCAT('$recomendacoes_texto', TEXTO) WHERE MID = {$cc['MID']};<br />\n";
        
        $rs -> MoveNext();
    }
    
    
    echo "<br />\n";
    echo "<br />\n";
    echo "# COLOCANDO OS MATERIAIS NOS ALMOXARIFADOS... \n";
    echo "<br />\n";
    
    // Cadastrando o almoxarifado
    $mid_emp = VoltaValor("cargas", 'MID_EMPRESA', 'MID', $carga_sel);
    
    // Inserindo os mateiriais
    echo "# Cadastrando os materiais no almoxarifado... <br />\n";
    echo "INSERT INTO " . MATERIAIS_ALMOXARIFADO . " (MID_MATERIAL, MID_ALMOXARIFADO, ESTOQUE_ATUAL, CUSTO_UNITARIO)
    SELECT MID, $mid_emp, ESTOQUE_ATUAL, CUSTO_UNITARIO FROM materiais WHERE MID_EMPRESA = $mid_emp;<br />\n";
       
    echo "<br />UPDATE FROM ".PROGRAMACAO." SET STATUS = 3 WHERE STATUS = 2;<br />\n";   
       
}

echo "</div>
</body>
</html>";


function grava_previsao_os_tmp($osmid, $tipo) {
     global $dba,$tdb;
     
     // Buscando os novos
     if ($tipo == 1) {
         $sql = "SELECT ESPECIALIDADE, TEMPO_PREVISTO, QUANTIDADE_MO, MID_MATERIAL, QUANTIDADE FROM " . ORDEM_PREV . " WHERE MID_ORDEM = $osmid";
         $dba_atv = $tdb[ORDEM_PREV]['dba'];
     }
     elseif ($tipo == 2) {
         $sql = "SELECT ESPECIALIDADE, TEMPO_PREVISTO, QUANTIDADE_MO, MID_MATERIAL, QUANTIDADE FROM " . ORDEM_LUB . " WHERE MID_ORDEM = $osmid";
         $dba_atv = $tdb[ORDEM_LUB]['dba'];
     }
     if (! $rs = $dba[$dba_atv] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$dba_atv] ->ErrorMsg() . "<br />" . $sql);
     }

     // SALVANDO AS QUANTIDADES
     $mo_prev = array();
     $mt_prev = array();
     while (! $rs -> EOF) {
         if (! $mo_prev[$rs -> fields['ESPECIALIDADE']]) {
             $mo_prev[$rs -> fields['ESPECIALIDADE']] = 0;
         }
         
         if ($rs -> fields['QUANTIDADE_MO'] == 0) {
             $rs -> fields['QUANTIDADE_MO'] = 1;
         }
         
         $mo_prev[$rs -> fields['ESPECIALIDADE']] += $rs -> fields['TEMPO_PREVISTO'] * $rs -> fields['QUANTIDADE_MO'];

         // Tem Material na Atividade
         if ($rs -> fields['MID_MATERIAL'] != 0) {
             if (! $mt_prev[$rs -> fields['MID_MATERIAL']]) {
                 $mt_prev[$rs -> fields['MID_MATERIAL']] = 0;
             }

             $mt_prev[$rs -> fields['MID_MATERIAL']]  += $rs -> fields['QUANTIDADE'];
         }
         $rs -> MoveNext();
     }

     // M�O DE OBRA
     foreach ($mo_prev as $esp => $tempo) {
         echo "INSERT INTO " . ORDEM_MO_PREVISTO . " (MID_ORDEM, MID_ESPECIALIDADE, TEMPO, QUANTIDADE) VALUES ($osmid, $esp, $tempo, 0);<br />\n";
     }

     // MATERIAIS
     foreach ($mt_prev as $mat => $quant) {
         echo "INSERT INTO " . ORDEM_MAT_PREVISTO . " (MID_ORDEM, MID_MATERIAL, QUANTIDADE) VALUES ($osmid, $mat, $quant);<br />\n";
     }
 
}
?>
