<?
/** 
* Autentificação de ususario e sessão
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  engine
*/

if ($_POST['logar'] != "") {
	
	//     Bloquear para acesso apenas pela Manuservice
//    if  ($_SERVER['REMOTE_ADDR'] != "201.22.58.177") {
//        errofatal($ling['autent05']);
//        exit();
//    }
	
	if (strlen($_POST['usuario']) > 20) errofatal($ling['autent03']); 
	elseif (strlen($_POST['senha']) > 12) errofatal($ling['autent03']);
	if (($_POST['usuario'] == $manusis['admin']['usuario']) and  ($_POST['senha'] == $manusis['admin']['senha'])) {
		$_SESSION[ManuSess]['user']['NOME']=$manusis['admin']['nome'];
		$_SESSION[ManuSess]['user']['EMAIL']=$manusis['admin']['email'];
		$_SESSION[ManuSess]['user']['USUARIO']=$manusis['admin']['usuario'];
		$_SESSION[ManuSess]['user']['MID']="ROOT";
		$_SESSION[ManuSess]['user']['SENHA']=md5($_POST['senha']);	
		$_SESSION[ManuSess]['user']['IDLE']=mktime(date('H'),date('i'),date('s'),date('d'),date('m'),date('Y'));
		if ($manusis['log']['logon'] == 1) logar(1,"","");
	}
	else {
		$senha=md5($_POST['senha']);
		$resultado=$dba[$tdb[USUARIOS]['dba']] -> Execute("SELECT * FROM ".USUARIOS." WHERE USUARIO = '".$_POST['usuario']."' AND SENHA = '$senha'");
		$tmp=$resultado->fields;
		$i=count($resultado -> getrows());
      	if ($i == 0) errofatal($ling['autent02']);		
		else {
			$_SESSION[ManuSess]['user']['NOME']=$tmp['NOME'];
			$_SESSION[ManuSess]['user']['EMAIL']=$tmp['EMAIL'];
			$_SESSION[ManuSess]['user']['USUARIO']=$tmp['USUARIO'];
			$_SESSION[ManuSess]['user']['GRUPO']=$tmp['GRUPO'];
			$_SESSION[ManuSess]['user']['MID']=$tmp['MID'];
			$_SESSION[ManuSess]['user']['SENHA']=$tmp['SENHA'];
			$_SESSION[ManuSess]['user']['IDLE']=mktime(date('H'),date('i'),date('s'),date('d'),date('m'),date('Y'));
			Setar_Usuario_Online($_SESSION[ManuSess]['user']['NOME'],$_SESSION[ManuSess]['user']['MID'],$_SERVER['REMOTE_ADDR'],$_SESSION[ManuSess]['user']['IDLE']);
			if ($manusis['log']['logon'] == 1) logar(1,"","");
		}			
	}
}
else {
	if (strlen($_SESSION[ManuSess]['user']['USUARIO']) > 20) errofatal($ling['autent03']);
	if (($_SESSION[ManuSess]['user']['USUARIO'] == $manusis['admin']['usuario']) and  ($_SESSION[ManuSess]['user']['SENHA'] == md5($manusis['admin']['senha']))) {
		$_SESSION[ManuSess]['user']['NOME']=$manusis['admin']['nome'];
		$_SESSION[ManuSess]['user']['EMAIL']=$manusis['admin']['email'];
		$_SESSION[ManuSess]['user']['USUARIO']=$manusis['admin']['usuario'];
					
		$_SESSION[ManuSess]['user']['MID']="ROOT";
		$tmp=mktime(date('H'),date('i'),date('s'),date('d'),date('m'),date('Y')) - $_SESSION[ManuSess]['user']['IDLE'];
		if ($manusis['sess']['expira'] == 1) {
			if ($tmp > $manusis['sess']['tempo']) {
				if ($manusis['log']['logoff'] == 1) logar(2,"");
				unset ($_SESSION[ManuSess]['user']);        		
				errofatal($ling['autent04']);
			}
		}
		else $_SESSION[ManuSess]['user']['IDLE']=mktime(date('H'),date('i'),date('s'),date('d'),date('m'),date('Y'));
	}
	else {
		if ($manusis['sess']['expira'] == 1) {
			$tmp_expira=$dba[$tdb[USUARIOS_ONLINE]['dba']] -> Execute("SELECT USUARIO FROM ".USUARIOS_ONLINE." WHERE MID = '".$_SESSION[ManuSess]['user']['MID']."'");
			$tmp=$tmp_expira -> Fields['MID'];
        	if (count($tmp_expira -> getrows()) == 0) { 
        		if ($manusis['log']['logoff'] == 1) logar(2,"");
        		unset ($_SESSION[ManuSess]['user']);        		
        		errofatal($ling['autent04']);
        	}
		}
		$resultado=$dba[$tdb[USUARIOS]['dba']] -> Execute("SELECT * FROM ".USUARIOS." WHERE USUARIO = '".$_SESSION[ManuSess]['user']['USUARIO']."' AND SENHA = '".$_SESSION[ManuSess]['user']['SENHA']."'");
        $tmp=$resultado->fields;
		$i=count($resultado -> getrows());
        if ($i == 0) errofatal($ling['autent02']);
		else {
			$_SESSION[ManuSess]['user']['NOME']=$tmp['NOME'];
			$_SESSION[ManuSess]['user']['EMAIL']=$tmp['EMAIL'];
			$_SESSION[ManuSess]['user']['USUARIO']=$tmp['USUARIO'];
			$_SESSION[ManuSess]['user']['GRUPO']=$tmp['GRUPO'];
			$_SESSION[ManuSess]['user']['MID']=$tmp['MID'];
			$_SESSION[ManuSess]['user']['SENHA']=$tmp['SENHA'];
			$_SESSION[ManuSess]['user']['IDLE']=mktime(date('H'),date('i'),date('s'),date('d'),date('m'),date('Y'));
			$dba[$tdb[USUARIOS]['dba']] -> Execute("UPDATE ".USUARIOS_ONLINE." SET TEMPO = '".$_SESSION[ManuSess]['user']['IDLE']."' WHERE MID = '".$_SESSION[ManuSess]['user']['MID']."'");
		}			
	}
}
?>