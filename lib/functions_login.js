/*
 * Função para controlar a visualização dos botões de downloads dos app android
 * aparecem somente com aparelhos que sejam móveis
 */
function toggleDownloadLinks(){
    var box = document.getElementById("download-box");
    var warning = document.getElementById("download-warning");
    if(box.style.display != 'none'){
        warning.style.display= 'block';
        box.style.display= 'none';
    }else{
        warning.style.display= 'none';
        box.style.display= 'block';
    }
}

/*
 * Valida campos
 */
function validate()
{
    var user = document.getElementById("user").value;
    var pass = document.getElementById("password").value;

    if (user == '' || pass == '') {
        return false;
    }
    return true;
}

/*
 * Atribui foco direto no campo de texto
 */
function setFocusUser()
{
    document.getElementById("user").focus();
}

function setFocusEmail()
{
    document.getElementById("email").focus();
}

/*
 * Controla visualização da caixa de mensagens (warnings) de tentativa incorreta de autenticação
 */
 function setDisplayDiv(){
    if (document.getElementById('error-warning') !== null) {
        document.getElementById('error-warning').style.display = 'none';
    }
}

/*
 * Verifica dispositivo
 * 1. Usado para teste (comentar para invalidar o navegador chrome),
 * não deixando que os botões de download dos apps apareçãm quando acessado via browsers de desktop;
 */
 function checkUserAgent(){
    if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
//        || navigator.userAgent.match(/Chrome/i)  // 1
    ) {
        document.getElementById("download-box").style.display= "inline-table";
        document.getElementsByTagName('html')[0].className += ' ' + 'vert-scroll';
    }else{
        document.getElementById("download-box").style.display= "none";
    }
}

/*
 * Obtem a altura da janela do navegador
 */
function getViewport() {
    var viewPortHeight;
    if (typeof window.innerWidth != 'undefined') {
      viewPortHeight = window.innerHeight
    }
    else if (typeof document.documentElement != 'undefined'
    && typeof document.documentElement.clientWidth !=
    'undefined' && document.documentElement.clientWidth != 0) {
       viewPortHeight = document.documentElement.clientHeight
    }
    else {
      viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
    }
    return viewPortHeight;
}

/*
 * Obtem a altura do documento html, medida interna da página
 */
function getDocHeight() {
    var D = document;
    return Math.max(
        //D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

/*
 * Calcula posição e tamanho do rodapé
 * 1. Ajusta comprimento do footer
 * 2. Ajusta inicio da barra do rodapé
 */
function mobileFooterAdjust(){
    var footer = document.getElementsByClassName("footer-container");

    var d = getDocHeight();
    var w = getViewport();

    if(w<d) {
//        console.log("Desfixar");
        footer[0].style.position= 'static';
    }else{
//        console.log("Fixar");
        footer[0].style.position= 'fixed';
        footer[0].style.bottom= 0;
    }
    footer[0].style.width= '100%'; // 1
    footer[0].style.left= '0'; // 2
}


/*
 * Verifica qual navegador está acessando e roda a função de ajustar rodapé (exceção firefox)
 * @returns {}
 */
function stickyFooter() {
    if (navigator.userAgent.match(/Firefox/i)) {
        window.onload=function() {
            mobileFooterAdjust();
        };
    }else{
        mobileFooterAdjust();
    }
}