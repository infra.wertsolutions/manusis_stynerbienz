/*
* Altera��es
* Autor: Welington Nunes Siemiatkowski
* Data: 02/06/2006
* Descri��o: Foram adicionadas as funcoes para controle do calendario
*
* Autor: Welington Nunes Siemiatkowski
* Data: 17/06/2006
* Descri��o: Foi adicionada a funcao para validacao de numeros inteiros
*/


function mostraCarregando (mostra, frame, texto) {
    
    if (! texto) {
        texto = "Aguarde ...";
    }
    
    if(window.parent) {
        frame = true;
    }
    
    if (frame) {
        div_carregando = parent.document.getElementById('div_carregando');
    }
    else {
        div_carregando = document.getElementById('div_carregando');
    }
    
    // Criando elemento
    if (! div_carregando) {
        // div
        div_carregando = document.createElement('div');
        div_carregando.id = 'div_carregando';
        div_carregando.style.display = 'none';
        div_carregando.style.padding = '10px';
        div_carregando.style.zIndex = '99';
        div_carregando.style.backgroundColor = 'white';
        div_carregando.style.border = '1px solid #ccc';
        div_carregando.style.position = 'fixed';
        div_carregando.style.top = '5px';
        div_carregando.style.right = '5px';
        
        
        // Adicionar texto e imagem
        div_carregando.innerHTML = '<img src="imagens/icones/ajax-loader.gif" border="0" align="middle" style="margin-right:5px;" /><strong>' + texto + '</strong>';
        
        // Colocando na tela
        if (frame) {
            parent.document.body.appendChild(div_carregando);
        }
        else {
            document.body.appendChild(div_carregando);
        }
    }
    else {
        // Adicionar texto e imagem
        div_carregando.innerHTML = '<img src="imagens/icones/ajax-loader.gif" border="0" align="middle" style="margin-right:5px;" /><strong>' + texto + '</strong>';
    }
    
    if (mostra === true) {
        div_carregando.style.display = '';
    }
    else {
        div_carregando.style.display = 'none';
    }
}

function bloqueiaPagina (mostra, frame) {
    
    if(window.parent) {
        frame = true;
    }
    
    if (frame) {
        div_bloq = parent.document.getElementById('div_bloqueia');
    }
    else {
        div_bloq = document.getElementById('div_bloqueia');
    }
    
    // Criando elemento
    if (! div_bloq) {
        // div
        div_bloq = document.createElement('div');
        div_bloq.id = 'div_bloqueia';
        div_bloq.style.display = 'none';
        div_bloq.style.zIndex = '90';
        div_bloq.style.position = 'fixed';
        div_bloq.style.top = '0px';
        div_bloq.style.left = '0px';
        div_bloq.style.backgroundColor = "#ccc";
        div_bloq.style.width = '100%';
        div_bloq.style.height = '100%';
        div_bloq.style.filter= "alpha(opacity=50)";
        div_bloq.style.opacity= "0.5";
        
        // Colocando na tela
        if (frame) {
            parent.document.body.appendChild(div_bloq);
        }
        else {
            document.body.appendChild(div_bloq);
        }
    }
    
    if (mostra === true) {
        div_bloq.style.display = '';
        if (frame) {
            window.parent.onbeforeunload = function(e) {
                var e = e || window.event;

                var msg = 'Ao fechar a janela a atualiza��o n�o ser� concluida!!';

                // For IE and Firefox
                if (e) {
                    e.returnValue = msg;
                }

                // For Safari
                return msg;
            }
        }
        else {
            window.onbeforeunload = function(e) {
                var e = e || window.event;

                var msg = 'Ao fechar a janela a atualiza��o n�o ser� concluida!!';

                // For IE and Firefox
                if (e) {
                    e.returnValue = msg;
                }

                // For Safari
                return msg;
            }
        }
    }
    else {
        if (frame) {
            window.parent.onbeforeunload = null;
        }
        else {
            window.onbeforeunload = null;
        }
        
        div_bloq.style.display = 'none';
    }
}

function urlencode(str) {
    return encodeURIComponent(str);
}

// XMLHTTP Fun��es
try{
    xmlvar = new XMLHttpRequest();
}catch(ee){
    try{
        xmlvar = new ActiveXObject("Msxml2.XMLHTTP");
    }catch(e){
        try{
            xmlvar = new ActiveXObject("Microsoft.XMLHTTP");
        }catch(E){
            xmlvar = false;
        }
    }
}
function ajax_get(url, ob){
    // XMLHTTP Fun��es
    try{
        xmlvar = new XMLHttpRequest();
    }catch(ee){
        try{
            xmlvar = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(e){
            try{
                xmlvar = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(E){
                xmlvar = false;
            }
        }
    }

    xmlvar.open("GET", url, true);
    xmlvar.onreadystatechange=function() {
        // Mostrar div de carregando
        if (xmlvar.readyState==1){
            mostraCarregando(true);
        }

        if (xmlvar.readyState==4){
            var texto=xmlvar.responseText;
            texto = texto.replace(/^\s+|\s+$/g,"")
            if(texto != ""){
                alert(texto);
            }

            if((xmlvar.borda) && ((browser.indexOf("MSIE") == -1) || ((browser.indexOf("MSIE") != -1) && (version > 6)))){
                xmlvar.borda[0].style.borderColor = xmlvar.borda[1];
                xmlvar.borda = null;
            }

            // Fecha o div
            mostraCarregando(false);
        }
    }
    xmlvar.send(null)
}

function ajax_get2(url, ob){
    // XMLHTTP Fun��es
    try{
        xmlvar = new XMLHttpRequest();
    }catch(ee){
        try{
            xmlvar = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(e){
            try{
                xmlvar = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(E){
                xmlvar = false;
            }
        }
    }

    xmlvar.open("GET", url, false);
    xmlvar.onreadystatechange = function() {
        // Mostrar div de carregando
        if (xmlvar.readyState==1){
            mostraCarregando(true);
        }

        if (xmlvar.readyState==4){
            // Fecha o div
            mostraCarregando(false);
        }
    }
    xmlvar.send(null);
    
    var texto=xmlvar.responseText;
    texto = texto.replace(/^\s+|\s+$/g,"")
    if(texto != ""){
        return texto;
    }
}

function atualiza_area(id,url){
    var area=document.getElementById(id)
    //area.innerHTML='<div id=\"carregando\">carregando...</div>';
    xmlvar.open("GET", url,true);
    xmlvar.onreadystatechange=function() {
        // Mostrar div de carregando
        if (xmlvar.readyState==1){
            mostraCarregando(true);
        }
        
        if (xmlvar.readyState==4){
            var texto=xmlvar.responseText
            var area=document.getElementById(id)
            area.innerHTML=texto
            
            mostraCarregando(false);
        }
    }
    xmlvar.send(null)
}

function atualiza_formselect(id,url){
    var area=document.getElementById(id)
    area.innerHTML='<select class=\"campo_select\"><option>carregando...</option></select>'
    xmlvar.open("GET", url,true);
    xmlvar.onreadystatechange=function() {
        if (xmlvar.readyState==4){
            var texto=xmlvar.responseText
            var area=document.getElementById(id)
            area.innerHTML=texto
        }
    }
    xmlvar.send(null)
}

//Fila
var fila=[];
var ifila=0;


function atualiza_area2(id, url, func, progresso){
    fila[fila.length] = [id, url, func, progresso]
    if ((ifila + 1) == fila.length) {
        RodaAjax()
    }
}

function RodaAjax(){
    xmlvar.open("GET",fila[ifila][1],true);
    xmlvar.onreadystatechange=function() {
        // Mostrar div de carregando
        if ((xmlvar.readyState == 1) && (fila[ifila][3] !== false)){
            mostraCarregando(true);
        }

        if (xmlvar.readyState==4){
            // retorno=unescape()
            
            document.getElementById(fila[ifila][0]).innerHTML=xmlvar.responseText;
            
            if (fila[ifila][2]) {
                eval(fila[ifila][2] + "()");
            }
                        
            // Fechar div de carregando
            if (fila[ifila][3] !== false) {
                mostraCarregando(false);
            }
            
            // Corre pro proximo
            ifila++;
            if (ifila < fila.length) {
                setTimeout("RodaAjax()", 5);
            }
        }
    }
    xmlvar.send(null)
}


function atualiza_area_frame(id,url){
    var area=parent.document.getElementById(id)
    xmlvar.open("GET", url,true);
    xmlvar.onreadystatechange = function() {
        // Mostrar div de carregando
        if (xmlvar.readyState==1){
            mostraCarregando(true, true);
        }
        
        if (xmlvar.readyState==4){
            var texto=xmlvar.responseText
            var area=parent.document.getElementById(id)
            area.innerHTML=texto
            
            mostraCarregando(false, true);
        }
    }
    xmlvar.send(null)
}
var fila2=[];
var ifila2=0;
function atualiza_area_frame2(id,url,atualiza){
    fila2[fila2.length]=[id,url,atualiza]
    if ((ifila2 + 1) == fila2.length) {
        RodaAjax2()
    }
}


function RodaAjax2(){
    
    // XMLHTTP Fun��es
    try{
        xmlvar3 = new XMLHttpRequest();
    }catch(ee){
        try{
            xmlvar3 = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(e){
            try{
                xmlvar3 = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(E){
                xmlvar3 = false;
            }
        }
    }
    
    xmlvar3.open("GET",fila2[ifila2][1],true);
    xmlvar3.onreadystatechange=function() {
        // Mostrar div de carregando
        if (xmlvar3.readyState==1){
            mostraCarregando(true);
        }

        if (xmlvar3.readyState==4){
            // retorno=unescape()
            if(fila2[ifila2][3] === true){
                parent.document.getElementById(fila2[ifila2][0]).value = xmlvar3.responseText;
            }
            else {
                if(parent.document.getElementById(fila2[ifila2][0])){
                    parent.document.getElementById(fila2[ifila2][0]).innerHTML=xmlvar3.responseText;
                }
            }
            // Verifica se deve atualizar a pagina
            if(fila2[ifila2][2] === true){
                parent.window.location.href = parent.window.location.href;
            }
            
            // Fechar div de carregando
            mostraCarregando(false);
            
            // Corre pro proximo
            ifila2++
            if (ifila2 < fila2.length) {
                setTimeout("RodaAjax2()", 5)
            }

            

        }
    }
    xmlvar3.send(null)
}

function submit_form(){
    document.forms[0].submit();
}
tms=new Array()
function menu_sobre(n){
    if(typeof(tms[n])!="undefined")clearTimeout(tms[n])
    document.getElementById("m_"+n).style.visibility="visible"
}
//Esconde o submenu no mouseout
function menu_fora(n){
    tms[n]=setTimeout('document.getElementById("m_'+n+'").style.visibility="hidden"',200)
}

function bloco_mostra(n){
    if(typeof(tms[n])!="undefined")clearTimeout(tms[n])
    document.getElementById(n).style.visibility="visible"
}
//Esconde o submenu no mouseout
function bloco_esc(n){
    tms[n]=setTimeout('document.getElementById("' + n + '").style.visibility="hidden"',200)
}





function abre_filtro_formselect(id,form){
    if (document.getElementById(id).style.visibility == "hidden") {
        document.getElementById(id).style.visibility = "visible";
        document.getElementById(form).focus();
        return;
    }
    return;
}








function abre_janela_deleta(deleta, oq, logar){
    // Bloqueando a pagina
    bloqueiaPagina(true);
    
    document.getElementById('div_formulario').style.height = '105px';
    document.getElementById('div_formulario').style.width  = '300px';
    document.getElementById('div_formulario').style.visibility = "visible";
    document.getElementById('div_formulario').style.zIndex = '99';
    atualiza_area2('div_formulario','manusis.php?st=8&deleta=' + deleta + '&oq=' + oq + '&logar=' + logar)
    return;
}

function abre_janela_apontaos(osmid){
    document.getElementById('div_formulario').style.height='420px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','manusis.php?st=4&osmid=' + osmid)
    return;
}
function abre_janela_prog(mid){
    document.getElementById('div_formulario').style.height='420px';
    document.getElementById('div_formulario').style.width='630px';
    document.getElementById('div_formulario').style.visibility="visible";
    atualiza_area2('div_formulario','manusis.php?st=6&foq=' + mid);
    return;
}
function abre_janela_sol(mid){
    document.getElementById('div_formulario').style.height='420px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','manusis.php?st=7&foq=' + mid)
    return;
}
function abre_janela_apontaosplan(osmid){
    document.getElementById('div_formulario').style.height='420px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','manusis.php?st=5&osmid=' + osmid)
    return;
}
function abre_janela_repeteativ(mid, arq, id, op, exe, oq){
    document.getElementById('div_formulario').style.height='420px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','repeteativ_chama.php?mid_plano=' + mid + '&a=' + arq + '&id='+ id + '&op='+ op + '&exe='+ exe + '&oq='+ oq)
    return;
}
function abre_janela_linka_mat(mid_maq, mid){
    document.getElementById('div_formulario').style.height='300px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','linka_mat_chama.php?mid_maq=' + mid_maq + '&mid=' + mid)
    return;
}
function abre_janela_solicitacao(){
    document.getElementById('div_formulario').style.height='420px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','solic_cadastro_chama.php')
    return;
}
function abre_janela_apontaosplanPen(osmid,ospen){
    document.getElementById('div_formulario').style.height='420px'
    document.getElementById('div_formulario').style.width='630px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','manusis.php?st=5&osmid=' + osmid + '&ospen=' + ospen)
    return;
}
function abre_janela_arq(form,oq,id,desc){
    
    if(desc == undefined){
	desc = '';
    }
    else {
	desc = escape(desc);
    }
    
    document.getElementById('div_formulario').style.height='400px'
    document.getElementById('div_formulario').style.width='520px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','manusis.php?st=3&oq=' + oq + '&f=' + form + '&id=' + id + '&desc=' + desc)
    return;
}
function fecha_janela_form(){
    var area=document.getElementById('div_formulario_corpo')
    area.innerHTML=''

    document.getElementById('div_formulario').style.visibility="hidden"
    document.getElementById('div_formulario_corpo').style.visibility="hidden"
    return;
}
function fecha_janela_form_frame(){
    parent.document.getElementById('div_formulario').style.visibility="hidden"
    parent.document.getElementById('div_formulario_corpo').style.visibility="hidden"
    return;
}
function fecha_janela_parm(){
    document.getElementById('div_formulario_parm').style.visibility="hidden"
    document.getElementById('div_formulario_corpo_parm').style.visibility="hidden"
    return;
}
// Alterado por Welington Nunes Siemiatkowski - 03/06/2006 15:35
function abre_janela_form(form,foq,oq,act,idm,exe,op,altura,largura,atualiza,recb_valor){
    document.getElementById('div_formulario').style.height=altura + 'px'
    document.getElementById('div_formulario').style.width=largura + 'px'
    document.getElementById('div_formulario').style.visibility="visible"
    atualiza_area2('div_formulario','manusis.php?st=2&act=' + act + '&id=' + idm + '&exe=' + exe + '&op=' + op + '&foq=' + foq + '&f=' + form + '&oq=' + oq + '&atualiza=' + atualiza + '&form_recb_valor=' + recb_valor)
    return;
}
// ---------------------------------------------
// --- Função para Fechar a TreeView  --
// ---------------------------------------------
function FecharEmpresa(id){
    idm=document.getElementById(id);
    if (idm.childNodes.item(4).style.display == "") {
        idm.childNodes.item(4).style.display="block";
        idm.childNodes.item(0).childNodes.item(0).src = "imagens/icones/menos.gif"
    }
    else {
        idm.childNodes.item(4).style.display="";
        idm.childNodes.item(0).childNodes.item(0).src = "imagens/icones/mais.gif"
    }
}

function FecharArea(id){
    idm=document.getElementById(id);
    idimg=idm.getElementsByTagName("IMG");
    idul=idm.getElementsByTagName("UL");
    if (idul[0].style.display == "") {
        idul[0].style.display="block";
        idimg[0].src = "imagens/icones/menos.gif"
        idimg[1].src = "imagens/icones/22x22/local1_abre.png"

    }
    else {
        idul[0].style.display="";
        idimg[0].src = "imagens/icones/mais.gif"
        idimg[1].src = "imagens/icones/22x22/local1.png"
    }
}


function FecharSetor(id,icone){
    idul=document.getElementById('setor_' + id);
    idimg=document.getElementById('isetor_' + id);
    if (idul.style.display == "") {
        idul.style.display="block";
        icone.src = "imagens/icones/menos.gif"
        idimg.src = "imagens/icones/22x22/local2_abre.png"
        atualiza_area('setor_' + id,'modulos/cadastro/va_ajax.php?va_atu=setor&mid=' + id)
    }
    else {
        idul.style.display="";
        icone.src = "imagens/icones/mais.gif"
        idimg.src = "imagens/icones/22x22/local2.png"
    }
}

function FecharMaquina(id,icone){
    idul=document.getElementById('maq_' + id);
    idimg=document.getElementById('imaq_' + id);
    
    if (idul.style.display == "") {
        idul.style.display="block";
        icone.src = "imagens/icones/menos.gif"
        idimg.src = "imagens/icones/22x22/local3_abre.png"
        atualiza_area('maq_' + id,'modulos/cadastro/va_ajax.php?va_atu=maq&mid=' + id)

    }
    else {
        idul.style.display="";
        icone.src = "imagens/icones/mais.gif"
        idimg.src = "imagens/icones/22x22/local3.png"
    }
}

function FecharItem(id, link){
    idul=document.getElementById(id).getElementsByTagName("ul")[0];
    icone = link.getElementsByTagName("img")[0];
    
    if (idul.style.display == "") {
        idul.style.display="block";
        icone.src = "imagens/icones/menos.gif";
    }
    else {
        idul.style.display="";
        icone.src = "imagens/icones/mais.gif";
    }
}

function FecharConj(id,icone){
    idul=document.getElementById(id);
    if (idul.style.display == "") {
        idul.style.display="block";
        icone.src = "imagens/icones/menos.gif"
    }
    else {
        idul.style.display="";
        icone.src = "imagens/icones/mais.gif"
    }
}
function FecharSConj(id){
    idm=document.getElementById(id);
    idimg=idm.getElementsByTagName("IMG");
    idul=idm.getElementsByTagName("UL");
    if (idul[0].style.display == "") {
        idul[0].style.display="block";
        idimg[0].src = "imagens/icones/menos.gif"
        idimg[1].src = "imagens/icones/22x22/tag.png"

    }
    else {
        idul[0].style.display="";
        idimg[0].src = "imagens/icones/mais.gif"
        idimg[1].src = "imagens/icones/22x22/tag.png"
    }
}
function FecharEquip(id){
    idm=document.getElementById(id);
    idimg=idm.getElementsByTagName("IMG");
    idul=idm.getElementsByTagName("UL");
    if (idul[0].style.display == "") {
        idul[0].style.display="block";
        idimg[0].src = "imagens/icones/menos.gif"
        idimg[1].src = "imagens/icones/22x22/equip.png"

    }
    else {
        idul[0].style.display="";
        idimg[0].src = "imagens/icones/mais.gif"
        idimg[1].src = "imagens/icones/22x22/equip.png"
    }
}
function FecharMat(id){
    idm=document.getElementById(id);
    idimg=idm.getElementsByTagName("IMG");
    idul=idm.getElementsByTagName("UL");
    if (idul[0].style.display == "") {
        idul[0].style.display="block";
        idimg[0].src = "imagens/icones/menos.gif"
        idimg[1].src = "imagens/icones/22x22/material.png"

    }
    else {
        idul[0].style.display="";
        idimg[0].src = "imagens/icones/mais.gif"
        idimg[1].src = "imagens/icones/22x22/material.png"
    }
}
function abre_div(id,form){
    if (document.getElementById(id).style.visibility == "hidden") {
        document.getElementById(id).style.visibility = "visible";
        document.getElementById(form).focus();
        return;
    }
    if (document.getElementById(id).style.visibility == "visible") {
        document.getElementById(id).style.visibility = "hidden";

        return;
    }

    if (document.getElementById(id).style.display == "none") {
        document.getElementById(id).style.display = "";
        return;
    }
    if ((document.getElementById(id).style.display == "block") || (document.getElementById(id).style.display == "")) {
        document.getElementById(id).style.display = "none";
        return;
    }
    return;
}

// função genérica que funciona para todas as localizações e DIVs de visão em árvore
function FecharGenerico(id, campo_img){
    idm=document.getElementById(id);
    idimg=idm.getElementsByTagName("IMG");
    idul=idm.getElementsByTagName("UL");
    if (idul[0].style.display == "") {
        idul[0].style.display="block";
        idimg[0].src = "imagens/icones/menos.gif";
        idimg[1].src = "imagens/icones/22x22/"+campo_img+"_abre.png";

    }
    else {
        idul[0].style.display="";
        idimg[0].src = "imagens/icones/mais.gif";
        idimg[1].src = "imagens/icones/22x22/"+campo_img+".png";
    }
}

function Aba(url, id, td) {
    if (td == null) td = this;
    if (id != null) {
        var list = td.parentNode.getElementsByTagName('td');
        for (var i = 0; i < list.length; i++) {
            if ((list[i].className == 'aba_sel') && (list[i] != td)) list[i].className = 'aba';
        }
        td.className = 'aba_sel';
        atualiza_area2(id,url);
    }
    else {
        location.href=url;
    }
}

function janela_varvore(theURL,winName) {
    window.open(theURL,winName,'resizable=1, scrollbars=1, status=0, titlebar=0, toolbar=0 ,width=450,height=460');
}
function janela(theURL,winName) {
    window.open(theURL,winName,'resizable=1, scrollbars=1, status=0, titlebar=0, toolbar=0, width=100, height=100');
}

function confirma_delete(deleta, oq, texto) {
    // Verificando se esse registro pode ser deletado
    tab_motivo = unescape(ajax_get2('parametros.php?id=verifica_delcascata&tabela=' + deleta + '&oq=' + oq));
    if ((tab_motivo != '') && ((tab_motivo != 'undefined'))) {
        alert("O REGISTRO N�O PODE SER DELETADO.\nMOTIVO: " + tab_motivo);
    }
    // Sem impedimentos
    else {        
        // Texto padr�o
        if (! texto) {
            texto = 'DESEJA REALMENTE REMOVER ESSE ITEM?';
        }
        
        // Verificando a cascata
        tab_cascata = unescape(ajax_get2('parametros.php?id=lista_delcascata&tabela=' + deleta));
        if ((tab_cascata != '') && ((tab_cascata != 'undefined'))) {
            texto += "\nOS SEGUINTES CADASTROS SER�O REMOVIDOS EM CASCATA: " + tab_cascata;
        }
        
        // Confirmando a opera��o
        var is_confirmed = confirm(texto);
        if (is_confirmed) {
            abre_janela_deleta(deleta, oq);
        }
    }
}


function confirma(Link, Alvo, Tipo) {
    if(Tipo == 2) {
        var is_confirmed = prompt(Alvo);
        if ((is_confirmed != '') && (is_confirmed != null)) {
            Link.href += '&confirma=1&msg=' + is_confirmed;
            return true;
        }
        else {
            return false;
        }
    }   
    else {
        var is_confirmed = confirm(Alvo);
        if (is_confirmed) {
            Link.href += '&confirma=1';
        }
    }
    
    return is_confirmed;
}

function irurl() {
    var i, args=irurl.arguments; document.r = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function ajustar_data(input , evento){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;
    var key;
    var tecla;
    var strValidos = "0123456789" ;
    var temp;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (( tecla == BACKSPACE )||(tecla == DEL)||(tecla == FRENTE)||(tecla == TRAS)||( tecla == ENTER ) || (tecla == TAB)) {
        return true;
    }

    if ((tecla<48)||(tecla>57)){
        return false;
    }
    
    if(input.value.length == 10){
       input.value = ""; 
    }
    key = String.fromCharCode(tecla);
    input.value = input.value+key;
    temp="";
    for (var i = 0; i<input.value.length;i++ )
    {
        if (temp.length==2) temp=temp+"/";
        if (temp.length==5) temp=temp+"/";
        if ( strValidos.indexOf( input.value.substr(i,1) ) != -1 ) {
            temp=temp+input.value.substr(i,1);
        }
    }
    input.value = temp.substr(0,10);
    return false;
}

function is_enter(input , evento){
    var ENTER=13;
    var tecla;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if ( tecla == ENTER ) {
        return true;
    }
    else {
        return false;
    }
}
function ajustar_hora(input , evento){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;
    var key;
    var tecla;
    var strValidos = "0123456789" ;
    var temp;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (( tecla == BACKSPACE )||(tecla == DEL)||(tecla == FRENTE)||(tecla == TRAS)||(tecla == TAB)) {
        return true;
    }
    if ((tecla<48)||(tecla>57)){
        return false;
    }
    
    if(input.value.length == 8){
       input.value = ""; 
    }
    
    key = String.fromCharCode(tecla);
    input.value = input.value+key;
    temp="";
    for (var i = 0; i<input.value.length;i++ )
    {
        if (temp.length==2) temp=temp+":";
        if (temp.length==5) temp=temp+":";
        if ( strValidos.indexOf( input.value.substr(i,1) ) != -1 ) {
            temp=temp+input.value.substr(i,1);
        }
    }
    input.value = temp.substr(0,10);
    return false;
}
function calcula_total(qto,preco,alvo){
    fo=document.getElementById('Fo');
    campo=fo.getElementsByTagName("INPUT");
    var v1 = campo[1].value;
    var v2 = campo[2].value;
    valor = v1.replace(',','.') * v2.replace(',','.');
    var v3 = Math.round(valor*100)/100 + '';
    campo[3].value = v3;
}
function janela(theURL,winName,W,H) {
    window.open(theURL,winName,'resizable=1, scrollbars=1, status=0, titlebar=0, toolbar=0, width=' + W +', height=' + H);
}
/*
* Nome: void selecionaTodos( document.form.element, int )
* Desc: Marca todos os checkboxes do formulário como "checked"
* Autor: Ygor Thomaz (Control C)

function seleciona(form) {
for(var i=0; i<form.elements.length; i++) {
if(form.elements[i].type == 'checkbox') {
form.elements[i].checked = form.todos.checked;
}
}
}*/

function seleciona_tudo() {
    idm=document.getElementById('formulario');
    id=idm.getElementsByTagName("INPUT");
    for(var i=0; id[i] != 0; i++) {
        if(id[i].type == 'checkbox'){
            id[i].checked = 1;
        }
    }
}

function dseleciona_tudo() {
    idm=document.getElementById('formulario');
    id=idm.getElementsByTagName("INPUT");
    for(var i=0; id[i] != 0; i++) {
        if(id[i].type == 'checkbox') {
            id[i].checked = 0;
        }
    }
}
function seleciona(Sem) {
    idm=document.getElementById('formulario');
    id=idm.getElementsByTagName("INPUT");
    for(var i=0; id[i] != 0; i++) {
        if(id[i].value == Sem) {
            id[i].checked = 1;
        }
    }
}
function dseleciona(Sem) {
    idm=document.getElementById('formulario');
    id=idm.getElementsByTagName("INPUT");
    for(var i=0; id[i] != 0; i++) {
        if(id[i].value == Sem) {
            id[i].checked = 0;
        }
    }
}

/*
* Autor: Welington Nunes Siemiatkowski
* Data: 02/06/2006
* Descrição: Funcao que cria um calendario
*/
function Calendario(obj,div,tam,ddd)
{
    if(ddd.length != 10)
    ddd = '';

    if (ddd)
    {
        day = ""
        mmonth = ""
        ano = ""
        c = 1
        char = ""
        for (s=0;s<parseInt(ddd.length);s++)
        {
            char = ddd.substr(s,1)
            if (char == "/")
            {
                c++;
                s++;
                char = ddd.substr(s,1);
            }
            if (c==1) day    += char
            if (c==2) mmonth += char
            if (c==3) ano    += char
        }
        ddd = mmonth + "/" + day + "/" + ano
    }

    if(!ddd) {today = new Date()} else {today = new Date(ddd)}

    date_Form = eval (obj)
    if (date_Form.value == "") { date_Form = new Date()} else {date_Form = new Date(date_Form.value)}

    ano = today.getFullYear();
    mmonth = today.getMonth ();
    day = today.toString ().substr (8,2)

    umonth = new Array ("Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro")
    days_Feb = (!(ano % 4) ? 29 : 28)
    days = new Array (31, days_Feb, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

    if ((mmonth < 0) || (mmonth > 11))  alert(mmonth)
    if ((mmonth - 1) == -1) {month_prior = 11; year_prior = ano - 1} else {month_prior = mmonth - 1; year_prior = ano}
    if ((mmonth + 1) == 12) {month_next  = 0;  year_next  = ano + 1} else {month_next  = mmonth + 1; year_next  = ano}
    txt =  "<div style=z-index:999><table class='stCalendario' cellspacing='0' cellpadding='3' border='0' width='"+tam+"' height='"+tam*1.1 +"'>"
    txt += "<tr bgcolor='#FFFFFF'><td colspan='7' align='center'><table border='0' cellpadding='0' width='100%' bgcolor='#ffffFF'><tr>"

    if((month_prior+1)>= 10)
    txt += "<td class='stCalendarioCabecalho' width=20% align=center><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+( "01/" + (month_prior+1).toString() + "/" + year_prior.toString())+"') class='stCalendarioCabecalho' title='M&ecric;s Anterior'><</a>"
    else
    txt += "<td class='stCalendarioCabecalho' width=20% align=center><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+( "01/0" + (month_prior+1).toString() + "/" + year_prior.toString())+"') class='stCalendarioCabecalho' title='M&ecric;s Anterior'><</a>"

    txt += umonth[mmonth].toUpperCase().substring(0,3);

    if((month_next+1)>=10)
    txt += "<a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+( "01/" + (month_next+1).toString()  + "/" + year_next.toString())+"') class='stCalendarioCabecalho' title='Pr&oacute;ximo M&ecric;s'>></a></td>"
    else
    txt += "<a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+( "01/0" + (month_next+1).toString()  + "/" + year_next.toString())+"') class='stCalendarioCabecalho' title='Pr&oacute;ximo M&ecric;s'>></a></td>"

    txt += "<td width=20% align=center class='stCalendarioCabecalho'><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','01/01/" + (ano-1).toString() + "') class='stCalendarioCabecalho' title='Ano Anterior'><</a>" + ano.toString() + "<a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','01/01/" + (ano+1).toString() + "') class='stCalendarioCabecalho' title='Pr&oacute;ximo Ano'>></a></td>";

    txt += "<td width=20% align=right><a href=javascript:CalendarioFechar('"+div+"') class='stCalendarioCabecalho' title='Fechar Calend&aacute;rio'><b>X</b></a></td></tr></table></td></tr>"
    txt += "<tr><td colspan='7' class='stCalendarioMeses'><a href=javascript:CalendarioMostraAnos('"+obj+"','"+div+"','"+tam+"','" + (mmonth+1) + "') style='font-family: helvetica, arial; font-size: 8pt; color: #000000; text-decoration:none'>" + ano.toString() + "</a>"
    txt += " <a href=javascript:CalendarioMostraMeses('"+obj+"','"+div+"','"+tam+"','" + ano + "') style='font-family: helvetica, arial; font-size: 8pt; color: #000000; text-decoration:none'>" + umonth[mmonth] + "</a> <div id='popd' style='position:absolute'></div></td></tr>"
    txt += "<tr bgcolor='#cccccc'><td width='14%' class='stCalendarioDiasSemana' align=center><b>Dom</b></td><td width='14%' class='stCalendarioDiasSemana' align=center><b>Seg</b></td><td width='14%' class='stCalendarioDiasSemana' align=center><b>Ter</b></td><td width='14%' class='stCalendarioDiasSemana' align=center><b>Qua</b></td><td width='14%' class='stCalendarioDiasSemana' align=center><b>Qui</b></td><td width='14%' class='stCalendarioDiasSemana' align=center><b>Sex<b></td><td width='14%' class='stCalendarioDiasSemana' align=center><b>Sab</b></td></tr>"
    today1 = new Date((mmonth+1).toString() +"/01/"+ano.toString());
    diainicio = today1.getDay () + 1;
    week = d = 1
    start = false;

    for (n=1;n<= 42;n++)
    {
        if (week == 1)  txt += "<tr bgcolor='#ffffff' align=center>"
        if (week==diainicio) {start = true}
        if (d > days[mmonth]) {start=false}
        if (start)
        {
            dat = new Date((mmonth+1).toString() + "/" + d + "/" + ano.toString())
            day_dat   = dat.toString().substr(0,10)
            day_today  = date_Form.toString().substr(0,10)
            year_dat  = dat.getFullYear ()
            year_today = date_Form.getFullYear ()
            colorcell = ((day_dat == day_today) && (year_dat == year_today) ? " bgcolor='#eeeeee' " : "" )

            dprint = d.toString();
            mprint = (mmonth+1).toString();

            if(d<10)
            dprint = '0' + dprint;

            if((mmonth+1)<10)
            mprint = '0' + mprint;


            txt += "<td"+colorcell+" align=center><a href=javascript:CalendarioAtribuir('"+  dprint + "/" + mprint + "/" + ano.toString() +"','"+ obj +"','" + div +"') class='stCalendarioData'>"+ d.toString() + "</a></td>"
            d ++
        }
        else
        {
            txt += "<td class='stCalendarioDataAtual' align=center> </td>"
        }

        week ++
        if (week == 8)
        {
            week = 1; txt += "</tr>"}
    }

    txt += "</table></div>"

    div2 = document.getElementById(div);
    div2.innerHTML = txt

}

/*
* Autor: Welington Nunes Siemiatkowski
* Data: 02/06/2006
* Descrição: Funcao para exibir a janela com os meses do calendario
*/
function CalendarioMostraMeses(obj, div, tam, ano)
{
    txt  = "<table width=80 class='stCalendario' cellspacing='0' cellpadding='3' border='0'>"
    for (n = 0; n < 12; n++)
    {
        if(n>=10)
        txt += "<tr><td style='font-family: helvetica, arial; font-size: 8pt; color: #000000; text-decoration:none' align=center><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+("01/" + (n+1).toString() + "/" + ano.toString())+"') class='stCalendarioData'> " + umonth[n] +"</a></td></tr>"
        else
        txt += "<tr><td style='font-family: helvetica, arial; font-size: 8pt; color: #000000; text-decoration:none' align=center><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+("01/0" + (n+1).toString() + "/" + ano.toString())+"') class='stCalendarioData'>" + umonth[n] +"</a></td></tr>"
    }
    txt += "</table>"
    popd.innerHTML = txt
}

/*
* Autor: Welington Nunes Siemiatkowski
* Data: 02/06/2006
* Descrição: Funcao para exibir a janela com os anos do calendario
*/
function CalendarioMostraAnos(obj, div, tam, umonth)
{
    txt  = "<table width=160 class='stCalendario' cellspacing='0' cellpadding='3' border='0'>"
    l = 1
    for (n=1991; n<2012; n++)
    {  if (l == 1)
    txt += "<tr>"

    if(umonth>=10)
    txt += "<td style='font-family: helvetica, arial; font-size: 8pt; color: #000000; text-decoration:none' align=center><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+"01/" + (umonth.toString () + "/" + n) +"') class='stCalendarioData'>" + n + "</a></td>"
    else
    txt += "<td style='font-family: helvetica, arial; font-size: 8pt; color: #000000; text-decoration:none' align=center><a href=javascript:Calendario('"+obj+"','"+div+"','"+tam+"','"+"01/0" + (umonth.toString () + "/" + n) +"') class='stCalendarioData'>" + n + "</a></td>"

    l++
    if (l == 4)
    {txt += "</tr>"; l = 1 }
    }
    txt += "</tr></table>"
    popd.innerHTML = txt
}

/*
* Autor: Welington Nunes Siemiatkowski
* Data: 02/06/2006
* Descrição: Funcao para fechar o calendario
*/
function CalendarioFechar(div){
    div2 = document.getElementById(div); div2.innerHTML = '';
    parent.document.getElementById(div).style.visibility='hidden';
}

/*
* Autor: Welington Nunes Siemiatkowski
* Data: 02/06/2006
* Descrição: Funcao para fechar o calendario e setar a data no campo de data associado
*/
function CalendarioAtribuir(data, obj, div)
{
    CalendarioFechar (div)
    obj2 = eval(obj)
    obj2.value = data
}
/*
* Autor: Welington Nunes Siemiatkowski
* Data: 17/06/2006
* Descrição: Função que valida numeros inteiros
*/
function validaNumeroInteiro(input , evento){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;
    var key;
    var tecla;
    var strValidos = "0123456789" ;
    var temp;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (( tecla == BACKSPACE )||(tecla == DEL)||(tecla == FRENTE)||(tecla == TRAS)||(tecla == ENTER)||(tecla == TAB)) {
        return true;
    }
    if ((tecla<48)||(tecla>57)){
        return false;
    }
    key = String.fromCharCode(tecla);
    input.value = input.value+key;
    return false;
}
busca = new Array();
function AutoCompletaSelect(input , evento, bla, blabla){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;

    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (tecla == ENTER) {
        atualiza_formselect(bla,blabla + input.value);  
        //clearTimeout(busca[' ' + input]);
        return false;
    }
    else {
        
        //clearTimeout(busca[' ' + input]);
        //busca[' ' + input]=setTimeout('atualiza_formselect("'+ bla +'","' + blabla + input.value + String.fromCharCode(tecla) + '")',1000)
        
    }
}

function DesabilitaEnter(input , evento){
    var ENTER=13;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (tecla == ENTER) {
        return false;
    }
}
// Troca listas de multiplos selects
function AdicionaItemLista(ListOrigem,ListDestino){
    var i;
    for (i = 0; i < ListOrigem.options.length ; i++) {
        if (ListOrigem.options[i].selected == true) {
            var Op = document.createElement("OPTION");
            Op.text = ListOrigem.options[i].text;
            Op.value = ListOrigem.options[i].value;
            ListDestino.options.add(Op);
            ListOrigem.options[i] = null;
        }
    }
}
function RemoverItemLista(ListOrigem) {
    for (i = 0; i < ListOrigem.options.length ; i++) {
        if (ListOrigem.options[i].selected == true) {
            ListOrigem.options[i] = null;
        }
    }
}
function SelecionaTodosLista(ListOrigem){
    var i;
    for (i = 0; i < ListOrigem.options.length ; i++) {
        ListOrigem.options[i].selected = true;

    }
}

function ajustar_data_bloq(input , evento){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;
    var key;
    var tecla;
    var strValidos = "0123456789" ;
    var temp;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (( tecla == BACKSPACE )||(tecla == DEL)||(tecla == FRENTE)||(tecla == TRAS)||(tecla == TAB)) {
        return true;
    }
    if ( tecla == ENTER ){
        return false;
    }
    if ((tecla<48)||(tecla>57)){
        return false;
    }
    key = String.fromCharCode(tecla);
    input.value = input.value+key;
    temp="";
    for (var i = 0; i<input.value.length;i++ )
    {
        if (temp.length==2) temp=temp+"/";
        if (temp.length==5) temp=temp+"/";
        if ( strValidos.indexOf( input.value.substr(i,1) ) != -1 ) {
            temp=temp+input.value.substr(i,1);
        }
    }
    input.value = temp.substr(0,10);
    return false;
}

function ajustar_hora_bloq(input , evento){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;
    var key;
    var tecla;
    var strValidos = "0123456789" ;
    var temp;
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (( tecla == BACKSPACE )||(tecla == DEL)||(tecla == FRENTE)||(tecla == TRAS)||(tecla == TAB)) {
        return true;
    }
    if ((tecla<48)||(tecla>57)){
        return false;
    }
    if ( tecla == ENTER ){
        return false;
    }
    key = String.fromCharCode(tecla);
    input.value = input.value+key;
    temp="";
    for (var i = 0; i<input.value.length;i++ )
    {
        if (temp.length==2) temp=temp+":";
        if (temp.length==5) temp=temp+":";
        if ( strValidos.indexOf( input.value.substr(i,1) ) != -1 ) {
            temp=temp+input.value.substr(i,1);
        }
    }
    input.value = temp.substr(0,10);
    return false;
}
function EnviaForm(input , evento, url){
    var BACKSPACE=8;
    var DEL=46;
    var FRENTE=39;
    var TRAS=37;
    var ENTER=13;
    var TAB=9;
    
    tecla= (evento.keyCode ? evento.keyCode: evento.which ? evento.which : evento.charCode)
    if (tecla == ENTER) {
        window.open(url + input.value,'busca','');
        //clearTimeout(busca[' ' + input]);
        return false;
    }
    else {
        
        //clearTimeout(busca[' ' + input]);
        //busca[' ' + input]=setTimeout('atualiza_formselect("'+ bla +'","' + blabla + input.value + String.fromCharCode(tecla) + '")',1000)
        
    }
}

function checaSubModulos(mid_modulo, mid_permissao, mid_grupo)
{
    inputs = document.getElementById('modulo-'+mid_modulo).getElementsByTagName('input');
    idInputs = 'permissao-'+mid_modulo+'-'+mid_permissao;
    for(i=0;i<inputs.length;i++)
    {
        if(inputs[i].id.substring(0, idInputs.length) == 'permissao-'+mid_modulo+'-'+mid_permissao)
        {
            inputs[i].checked = true;           
        }
    }
    
    ajax_get('parametros.php?id=modulos_permissao_modulo&mid_modulo='+mid_modulo+'&mid_permissao='+mid_permissao+'&mid_grupo='+mid_grupo);
}

function roundNumber(numero, decimais) {
	var newnumber = new Number(numero+'').toFixed(parseInt(decimais));
	return  parseFloat(newnumber);
}
