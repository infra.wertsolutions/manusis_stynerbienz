<?
/**
* Formularios do sistema
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  engine
*/


// Muito usado em v�rios formul�rios
$act = (int) $_GET['act'];


$form['MAQUINA_DISPONIBILIDADE'][0]['nome']=MAQUINAS_DISPONIBILIDADE;
$form['MAQUINA_DISPONIBILIDADE'][0]['titulo']=$tdb[MAQUINAS_DISPONIBILIDADE]['DESC'];
$form['MAQUINA_DISPONIBILIDADE'][0]['obs']="";
$form['MAQUINA_DISPONIBILIDADE'][0]['largura']=490;
$form['MAQUINA_DISPONIBILIDADE'][0]['altura']=230;
$form['MAQUINA_DISPONIBILIDADE'][0]['chave']="MID";

$form['MAQUINA_DISPONIBILIDADE'][1]['abre_fieldset']=$tdb[MAQUINAS_DISPONIBILIDADE]['DESC'];

$form['MAQUINA_DISPONIBILIDADE'][1]['campo']="MID_MAQUINA";
$form['MAQUINA_DISPONIBILIDADE'][1]['tipo']="select_rela";
$form['MAQUINA_DISPONIBILIDADE'][1]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][1]['tam']=30;
$form['MAQUINA_DISPONIBILIDADE'][1]['max']=75;
$form['MAQUINA_DISPONIBILIDADE'][1]['unico']=0;

$form['MAQUINA_DISPONIBILIDADE'][2]['campo']="MES";
$form['MAQUINA_DISPONIBILIDADE'][2]['tipo']="mes";
$form['MAQUINA_DISPONIBILIDADE'][2]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][2]['tam']=2;
$form['MAQUINA_DISPONIBILIDADE'][2]['max']=2;
$form['MAQUINA_DISPONIBILIDADE'][2]['unico']=0;

$form['MAQUINA_DISPONIBILIDADE'][3]['campo']="ANO";
$form['MAQUINA_DISPONIBILIDADE'][3]['tipo']="texto_int";
$form['MAQUINA_DISPONIBILIDADE'][3]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][3]['tam']=4;
$form['MAQUINA_DISPONIBILIDADE'][3]['max']=4;
$form['MAQUINA_DISPONIBILIDADE'][3]['unico']=0;
$form['MAQUINA_DISPONIBILIDADE'][3]['nao_negativo']=1;

$form['MAQUINA_DISPONIBILIDADE'][4]['campo']="HORAS";
$form['MAQUINA_DISPONIBILIDADE'][4]['tipo']="texto_float";
$form['MAQUINA_DISPONIBILIDADE'][4]['null']=0;
$form['MAQUINA_DISPONIBILIDADE'][4]['tam']=11;
$form['MAQUINA_DISPONIBILIDADE'][4]['max']=11;
$form['MAQUINA_DISPONIBILIDADE'][4]['unico']=0;
$form['MAQUINA_DISPONIBILIDADE'][4]['nao_negativo']=1;

$form['MAQUINA_DISPONIBILIDADE'][4]['fecha_fieldset']=1;
/**
 *  FORMULARIOS REVISADOS
 */

$form['USUARIO_SOL'][0]['nome']=USUARIOS_PERMISAO_SOLICITACAO;
$form['USUARIO_SOL'][0]['titulo']=$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC'];
$form['USUARIO_SOL'][0]['obs']="";
$form['USUARIO_SOL'][0]['largura']=480;
$form['USUARIO_SOL'][0]['altura']=160;
$form['USUARIO_SOL'][0]['chave']="MID";

$form['USUARIO_SOL'][1]['abre_fieldset']=$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC'];

$form['USUARIO_SOL'][1]['campo']="USUARIO";
$form['USUARIO_SOL'][1]['tipo']="select_rela";
$form['USUARIO_SOL'][1]['null']=0;
$form['USUARIO_SOL'][1]['tam']=30;
$form['USUARIO_SOL'][1]['max']=75;
$form['USUARIO_SOL'][1]['unico']=0;

$form['USUARIO_SOL'][2]['campo']="MID_EMPRESA";
$form['USUARIO_SOL'][2]['tipo']="select_rela";
$form['USUARIO_SOL'][2]['null']=0;
$form['USUARIO_SOL'][2]['tam']=30;
$form['USUARIO_SOL'][2]['max']=75;
$form['USUARIO_SOL'][2]['unico']=0;

$form['USUARIO_SOL'][2]['fecha_fieldset']=1;

$form['USUARIO_CC'][0]['nome']=USUARIOS_PERMISAO_CENTRODECUSTO;
$form['USUARIO_CC'][0]['titulo']=$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC'];
$form['USUARIO_CC'][0]['obs']="";
$form['USUARIO_CC'][0]['largura']=480;
$form['USUARIO_CC'][0]['altura']=160;
$form['USUARIO_CC'][0]['chave']="MID";

$form['USUARIO_CC'][1]['abre_fieldset']=$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC'];

$form['USUARIO_CC'][1]['campo']="USUARIO";
$form['USUARIO_CC'][1]['tipo']="select_rela";
$form['USUARIO_CC'][1]['null']=0;
$form['USUARIO_CC'][1]['tam']=30;
$form['USUARIO_CC'][1]['max']=75;
$form['USUARIO_CC'][1]['unico']=0;
$form['USUARIO_CC'][1]['js']="atualiza_area2('select_cc[" . USUARIOS_PERMISAO_CENTRODECUSTO . "][2]', 'parametros.php?id=filtra_cc&campo_id=cc[" . USUARIOS_PERMISAO_CENTRODECUSTO . "][2]&campo_nome=cc[" . USUARIOS_PERMISAO_CENTRODECUSTO . "][2]&mid_u=' + this.value);";

$form['USUARIO_CC'][2]['campo']="CENTRO_DE_CUSTO";
$form['USUARIO_CC'][2]['tipo']="select_rela";
$form['USUARIO_CC'][2]['null']=0;
$form['USUARIO_CC'][2]['tam']=30;
$form['USUARIO_CC'][2]['max']=75;
$form['USUARIO_CC'][2]['autofiltro']='N';
$form['USUARIO_CC'][2]['unico']=0;

if ($act==2) {
    $form['USUARIO_CC'][2]['sql']="WHERE MID_EMPRESA IN (SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE USUARIO = '".(int)VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, "USUARIO", "MID", (int)$_GET['foq'], 0)."')";
}
elseif ((int)$_POST['cc'][USUARIOS_PERMISAO_CENTRODECUSTO][1]) {
    $form['USUARIO_CC'][2]['sql']="WHERE MID_EMPRESA IN (SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE USUARIO = '".(int)$_POST['cc'][USUARIOS_PERMISAO_CENTRODECUSTO][1]."')";
}

$form['USUARIO_CC'][2]['fecha_fieldset']=1;

$form['USUARIO_PERMISSAO'][0]['nome']=USUARIOS_PERMISSAO;
$form['USUARIO_PERMISSAO'][0]['titulo']=$tdb[USUARIOS_PERMISSAO]['DESC'];
$form['USUARIO_PERMISSAO'][0]['obs']="";
$form['USUARIO_PERMISSAO'][0]['largura']=410;
$form['USUARIO_PERMISSAO'][0]['altura']=180;
$form['USUARIO_PERMISSAO'][0]['chave']="MID";

$form['USUARIO_PERMISSAO'][1]['abre_fieldset']=$tdb[USUARIOS_PERMISSAO]['DESC'];

$form['USUARIO_PERMISSAO'][1]['campo']="GRUPO";
$form['USUARIO_PERMISSAO'][1]['tipo']="select_rela";
$form['USUARIO_PERMISSAO'][1]['null']=0;
$form['USUARIO_PERMISSAO'][1]['tam']=30;
$form['USUARIO_PERMISSAO'][1]['max']=75;
$form['USUARIO_PERMISSAO'][1]['unico']=0;

$form['USUARIO_PERMISSAO'][3]['campo']="MODULO";
$form['USUARIO_PERMISSAO'][3]['tipo']="select_rela";
$form['USUARIO_PERMISSAO'][3]['null']=0;
$form['USUARIO_PERMISSAO'][3]['tam']=30;
$form['USUARIO_PERMISSAO'][3]['max']=75;
$form['USUARIO_PERMISSAO'][3]['unico']=0;

$form['USUARIO_PERMISSAO'][2]['campo']="PERMISSAO";
$form['USUARIO_PERMISSAO'][2]['tipo']="select_rela";
$form['USUARIO_PERMISSAO'][2]['null']=0;
$form['USUARIO_PERMISSAO'][2]['tam']=30;
$form['USUARIO_PERMISSAO'][2]['max']=75;
$form['USUARIO_PERMISSAO'][2]['unico']=0;

$form['USUARIO_PERMISSAO'][3]['fecha_fieldset']=1;

$form['USUARIO_GRUPO'][0]['nome']=USUARIOS_GRUPOS;
$form['USUARIO_GRUPO'][0]['titulo']=$tdb[USUARIOS_GRUPOS]['DESC'];
$form['USUARIO_GRUPO'][0]['obs']="";
$form['USUARIO_GRUPO'][0]['largura']=420;
$form['USUARIO_GRUPO'][0]['altura']=150;
$form['USUARIO_GRUPO'][0]['chave']="MID";

$form['USUARIO_GRUPO'][1]['abre_fieldset']=$tdb[USUARIOS_GRUPOS]['DESC'];

$form['USUARIO_GRUPO'][1]['campo']="DESCRICAO";
$form['USUARIO_GRUPO'][1]['tipo']="texto";
$form['USUARIO_GRUPO'][1]['null']=0;
$form['USUARIO_GRUPO'][1]['tam']=30;
$form['USUARIO_GRUPO'][1]['max']=75;
$form['USUARIO_GRUPO'][1]['unico']=1;

$form['USUARIO_GRUPO'][1]['fecha_fieldset']=1;


$form['USUARIO'][0]['nome']=USUARIOS;
$form['USUARIO'][0]['titulo']=$tdb[USUARIOS]['DESC'];
$form['USUARIO'][0]['obs']="";
$form['USUARIO'][0]['largura']=400;
$form['USUARIO'][0]['altura']=320;
$form['USUARIO'][0]['chave']="MID";

$form['USUARIO'][1]['abre_fieldset']=$tdb[USUARIOS]['DESC'];

$form['USUARIO'][1]['campo']="NOME";
$form['USUARIO'][1]['tipo']="texto";
$form['USUARIO'][1]['null']=0;
$form['USUARIO'][1]['tam']=30;
$form['USUARIO'][1]['max']=120;
$form['USUARIO'][1]['unico']=0;

$form['USUARIO'][2]['campo']="EMAIL";
$form['USUARIO'][2]['tipo']="texto";
$form['USUARIO'][2]['null']=0;
$form['USUARIO'][2]['tam']=30;
$form['USUARIO'][2]['max']=120;
$form['USUARIO'][2]['unico']=0;

$form['USUARIO'][3]['campo']="CELULAR";
$form['USUARIO'][3]['tipo']="texto";
$form['USUARIO'][3]['null']=1;
$form['USUARIO'][3]['tam']=20;
$form['USUARIO'][3]['max']=75;
$form['USUARIO'][3]['unico']=0;

$form['USUARIO'][4]['campo']="USUARIO";
$form['USUARIO'][4]['tipo']="texto";
$form['USUARIO'][4]['null']=0;
$form['USUARIO'][4]['tam']=30;
$form['USUARIO'][4]['max']=30;
$form['USUARIO'][4]['unico']=1;

$form['USUARIO'][5]['campo']="SENHA";
$form['USUARIO'][5]['tipo']="senha";
$form['USUARIO'][5]['null']=0;
$form['USUARIO'][5]['tam']=20;
$form['USUARIO'][5]['max']=75;
$form['USUARIO'][5]['unico']=0;

// APENAS ADMINISTRADOR PODE EDITAR ISSO
if ((int)$_SESSION[ManuSess]['user']['MID'] == 0) {
    $form['USUARIO'][6]['campo']="GRUPO";
    $form['USUARIO'][6]['tipo']="select_rela";
    $form['USUARIO'][6]['null']=0;
    $form['USUARIO'][6]['tam']=30;
    $form['USUARIO'][6]['max']=75;
    $form['USUARIO'][6]['unico']=0;

    $form['USUARIO'][7]['campo']="MID_FUNCIONARIO";
    $form['USUARIO'][7]['tipo']="select_rela";
    $form['USUARIO'][7]['null']=1;
    $form['USUARIO'][7]['unico']=0;

    $form['USUARIO'][7]['fecha_fieldset']=1;
}
// TODOS OS USU&Aacute;RIOS
else {
    $form['USUARIO'][5]['fecha_fieldset']=1;
}

// AREAS
$form['AREA'][0]['nome']=AREAS;
$form['AREA'][0]['titulo']=$tdb[AREAS]['DESC'];
$form['AREA'][0]['obs']="";
$form['AREA'][0]['largura']=500;
$form['AREA'][0]['altura']=180;
$form['AREA'][0]['chave']="MID";

$form['AREA'][1]['abre_fieldset']=$tdb[AREAS]['DESC'];

$form['AREA'][1]['campo']="MID_EMPRESA";
$form['AREA'][1]['tipo']="select_rela";
$form['AREA'][1]['null']=0;
$form['AREA'][1]['tam']=15;
$form['AREA'][1]['max']=50;

$form['AREA'][2]['campo']="COD";
$form['AREA'][2]['tipo']="texto";
$form['AREA'][2]['null']=0;
$form['AREA'][2]['tam']=15;
$form['AREA'][2]['max']=50;
$form['AREA'][2]['unico']=1;

$form['AREA'][3]['campo']="DESCRICAO";
$form['AREA'][3]['tipo']="texto";
$form['AREA'][3]['null']=0;
$form['AREA'][3]['tam']=30;
$form['AREA'][3]['max']=30;
$form['AREA'][3]['unico']=0;

$form['AREA'][3]['fecha_fieldset']=1;


// SETOR
$iform = 0;
$form['SETOR'][$iform]['nome']=SETORES;
$form['SETOR'][$iform]['titulo']=$tdb[SETORES]['DESC'];
$form['SETOR'][$iform]['obs']="";
$form['SETOR'][$iform]['largura']=570;
$form['SETOR'][$iform]['altura']=225;
$form['SETOR'][$iform]['chave']="MID";

$iform ++;
$form['SETOR'][$iform]['abre_fieldset']=$tdb[SETORES]['DESC'];

$form['SETOR'][$iform]['campo']="MID_AREA";
$form['SETOR'][$iform]['tipo']="select_setor_autotag";
$form['SETOR'][$iform]['null']=0;

$iform ++;
$form['SETOR'][$iform]['campo']="COD";
$form['SETOR'][$iform]['tipo']="texto_setor_autotag";
$form['SETOR'][$iform]['null']=0;
$form['SETOR'][$iform]['tam']=15;
$form['SETOR'][$iform]['max']=15;
$form['SETOR'][$iform]['unico']=1;

$iform ++;
$form['SETOR'][$iform]['campo']="DESCRICAO";
$form['SETOR'][$iform]['tipo']="texto";
$form['SETOR'][$iform]['null']=0;
$form['SETOR'][$iform]['tam']=30;
$form['SETOR'][$iform]['max']=60;
$form['SETOR'][$iform]['unico']=0;

$form['SETOR'][$iform]['fecha_fieldset']=1;


// OBJ DE MANUTEN��O
$iform = 0;
$form['MAQUINA'][$iform]['nome']=MAQUINAS;
$form['MAQUINA'][$iform]['titulo']=$tdb[MAQUINAS]['DESC'];
$form['MAQUINA'][$iform]['largura']=620;
$form['MAQUINA'][$iform]['altura']=430;
$form['MAQUINA'][$iform]['chave']="MID";
$form['MAQUINA'][$iform]['dir']=1;

$iform ++;
$form['MAQUINA'][$iform]['abre_fieldset']=$tdb[MAQUINAS]['DESC'];

$form['MAQUINA'][$iform]['campo']="MID_SETOR";
$form['MAQUINA'][$iform]['null']=0;
$form['MAQUINA'][$iform]['tam']=34;
$form['MAQUINA'][$iform]['max']=6;

// FILTRAR OS CADASTROS POR EMPRESA
$filtro_emp = "WHERE (MID_EMPRESA=0";

// N�O EDITAR ESSE CAMPO
if ($act == 2) {
    $form['MAQUINA'][$iform]['tipo']="recb_valor";
    // Colocando apenas a empresa cadastrada
    $filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', (int)$_GET['foq']);
}
// NO CADASTRO E NO COPIAR
else {
    $form['MAQUINA'][$iform]['tipo']="select_rela";
    $form['MAQUINA'][$iform]['js']="this.form.submit()";

    // Buscando a empresa do setor
    $setor = $_POST['cc'][MAQUINAS][$iform];
    $area  = (int) VoltaValor (SETORES, 'MID_AREA', 'MID', $setor);
    $filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(AREAS, 'MID_EMPRESA', 'MID', $area);
}

$filtro_emp .= ")";

$iform ++;
$form['MAQUINA'][$iform]['campo']="CENTRO_DE_CUSTO";
$form['MAQUINA'][$iform]['tipo']="select_rela";
$form['MAQUINA'][$iform]['null']=0;
$form['MAQUINA'][$iform]['tam']=25;
$form['MAQUINA'][$iform]['max']=32;
$form['MAQUINA'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['MAQUINA'][$iform]['campo']="FAMILIA";
$form['MAQUINA'][$iform]['tipo']="select_maq_autotag";
$form['MAQUINA'][$iform]['null']=0;
$form['MAQUINA'][$iform]['tam']=10;
$form['MAQUINA'][$iform]['max']=10;
$form['MAQUINA'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['MAQUINA'][$iform]['campo']="COD";
$form['MAQUINA'][$iform]['tipo']="texto_maq_autotag";
$form['MAQUINA'][$iform]['null']=0;
$form['MAQUINA'][$iform]['tam']=30;
$form['MAQUINA'][$iform]['max']=50;
$form['MAQUINA'][$iform]['unico']=1;

$iform ++;
$form['MAQUINA'][$iform]['campo']="DESCRICAO";
$form['MAQUINA'][$iform]['tipo']="texto";
$form['MAQUINA'][$iform]['null']=0;
$form['MAQUINA'][$iform]['tam']=40;
$form['MAQUINA'][$iform]['max']=70;

$form['MAQUINA'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['MAQUINA'][$iform]['abre_fieldset']=$ling['fildset_local3'];

$form['MAQUINA'][$iform]['campo']="FABRICANTE";
$form['MAQUINA'][$iform]['tipo']="select_rela";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=8;
$form['MAQUINA'][$iform]['max']=8;

$iform ++;
$form['MAQUINA'][$iform]['campo']="FORNECEDOR";
$form['MAQUINA'][$iform]['tipo']="select_rela";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=8;
$form['MAQUINA'][$iform]['max']=8;

$iform ++;
$form['MAQUINA'][$iform]['campo']="CLASSE";
$form['MAQUINA'][$iform]['tipo']="select_rela";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=8;
$form['MAQUINA'][$iform]['max']=8;


$iform ++;
$form['MAQUINA'][$iform]['campo']="STATUS";
$form['MAQUINA'][$iform]['tipo']="select_rela";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=8;
$form['MAQUINA'][$iform]['max']=8;

$iform ++;
$form['MAQUINA'][$iform]['campo']="LOCALIZACAO_FISICA";
$form['MAQUINA'][$iform]['tipo']="texto";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=30;
$form['MAQUINA'][$iform]['max']=125;

$iform ++;
$form['MAQUINA'][$iform]['campo']="MODELO";
$form['MAQUINA'][$iform]['tipo']="texto";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=30;
$form['MAQUINA'][$iform]['max']=50;

$iform ++;
$form['MAQUINA'][$iform]['campo']="NUMERO_DE_SERIE";
$form['MAQUINA'][$iform]['tipo']="texto";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=15;
$form['MAQUINA'][$iform]['max']=20;

$iform ++;
$form['MAQUINA'][$iform]['campo']="ANO_DE_FABRICACAO";
$form['MAQUINA'][$iform]['tipo']="data";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=10;
$form['MAQUINA'][$iform]['max']=10;

$iform ++;
$form['MAQUINA'][$iform]['campo']="GARANTIA";
$form['MAQUINA'][$iform]['tipo']="data";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=10;
$form['MAQUINA'][$iform]['max']=10;

$iform ++;
$form['MAQUINA'][$iform]['campo']="NUMERO_DO_PATRIMONIO";
$form['MAQUINA'][$iform]['tipo']="texto";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['unico']=1;
$form['MAQUINA'][$iform]['tam']=15;
$form['MAQUINA'][$iform]['max']=20;

$iform ++;
$form['MAQUINA'][$iform]['campo']="DADOS_TECNICO";
$form['MAQUINA'][$iform]['tipo']="blog";
$form['MAQUINA'][$iform]['null']=1;
$form['MAQUINA'][$iform]['tam']=50;
$form['MAQUINA'][$iform]['max']=5;

$form['MAQUINA'][$iform]['fecha_fieldset']=1;


// CONJUNTO
$iform = 0;
$form['CONJUNTO'][$iform]['nome']=MAQUINAS_CONJUNTO;
$form['CONJUNTO'][$iform]['titulo']=$tdb[MAQUINAS_CONJUNTO]['DESC'];
$form['CONJUNTO'][$iform]['obs']="";
$form['CONJUNTO'][$iform]['dir']=1;
$form['CONJUNTO'][$iform]['largura']=560;
$form['CONJUNTO'][$iform]['altura']=430;
$form['CONJUNTO'][$iform]['chave']="MID";

// FILTRAR OS CADASTROS POR EMPRESA
$filtro_emp = "WHERE (MID_EMPRESA=0 OR MID_EMPRESA=" . (int)VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', (int)$form_recb_valor) . ")";

$iform ++;
$form['CONJUNTO'][$iform]['abre_fieldset']=$tdb[MAQUINAS_CONJUNTO]['DESC'];

$form['CONJUNTO'][$iform]['campo']="TAG";
$form['CONJUNTO'][$iform]['tipo']="texto_conj_autotag";
$form['CONJUNTO'][$iform]['null']=0;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=1;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="DESCRICAO";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=0;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=250;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="COMPLEMENTO";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=150;
$form['CONJUNTO'][$iform]['unico']=0;

$form['CONJUNTO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['CONJUNTO'][$iform]['abre_fieldset']=$ling['fildset_local3'];

$form['CONJUNTO'][$iform]['campo']="FABRICANTE";
$form['CONJUNTO'][$iform]['tipo']="select_rela";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=8;
$form['CONJUNTO'][$iform]['max']=8;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="MARCA";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="MODELO";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="NSERIE";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="POTENCIA";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="PRESSAO";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="CORRENTE";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="TENSAO";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="VAZAO";
$form['CONJUNTO'][$iform]['tipo']="texto";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=30;
$form['CONJUNTO'][$iform]['max']=30;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="MID_MAQUINA";
$form['CONJUNTO'][$iform]['tipo']="recb_valor";
$form['CONJUNTO'][$iform]['valor']=$form_recb_valor;
$form['CONJUNTO'][$iform]['null']=0;
$form['CONJUNTO'][$iform]['tam']=0;
$form['CONJUNTO'][$iform]['max']=0;
$form['CONJUNTO'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO'][$iform]['campo']="FICHA_TECNICA";
$form['CONJUNTO'][$iform]['tipo']="blog";
$form['CONJUNTO'][$iform]['null']=1;
$form['CONJUNTO'][$iform]['tam']=25;
$form['CONJUNTO'][$iform]['max']=10;

$form['CONJUNTO'][$iform]['fecha_fieldset']=1;


// USADO PARA EDITAR O CONJUNTO
$iform = 0;
$form['CONJUNTO2'][$iform]['nome']=MAQUINAS_CONJUNTO;
$form['CONJUNTO2'][$iform]['titulo']=$tdb[MAQUINAS_CONJUNTO]['DESC'];
$form['CONJUNTO2'][$iform]['obs']="";
$form['CONJUNTO2'][$iform]['dir']=1;
$form['CONJUNTO2'][$iform]['largura']=560;
$form['CONJUNTO2'][$iform]['altura']=430;
$form['CONJUNTO2'][$iform]['chave']="MID";

// FILTRAR OS CADASTROS POR EMPRESA
$filtro_emp = "WHERE (MID_EMPRESA=0 OR MID_EMPRESA=" . (int)VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', (int)$form_recb_valor) . ")";

$iform ++;
$form['CONJUNTO2'][$iform]['abre_fieldset']=$tdb[MAQUINAS_CONJUNTO]['DESC'];

$form['CONJUNTO2'][$iform]['campo']="MID_CONJUNTO";
$form['CONJUNTO2'][$iform]['tipo']="select_rela";
$form['CONJUNTO2'][$iform]['sql']="WHERE MID_MAQUINA = '$form_recb_valor' AND MID_CONJUNTO = '0' AND MID != '".$_GET['foq']."'";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="TAG";
$form['CONJUNTO2'][$iform]['tipo']="texto_conj_autotag";
$form['CONJUNTO2'][$iform]['null']=0;
$form['CONJUNTO2'][$iform]['tam']=15;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=1;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="DESCRICAO";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=0;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=250;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="COMPLEMENTO";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=150;
$form['CONJUNTO2'][$iform]['unico']=0;

$form['CONJUNTO2'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['CONJUNTO2'][$iform]['abre_fieldset']=$ling['fildset_local3'];

$form['CONJUNTO2'][$iform]['campo']="FABRICANTE";
$form['CONJUNTO2'][$iform]['tipo']="select_rela";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=8;
$form['CONJUNTO2'][$iform]['max']=8;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="MARCA";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="MODELO";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="NSERIE";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="POTENCIA";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="PRESSAO";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="CORRENTE";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="TENSAO";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="VAZAO";
$form['CONJUNTO2'][$iform]['tipo']="texto";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=30;
$form['CONJUNTO2'][$iform]['max']=30;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="MID_MAQUINA";
$form['CONJUNTO2'][$iform]['tipo']="recb_valor";
$form['CONJUNTO2'][$iform]['valor']=$form_recb_valor;
$form['CONJUNTO2'][$iform]['null']=0;
$form['CONJUNTO2'][$iform]['tam']=0;
$form['CONJUNTO2'][$iform]['max']=0;
$form['CONJUNTO2'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO2'][$iform]['campo']="FICHA_TECNICA";
$form['CONJUNTO2'][$iform]['tipo']="blog";
$form['CONJUNTO2'][$iform]['null']=1;
$form['CONJUNTO2'][$iform]['tam']=50;
$form['CONJUNTO2'][$iform]['max']=5;

$form['CONJUNTO2'][$iform]['fecha_fieldset']=1;

// FORNECEDORES DA MAQUINA
$iform = 0;
$form['MAQUINAS_FORNECEDOR'][$iform]['nome']=MAQUINAS_FORNECEDOR;
$form['MAQUINAS_FORNECEDOR'][$iform]['titulo']=$tdb[MAQUINAS_FORNECEDOR]['DESC'];
$form['MAQUINAS_FORNECEDOR'][$iform]['obs']="";
$form['MAQUINAS_FORNECEDOR'][$iform]['largura']=500;
$form['MAQUINAS_FORNECEDOR'][$iform]['altura']=250;
$form['MAQUINAS_FORNECEDOR'][$iform]['chave']="MID";

$iform ++;
$form['MAQUINAS_FORNECEDOR'][$iform]['abre_fieldset']=$tdb[MAQUINAS_FORNECEDOR]['DESC'];

$form['MAQUINAS_FORNECEDOR'][$iform]['campo']="MID_FORNECEDOR";
$form['MAQUINAS_FORNECEDOR'][$iform]['tipo']="select_rela";
$form['MAQUINAS_FORNECEDOR'][$iform]['null']=0;

$iform ++;
$form['MAQUINAS_FORNECEDOR'][$iform]['campo']="OBS";
$form['MAQUINAS_FORNECEDOR'][$iform]['tipo']="blog";
$form['MAQUINAS_FORNECEDOR'][$iform]['null']=1;
$form['MAQUINAS_FORNECEDOR'][$iform]['tam']=30;
$form['MAQUINAS_FORNECEDOR'][$iform]['max']=5;
$form['MAQUINAS_FORNECEDOR'][$iform]['unico']=0;

$iform ++;
$form['MAQUINAS_FORNECEDOR'][$iform]['campo']="MID_MAQUINA";
$form['MAQUINAS_FORNECEDOR'][$iform]['tipo']="recb_valor";
$form['MAQUINAS_FORNECEDOR'][$iform]['valor']=$form_recb_valor;
$form['MAQUINAS_FORNECEDOR'][$iform]['null']=0;
$form['MAQUINAS_FORNECEDOR'][$iform]['tam']=0;
$form['MAQUINAS_FORNECEDOR'][$iform]['max']=0;
$form['MAQUINAS_FORNECEDOR'][$iform]['unico']=0;

$form['MAQUINAS_FORNECEDOR'][$iform]['fecha_fieldset']=1;


// FORMULARIO DE PONTO DE MONITORAMENTO

$form['PONTO_PREDITIVA'][0]['nome']=PONTOS_PREDITIVA;
$form['PONTO_PREDITIVA'][0]['titulo']=$tdb[PONTOS_PREDITIVA]['DESC'];
$form['PONTO_PREDITIVA'][0]['obs']="";
$form['PONTO_PREDITIVA'][0]['largura']=450;
$form['PONTO_PREDITIVA'][0]['altura']=320;
$form['PONTO_PREDITIVA'][0]['chave']="MID";

$form['PONTO_PREDITIVA'][1]['abre_fieldset']=$tdb[PONTOS_PREDITIVA]['DESC'];

$form['PONTO_PREDITIVA'][1]['campo']="PONTO";
$form['PONTO_PREDITIVA'][1]['tipo']="texto";
$form['PONTO_PREDITIVA'][1]['null']=0;
$form['PONTO_PREDITIVA'][1]['tam']=30;
$form['PONTO_PREDITIVA'][1]['max']=250;
$form['PONTO_PREDITIVA'][1]['unico']=1;

$form['PONTO_PREDITIVA'][2]['campo']="GRANDEZA";
$form['PONTO_PREDITIVA'][2]['tipo']="texto";
$form['PONTO_PREDITIVA'][2]['null']=0;
$form['PONTO_PREDITIVA'][2]['tam']=30;
$form['PONTO_PREDITIVA'][2]['max']=50;
$form['PONTO_PREDITIVA'][2]['unico']=0;

$form['PONTO_PREDITIVA'][3]['campo']="VALOR_MINIMO";
$form['PONTO_PREDITIVA'][3]['tipo']="texto_float";
$form['PONTO_PREDITIVA'][3]['null']=0;
$form['PONTO_PREDITIVA'][3]['tam']=15;
$form['PONTO_PREDITIVA'][3]['max']=30;
$form['PONTO_PREDITIVA'][3]['unico']=0;

$form['PONTO_PREDITIVA'][4]['campo']="VALOR_MAXIMO";
$form['PONTO_PREDITIVA'][4]['tipo']="texto_float";
$form['PONTO_PREDITIVA'][4]['null']=0;
$form['PONTO_PREDITIVA'][4]['tam']=15;
$form['PONTO_PREDITIVA'][4]['max']=30;
$form['PONTO_PREDITIVA'][4]['unico']=0;

$form['PONTO_PREDITIVA'][5]['campo']="MID_CONJUNTO";
$form['PONTO_PREDITIVA'][5]['tipo']="select_conjunto_filtrado";
$form['PONTO_PREDITIVA'][5]['valor']=$form_recb_valor;
$form['PONTO_PREDITIVA'][5]['null']=0;
$form['PONTO_PREDITIVA'][5]['tam']=30;
$form['PONTO_PREDITIVA'][5]['max']=250;
$form['PONTO_PREDITIVA'][5]['unico']=0;

$form['PONTO_PREDITIVA'][6]['campo']="OBSERVACAO";
$form['PONTO_PREDITIVA'][6]['tipo']="blog";
$form['PONTO_PREDITIVA'][6]['null']=1;
$form['PONTO_PREDITIVA'][6]['tam']=30;
$form['PONTO_PREDITIVA'][6]['max']=5;
$form['PONTO_PREDITIVA'][6]['unico']=0;

$form['PONTO_PREDITIVA'][7]['campo']="MID_MAQUINA";
$form['PONTO_PREDITIVA'][7]['tipo']="recb_valor";
$form['PONTO_PREDITIVA'][7]['valor']=$form_recb_valor;
$form['PONTO_PREDITIVA'][7]['null']=0;
$form['PONTO_PREDITIVA'][7]['tam']=30;
$form['PONTO_PREDITIVA'][7]['max']=250;
$form['PONTO_PREDITIVA'][7]['unico']=0;

$form['PONTO_PREDITIVA'][7]['fecha_fieldset']=1;


// FORMULARIO PONTOS DE LUBRIFICACAO
$iform = 0;
$form['PONTO_LUBRIFICACAO'][$iform]['nome']=PONTOS_LUBRIFICACAO;
$form['PONTO_LUBRIFICACAO'][$iform]['titulo']=$tdb[PONTOS_LUBRIFICACAO]['DESC'];
$form['PONTO_LUBRIFICACAO'][$iform]['obs']="";
$form['PONTO_LUBRIFICACAO'][$iform]['largura']=500;
$form['PONTO_LUBRIFICACAO'][$iform]['altura']=210;
$form['PONTO_LUBRIFICACAO'][$iform]['chave']="MID";
$iform ++;
$form['PONTO_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['def_ponto'];

$form['PONTO_LUBRIFICACAO'][$iform]['campo']="PONTO";
$form['PONTO_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['PONTO_LUBRIFICACAO'][$iform]['null']=0;
$form['PONTO_LUBRIFICACAO'][$iform]['tam']=30;
$form['PONTO_LUBRIFICACAO'][$iform]['max']=75;
$form['PONTO_LUBRIFICACAO'][$iform]['unico']=1;
$iform ++;
$form['PONTO_LUBRIFICACAO'][$iform]['campo']="NPONTO";
$form['PONTO_LUBRIFICACAO'][$iform]['tipo']="texto_int";
$form['PONTO_LUBRIFICACAO'][$iform]['null']=0;
$form['PONTO_LUBRIFICACAO'][$iform]['tam']=3;
$form['PONTO_LUBRIFICACAO'][$iform]['max']=3;
$form['PONTO_LUBRIFICACAO'][$iform]['nao_negativo'] = 1;
$form['PONTO_LUBRIFICACAO'][$iform]['unico']=0;
$iform ++;
$form['PONTO_LUBRIFICACAO'][$iform]['campo']="METODO";
$form['PONTO_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['PONTO_LUBRIFICACAO'][$iform]['null']=0;
$form['PONTO_LUBRIFICACAO'][$iform]['tam']=30;
$form['PONTO_LUBRIFICACAO'][$iform]['max']=50;
$form['PONTO_LUBRIFICACAO'][$iform]['unico']=0;
$iform ++;
$form['PONTO_LUBRIFICACAO'][$iform]['campo']="MID_CONJUNTO";
$form['PONTO_LUBRIFICACAO'][$iform]['tipo']="select_conjunto_filtrado";
$form['PONTO_LUBRIFICACAO'][$iform]['valor']=$form_recb_valor;
$form['PONTO_LUBRIFICACAO'][$iform]['null']=0;
$form['PONTO_LUBRIFICACAO'][$iform]['unico']=0;
$iform ++;
$form['PONTO_LUBRIFICACAO'][$iform]['campo']="MID_MAQUINA";
$form['PONTO_LUBRIFICACAO'][$iform]['tipo']="recb_valor";
$form['PONTO_LUBRIFICACAO'][$iform]['valor']=$form_recb_valor;
$form['PONTO_LUBRIFICACAO'][$iform]['unico']=0;

$form['PONTO_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;


// FORMULARIO CONTADOR MAQUINA
$iform =0;
$form['CONTADOR_MAQ'][$iform]['nome']=MAQUINAS_CONTADOR;
$form['CONTADOR_MAQ'][$iform]['titulo']=$tdb[MAQUINAS_CONTADOR]['DESC'];
$form['CONTADOR_MAQ'][$iform]['obs']="";
$form['CONTADOR_MAQ'][$iform]['largura']=500;
$form['CONTADOR_MAQ'][$iform]['altura']=250;
$form['CONTADOR_MAQ'][$iform]['chave']="MID";

$iform ++;
$form['CONTADOR_MAQ'][$iform]['abre_fieldset']=$tdb[MAQUINAS_CONTADOR]['DESC'];

$form['CONTADOR_MAQ'][$iform]['campo']="DESCRICAO";
$form['CONTADOR_MAQ'][$iform]['tipo']="texto";
$form['CONTADOR_MAQ'][$iform]['null']=0;
$form['CONTADOR_MAQ'][$iform]['tam']=35;
$form['CONTADOR_MAQ'][$iform]['max']=70;
$form['CONTADOR_MAQ'][$iform]['unico']=1;

$iform ++;
$form['CONTADOR_MAQ'][$iform]['campo']="TIPO";
$form['CONTADOR_MAQ'][$iform]['tipo']="select_rela";
$form['CONTADOR_MAQ'][$iform]['null']=0;
$form['CONTADOR_MAQ'][$iform]['tam']=20;
$form['CONTADOR_MAQ'][$iform]['max']=13;
$form['CONTADOR_MAQ'][$iform]['unico']=0;

$iform ++;
$form['CONTADOR_MAQ'][$iform]['campo']="ZERA";
$form['CONTADOR_MAQ'][$iform]['tipo']="texto_int";
$form['CONTADOR_MAQ'][$iform]['null']=0;
$form['CONTADOR_MAQ'][$iform]['tam']=10;
$form['CONTADOR_MAQ'][$iform]['max']=10;
$form['CONTADOR_MAQ'][$iform]['nao_zero']=1;
$form['CONTADOR_MAQ'][$iform]['nao_negativo']=1;
$form['CONTADOR_MAQ'][$iform]['unico']=0;

$iform ++;
$form['CONTADOR_MAQ'][$iform]['campo']="MID_MAQUINA";
$form['CONTADOR_MAQ'][$iform]['tipo']="recb_valor";
$form['CONTADOR_MAQ'][$iform]['valor']=$form_recb_valor;
$form['CONTADOR_MAQ'][$iform]['null']=0;
$form['CONTADOR_MAQ'][$iform]['unico']=0;

$iform ++;
$form['CONTADOR_MAQ'][$iform]['campo']="INTEGRACAO";
$form['CONTADOR_MAQ'][$iform]['tipo']="select_rela";
$form['CONTADOR_MAQ'][$iform]['null']=0;
$form['CONTADOR_MAQ'][$iform]['tam']=20;
$form['CONTADOR_MAQ'][$iform]['max']=13;
$form['CONTADOR_MAQ'][$iform]['unico']=1;

$form['CONTADOR_MAQ'][$iform]['fecha_fieldset']=1;


// FORMULARIO MATERIAL DO EQUIPAMENTO
$iform = 0;
$form['CONJUNTO_MATERIAL'][$iform]['nome']=MAQUINAS_CONJUNTO_MATERIAL;
$form['CONJUNTO_MATERIAL'][$iform]['titulo']=$tdb[MAQUINAS_CONJUNTO_MATERIAL]['DESC'];
$form['CONJUNTO_MATERIAL'][$iform]['obs']="";
$form['CONJUNTO_MATERIAL'][$iform]['largura']=620;
$form['CONJUNTO_MATERIAL'][$iform]['altura']=200;
$form['CONJUNTO_MATERIAL'][$iform]['chave']="MID";

// FILTRAR OS CADASTROS POR EMPRESA
$filtro_emp = "WHERE (MID_EMPRESA=0 OR MID_EMPRESA=" . (int)VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', (int)$form_recb_valor) . ")";

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['abre_fieldset']=$tdb[MAQUINAS_CONJUNTO_MATERIAL]['DESC'];

$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_CONJUNTO";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="select_rela";
$form['CONJUNTO_MATERIAL'][$iform]['sql']="WHERE MID_MAQUINA={$form_recb_valor}";
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_MATERIAL";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="select_material";
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="QUANTIDADE";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="texto_unidade";
$form['CONJUNTO_MATERIAL'][$iform]['valor']=1;
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['tam']=5;
$form['CONJUNTO_MATERIAL'][$iform]['max']=5;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;
$form['CONJUNTO_MATERIAL'][$iform]['nao_negativo']=1;
$form['CONJUNTO_MATERIAL'][$iform]['nao_zero']=1;
$form['CONJUNTO_MATERIAL'][$iform]['campo_material']=($iform - 1);

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_CLASSE";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="select_rela";
$form['CONJUNTO_MATERIAL'][$iform]['null']=1;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$iform ++;
$form['CONJUNTO_MATERIAL'][$iform]['campo']="MID_MAQUINA";
$form['CONJUNTO_MATERIAL'][$iform]['tipo']="recb_valor";
$form['CONJUNTO_MATERIAL'][$iform]['valor']=$form_recb_valor;
$form['CONJUNTO_MATERIAL'][$iform]['null']=0;
$form['CONJUNTO_MATERIAL'][$iform]['unico']=0;

$form['CONJUNTO_MATERIAL'][$iform]['fecha_fieldset']=1;



//FORMULARIO EQUIPAMENTO/COMPONENTE

$form['EQUIPAMENTO'][0]['nome']=EQUIPAMENTOS;
$form['EQUIPAMENTO'][0]['titulo']=$tdb[EQUIPAMENTOS]['DESC'];
$form['EQUIPAMENTO'][0]['dir']=1;
$form['EQUIPAMENTO'][0]['largura']=630;
$form['EQUIPAMENTO'][0]['altura']=450;
$form['EQUIPAMENTO'][0]['chave']="MID";

$form['EQUIPAMENTO'][1]['abre_fieldset']=$tdb[EQUIPAMENTOS]['DESC'];

if (($act == 1) or ($act == 3)) {
    $form['EQUIPAMENTO'][1]['campo']="MID_MAQUINA";
    $form['EQUIPAMENTO'][1]['tipo']="select_maquina";
    $form['EQUIPAMENTO'][1]['null']=0;
    $form['EQUIPAMENTO'][1]['tam']=0;
    $form['EQUIPAMENTO'][1]['max']=0;
    $form['EQUIPAMENTO'][1]['unico']=0;

    $form['EQUIPAMENTO'][2]['campo']="MID_CONJUNTO";
    $form['EQUIPAMENTO'][2]['tipo']="select_conjunto_filtrado";
    $form['EQUIPAMENTO'][2]['null']=0;
    $form['EQUIPAMENTO'][2]['tam']=0;
    $form['EQUIPAMENTO'][2]['max']=0;
    $form['EQUIPAMENTO'][2]['unico']=0;
}
else {
    $form['EQUIPAMENTO'][1]['campo']="MID_MAQUINA";
    $form['EQUIPAMENTO'][1]['tipo']="recb_valor";
    $form['EQUIPAMENTO'][1]['null']=1;
    $form['EQUIPAMENTO'][1]['tam']=0;
    $form['EQUIPAMENTO'][1]['max']=0;
    $form['EQUIPAMENTO'][1]['unico']=0;

    $form['EQUIPAMENTO'][2]['campo']="MID_CONJUNTO";
    $form['EQUIPAMENTO'][2]['tipo']="recb_valor";
    $form['EQUIPAMENTO'][2]['null']=1;
    $form['EQUIPAMENTO'][2]['tam']=0;
    $form['EQUIPAMENTO'][2]['max']=0;
    $form['EQUIPAMENTO'][2]['unico']=0;
}

// FILTRAR OS CADASTROS POR EMPRESA
$filtro_emp = "WHERE (MID_EMPRESA=0";
// Ao cadastrar
if ($act == 1) {
	// Buscando a empresa da maquina
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', (int)$_POST['filtra_maq']);
}
// Ao editar
else {
	// Pegando a empresa do pr�prio compontente
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(EQUIPAMENTOS, 'MID_EMPRESA', 'MID', (int)$_GET['foq']);
}
$filtro_emp .= ")";

$form['EQUIPAMENTO'][3]['campo']="FAMILIA";
$form['EQUIPAMENTO'][3]['tipo']="select_equip_autotag";
$form['EQUIPAMENTO'][3]['null']=0;
$form['EQUIPAMENTO'][3]['tam']=10;
$form['EQUIPAMENTO'][3]['max']=10;
$form['EQUIPAMENTO'][3]['sql']=$filtro_emp;

$form['EQUIPAMENTO'][4]['campo']="COD";
$form['EQUIPAMENTO'][4]['tipo']="texto_equip_autotag";
$form['EQUIPAMENTO'][4]['null']=0;
$form['EQUIPAMENTO'][4]['tam']=20;
$form['EQUIPAMENTO'][4]['max']=30;
$form['EQUIPAMENTO'][4]['unico']=1;

$form['EQUIPAMENTO'][5]['campo']="DESCRICAO";
$form['EQUIPAMENTO'][5]['tipo']="texto";
$form['EQUIPAMENTO'][5]['null']=0;
$form['EQUIPAMENTO'][5]['tam']=30;
$form['EQUIPAMENTO'][5]['max']=255;
$form['EQUIPAMENTO'][5]['unico']=0;

$form['EQUIPAMENTO'][5]['fecha_fieldset']=1;

$form['EQUIPAMENTO'][6]['abre_fieldset']=$ling['fildset_local3'];

$form['EQUIPAMENTO'][6]['campo']="MID_STATUS";
$form['EQUIPAMENTO'][6]['tipo']="select_rela";
$form['EQUIPAMENTO'][6]['null']=0;
$form['EQUIPAMENTO'][6]['tam']=8;
$form['EQUIPAMENTO'][6]['max']=8;

$form['EQUIPAMENTO'][7]['campo']="FORNECEDOR";
$form['EQUIPAMENTO'][7]['tipo']="select_rela";
$form['EQUIPAMENTO'][7]['null']=1;
$form['EQUIPAMENTO'][7]['tam']=8;
$form['EQUIPAMENTO'][7]['max']=8;

$form['EQUIPAMENTO'][8]['campo']="FABRICANTE";
$form['EQUIPAMENTO'][8]['tipo']="select_rela";
$form['EQUIPAMENTO'][8]['null']=1;
$form['EQUIPAMENTO'][8]['tam']=8;
$form['EQUIPAMENTO'][8]['max']=8;

$form['EQUIPAMENTO'][9]['campo']="MARCA";
$form['EQUIPAMENTO'][9]['tipo']="texto";
$form['EQUIPAMENTO'][9]['null']=1;
$form['EQUIPAMENTO'][9]['tam']=30;
$form['EQUIPAMENTO'][9]['max']=30;
$form['EQUIPAMENTO'][9]['unico']=0;

$form['EQUIPAMENTO'][10]['campo']="MODELO";
$form['EQUIPAMENTO'][10]['tipo']="texto";
$form['EQUIPAMENTO'][10]['null']=1;
$form['EQUIPAMENTO'][10]['tam']=30;
$form['EQUIPAMENTO'][10]['max']=30;
$form['EQUIPAMENTO'][10]['unico']=0;

$form['EQUIPAMENTO'][11]['campo']="NSERIE";
$form['EQUIPAMENTO'][11]['tipo']="texto";
$form['EQUIPAMENTO'][11]['null']=1;
$form['EQUIPAMENTO'][11]['tam']=30;
$form['EQUIPAMENTO'][11]['max']=30;
$form['EQUIPAMENTO'][11]['unico']=0;

$form['EQUIPAMENTO'][12]['campo']="POTENCIA";
$form['EQUIPAMENTO'][12]['tipo']="texto";
$form['EQUIPAMENTO'][12]['null']=1;
$form['EQUIPAMENTO'][12]['tam']=30;
$form['EQUIPAMENTO'][12]['max']=30;
$form['EQUIPAMENTO'][12]['unico']=0;

$form['EQUIPAMENTO'][13]['campo']="PRESSAO";
$form['EQUIPAMENTO'][13]['tipo']="texto";
$form['EQUIPAMENTO'][13]['null']=1;
$form['EQUIPAMENTO'][13]['tam']=30;
$form['EQUIPAMENTO'][13]['max']=30;
$form['EQUIPAMENTO'][13]['unico']=0;

$form['EQUIPAMENTO'][14]['campo']="CORRENTE";
$form['EQUIPAMENTO'][14]['tipo']="texto";
$form['EQUIPAMENTO'][14]['null']=1;
$form['EQUIPAMENTO'][14]['tam']=30;
$form['EQUIPAMENTO'][14]['max']=30;
$form['EQUIPAMENTO'][14]['unico']=0;

$form['EQUIPAMENTO'][15]['campo']="TENSAO";
$form['EQUIPAMENTO'][15]['tipo']="texto";
$form['EQUIPAMENTO'][15]['null']=1;
$form['EQUIPAMENTO'][15]['tam']=30;
$form['EQUIPAMENTO'][15]['max']=30;
$form['EQUIPAMENTO'][15]['unico']=0;

$form['EQUIPAMENTO'][16]['campo']="VAZAO";
$form['EQUIPAMENTO'][16]['tipo']="texto";
$form['EQUIPAMENTO'][16]['null']=1;
$form['EQUIPAMENTO'][16]['tam']=30;
$form['EQUIPAMENTO'][16]['max']=30;
$form['EQUIPAMENTO'][16]['unico']=0;

$form['EQUIPAMENTO'][17]['campo']="FICHA_TECNICA";
$form['EQUIPAMENTO'][17]['tipo']="blog";
$form['EQUIPAMENTO'][17]['null']=1;
$form['EQUIPAMENTO'][17]['tam']=36;
$form['EQUIPAMENTO'][17]['max']=5;

$form['EQUIPAMENTO'][17]['fecha_fieldset']=1;


// FORMULARIO FUNCIONARIOS
// Filtra os campos por empresa
$filtro_emp = "WHERE (MID_EMPRESA=0";
// Buscando a empresa selecionada
if ((! $_POST['cc'][FUNCIONARIOS]) and ($act != 1)) {
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(FUNCIONARIOS, 'MID_EMPRESA', 'MID', (int)$_GET['foq']);
}
elseif ($_POST['cc'][FUNCIONARIOS][1] != 0) {
	$filtro_emp .= " OR MID_EMPRESA=" . (int)$_POST['cc'][FUNCIONARIOS][1];
}
$filtro_emp .= ")";

$iform = 0;
$form['FUNCIONARIO'][$iform]['nome']=FUNCIONARIOS;
$form['FUNCIONARIO'][$iform]['titulo']=$tdb[FUNCIONARIOS]['DESC'];
$form['FUNCIONARIO'][$iform]['obs']="";
$form['FUNCIONARIO'][$iform]['largura']=600;
$form['FUNCIONARIO'][$iform]['altura']=400;
$form['FUNCIONARIO'][$iform]['chave']="MID";

$iform ++;
$form['FUNCIONARIO'][$iform]['abre_fieldset']=$tdb[FUNCIONARIOS]['DESC'];

$form['FUNCIONARIO'][$iform]['campo']="MID_EMPRESA";
$form['FUNCIONARIO'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=35;
$form['FUNCIONARIO'][$iform]['max']=120;
$form['FUNCIONARIO'][$iform]['js']="this.form.submit();";

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="NOME";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=35;
$form['FUNCIONARIO'][$iform]['max']=120;
$form['FUNCIONARIO'][$iform]['unico']=1;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="RG";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=16;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="CPF";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=18;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="DATA_NASCIMENTO";
$form['FUNCIONARIO'][$iform]['tipo']="data";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=0;
$form['FUNCIONARIO'][$iform]['max']=0;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="MATRICULA";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="ESPECIALIDADE";
$form['FUNCIONARIO'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;
$form['FUNCIONARIO'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="NIVEL";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="EQUIPE";
$form['FUNCIONARIO'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;
$form['FUNCIONARIO'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="CENTRO_DE_CUSTO";
$form['FUNCIONARIO'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;
$form['FUNCIONARIO'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="VALOR_HORA";
$form['FUNCIONARIO'][$iform]['tipo']="texto_float";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=13;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="ENDERECO";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=35;
$form['FUNCIONARIO'][$iform]['max']=150;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="BAIRRO";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=35;
$form['FUNCIONARIO'][$iform]['max']=100;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="CEP";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=16;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="CIDADE";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=35;
$form['FUNCIONARIO'][$iform]['max']=100;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="UF";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=2;
$form['FUNCIONARIO'][$iform]['max']=2;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="TELEFONE";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=30;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="CELULAR";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=30;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="EMAIL";
$form['FUNCIONARIO'][$iform]['tipo']="texto";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=35;
$form['FUNCIONARIO'][$iform]['max']=150;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="SITUACAO";
$form['FUNCIONARIO'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="TURNO";
$form['FUNCIONARIO'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=16;
$form['FUNCIONARIO'][$iform]['max']=50;
$form['FUNCIONARIO'][$iform]['unico']=0;
$form['FUNCIONARIO'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="DATA_ADMISSAO";
$form['FUNCIONARIO'][$iform]['tipo']="data";
$form['FUNCIONARIO'][$iform]['null']=0;
$form['FUNCIONARIO'][$iform]['tam']=0;
$form['FUNCIONARIO'][$iform]['max']=0;
$form['FUNCIONARIO'][$iform]['unico']=0;
$form['FUNCIONARIO'][$iform]['menor_que']=$iform + 1;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="DATA_DEMISSAO";
$form['FUNCIONARIO'][$iform]['tipo']="data";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=0;
$form['FUNCIONARIO'][$iform]['max']=0;
$form['FUNCIONARIO'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO'][$iform]['campo']="OBSERVACAO";
$form['FUNCIONARIO'][$iform]['tipo']="blog";
$form['FUNCIONARIO'][$iform]['null']=1;
$form['FUNCIONARIO'][$iform]['tam']=30;
$form['FUNCIONARIO'][$iform]['max']=10;
$form['FUNCIONARIO'][$iform]['unico']=0;

$form['FUNCIONARIO'][$iform]['fecha_fieldset']=1;



// FORMULARIO PRESTADORES DE SERVICO
// Filtra os campos por empresa
$filtro_emp = "WHERE (MID_EMPRESA=0";
// Buscando a empresa selecionada
if ((! $_POST['cc'][FUNCIONARIOS]) and ($act == 2)) {
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(FUNCIONARIOS, 'MID_EMPRESA', 'MID', (int)$_GET['foq']);
}
elseif ($_POST['cc'][FUNCIONARIOS][1] != 0) {
	$filtro_emp .= " OR MID_EMPRESA=" . (int)$_POST['cc'][FUNCIONARIOS][1];
}
$filtro_emp .= ")";

$iform = 0;
$form['FUNCIONARIO2'][$iform]['nome']=FUNCIONARIOS;
$form['FUNCIONARIO2'][$iform]['titulo']=$ling['prestador'];
$form['FUNCIONARIO2'][$iform]['obs']="";
$form['FUNCIONARIO2'][$iform]['largura']=600;
$form['FUNCIONARIO2'][$iform]['altura']=250;
$form['FUNCIONARIO2'][$iform]['chave']="MID";

$iform ++;
$form['FUNCIONARIO2'][$iform]['abre_fieldset']=$ling['prestador'];

$form['FUNCIONARIO2'][$iform]['campo']="MID_EMPRESA";
$form['FUNCIONARIO2'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO2'][$iform]['null']=0;
$form['FUNCIONARIO2'][$iform]['tam']=35;
$form['FUNCIONARIO2'][$iform]['max']=120;
$form['FUNCIONARIO2'][$iform]['js']="this.form.submit();";

$iform ++;
$form['FUNCIONARIO2'][$iform]['campo']="FORNECEDOR";
$form['FUNCIONARIO2'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO2'][$iform]['null']=0;
$form['FUNCIONARIO2'][$iform]['tam']=16;
$form['FUNCIONARIO2'][$iform]['max']=16;
$form['FUNCIONARIO2'][$iform]['unico']=0;

$iform ++;
$form['FUNCIONARIO2'][$iform]['campo']="NOME";
$form['FUNCIONARIO2'][$iform]['tipo']="texto";
$form['FUNCIONARIO2'][$iform]['null']=0;
$form['FUNCIONARIO2'][$iform]['tam']=35;
$form['FUNCIONARIO2'][$iform]['max']=120;
$form['FUNCIONARIO2'][$iform]['unico']=1;

$iform ++;
$form['FUNCIONARIO2'][$iform]['campo']="ESPECIALIDADE";
$form['FUNCIONARIO2'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO2'][$iform]['null']=0;
$form['FUNCIONARIO2'][$iform]['tam']=16;
$form['FUNCIONARIO2'][$iform]['max']=50;
$form['FUNCIONARIO2'][$iform]['unico']=0;
$form['FUNCIONARIO2'][$iform]['sql']=$filtro_emp;

$iform ++;
$form['FUNCIONARIO2'][$iform]['campo']="SITUACAO";
$form['FUNCIONARIO2'][$iform]['tipo']="select_rela";
$form['FUNCIONARIO2'][$iform]['null']=0;
$form['FUNCIONARIO2'][$iform]['tam']=16;
$form['FUNCIONARIO2'][$iform]['max']=50;

$iform ++;
$form['FUNCIONARIO2'][$iform]['campo']="VALOR_HORA";
$form['FUNCIONARIO2'][$iform]['tipo']="texto";
$form['FUNCIONARIO2'][$iform]['null']=1;
$form['FUNCIONARIO2'][$iform]['tam']=13;
$form['FUNCIONARIO2'][$iform]['max']=13;
$form['FUNCIONARIO2'][$iform]['unico']=0;

$form['FUNCIONARIO2'][$iform]['fecha_fieldset']=1;


// FORMULARIO EQUIPE
$form['EQUIPE'][0]['nome']=EQUIPES;
$form['EQUIPE'][0]['titulo']=$tdb[EQUIPES]['DESC'];
$form['EQUIPE'][0]['obs']="";
$form['EQUIPE'][0]['largura']=500;
$form['EQUIPE'][0]['altura']=300;
$form['EQUIPE'][0]['chave']="MID";

$form['EQUIPE'][1]['abre_fieldset']=$tdb[EQUIPES]['DESC'];

$form['EQUIPE'][1]['campo']="MID_EMPRESA";
$form['EQUIPE'][1]['tipo']="select_rela";
$form['EQUIPE'][1]['null']=0;
$form['EQUIPE'][1]['tam']=35;
$form['EQUIPE'][1]['max']=120;

$form['EQUIPE'][2]['campo']="DESCRICAO";
$form['EQUIPE'][2]['tipo']="texto";
$form['EQUIPE'][2]['null']=0;
$form['EQUIPE'][2]['tam']=30;
$form['EQUIPE'][2]['max']=100;
$form['EQUIPE'][2]['unico']=1;

$form['EQUIPE'][3]['campo']="TIPO";
$form['EQUIPE'][3]['tipo']="texto";
$form['EQUIPE'][3]['null']=1;
$form['EQUIPE'][3]['tam']=30;
$form['EQUIPE'][3]['max']=50;
$form['EQUIPE'][3]['unico']=0;

$form['EQUIPE'][4]['campo']="FORNECEDOR";
$form['EQUIPE'][4]['tipo']="select_rela";
$form['EQUIPE'][4]['null']=1;
$form['EQUIPE'][4]['tam']=30;
$form['EQUIPE'][4]['max']=50;
$form['EQUIPE'][4]['unico']=0;

$form['EQUIPE'][5]['campo']="OBSERVACAO";
$form['EQUIPE'][5]['tipo']="blog";
$form['EQUIPE'][5]['null']=1;
$form['EQUIPE'][5]['tam']=35;
$form['EQUIPE'][5]['max']=5;
$form['EQUIPE'][5]['unico']=0;

$form['EQUIPE'][5]['fecha_fieldset']=1;

// FORMULARIO AFASTAMENTOS
$iform = 0;
$form['AFASTAMENTO'][$iform]['nome']=AFASTAMENTOS;
$form['AFASTAMENTO'][$iform]['titulo']=$tdb[AFASTAMENTOS]['DESC'];
$form['AFASTAMENTO'][$iform]['obs']="";
$form['AFASTAMENTO'][$iform]['largura']=500;
$form['AFASTAMENTO'][$iform]['altura']=210;
$form['AFASTAMENTO'][$iform]['chave']="MID";

$iform ++;
$form['AFASTAMENTO'][$iform]['abre_fieldset']=$tdb[AFASTAMENTOS]['DESC'];

$form['AFASTAMENTO'][$iform]['campo']="MID_FUNCIONARIO";
$form['AFASTAMENTO'][$iform]['tipo']="select_rela";
$form['AFASTAMENTO'][$iform]['null']=0;
$form['AFASTAMENTO'][$iform]['tam']=0;
$form['AFASTAMENTO'][$iform]['max']=0;
$form['AFASTAMENTO'][$iform]['unico']=0;

$iform ++;
$form['AFASTAMENTO'][$iform]['campo']="DATA_INICIAL";
$form['AFASTAMENTO'][$iform]['tipo']="data";
$form['AFASTAMENTO'][$iform]['null']=0;
$form['AFASTAMENTO'][$iform]['tam']=30;
$form['AFASTAMENTO'][$iform]['max']=100;
$form['AFASTAMENTO'][$iform]['unico']=0;
$form['AFASTAMENTO'][$iform]['menor_que']=3;

$iform ++;
$form['AFASTAMENTO'][$iform]['campo']="DATA_FINAL";
$form['AFASTAMENTO'][$iform]['tipo']="data";
$form['AFASTAMENTO'][$iform]['null']=0;
$form['AFASTAMENTO'][$iform]['tam']=30;
$form['AFASTAMENTO'][$iform]['max']=100;
$form['AFASTAMENTO'][$iform]['unico']=0;

$iform ++;
$form['AFASTAMENTO'][$iform]['campo']="MOTIVO";
$form['AFASTAMENTO'][$iform]['tipo']="texto";
$form['AFASTAMENTO'][$iform]['null']=0;
$form['AFASTAMENTO'][$iform]['tam']=30;
$form['AFASTAMENTO'][$iform]['max']=255;
$form['AFASTAMENTO'][$iform]['unico']=0;

$form['AFASTAMENTO'][$iform]['fecha_fieldset']=1;

// FORMULARIO AUSENCIAS
$iform = 0;
$form['AUSENCIA'][$iform]['nome']=AUSENCIAS;
$form['AUSENCIA'][$iform]['titulo']=$tdb[AUSENCIAS]['DESC'];
$form['AUSENCIA'][$iform]['obs']="";
$form['AUSENCIA'][$iform]['largura']=500;
$form['AUSENCIA'][$iform]['altura']=250;
$form['AUSENCIA'][$iform]['chave']="MID";

$iform ++;
$form['AUSENCIA'][$iform]['abre_fieldset']=$tdb[AUSENCIAS]['DESC'];

$form['AUSENCIA'][$iform]['campo']="MID_FUNCIONARIO";
$form['AUSENCIA'][$iform]['tipo']="select_rela";
$form['AUSENCIA'][$iform]['null']=0;
$form['AUSENCIA'][$iform]['tam']=0;
$form['AUSENCIA'][$iform]['max']=0;
$form['AUSENCIA'][$iform]['unico']=0;

$iform ++;
$form['AUSENCIA'][$iform]['campo']="HORA_INICIAL";
$form['AUSENCIA'][$iform]['tipo']="hora";
$form['AUSENCIA'][$iform]['null']=0;
$form['AUSENCIA'][$iform]['tam']=30;
$form['AUSENCIA'][$iform]['max']=30;
$form['AUSENCIA'][$iform]['unico']=0;
//Deve-se passar a posi��o num�rica do campo de compara��o ao qual esse campo deve ser 'menor_que' ex. 3 = HORA_Final
$form['AUSENCIA'][$iform]['menor_que']= 3;

$iform ++;
$form['AUSENCIA'][$iform]['campo']="HORA_FINAL";
$form['AUSENCIA'][$iform]['tipo']="hora";
$form['AUSENCIA'][$iform]['null']=0;
$form['AUSENCIA'][$iform]['tam']=30;
$form['AUSENCIA'][$iform]['max']=30;
$form['AUSENCIA'][$iform]['unico']=0;


$iform ++;
$form['AUSENCIA'][$iform]['campo']="DATA";
$form['AUSENCIA'][$iform]['tipo']="data";
$form['AUSENCIA'][$iform]['null']=0;
$form['AUSENCIA'][$iform]['tam']=30;
$form['AUSENCIA'][$iform]['max']=100;
$form['AUSENCIA'][$iform]['unico']=0;

$iform ++;
$form['AUSENCIA'][$iform]['campo']="MOTIVO";
$form['AUSENCIA'][$iform]['tipo']="texto";
$form['AUSENCIA'][$iform]['null']=0;
$form['AUSENCIA'][$iform]['tam']=60;
$form['AUSENCIA'][$iform]['max']=255;
$form['AUSENCIA'][$iform]['unico']=0;
$form['AUSENCIA'][$iform]['fecha_fieldset']=1;

// FORMULARIO ESPECIALIDADES
$form['ESPECIALIDADE'][0]['nome']=ESPECIALIDADES;
$form['ESPECIALIDADE'][0]['titulo']=$tdb[ESPECIALIDADES]['DESC'];
$form['ESPECIALIDADE'][0]['obs']="";
$form['ESPECIALIDADE'][0]['largura']=500;
$form['ESPECIALIDADE'][0]['altura']=160;
$form['ESPECIALIDADE'][0]['chave']="MID";

$form['ESPECIALIDADE'][1]['abre_fieldset']=$tdb[ESPECIALIDADES]['DESC'];

$form['ESPECIALIDADE'][1]['campo']="MID_EMPRESA";
$form['ESPECIALIDADE'][1]['tipo']="select_rela";
$form['ESPECIALIDADE'][1]['null']=1;
$form['ESPECIALIDADE'][1]['tam']=15;
$form['ESPECIALIDADE'][1]['max']=30;

$form['ESPECIALIDADE'][2]['campo']="DESCRICAO";
$form['ESPECIALIDADE'][2]['tipo']="texto";
$form['ESPECIALIDADE'][2]['null']=0;
$form['ESPECIALIDADE'][2]['tam']=30;
$form['ESPECIALIDADE'][2]['max']=250;
$form['ESPECIALIDADE'][2]['unico']=1;

$form['ESPECIALIDADE'][2]['fecha_fieldset']=1;

// FORMULARIO TURNOS
$form['TURNO'][0]['nome']=TURNOS;
$form['TURNO'][0]['titulo']=$tdb[TURNOS]['DESC'];
$form['TURNO'][0]['obs']="";
$form['TURNO'][0]['largura']=400;
$form['TURNO'][0]['altura']=200;
$form['TURNO'][0]['chave']="MID";

$form['TURNO'][1]['abre_fieldset']=$tdb[TURNOS]['DESC'];

$form['TURNO'][1]['campo']="MID_EMPRESA";
$form['TURNO'][1]['tipo']="select_rela";
$form['TURNO'][1]['null']=1;
$form['TURNO'][1]['tam']=15;
$form['TURNO'][1]['max']=30;

$form['TURNO'][2]['campo']="DESCRICAO";
$form['TURNO'][2]['tipo']="texto";
$form['TURNO'][2]['null']=0;
$form['TURNO'][2]['tam']=30;
$form['TURNO'][2]['max']=250;
$form['TURNO'][2]['unico']=1;

$form['TURNO'][2]['fecha_fieldset']=1;


$form['PONTO_PREDITIVA_EQUIP'][0]['nome']=PONTOS_PREDITIVA;
$form['PONTO_PREDITIVA_EQUIP'][0]['titulo']=$tdb[PONTOS_PREDITIVA]['DESC'];
$form['PONTO_PREDITIVA_EQUIP'][0]['obs']="";
$form['PONTO_PREDITIVA_EQUIP'][0]['largura']=450;
$form['PONTO_PREDITIVA_EQUIP'][0]['altura']=300;
$form['PONTO_PREDITIVA_EQUIP'][0]['chave']="MID";

$form['PONTO_PREDITIVA_EQUIP'][1]['abre_fieldset']=$tdb[PONTOS_PREDITIVA]['DESC'];

$form['PONTO_PREDITIVA_EQUIP'][1]['campo']="PONTO";
$form['PONTO_PREDITIVA_EQUIP'][1]['tipo']="texto";
$form['PONTO_PREDITIVA_EQUIP'][1]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][1]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][1]['max']=250;
$form['PONTO_PREDITIVA_EQUIP'][1]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][2]['campo']="GRANDEZA";
$form['PONTO_PREDITIVA_EQUIP'][2]['tipo']="texto";
$form['PONTO_PREDITIVA_EQUIP'][2]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][2]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][2]['max']=50;
$form['PONTO_PREDITIVA_EQUIP'][2]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][3]['campo']="VALOR_MINIMO";
$form['PONTO_PREDITIVA_EQUIP'][3]['tipo']="texto_float";
$form['PONTO_PREDITIVA_EQUIP'][3]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][3]['tam']=15;
$form['PONTO_PREDITIVA_EQUIP'][3]['max']=30;
$form['PONTO_PREDITIVA_EQUIP'][3]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][4]['campo']="VALOR_MAXIMO";
$form['PONTO_PREDITIVA_EQUIP'][4]['tipo']="texto_float";
$form['PONTO_PREDITIVA_EQUIP'][4]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][4]['tam']=15;
$form['PONTO_PREDITIVA_EQUIP'][4]['max']=30;
$form['PONTO_PREDITIVA_EQUIP'][4]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][5]['campo']="OBSERVACAO";
$form['PONTO_PREDITIVA_EQUIP'][5]['tipo']="blog";
$form['PONTO_PREDITIVA_EQUIP'][5]['null']=1;
$form['PONTO_PREDITIVA_EQUIP'][5]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][5]['max']=5;
$form['PONTO_PREDITIVA_EQUIP'][5]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][6]['campo']="MID_MAQUINA";
$form['PONTO_PREDITIVA_EQUIP'][6]['tipo']="recb_valor";
$form['PONTO_PREDITIVA_EQUIP'][6]['valor']=(int)VoltaValor(EQUIPAMENTOS,'MID_MAQUINA','MID',(int)$form_recb_valor,$tdb[PONTOS_PREDITIVA]['dba']);
$form['PONTO_PREDITIVA_EQUIP'][6]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][6]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][6]['max']=250;
$form['PONTO_PREDITIVA_EQUIP'][6]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][7]['campo']="MID_EQUIPAMENTO";
$form['PONTO_PREDITIVA_EQUIP'][7]['tipo']="recb_valor";
$form['PONTO_PREDITIVA_EQUIP'][7]['valor']=$form_recb_valor;
$form['PONTO_PREDITIVA_EQUIP'][7]['null']=0;
$form['PONTO_PREDITIVA_EQUIP'][7]['tam']=30;
$form['PONTO_PREDITIVA_EQUIP'][7]['max']=250;
$form['PONTO_PREDITIVA_EQUIP'][7]['unico']=0;

$form['PONTO_PREDITIVA_EQUIP'][7]['fecha_fieldset']=1;


// FORMULARIO MATERIAL DO EQUIPAMENTO
$form['EQUIPAMENTO_MATERIAL'][0]['nome']=EQUIPAMENTOS_MATERIAL;
$form['EQUIPAMENTO_MATERIAL'][0]['titulo']=$tdb[EQUIPAMENTOS_MATERIAL]['DESC'];
$form['EQUIPAMENTO_MATERIAL'][0]['obs']="";
$form['EQUIPAMENTO_MATERIAL'][0]['largura']=550;
$form['EQUIPAMENTO_MATERIAL'][0]['altura']=200;
$form['EQUIPAMENTO_MATERIAL'][0]['chave']="MID";

$form['EQUIPAMENTO_MATERIAL'][1]['abre_fieldset']=$tdb[EQUIPAMENTOS_MATERIAL]['DESC'];

$form['EQUIPAMENTO_MATERIAL'][1]['campo']="MID_MATERIAL";
$form['EQUIPAMENTO_MATERIAL'][1]['tipo']="select_material";
$form['EQUIPAMENTO_MATERIAL'][1]['null']=0;
$form['EQUIPAMENTO_MATERIAL'][1]['tam']=10;
$form['EQUIPAMENTO_MATERIAL'][1]['max']=10;
$form['EQUIPAMENTO_MATERIAL'][1]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][2]['campo']="QUANTIDADE";
$form['EQUIPAMENTO_MATERIAL'][2]['tipo']="texto_unidade";
$form['EQUIPAMENTO_MATERIAL'][2]['null']=0;
$form['EQUIPAMENTO_MATERIAL'][2]['tam']=5;
$form['EQUIPAMENTO_MATERIAL'][2]['max']=5;
$form['EQUIPAMENTO_MATERIAL'][2]['unico']=0;
$form['EQUIPAMENTO_MATERIAL'][2]['nao_negativo']=1;
$form['EQUIPAMENTO_MATERIAL'][2]['nao_zero']=1;
$form['EQUIPAMENTO_MATERIAL'][2]['campo_material']=1;

$form['EQUIPAMENTO_MATERIAL'][3]['campo']="MID_CLASSE";
$form['EQUIPAMENTO_MATERIAL'][3]['tipo']="select_rela";
$form['EQUIPAMENTO_MATERIAL'][3]['null']=1;
$form['EQUIPAMENTO_MATERIAL'][3]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][4]['campo']="MID_EQUIPAMENTO";
$form['EQUIPAMENTO_MATERIAL'][4]['tipo']="recb_valor";
$form['EQUIPAMENTO_MATERIAL'][4]['valor']=$form_recb_valor;
$form['EQUIPAMENTO_MATERIAL'][4]['null']=0;
$form['EQUIPAMENTO_MATERIAL'][4]['tam']=0;
$form['EQUIPAMENTO_MATERIAL'][4]['max']=0;
$form['EQUIPAMENTO_MATERIAL'][4]['unico']=0;

$form['EQUIPAMENTO_MATERIAL'][4]['fecha_fieldset']=1;

// FORMULARIO MATERIAL DO EQUIPAMENTO
// Filtrando os CENTRO DE CUSTO pela empresa do Almoxarifado
$filtro_emp = "WHERE (MID_EMPRESA=0";
// Buscando a empresa selecionada
if ((! $_POST['cc'][ESTOQUE_ENTRADA]) and ($act == 2)) {
	$almox = (int)VoltaValor(ESTOQUE_ENTRADA, 'MID_ALMOXARIFADO', 'MID', (int)$_GET['foq']);
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(ALMOXARIFADO, 'MID_EMPRESA', 'MID', $almox);
}
elseif ($_POST['cc'][ESTOQUE_ENTRADA][1] != 0) {
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(ALMOXARIFADO, 'MID_EMPRESA', 'MID', (int)$_POST['cc'][ESTOQUE_ENTRADA][1]);
}
$filtro_emp .= ")";

$form['ESTOQUE_ENTRADA'][0]['nome']=ESTOQUE_ENTRADA;
$form['ESTOQUE_ENTRADA'][0]['titulo']=$tdb[ESTOQUE_ENTRADA]['DESC'];
$form['ESTOQUE_ENTRADA'][0]['obs']="";
$form['ESTOQUE_ENTRADA'][0]['largura']=600;
$form['ESTOQUE_ENTRADA'][0]['altura']=440;
$form['ESTOQUE_ENTRADA'][0]['chave']="MID";

$form['ESTOQUE_ENTRADA'][1]['abre_fieldset']=$tdb[ESTOQUE_ENTRADA]['DESC'];

$form['ESTOQUE_ENTRADA'][1]['campo']="MID_ALMOXARIFADO";
$form['ESTOQUE_ENTRADA'][1]['tipo']="select_rela";
$form['ESTOQUE_ENTRADA'][1]['null']=0;
$form['ESTOQUE_ENTRADA'][1]['unico']=0;
$form['ESTOQUE_ENTRADA'][1]['js']="this.form.submit();";

$form['ESTOQUE_ENTRADA'][2]['campo']="MID_MATERIAL";
$form['ESTOQUE_ENTRADA'][2]['tipo']="select_material";
$form['ESTOQUE_ENTRADA'][2]['null']=0;
$form['ESTOQUE_ENTRADA'][2]['unico']=0;

$form['ESTOQUE_ENTRADA'][3]['campo']="QUANTIDADE";
$form['ESTOQUE_ENTRADA'][3]['tipo']="texto_unidade";
$form['ESTOQUE_ENTRADA'][3]['null']=0;
$form['ESTOQUE_ENTRADA'][3]['tam']=5;
$form['ESTOQUE_ENTRADA'][3]['max']=5;
$form['ESTOQUE_ENTRADA'][3]['onblur']="document.getElementById('cc[" . ESTOQUE_ENTRADA . "][5]').value = (roundNumber(parseFloat(this.value.replace(',', '.')) * parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][4]').value.replace(',', '.')), 2)).toString().replace('.', ',').replace('NaN', '')";
$form['ESTOQUE_ENTRADA'][3]['nao_negativo']=1;
$form['ESTOQUE_ENTRADA'][3]['nao_zero']=1;
$form['ESTOQUE_ENTRADA'][3]['campo_material']=2;

$form['ESTOQUE_ENTRADA'][4]['campo']="VALOR_UNITARIO";
$form['ESTOQUE_ENTRADA'][4]['tipo']="texto_float";
$form['ESTOQUE_ENTRADA'][4]['null']=0;
$form['ESTOQUE_ENTRADA'][4]['tam']=10;
$form['ESTOQUE_ENTRADA'][4]['max']=10;
$form['ESTOQUE_ENTRADA'][4]['onblur']="document.getElementById('cc[" . ESTOQUE_ENTRADA . "][5]').value = (roundNumber(parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][3]').value.replace(',', '.')) * parseFloat(this.value.replace(',', '.')), 2)).toString().replace('.', ',').replace('NaN', '')";
$form['ESTOQUE_ENTRADA'][4]['nao_negativo']=1;

$form['ESTOQUE_ENTRADA'][5]['campo']="VALOR_TOTAL";
$form['ESTOQUE_ENTRADA'][5]['tipo']="texto_float";
$form['ESTOQUE_ENTRADA'][5]['null']=0;
$form['ESTOQUE_ENTRADA'][5]['tam']=10;
$form['ESTOQUE_ENTRADA'][5]['max']=10;
$form['ESTOQUE_ENTRADA'][5]['onfocus']="this.value = (roundNumber(parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][3]').value.replace(',', '.')) * parseFloat(document.getElementById('cc[" . ESTOQUE_ENTRADA . "][4]').value.replace(',', '.')), 2)).toString().replace('.', ',').replace('NaN', '')";
$form['ESTOQUE_ENTRADA'][5]['nao_negativo']=1;

$form['ESTOQUE_ENTRADA'][6]['campo']="CENTRO_DE_CUSTO";
$form['ESTOQUE_ENTRADA'][6]['tipo']="select_rela";
$form['ESTOQUE_ENTRADA'][6]['null']=1;
$form['ESTOQUE_ENTRADA'][6]['unico']=0;
$form['ESTOQUE_ENTRADA'][6]['sql']=$filtro_emp;

$form['ESTOQUE_ENTRADA'][7]['campo']="DATA";
$form['ESTOQUE_ENTRADA'][7]['tipo']="data";
$form['ESTOQUE_ENTRADA'][7]['valor']=1;
$form['ESTOQUE_ENTRADA'][7]['null']=0;
$form['ESTOQUE_ENTRADA'][7]['unico']=0;

$form['ESTOQUE_ENTRADA'][8]['campo']="HORA";
$form['ESTOQUE_ENTRADA'][8]['tipo']="hora";
$form['ESTOQUE_ENTRADA'][8]['valor']=1;
$form['ESTOQUE_ENTRADA'][8]['null']=0;
$form['ESTOQUE_ENTRADA'][8]['unico']=0;

$form['ESTOQUE_ENTRADA'][9]['campo']="MOTIVO";
$form['ESTOQUE_ENTRADA'][9]['tipo']="texto";
$form['ESTOQUE_ENTRADA'][9]['null']=1;
$form['ESTOQUE_ENTRADA'][9]['tam']=40;
$form['ESTOQUE_ENTRADA'][9]['max']=150;
$form['ESTOQUE_ENTRADA'][9]['unico']=0;

$form['ESTOQUE_ENTRADA'][10]['campo']="DOCUMENTO";
$form['ESTOQUE_ENTRADA'][10]['tipo']="texto";
$form['ESTOQUE_ENTRADA'][10]['null']=1;
$form['ESTOQUE_ENTRADA'][10]['tam']=40;
$form['ESTOQUE_ENTRADA'][10]['max']=150;
$form['ESTOQUE_ENTRADA'][10]['unico']=0;

$form['ESTOQUE_ENTRADA'][11]['campo']="OBSERVACAO";
$form['ESTOQUE_ENTRADA'][11]['tipo']="blog";
$form['ESTOQUE_ENTRADA'][11]['null']=1;
$form['ESTOQUE_ENTRADA'][11]['tam']=40;
$form['ESTOQUE_ENTRADA'][11]['max']=3;
$form['ESTOQUE_ENTRADA'][11]['unico']=0;

$form['ESTOQUE_ENTRADA'][11]['fecha_fieldset']=1;





// FORMULARIO SAIDA DE ESTOQUE
// Filtrando os CENTRO DE CUSTO pela empresa do Almoxarifado
$filtro_emp = "WHERE (MID_EMPRESA=0";
// Buscando a empresa selecionada
if ((! $_POST['cc'][ESTOQUE_SAIDA]) and ($act == 2)) {
	$almox = (int)VoltaValor(ESTOQUE_SAIDA, 'MID_ALMOXARIFADO', 'MID', (int)$_GET['foq']);
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(ALMOXARIFADO, 'MID_EMPRESA', 'MID', $almox);
}
elseif ($_POST['cc'][ESTOQUE_SAIDA][1] != 0) {
	$filtro_emp .= " OR MID_EMPRESA=" . (int)VoltaValor(ALMOXARIFADO, 'MID_EMPRESA', 'MID', (int)$_POST['cc'][ESTOQUE_SAIDA][1]);
}
$filtro_emp .= ")";

$form['ESTOQUE_SAIDA'][0]['nome']=ESTOQUE_SAIDA;
$form['ESTOQUE_SAIDA'][0]['titulo']=$tdb[ESTOQUE_SAIDA]['DESC'];
$form['ESTOQUE_SAIDA'][0]['obs']="";
$form['ESTOQUE_SAIDA'][0]['largura']=600;
$form['ESTOQUE_SAIDA'][0]['altura']=380;
$form['ESTOQUE_SAIDA'][0]['chave']="MID";

$form['ESTOQUE_SAIDA'][1]['abre_fieldset']=$tdb[ESTOQUE_SAIDA]['DESC'];

$form['ESTOQUE_SAIDA'][1]['campo']="MID_ALMOXARIFADO";
$form['ESTOQUE_SAIDA'][1]['tipo']="select_rela";
$form['ESTOQUE_SAIDA'][1]['null']=0;
$form['ESTOQUE_SAIDA'][1]['unico']=0;
$form['ESTOQUE_SAIDA'][1]['js']="document.forms[0].submit()";

// Mostrar campo somente depois de selecionar o almoxarifado
if ($_POST['cc']) {
    $form['ESTOQUE_SAIDA'][2]['campo']="MID_MATERIAL";
    $form['ESTOQUE_SAIDA'][2]['tipo']="select_material";
    $form['ESTOQUE_SAIDA'][2]['null']=0;
    $form['ESTOQUE_SAIDA'][2]['unico']=0;
}
else {
    $form['ESTOQUE_SAIDA'][2]['campo']="MID_MATERIAL";
    $form['ESTOQUE_SAIDA'][2]['tipo']="recb_valor";
    $form['ESTOQUE_SAIDA'][2]['null']=0;
    $form['ESTOQUE_SAIDA'][2]['unico']=0;
    $form['ESTOQUE_SAIDA'][2]['valor']=0;
}

$form['ESTOQUE_SAIDA'][3]['campo']="QUANTIDADE";
$form['ESTOQUE_SAIDA'][3]['tipo']="texto_unidade";
$form['ESTOQUE_SAIDA'][3]['null']=0;
$form['ESTOQUE_SAIDA'][3]['tam']=5;
$form['ESTOQUE_SAIDA'][3]['max']=5;
$form['ESTOQUE_SAIDA'][3]['unico']=0;
$form['ESTOQUE_SAIDA'][3]['campo_material']=2;

$form['ESTOQUE_SAIDA'][4]['campo']="CENTRO_DE_CUSTO";
$form['ESTOQUE_SAIDA'][4]['tipo']="select_rela";
$form['ESTOQUE_SAIDA'][4]['null']=1;
$form['ESTOQUE_SAIDA'][4]['unico']=0;
$form['ESTOQUE_SAIDA'][4]['sql']=$filtro_emp;

$form['ESTOQUE_SAIDA'][5]['campo']="DATA";
$form['ESTOQUE_SAIDA'][5]['tipo']="data";
$form['ESTOQUE_SAIDA'][5]['valor']=1;
$form['ESTOQUE_SAIDA'][5]['null']=0;
$form['ESTOQUE_SAIDA'][5]['unico']=0;

$form['ESTOQUE_SAIDA'][6]['campo']="HORA";
$form['ESTOQUE_SAIDA'][6]['tipo']="hora";
$form['ESTOQUE_SAIDA'][6]['valor']=1;
$form['ESTOQUE_SAIDA'][6]['null']=0;
$form['ESTOQUE_SAIDA'][6]['unico']=0;

$form['ESTOQUE_SAIDA'][7]['campo']="MOTIVO";
$form['ESTOQUE_SAIDA'][7]['tipo']="texto";
$form['ESTOQUE_SAIDA'][7]['null']=1;
$form['ESTOQUE_SAIDA'][7]['tam']=40;
$form['ESTOQUE_SAIDA'][7]['max']=150;
$form['ESTOQUE_SAIDA'][7]['unico']=0;

$form['ESTOQUE_SAIDA'][8]['campo']="ORDEM";
$form['ESTOQUE_SAIDA'][8]['tipo']="select_rela";
$form['ESTOQUE_SAIDA'][8]['null']=1;
$form['ESTOQUE_SAIDA'][8]['tam']=10;
$form['ESTOQUE_SAIDA'][8]['max']=15;
$form['ESTOQUE_SAIDA'][8]['unico']=0;
$form['ESTOQUE_SAIDA'][8]['sql']=$filtro_emp;

$form['ESTOQUE_SAIDA'][9]['campo']="OBSERVACAO";
$form['ESTOQUE_SAIDA'][9]['tipo']="blog";
$form['ESTOQUE_SAIDA'][9]['null']=1;
$form['ESTOQUE_SAIDA'][9]['tam']=40;
$form['ESTOQUE_SAIDA'][9]['max']=3;
$form['ESTOQUE_SAIDA'][9]['unico']=0;

$form['ESTOQUE_SAIDA'][9]['fecha_fieldset']=1;

// FORMULARIO LANCAMENTO DO CONTADOR
$form['LANCA_PREDITIVA'][0]['nome']=LANC_PREDITIVA;
$form['LANCA_PREDITIVA'][0]['titulo']=$ling['lanc_pontos_preditiva_aponta'];
$form['LANCA_PREDITIVA'][0]['obs']="";
$form['LANCA_PREDITIVA'][0]['largura']=500;
$form['LANCA_PREDITIVA'][0]['altura']=350;
$form['LANCA_PREDITIVA'][0]['chave']="MID";

$form['LANCA_PREDITIVA'][1]['campo']="VALOR";
$form['LANCA_PREDITIVA'][1]['tipo']="texto";
$form['LANCA_PREDITIVA'][1]['null']=1;
$form['LANCA_PREDITIVA'][1]['tam']=20;
$form['LANCA_PREDITIVA'][1]['max']=13;
$form['LANCA_PREDITIVA'][1]['unico']=0;

$form['LANCA_PREDITIVA'][2]['campo']="DATA";
$form['LANCA_PREDITIVA'][2]['tipo']="data";
$form['LANCA_PREDITIVA'][2]['valor']=1; // Recebe dia ATUAL
$form['LANCA_PREDITIVA'][2]['null']=1;
$form['LANCA_PREDITIVA'][2]['tam']=20;
$form['LANCA_PREDITIVA'][2]['max']=13;
$form['LANCA_PREDITIVA'][2]['unico']=0;

$form['LANCA_PREDITIVA'][3]['campo']="OBSERVACAO";
$form['LANCA_PREDITIVA'][3]['tipo']="blog";
$form['LANCA_PREDITIVA'][3]['null']=1;
$form['LANCA_PREDITIVA'][3]['tam']=20;
$form['LANCA_PREDITIVA'][3]['max']=5;
$form['LANCA_PREDITIVA'][3]['unico']=0;

$form['LANCA_PREDITIVA'][4]['campo']="MID_PONTO";
$form['LANCA_PREDITIVA'][4]['tipo']="recb_valor";
$form['LANCA_PREDITIVA'][4]['valor']=$form_recb_valor;
$form['LANCA_PREDITIVA'][4]['null']=0;
$form['LANCA_PREDITIVA'][4]['tam']=0;
$form['LANCA_PREDITIVA'][4]['max']=0;
$form['LANCA_PREDITIVA'][4]['unico']=0;


// FORMULARIO MATRIX
$form['MATRIX'][0]['nome']=EMPRESAS;
$form['MATRIX'][0]['titulo']=$tdb[EMPRESAS]['DESC'];
$form['MATRIX'][0]['obs']="";
$form['MATRIX'][0]['largura']=500;
$form['MATRIX'][0]['altura']=530;
$form['MATRIX'][0]['chave']="MID";

$form['MATRIX'][1]['abre_fieldset']=$tdb[EMPRESAS]['DESC'];

$form['MATRIX'][1]['campo']="EMP_MATRIZ";
$form['MATRIX'][1]['tipo']="select_rela";
$form['MATRIX'][1]['null']=0;
$form['MATRIX'][1]['tam']=15;
$form['MATRIX'][1]['max']=21;

$form['MATRIX'][2]['campo']="NOME";
$form['MATRIX'][2]['tipo']="texto";
$form['MATRIX'][2]['null']=0;
$form['MATRIX'][2]['tam']=40;
$form['MATRIX'][2]['max']=48;
$form['MATRIX'][2]['unico']=1;

$form['MATRIX'][3]['campo']="FANTASIA";
$form['MATRIX'][3]['tipo']="texto";
$form['MATRIX'][3]['null']=1;
$form['MATRIX'][3]['tam']=40;
$form['MATRIX'][3]['max']=48;

$form['MATRIX'][4]['campo']="CNPJ";
$form['MATRIX'][4]['tipo']="texto";
$form['MATRIX'][4]['null']=0;
$form['MATRIX'][4]['tam']=14;
$form['MATRIX'][4]['max']=14;

$form['MATRIX'][5]['campo']="IE";
$form['MATRIX'][5]['tipo']="texto";
$form['MATRIX'][5]['null']=1;
$form['MATRIX'][5]['tam']=14;
$form['MATRIX'][5]['max']=15;

$form['MATRIX'][6]['campo']="ENDERECO";
$form['MATRIX'][6]['tipo']="texto";
$form['MATRIX'][6]['null']=1;
$form['MATRIX'][6]['tam']=40;
$form['MATRIX'][6]['max']=55;

$form['MATRIX'][7]['campo']="NUMERO";
$form['MATRIX'][7]['tipo']="texto";
$form['MATRIX'][7]['null']=1;
$form['MATRIX'][7]['tam']=5;
$form['MATRIX'][7]['max']=11;

$form['MATRIX'][8]['campo']="COMPLEMENTO";
$form['MATRIX'][8]['tipo']="texto";
$form['MATRIX'][8]['null']=1;
$form['MATRIX'][8]['tam']=25;
$form['MATRIX'][8]['max']=25;

$form['MATRIX'][9]['campo']="BAIRRO";
$form['MATRIX'][9]['tipo']="texto";
$form['MATRIX'][9]['null']=1;
$form['MATRIX'][9]['tam']=25;
$form['MATRIX'][9]['max']=35;

$form['MATRIX'][10]['campo']="CEP";
$form['MATRIX'][10]['tipo']="texto";
$form['MATRIX'][10]['null']=1;
$form['MATRIX'][10]['tam']=10;
$form['MATRIX'][10]['max']=10;

$form['MATRIX'][11]['campo']="CIDADE";
$form['MATRIX'][11]['tipo']="texto";
$form['MATRIX'][11]['null']=1;
$form['MATRIX'][11]['tam']=25;
$form['MATRIX'][11]['max']=32;

$form['MATRIX'][12]['campo']="UF";
$form['MATRIX'][12]['tipo']="texto";
$form['MATRIX'][12]['null']=1;
$form['MATRIX'][12]['tam']=2;
$form['MATRIX'][12]['max']=2;

$form['MATRIX'][13]['campo']="PAIS";
$form['MATRIX'][13]['tipo']="texto";
$form['MATRIX'][13]['null']=1;
$form['MATRIX'][13]['tam']=25;
$form['MATRIX'][13]['max']=32;

$form['MATRIX'][14]['campo']="TELEFONE_1";
$form['MATRIX'][14]['tipo']="tel";
$form['MATRIX'][14]['null']=1;
$form['MATRIX'][14]['tam']=15;
$form['MATRIX'][14]['max']=21;

$form['MATRIX'][15]['campo']="TELEFONE_2";
$form['MATRIX'][15]['tipo']="tel";
$form['MATRIX'][15]['null']=1;
$form['MATRIX'][15]['tam']=15;
$form['MATRIX'][15]['max']=21;

$form['MATRIX'][16]['campo']="FAX";
$form['MATRIX'][16]['tipo']="tel";
$form['MATRIX'][16]['null']=1;
$form['MATRIX'][16]['tam']=15;
$form['MATRIX'][16]['max']=21;

$form['MATRIX'][17]['campo']="OBSERVACAO";
$form['MATRIX'][17]['tipo']="blog";
$form['MATRIX'][17]['null']=1;
$form['MATRIX'][17]['tam']=36;
$form['MATRIX'][17]['max']=6;

$form['MATRIX'][17]['fecha_fieldset']=1;


// FORMULARIO MATRIX
$form['FORNECEDOR2'][0]['nome']=FABRICANTES;
$form['FORNECEDOR2'][0]['titulo']=$tdb[FABRICANTES]['DESC'];
$form['FORNECEDOR2'][0]['obs']="";
$form['FORNECEDOR2'][0]['largura']=500;
$form['FORNECEDOR2'][0]['altura']=430;
$form['FORNECEDOR2'][0]['chave']="MID";

$form['FORNECEDOR2'][1]['abre_fieldset']=$tdb[FABRICANTES]['DESC'];

$form['FORNECEDOR2'][1]['campo']="NOME";
$form['FORNECEDOR2'][1]['tipo']="texto";
$form['FORNECEDOR2'][1]['null']=0;
$form['FORNECEDOR2'][1]['tam']=40;
$form['FORNECEDOR2'][1]['max']=120;
$form['FORNECEDOR2'][1]['unico']=1;

$form['FORNECEDOR2'][2]['campo']="CNPJ";
$form['FORNECEDOR2'][2]['tipo']="texto";
$form['FORNECEDOR2'][2]['null']=1;
$form['FORNECEDOR2'][2]['tam']=14;
$form['FORNECEDOR2'][2]['max']=14;

$form['FORNECEDOR2'][3]['campo']="IE";
$form['FORNECEDOR2'][3]['tipo']="texto";
$form['FORNECEDOR2'][3]['null']=1;
$form['FORNECEDOR2'][3]['tam']=14;
$form['FORNECEDOR2'][3]['max']=15;

$form['FORNECEDOR2'][4]['campo']="ENDERECO";
$form['FORNECEDOR2'][4]['tipo']="texto";
$form['FORNECEDOR2'][4]['null']=1;
$form['FORNECEDOR2'][4]['tam']=40;
$form['FORNECEDOR2'][4]['max']=55;

$form['FORNECEDOR2'][5]['campo']="BAIRRO";
$form['FORNECEDOR2'][5]['tipo']="texto";
$form['FORNECEDOR2'][5]['null']=1;
$form['FORNECEDOR2'][5]['tam']=25;
$form['FORNECEDOR2'][5]['max']=50;

$form['FORNECEDOR2'][6]['campo']="CEP";
$form['FORNECEDOR2'][6]['tipo']="texto";
$form['FORNECEDOR2'][6]['null']=1;
$form['FORNECEDOR2'][6]['tam']=10;
$form['FORNECEDOR2'][6]['max']=10;

$form['FORNECEDOR2'][7]['campo']="CIDADE";
$form['FORNECEDOR2'][7]['tipo']="texto";
$form['FORNECEDOR2'][7]['null']=1;
$form['FORNECEDOR2'][7]['tam']=40;
$form['FORNECEDOR2'][7]['max']=150;

$form['FORNECEDOR2'][8]['campo']="ESTADO";
$form['FORNECEDOR2'][8]['tipo']="texto";
$form['FORNECEDOR2'][8]['null']=1;
$form['FORNECEDOR2'][8]['tam']=2;
$form['FORNECEDOR2'][8]['max']=2;

$form['FORNECEDOR2'][9]['campo']="TELEFONE";
$form['FORNECEDOR2'][9]['tipo']="texto";
$form['FORNECEDOR2'][9]['null']=1;
$form['FORNECEDOR2'][9]['tam']=15;
$form['FORNECEDOR2'][9]['max']=21;

$form['FORNECEDOR2'][10]['campo']="FAX";
$form['FORNECEDOR2'][10]['tipo']="texto";
$form['FORNECEDOR2'][10]['null']=1;
$form['FORNECEDOR2'][10]['tam']=15;
$form['FORNECEDOR2'][10]['max']=21;

$form['FORNECEDOR2'][11]['campo']="SITE";
$form['FORNECEDOR2'][11]['tipo']="texto";
$form['FORNECEDOR2'][11]['null']=1;
$form['FORNECEDOR2'][11]['tam']=40;
$form['FORNECEDOR2'][11]['max']=150;

$form['FORNECEDOR2'][12]['campo']="EMAIL";
$form['FORNECEDOR2'][12]['tipo']="texto";
$form['FORNECEDOR2'][12]['null']=1;
$form['FORNECEDOR2'][12]['tam']=40;
$form['FORNECEDOR2'][12]['max']=150;

$form['FORNECEDOR2'][13]['campo']="OBSERVACAO";
$form['FORNECEDOR2'][13]['tipo']="blog";
$form['FORNECEDOR2'][13]['null']=1;
$form['FORNECEDOR2'][13]['tam']=40;
$form['FORNECEDOR2'][13]['max']=6;
$form['FORNECEDOR2'][13]['fecha_fieldset']=1;


// FORMULARIO MATRIX
$form['FORNECEDOR'][0]['nome']=FORNECEDORES;
$form['FORNECEDOR'][0]['titulo']=$tdb[FORNECEDORES]['DESC'];
$form['FORNECEDOR'][0]['obs']="";
$form['FORNECEDOR'][0]['largura']=500;
$form['FORNECEDOR'][0]['altura']=430;
$form['FORNECEDOR'][0]['chave']="MID";

$form['FORNECEDOR'][1]['abre_fieldset']=$tdb[FORNECEDORES]['DESC'];

$form['FORNECEDOR'][1]['campo']="NOME";
$form['FORNECEDOR'][1]['tipo']="texto";
$form['FORNECEDOR'][1]['null']=0;
$form['FORNECEDOR'][1]['tam']=40;
$form['FORNECEDOR'][1]['max']=120;
$form['FORNECEDOR'][1]['unico']=1;

$form['FORNECEDOR'][2]['campo']="FANTASIA";
$form['FORNECEDOR'][2]['tipo']="texto";
$form['FORNECEDOR'][2]['null']=1;
$form['FORNECEDOR'][2]['tam']=40;
$form['FORNECEDOR'][2]['max']=120;
$form['FORNECEDOR'][2]['unico']=1;

$form['FORNECEDOR'][3]['campo']="CNPJ";
$form['FORNECEDOR'][3]['tipo']="texto";
$form['FORNECEDOR'][3]['null']=1;
$form['FORNECEDOR'][3]['tam']=14;
$form['FORNECEDOR'][3]['max']=18;

$form['FORNECEDOR'][4]['campo']="CGC";
$form['FORNECEDOR'][4]['tipo']="texto";
$form['FORNECEDOR'][4]['null']=1;
$form['FORNECEDOR'][4]['tam']=14;
$form['FORNECEDOR'][4]['max']=18;

$form['FORNECEDOR'][5]['campo']="ENDERECO";
$form['FORNECEDOR'][5]['tipo']="texto";
$form['FORNECEDOR'][5]['null']=1;
$form['FORNECEDOR'][5]['tam']=40;
$form['FORNECEDOR'][5]['max']=150;

$form['FORNECEDOR'][6]['campo']="NUMERO";
$form['FORNECEDOR'][6]['tipo']="texto";
$form['FORNECEDOR'][6]['null']=1;
$form['FORNECEDOR'][6]['tam']=4;
$form['FORNECEDOR'][6]['max']=10;

$form['FORNECEDOR'][7]['campo']="COMPLEMENTO";
$form['FORNECEDOR'][7]['tipo']="texto";
$form['FORNECEDOR'][7]['null']=1;
$form['FORNECEDOR'][7]['tam']=25;
$form['FORNECEDOR'][7]['max']=50;

$form['FORNECEDOR'][8]['campo']="BAIRRO";
$form['FORNECEDOR'][8]['tipo']="texto";
$form['FORNECEDOR'][8]['null']=1;
$form['FORNECEDOR'][8]['tam']=25;
$form['FORNECEDOR'][8]['max']=50;

$form['FORNECEDOR'][9]['campo']="CIDADE";
$form['FORNECEDOR'][9]['tipo']="texto";
$form['FORNECEDOR'][9]['null']=1;
$form['FORNECEDOR'][9]['tam']=40;
$form['FORNECEDOR'][9]['max']=150;

$form['FORNECEDOR'][10]['campo']="UF";
$form['FORNECEDOR'][10]['tipo']="texto";
$form['FORNECEDOR'][10]['null']=1;
$form['FORNECEDOR'][10]['tam']=2;
$form['FORNECEDOR'][10]['max']=2;

$form['FORNECEDOR'][11]['campo']="CEP";
$form['FORNECEDOR'][11]['tipo']="texto";
$form['FORNECEDOR'][11]['null']=1;
$form['FORNECEDOR'][11]['tam']=10;
$form['FORNECEDOR'][11]['max']=10;

$form['FORNECEDOR'][12]['campo']="TELEFONE_1";
$form['FORNECEDOR'][12]['tipo']="texto";
$form['FORNECEDOR'][12]['null']=1;
$form['FORNECEDOR'][12]['tam']=15;
$form['FORNECEDOR'][12]['max']=21;

$form['FORNECEDOR'][13]['campo']="TELEFONE_2";
$form['FORNECEDOR'][13]['tipo']="texto";
$form['FORNECEDOR'][13]['null']=1;
$form['FORNECEDOR'][13]['tam']=15;
$form['FORNECEDOR'][13]['max']=21;

$form['FORNECEDOR'][14]['campo']="TELEFONE_3";
$form['FORNECEDOR'][14]['tipo']="texto";
$form['FORNECEDOR'][14]['null']=1;
$form['FORNECEDOR'][14]['tam']=15;
$form['FORNECEDOR'][14]['max']=21;

$form['FORNECEDOR'][15]['campo']="SITE";
$form['FORNECEDOR'][15]['tipo']="texto";
$form['FORNECEDOR'][15]['null']=1;
$form['FORNECEDOR'][15]['tam']=40;
$form['FORNECEDOR'][15]['max']=150;

$form['FORNECEDOR'][16]['campo']="E_MAIL";
$form['FORNECEDOR'][16]['tipo']="texto";
$form['FORNECEDOR'][16]['null']=1;
$form['FORNECEDOR'][16]['tam']=40;
$form['FORNECEDOR'][16]['max']=150;

$form['FORNECEDOR'][16]['fecha_fieldset']=1;


// FORMULARIO MAQUINAS FAMILIA
$form['MAQUINAS_FAMILIA'][0]['nome']=MAQUINAS_FAMILIA;
$form['MAQUINAS_FAMILIA'][0]['titulo']=
$form['MAQUINAS_FAMILIA'][1]['abre_fieldset']=$tdb[MAQUINAS_FAMILIA]['DESC'];
$form['MAQUINAS_FAMILIA'][0]['obs']="";
$form['MAQUINAS_FAMILIA'][0]['largura']=500;
$form['MAQUINAS_FAMILIA'][0]['altura']=180;
$form['MAQUINAS_FAMILIA'][0]['chave']="MID";

$form['MAQUINAS_FAMILIA'][1]['abre_fieldset']=$tdb[MAQUINAS_FAMILIA]['DESC'];

$form['MAQUINAS_FAMILIA'][1]['campo']="MID_EMPRESA";
$form['MAQUINAS_FAMILIA'][1]['tipo']="select_rela";
$form['MAQUINAS_FAMILIA'][1]['null']=1;
$form['MAQUINAS_FAMILIA'][1]['tam']=15;
$form['MAQUINAS_FAMILIA'][1]['max']=30;

if ($_autotag_fammaq == 0) {
    $form['MAQUINAS_FAMILIA'][2]['campo']="COD";
    $form['MAQUINAS_FAMILIA'][2]['tipo']="texto";
    $form['MAQUINAS_FAMILIA'][2]['null']=0;
    $form['MAQUINAS_FAMILIA'][2]['tam']=15;
    $form['MAQUINAS_FAMILIA'][2]['max']=30;
    $form['MAQUINAS_FAMILIA'][2]['unico']=1;
}
else {
    $form['MAQUINAS_FAMILIA'][2]['campo']="COD";
    $form['MAQUINAS_FAMILIA'][2]['tipo']="texto_fammaq_autotag";
    $form['MAQUINAS_FAMILIA'][2]['null']=0;
    $form['MAQUINAS_FAMILIA'][2]['valor']=1;
    $form['MAQUINAS_FAMILIA'][2]['unico']=1;
}
$form['MAQUINAS_FAMILIA'][3]['campo']="DESCRICAO";
$form['MAQUINAS_FAMILIA'][3]['tipo']="texto";
$form['MAQUINAS_FAMILIA'][3]['null']=0;
$form['MAQUINAS_FAMILIA'][3]['tam']=30;
$form['MAQUINAS_FAMILIA'][3]['max']=30;
$form['MAQUINAS_FAMILIA'][3]['unico']=0;
$form['MAQUINAS_FAMILIA'][3]['fecha_fieldset']=1;

// FORMULARIO MAQUINAS CLASSE
$form['MAQUINAS_CLASSE'][0]['nome']=MAQUINAS_CLASSE;
$form['MAQUINAS_CLASSE'][0]['titulo']=$tdb[MAQUINAS_CLASSE]['DESC'];
$form['MAQUINAS_CLASSE'][0]['obs']="";
$form['MAQUINAS_CLASSE'][0]['largura']=500;
$form['MAQUINAS_CLASSE'][0]['altura']=150;
$form['MAQUINAS_CLASSE'][0]['chave']="MID";

$form['MAQUINAS_CLASSE'][1]['abre_fieldset']=$tdb[MAQUINAS_CLASSE]['DESC'];

$form['MAQUINAS_CLASSE'][1]['campo']="DESCRICAO";
$form['MAQUINAS_CLASSE'][1]['tipo']="texto";
$form['MAQUINAS_CLASSE'][1]['null']=0;
$form['MAQUINAS_CLASSE'][1]['tam']=30;
$form['MAQUINAS_CLASSE'][1]['max']=30;
$form['MAQUINAS_CLASSE'][1]['unico']=1;

$form['MAQUINAS_CLASSE'][1]['fecha_fieldset']=1;

// FORMULARIO CENTRO DE CUSTO
$form['CENTRO_DE_CUSTO'][0]['nome']=CENTRO_DE_CUSTO;
$form['CENTRO_DE_CUSTO'][0]['titulo']=$tdb[CENTRO_DE_CUSTO]['DESC'];
$form['CENTRO_DE_CUSTO'][0]['obs']="";
$form['CENTRO_DE_CUSTO'][0]['largura']=500;
$form['CENTRO_DE_CUSTO'][0]['altura']=200;
$form['CENTRO_DE_CUSTO'][0]['chave']="MID";

$form['CENTRO_DE_CUSTO'][1]['abre_fieldset']=$tdb[CENTRO_DE_CUSTO]['DESC'];

$form['CENTRO_DE_CUSTO'][1]['campo']="MID_EMPRESA";
$form['CENTRO_DE_CUSTO'][1]['tipo']="select_rela";
$form['CENTRO_DE_CUSTO'][1]['null']=0;
$form['CENTRO_DE_CUSTO'][1]['tam']=15;
$form['CENTRO_DE_CUSTO'][1]['max']=30;
$form['CENTRO_DE_CUSTO'][1]['js']="atualiza_area2('select_cc[".CENTRO_DE_CUSTO."][4]','parametros.php?id=filtro_ccp&campo_nome=cc[".CENTRO_DE_CUSTO."][4]&campo_id=cc[".CENTRO_DE_CUSTO."][4]&null=1&mid_emp='+this.options[this.selectedIndex].value)";

$form['CENTRO_DE_CUSTO'][2]['campo']="COD";
$form['CENTRO_DE_CUSTO'][2]['tipo']="texto";
$form['CENTRO_DE_CUSTO'][2]['null']=0;
$form['CENTRO_DE_CUSTO'][2]['tam']=15;
$form['CENTRO_DE_CUSTO'][2]['max']=30;
$form['CENTRO_DE_CUSTO'][2]['unico']=1;

$form['CENTRO_DE_CUSTO'][3]['campo']="DESCRICAO";
$form['CENTRO_DE_CUSTO'][3]['tipo']="texto";
$form['CENTRO_DE_CUSTO'][3]['null']=0;
$form['CENTRO_DE_CUSTO'][3]['tam']=30;
$form['CENTRO_DE_CUSTO'][3]['max']=120;
$form['CENTRO_DE_CUSTO'][3]['unico']=0;

$form['CENTRO_DE_CUSTO'][4]['campo']="PAI";
$form['CENTRO_DE_CUSTO'][4]['tipo']="select_rela";
$form['CENTRO_DE_CUSTO'][4]['null']=1;
$form['CENTRO_DE_CUSTO'][4]['unico']=0;
$form['CENTRO_DE_CUSTO'][4]['autofiltro']='N';
if ($act == 2) $form['CENTRO_DE_CUSTO'][4]['sql'] = "WHERE MID_EMPRESA = '".(int)VoltaValor(CENTRO_DE_CUSTO,'MID_EMPRESA','MID',(int)$_GET['foq'],$tdb[CENTRO_DE_CUSTO]['dba'])."' AND MID !=  ".(int)$_GET['foq']."";
elseif ($_POST['cc']) $form['CENTRO_DE_CUSTO'][4]['sql'] = "WHERE MID_EMPRESA = '" . (int)$_POST['cc'][CENTRO_DE_CUSTO][1] . "'";

$form['CENTRO_DE_CUSTO'][4]['fecha_fieldset']=1;

// FORMULARIO CONTADOR TIPO
$form['CONTADOR_TIPO'][0]['nome']=MAQUINAS_CONTADOR_TIPO;
$form['CONTADOR_TIPO'][0]['titulo']=$tdb[MAQUINAS_CONTADOR_TIPO]['DESC'];
$form['CONTADOR_TIPO'][0]['obs']="";
$form['CONTADOR_TIPO'][0]['largura']=500;
$form['CONTADOR_TIPO'][0]['altura']=180;
$form['CONTADOR_TIPO'][0]['chave']="MID";
$form['CONTADOR_TIPO'][1]['abre_fieldset']=$tdb[MAQUINAS_CONTADOR_TIPO]['DESC'];

$form['CONTADOR_TIPO'][1]['campo']="DESCRICAO";
$form['CONTADOR_TIPO'][1]['tipo']="texto";
$form['CONTADOR_TIPO'][1]['null']=0;
$form['CONTADOR_TIPO'][1]['tam']=30;
$form['CONTADOR_TIPO'][1]['max']=30;
$form['CONTADOR_TIPO'][1]['unico']=1;

$form['CONTADOR_TIPO'][1]['fecha_fieldset']=1;


// FORMULARIO EQUIPAMENTO FAMILIA
$form['EQUIPAMENTO_FAMILIA'][0]['nome']=EQUIPAMENTOS_FAMILIA;
$form['EQUIPAMENTO_FAMILIA'][0]['titulo']=$tdb[EQUIPAMENTOS_FAMILIA]['DESC'];
$form['EQUIPAMENTO_FAMILIA'][0]['obs']="";
$form['EQUIPAMENTO_FAMILIA'][0]['largura']=500;
$form['EQUIPAMENTO_FAMILIA'][0]['altura']=180;
$form['EQUIPAMENTO_FAMILIA'][0]['chave']="MID";

$form['EQUIPAMENTO_FAMILIA'][1]['abre_fieldset']=$tdb[EQUIPAMENTOS_FAMILIA]['DESC'];

$form['EQUIPAMENTO_FAMILIA'][1]['campo']="MID_EMPRESA";
$form['EQUIPAMENTO_FAMILIA'][1]['tipo']="select_rela";
$form['EQUIPAMENTO_FAMILIA'][1]['null']=1;
$form['EQUIPAMENTO_FAMILIA'][1]['tam']=15;
$form['EQUIPAMENTO_FAMILIA'][1]['max']=30;

if ($_autotag_famequip == 0) {
    $form['EQUIPAMENTO_FAMILIA'][2]['campo']="COD";
    $form['EQUIPAMENTO_FAMILIA'][2]['tipo']="texto";
    $form['EQUIPAMENTO_FAMILIA'][2]['null']=0;
    $form['EQUIPAMENTO_FAMILIA'][2]['tam']=15;
    $form['EQUIPAMENTO_FAMILIA'][2]['max']=30;
    $form['EQUIPAMENTO_FAMILIA'][2]['unico']=1;
}
else {
    $form['EQUIPAMENTO_FAMILIA'][2]['campo']="COD";
    $form['EQUIPAMENTO_FAMILIA'][2]['tipo']="texto_famequip_autotag";
    $form['EQUIPAMENTO_FAMILIA'][2]['null']=0;
    $form['EQUIPAMENTO_FAMILIA'][2]['unico']=0;
    $form['EQUIPAMENTO_FAMILIA'][2]['valor']=1;

}
$form['EQUIPAMENTO_FAMILIA'][3]['campo']="DESCRICAO";
$form['EQUIPAMENTO_FAMILIA'][3]['tipo']="texto";
$form['EQUIPAMENTO_FAMILIA'][3]['null']=0;
$form['EQUIPAMENTO_FAMILIA'][3]['tam']=30;
$form['EQUIPAMENTO_FAMILIA'][3]['max']=30;
$form['EQUIPAMENTO_FAMILIA'][3]['unico']=1;

$form['EQUIPAMENTO_FAMILIA'][3]['fecha_fieldset']=1;

// FORMULARIO MATERIAIS
$i_form=0;
$form['MATERIAIS'][0]['nome']=MATERIAIS;
$form['MATERIAIS'][0]['titulo']=$tdb[MATERIAIS]['DESC'];
$form['MATERIAIS'][0]['obs']="";
$form['MATERIAIS'][0]['largura']=600;
$form['MATERIAIS'][0]['altura']=360;
$form['MATERIAIS'][0]['chave']="MID";

$i_form++;
$form['MATERIAIS'][$i_form]['abre_fieldset']=$tdb[MATERIAIS]['DESC'];

$form['MATERIAIS'][$i_form]['campo']="FAMILIA";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['js']="atualiza_area2('subfamilia','parametros.php?id=lista_subfam_mat&form=MATERIAIS&pos=" . ($i_form + 1) . "&fam=' + this.value)";

$i_form++;
$form['MATERIAIS'][$i_form]['abre_div'] = "subfamilia";

$form['MATERIAIS'][$i_form]['campo']="MID_SUBFAMILIA";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=0;

$form['MATERIAIS'][$i_form]['fecha_div'] = "subfamilia";

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="COD";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=40;
$form['MATERIAIS'][$i_form]['unico']=1;


$i_form++;
$form['MATERIAIS'][$i_form]['campo']="DESCRICAO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=50;
$form['MATERIAIS'][$i_form]['unico']=0;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="COMPLEMENTO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=50;
$form['MATERIAIS'][$i_form]['unico']=0;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="LOCALIZACAO";
$form['MATERIAIS'][$i_form]['tipo']="texto";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=75;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="MID_CRITICIDADE";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="UNIDADE";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=0;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="ESTOQUE_MINIMO";
$form['MATERIAIS'][$i_form]['tipo']="texto_float";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=9;
$form['MATERIAIS'][$i_form]['max']=9;
$form['MATERIAIS'][$i_form]['menor_que']=$i_form + 1;
$form['MATERIAIS'][$i_form]['nao_negativo'] = 1;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="ESTOQUE_MAXIMO";
$form['MATERIAIS'][$i_form]['tipo']="texto_float";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=9;
$form['MATERIAIS'][$i_form]['max']=9;
$form['MATERIAIS'][$i_form]['nao_negativo'] = 1;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="FABRICANTE";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="FORNECEDOR";
$form['MATERIAIS'][$i_form]['tipo']="select_rela";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=10;
$form['MATERIAIS'][$i_form]['max']=10;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="CUSTO_UNITARIO";
$form['MATERIAIS'][$i_form]['tipo']="texto_float";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=9;
$form['MATERIAIS'][$i_form]['max']=9;
$form['MATERIAIS'][$i_form]['nao_negativo'] = 1;

$i_form++;
$form['MATERIAIS'][$i_form]['campo']="OBSERVACAO";
$form['MATERIAIS'][$i_form]['tipo']="blog";
$form['MATERIAIS'][$i_form]['null']=1;
$form['MATERIAIS'][$i_form]['tam']=40;
$form['MATERIAIS'][$i_form]['max']=10;

$form['MATERIAIS'][$i_form]['fecha_fieldset']=1;




// FORMULARIO CRITICIDADE DE MATERIAL
$form['MATERIAL_CRITICIDADE'][0]['nome']=MATERIAIS_CRITICIDADE;
$form['MATERIAL_CRITICIDADE'][0]['titulo']=$tdb[MATERIAIS_CRITICIDADE]['DESC'];
$form['MATERIAL_CRITICIDADE'][0]['obs']="";
$form['MATERIAL_CRITICIDADE'][0]['largura']=500;
$form['MATERIAL_CRITICIDADE'][0]['altura']=160;
$form['MATERIAL_CRITICIDADE'][0]['chave']="MID";

$form['MATERIAL_CRITICIDADE'][1]['abre_fieldset']=$tdb[MATERIAIS_CRITICIDADE]['DESC'];

$form['MATERIAL_CRITICIDADE'][1]['campo']="DESCRICAO";
$form['MATERIAL_CRITICIDADE'][1]['tipo']="texto";
$form['MATERIAL_CRITICIDADE'][1]['null']=0;
$form['MATERIAL_CRITICIDADE'][1]['tam']=30;
$form['MATERIAL_CRITICIDADE'][1]['max']=30;
$form['MATERIAL_CRITICIDADE'][1]['unico']=0;

$form['MATERIAL_CRITICIDADE'][1]['fecha_fieldset']=1;

// FORMULARIO FAMILIA DE MATERIAL
$form['MATERIAL_FAMILIA'][0]['nome']=MATERIAIS_FAMILIA;
$form['MATERIAL_FAMILIA'][0]['titulo']=$tdb[MATERIAIS_FAMILIA]['DESC'];
$form['MATERIAL_FAMILIA'][0]['obs']="";
$form['MATERIAL_FAMILIA'][0]['largura']=500;
$form['MATERIAL_FAMILIA'][0]['altura']=160;
$form['MATERIAL_FAMILIA'][0]['chave']="MID";

$form['MATERIAL_FAMILIA'][1]['abre_fieldset']=$tdb[MATERIAIS_FAMILIA]['DESC'];

$form['MATERIAL_FAMILIA'][1]['campo']="COD";
$form['MATERIAL_FAMILIA'][1]['tipo']="texto";
$form['MATERIAL_FAMILIA'][1]['null']=0;
$form['MATERIAL_FAMILIA'][1]['tam']=15;
$form['MATERIAL_FAMILIA'][1]['max']=25;
$form['MATERIAL_FAMILIA'][1]['unico']=1;
$form['MATERIAL_FAMILIA'][1]['valor']=0;

$form['MATERIAL_FAMILIA'][2]['campo']="DESCRICAO";
$form['MATERIAL_FAMILIA'][2]['tipo']="texto";
$form['MATERIAL_FAMILIA'][2]['null']=0;
$form['MATERIAL_FAMILIA'][2]['tam']=30;
$form['MATERIAL_FAMILIA'][2]['max']=30;
$form['MATERIAL_FAMILIA'][2]['unico']=0;

$form['MATERIAL_FAMILIA'][2]['fecha_fieldset']=1;

// FORMULARIO FAMILIA DE MATERIAL
$form['MATERIAL_SUBFAMILIA'][0]['nome']=MATERIAIS_SUBFAMILIA;
$form['MATERIAL_SUBFAMILIA'][0]['titulo']=$tdb[MATERIAIS_SUBFAMILIA]['DESC'];
$form['MATERIAL_SUBFAMILIA'][0]['obs']="";
$form['MATERIAL_SUBFAMILIA'][0]['largura']=500;
$form['MATERIAL_SUBFAMILIA'][0]['altura']=180;
$form['MATERIAL_SUBFAMILIA'][0]['chave']="MID";

$form['MATERIAL_SUBFAMILIA'][1]['abre_fieldset']=$tdb[MATERIAIS_SUBFAMILIA]['DESC'];

$form['MATERIAL_SUBFAMILIA'][1]['campo']="MID_FAMILIA";
$form['MATERIAL_SUBFAMILIA'][1]['tipo']="select_rela";
$form['MATERIAL_SUBFAMILIA'][1]['null']=0;


$form['MATERIAL_SUBFAMILIA'][2]['campo']="COD"; ##### a ordem deste campo aparece em parametros.php
$form['MATERIAL_SUBFAMILIA'][2]['tipo']="texto";
$form['MATERIAL_SUBFAMILIA'][2]['null']=0;
$form['MATERIAL_SUBFAMILIA'][2]['tam']=15;
$form['MATERIAL_SUBFAMILIA'][2]['max']=25;
$form['MATERIAL_SUBFAMILIA'][2]['unico']=1;

$form['MATERIAL_SUBFAMILIA'][3]['campo']="DESCRICAO";
$form['MATERIAL_SUBFAMILIA'][3]['tipo']="texto";
$form['MATERIAL_SUBFAMILIA'][3]['null']=0;
$form['MATERIAL_SUBFAMILIA'][3]['tam']=30;
$form['MATERIAL_SUBFAMILIA'][3]['max']=30;
$form['MATERIAL_SUBFAMILIA'][3]['unico']=0;

$form['MATERIAL_SUBFAMILIA'][3]['fecha_fieldset']=1;


// FORMULARIO UNIDADE DE MATERAIS
$form['MATERIAL_UNIDADE'][0]['nome']=MATERIAIS_UNIDADE;
$form['MATERIAL_UNIDADE'][0]['titulo']=$tdb[MATERIAIS_UNIDADE]['DESC'];
$form['MATERIAL_UNIDADE'][0]['obs']="";
$form['MATERIAL_UNIDADE'][0]['largura']=500;
$form['MATERIAL_UNIDADE'][0]['altura']=180;
$form['MATERIAL_UNIDADE'][0]['chave']="MID";

$form['MATERIAL_UNIDADE'][1]['abre_fieldset']=$tdb[MATERIAIS_UNIDADE]['DESC'];

$form['MATERIAL_UNIDADE'][1]['campo']="COD";
$form['MATERIAL_UNIDADE'][1]['tipo']="texto";
$form['MATERIAL_UNIDADE'][1]['null']=0;
$form['MATERIAL_UNIDADE'][1]['tam']=15;
$form['MATERIAL_UNIDADE'][1]['max']=50;
$form['MATERIAL_UNIDADE'][1]['unico']=1;

$form['MATERIAL_UNIDADE'][2]['campo']="DESCRICAO";
$form['MATERIAL_UNIDADE'][2]['tipo']="texto";
$form['MATERIAL_UNIDADE'][2]['null']=0;
$form['MATERIAL_UNIDADE'][2]['tam']=30;
$form['MATERIAL_UNIDADE'][2]['max']=50;
$form['MATERIAL_UNIDADE'][2]['unico']=1;

$form['MATERIAL_UNIDADE'][2]['fecha_fieldset']=1;


// CLASSE DE MATERIAIS
$iform = 0;
$form['MATERIAIS_CLASSE'][$iform]['nome']=MATERIAIS_CLASSE;
$form['MATERIAIS_CLASSE'][$iform]['titulo']=$tdb[MATERIAIS_CLASSE]['DESC'];
$form['MATERIAIS_CLASSE'][$iform]['obs']="";
$form['MATERIAIS_CLASSE'][$iform]['largura']=500;
$form['MATERIAIS_CLASSE'][$iform]['altura']=160;
$form['MATERIAIS_CLASSE'][$iform]['chave']="MID";

$iform ++;
$form['MATERIAIS_CLASSE'][$iform]['abre_fieldset']=$tdb[MATERIAIS_CLASSE]['DESC'];

$form['MATERIAIS_CLASSE'][$iform]['campo']="DESCRICAO";
$form['MATERIAIS_CLASSE'][$iform]['tipo']="texto";
$form['MATERIAIS_CLASSE'][$iform]['null']=0;
$form['MATERIAIS_CLASSE'][$iform]['tam']=50;
$form['MATERIAIS_CLASSE'][$iform]['max']=250;
$form['MATERIAIS_CLASSE'][$iform]['unico']=1;

$form['MATERIAIS_CLASSE'][$iform]['fecha_fieldset']=1;


// FORMULARIO PLANO PADRAO
$form['PLANOPADRAO'][0]['nome']=PLANO_PADRAO;
$form['PLANOPADRAO'][0]['titulo']=$tdb[PLANO_PADRAO]['DESC'];
$form['PLANOPADRAO'][0]['obs']="";
$form['PLANOPADRAO'][0]['dir']=1;
$form['PLANOPADRAO'][0]['largura']=630;
$form['PLANOPADRAO'][0]['altura']=400;
$form['PLANOPADRAO'][0]['chave']="MID";

$tipo = (int) VoltaValor(PLANO_PADRAO, 'TIPO', 'MID', (int)$_GET['foq']);


//Regra para n�o editar o objeto de manute��o e nem a aplica��o do plano.
if( $act == 2 AND $tipo == 1 ){
	 
	$form['PLANOPADRAO'][1]['abre_fieldset']=$ling['plano_info'];

    $form['PLANOPADRAO'][1]['campo']="DESCRICAO";
    $form['PLANOPADRAO'][1]['tipo']="texto";
    $form['PLANOPADRAO'][1]['null']=0;
    $form['PLANOPADRAO'][1]['tam']=60;
    $form['PLANOPADRAO'][1]['max']=150;
    $form['PLANOPADRAO'][1]['unico']=0;

    $form['PLANOPADRAO'][2]['campo']="OBSERVACAO";
    $form['PLANOPADRAO'][2]['tipo']="blog";
    $form['PLANOPADRAO'][2]['null']=1;
    $form['PLANOPADRAO'][2]['tam']=60;
    $form['PLANOPADRAO'][2]['max']=10;
    $form['PLANOPADRAO'][2]['unico']=0;

    $form['PLANOPADRAO'][2]['fecha_fieldset']=1;		
}
elseif (($act == 1) or ($act == 3) or (VoltaValor(PROGRAMACAO, 'MID', array('MID_PLANO', 'TIPO', 'STATUS'), array((int)$_GET['foq'], 1, 1)) == 0)) {
    $form['PLANOPADRAO'][1]['abre_fieldset']=$ling['plano_aplicacao'];

    $form['PLANOPADRAO'][1]['campo']="MID_EMPRESA";
    $form['PLANOPADRAO'][1]['tipo']="select_rela";
    $form['PLANOPADRAO'][1]['null']=1;
    $form['PLANOPADRAO'][1]['js']="document.forms[0].submit()";


    $form['PLANOPADRAO'][2]['campo']="TIPO";
    $form['PLANOPADRAO'][2]['tipo']="select_rela";
    $form['PLANOPADRAO'][2]['null']=0;
    $form['PLANOPADRAO'][2]['js']="document.forms[0].submit()";

    $cc = $_POST['cc'];
    if (($cc[PLANO_PADRAO][2] == 1) or ($act == 2 and $cc[PLANO_PADRAO][2] == 0 and $tipo == 1)) {
        $form['PLANOPADRAO'][3]['campo']="MAQUINA_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="recb_valor";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;
        $form['PLANOPADRAO'][3]['valor']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="select_rela";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }

    elseif (($cc[PLANO_PADRAO][2] == 2) or ($act == 2 and $cc[PLANO_PADRAO][2] == 0 and $tipo == 2)) {
        $form['PLANOPADRAO'][3]['campo']="MAQUINA_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="select_rela";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="recb_valor";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['valor']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }

    elseif (($cc[PLANO_PADRAO][2] == 4) or ($act == 2 and $cc[PLANO_PADRAO][2] == 0 and $tipo == 4)) {
        $form['PLANOPADRAO'][3]['campo']="EQUIPAMENTO_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="select_rela";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="recb_valor";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['valor']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }

    else {
        $form['PLANOPADRAO'][3]['campo']="EQUIPAMENTO_FAMILIA";
        $form['PLANOPADRAO'][3]['tipo']="recb_valor";
        $form['PLANOPADRAO'][3]['null']=0;
        $form['PLANOPADRAO'][3]['unico']=0;
        $form['PLANOPADRAO'][3]['valor']=0;

        $form['PLANOPADRAO'][4]['campo']="MID_MAQUINA";
        $form['PLANOPADRAO'][4]['tipo']="recb_valor";
        $form['PLANOPADRAO'][4]['null']=0;
        $form['PLANOPADRAO'][4]['valor']=0;
        $form['PLANOPADRAO'][4]['unico']=0;
    }


    $form['PLANOPADRAO'][4]['fecha_fieldset']=1;

    $form['PLANOPADRAO'][5]['abre_fieldset']=$ling['plano_info'];

    $form['PLANOPADRAO'][5]['campo']="DESCRICAO";
    $form['PLANOPADRAO'][5]['tipo']="texto";
    $form['PLANOPADRAO'][5]['null']=0;
    $form['PLANOPADRAO'][5]['tam']=60;
    $form['PLANOPADRAO'][5]['max']=150;
    $form['PLANOPADRAO'][5]['unico']=0;

    $form['PLANOPADRAO'][6]['campo']="OBSERVACAO";
    $form['PLANOPADRAO'][6]['tipo']="blog";
    $form['PLANOPADRAO'][6]['null']=1;
    $form['PLANOPADRAO'][6]['tam']=60;
    $form['PLANOPADRAO'][6]['max']=10;
    $form['PLANOPADRAO'][6]['unico']=0;

    $form['PLANOPADRAO'][6]['fecha_fieldset']=1;
}
else {
    $form['PLANOPADRAO'][1]['abre_fieldset']=$ling['plano_info'];

    $form['PLANOPADRAO'][1]['campo']="DESCRICAO";
    $form['PLANOPADRAO'][1]['tipo']="texto";
    $form['PLANOPADRAO'][1]['null']=0;
    $form['PLANOPADRAO'][1]['tam']=60;
    $form['PLANOPADRAO'][1]['max']=150;
    $form['PLANOPADRAO'][1]['unico']=0;

    $form['PLANOPADRAO'][2]['campo']="OBSERVACAO";
    $form['PLANOPADRAO'][2]['tipo']="blog";
    $form['PLANOPADRAO'][2]['null']=1;
    $form['PLANOPADRAO'][2]['tam']=60;
    $form['PLANOPADRAO'][2]['max']=10;
    $form['PLANOPADRAO'][2]['unico']=0;

    $form['PLANOPADRAO'][2]['fecha_fieldset']=1;
}


// FORMULARIO ATIVIDADES p/ �nico OBJ
// Filtro pela empresa do PLANO
$filtro_emp = "";
if ((int)VoltaValor(PLANO_PADRAO,"MID_EMPRESA","MID",(int)$form_recb_valor) != 0) {
	$filtro_emp = "WHERE (MID_EMPRESA=0 OR MID_EMPRESA=" . (int)VoltaValor(PLANO_PADRAO,"MID_EMPRESA","MID",(int)$form_recb_valor) . ")";
}
$form['ATIVIDADE'][0]['nome']=ATIVIDADES;
$form['ATIVIDADE'][0]['titulo']=$tdb[ATIVIDADES]['DESC'];
$form['ATIVIDADE'][0]['obs']="";
$form['ATIVIDADE'][0]['largura']=550;
$form['ATIVIDADE'][0]['altura']=420;
$form['ATIVIDADE'][0]['chave']="MID";

$form['ATIVIDADE'][1]['abre_fieldset']=$ling['plano_peri1'];

$form['ATIVIDADE'][1]['campo']="PERIODICIDADE";
$form['ATIVIDADE'][1]['tipo']="select_rela";
$form['ATIVIDADE'][1]['null']=0;
$form['ATIVIDADE'][1]['sql']="WHERE MID!=4";

$form['ATIVIDADE'][2]['campo']="FREQUENCIA";
$form['ATIVIDADE'][2]['tipo']="texto_int";
$form['ATIVIDADE'][2]['null']=0;
$form['ATIVIDADE'][2]['tam']=4;
$form['ATIVIDADE'][2]['max']=4;
$form['ATIVIDADE'][2]['nao_negativo']=1;
$form['ATIVIDADE'][2]['nao_zero']=1;

$form['ATIVIDADE'][3]['campo']="CONTADOR";
$form['ATIVIDADE'][3]['tipo']="recb_valor";
$form['ATIVIDADE'][3]['null']=1;
$form['ATIVIDADE'][3]['valor']=0;

$form['ATIVIDADE'][4]['campo']="DISPARO";
$form['ATIVIDADE'][4]['tipo']="recb_valor";
$form['ATIVIDADE'][4]['null']=1;
$form['ATIVIDADE'][4]['valor']=0;

$form['ATIVIDADE'][4]['fecha_fieldset']=1;

$form['ATIVIDADE'][5]['abre_fieldset']=$tdb[ATIVIDADES]['DESC'];

$form['ATIVIDADE'][5]['campo']="NUMERO";
$form['ATIVIDADE'][5]['tipo']="texto";
$form['ATIVIDADE'][5]['null']=0;
$form['ATIVIDADE'][5]['tam']=2;
$form['ATIVIDADE'][5]['max']=3;
$form['ATIVIDADE'][5]['unico']=0;

$form['ATIVIDADE'][6]['campo']="MID_CONJUNTO";
$form['ATIVIDADE'][6]['tipo']="recb_valor";
$form['ATIVIDADE'][6]['null']=1;
$form['ATIVIDADE'][6]['unico']=0;
$form['ATIVIDADE'][6]['valor']=0;

$form['ATIVIDADE'][7]['campo']="PARTE";
$form['ATIVIDADE'][7]['tipo']="texto";
$form['ATIVIDADE'][7]['null']=1;
$form['ATIVIDADE'][7]['tam']=40;
$form['ATIVIDADE'][7]['max']=150;
$form['ATIVIDADE'][7]['unico']=0;

$form['ATIVIDADE'][8]['campo']="TAREFA";
$form['ATIVIDADE'][8]['tipo']="texto";
$form['ATIVIDADE'][8]['null']=0;
$form['ATIVIDADE'][8]['tam']=40;
$form['ATIVIDADE'][8]['max']=150;
$form['ATIVIDADE'][8]['unico']=0;

$form['ATIVIDADE'][9]['campo']="INSTRUCAO_DE_TRABALHO";
$form['ATIVIDADE'][9]['tipo']="texto";
$form['ATIVIDADE'][9]['null']=1;
$form['ATIVIDADE'][9]['tam']=40;
$form['ATIVIDADE'][9]['max']=150;
$form['ATIVIDADE'][9]['unico']=0;

$form['ATIVIDADE'][10]['campo']="OBSERVACAO";
$form['ATIVIDADE'][10]['tipo']="texto";
$form['ATIVIDADE'][10]['tam']=40;
$form['ATIVIDADE'][10]['max']=150;
$form['ATIVIDADE'][10]['null']=1;
$form['ATIVIDADE'][10]['unico']=0;

$form['ATIVIDADE'][11]['campo']="MAQUINA_PARADA";
$form['ATIVIDADE'][11]['tipo']="select_rela";
$form['ATIVIDADE'][11]['null']=0;
$form['ATIVIDADE'][11]['unico']=0;

$form['ATIVIDADE'][11]['fecha_fieldset']=1;

$form['ATIVIDADE'][12]['abre_fieldset']=$ling['plano_prev_mo'];

$form['ATIVIDADE'][12]['campo']="ESPECIALIDADE";
$form['ATIVIDADE'][12]['tipo']="select_rela";
$form['ATIVIDADE'][12]['null']=0;
$form['ATIVIDADE'][12]['unico']=0;
$form['ATIVIDADE'][12]['sql']=$filtro_emp;

$form['ATIVIDADE'][13]['campo']="QUANTIDADE_MO";
$form['ATIVIDADE'][13]['tipo']="texto_int";
$form['ATIVIDADE'][13]['null']=0;
$form['ATIVIDADE'][13]['tam']=5;
$form['ATIVIDADE'][13]['nao_negativo']=1;
$form['ATIVIDADE'][13]['nao_zero']=1;

$form['ATIVIDADE'][14]['campo']="TEMPO_PREVISTO";
$form['ATIVIDADE'][14]['tipo']="texto_float";
$form['ATIVIDADE'][14]['null']=0;
$form['ATIVIDADE'][14]['tam']=5;
$form['ATIVIDADE'][14]['nao_negativo']=1;
$form['ATIVIDADE'][14]['nao_zero']=1;

$form['ATIVIDADE'][15]['campo']="MID_PLANO_PADRAO";
$form['ATIVIDADE'][15]['tipo']="recb_valor";
$form['ATIVIDADE'][15]['valor']=$form_recb_valor;
$form['ATIVIDADE'][15]['null']=0;
$form['ATIVIDADE'][15]['unico']=0;

$form['ATIVIDADE'][15]['fecha_fieldset']=1;

$form['ATIVIDADE'][16]['abre_fieldset']=$ling['plano_prev_mat'];

$form['ATIVIDADE'][16]['campo']="MID_MATERIAL";
$form['ATIVIDADE'][16]['tipo']="select_material";
$form['ATIVIDADE'][16]['null']=1;
$form['ATIVIDADE'][16]['tam']=0;
$form['ATIVIDADE'][16]['max']=0;
$form['ATIVIDADE'][16]['unico']=0;

$form['ATIVIDADE'][17]['campo']="QUANTIDADE";
$form['ATIVIDADE'][17]['tipo']="texto_unidade";
$form['ATIVIDADE'][17]['null']=1;
$form['ATIVIDADE'][17]['tam']=5;
$form['ATIVIDADE'][17]['max']=5;
$form['ATIVIDADE'][17]['nao_negativo']=1;
$form['ATIVIDADE'][17]['nao_zero']=1;
$form['ATIVIDADE'][17]['campo_material']=16;

$form['ATIVIDADE'][17]['fecha_fieldset']=1;


// Formul�rio de atividades p/ qualquer OBJ
// Filtro pela empresa do PLANO
$filtro_emp = "";
if ((int)VoltaValor(PLANO_PADRAO,"MID_EMPRESA","MID",(int)$form_recb_valor) != 0) {
	$filtro_emp = "WHERE (MID_EMPRESA=0 OR MID_EMPRESA=" . (int)VoltaValor(PLANO_PADRAO,"MID_EMPRESA","MID",(int)$form_recb_valor) . ")";
}


$form['ATIVIDADE2'][0]['nome']=ATIVIDADES;
$form['ATIVIDADE2'][0]['titulo']=$tdb[ATIVIDADES]['DESC'];
$form['ATIVIDADE2'][0]['obs']="";
$form['ATIVIDADE2'][0]['largura']=550;
$form['ATIVIDADE2'][0]['altura']=420;
$form['ATIVIDADE2'][0]['chave']="MID";

$form['ATIVIDADE2'][1]['abre_fieldset']=$ling['plano_peri'];

$form['ATIVIDADE2'][1]['campo']="PERIODICIDADE";
$form['ATIVIDADE2'][1]['tipo']="select_rela";
$form['ATIVIDADE2'][1]['null']=1;
$form['ATIVIDADE2'][1]['js']="this.form.submit()";

$cc=$_POST['cc'];
if ((is_array($cc) && $cc[ATIVIDADES][1] != 4) || (! is_array($cc) && VoltaValor(ATIVIDADES, "PERIODICIDADE", "MID", (int)$_GET['foq'], 0) != 4)) {
    $form['ATIVIDADE2'][2]['campo']="FREQUENCIA";
    $form['ATIVIDADE2'][2]['tipo']="texto_int";
    $form['ATIVIDADE2'][2]['null']=0;
    $form['ATIVIDADE2'][2]['tam']=4;
    $form['ATIVIDADE2'][2]['max']=4;
	$form['ATIVIDADE2'][2]['nao_negativo']=1;
    $form['ATIVIDADE2'][2]['nao_zero']=1;

    $form['ATIVIDADE2'][3]['campo']="CONTADOR";
    $form['ATIVIDADE2'][3]['tipo']="recb_valor";
    $form['ATIVIDADE2'][3]['null']=1;
    $form['ATIVIDADE2'][3]['valor']=0;
    $form['ATIVIDADE2'][3]['unico']=0;

    $form['ATIVIDADE2'][4]['campo']="DISPARO";
    $form['ATIVIDADE2'][4]['tipo']="recb_valor";
    $form['ATIVIDADE2'][4]['null']=1;
    $form['ATIVIDADE2'][4]['valor']=0;
    $form['ATIVIDADE2'][4]['unico']=0;
}
else {
    $form['ATIVIDADE2'][2]['campo']="FREQUENCIA";
    $form['ATIVIDADE2'][2]['tipo']="recb_valor";
    $form['ATIVIDADE2'][2]['null']=1;
    $form['ATIVIDADE2'][2]['valor']=0;
    $form['ATIVIDADE2'][2]['unico']=0;

    $form['ATIVIDADE2'][3]['campo']="CONTADOR";
    $form['ATIVIDADE2'][3]['tipo']="select_rela";
    $form['ATIVIDADE2'][3]['null']=0;
    $form['ATIVIDADE2'][3]['unico']=0;
    $form['ATIVIDADE2'][3]['sql']= "WHERE MID_MAQUINA=" . (int)VoltaValor(PLANO_PADRAO,"MID_MAQUINA","MID",(int)$form_recb_valor,0);

    $form['ATIVIDADE2'][4]['campo']="DISPARO";
    $form['ATIVIDADE2'][4]['tipo']="texto_int";
    $form['ATIVIDADE2'][4]['tam']=7;
    $form['ATIVIDADE2'][4]['max']=13;
    $form['ATIVIDADE2'][4]['null']=0;
    $form['ATIVIDADE2'][4]['nao_negativo']=1;
    $form['ATIVIDADE2'][4]['nao_zero']=1;
}
$form['ATIVIDADE2'][4]['fecha_fieldset']=1;

$form['ATIVIDADE2'][5]['abre_fieldset']=$tdb[ATIVIDADES]['DESC'];

$form['ATIVIDADE2'][5]['campo']="NUMERO";
$form['ATIVIDADE2'][5]['tipo']="texto";
$form['ATIVIDADE2'][5]['null']=0;
$form['ATIVIDADE2'][5]['tam']=2;
$form['ATIVIDADE2'][5]['max']=3;
$form['ATIVIDADE2'][5]['unico']=0;

$form['ATIVIDADE2'][6]['campo']="MID_CONJUNTO";
$form['ATIVIDADE2'][6]['tipo']="select_conjunto_filtrado";
$form['ATIVIDADE2'][6]['null']=1;
$form['ATIVIDADE2'][6]['unico']=0;
$form['ATIVIDADE2'][6]['valor']=(int)VoltaValor(PLANO_PADRAO,"MID_MAQUINA","MID",(int)$form_recb_valor,0);

$form['ATIVIDADE2'][7]['campo']="PARTE";
$form['ATIVIDADE2'][7]['tipo']="texto";
$form['ATIVIDADE2'][7]['null']=1;
$form['ATIVIDADE2'][7]['tam']=40;
$form['ATIVIDADE2'][7]['max']=150;
$form['ATIVIDADE2'][7]['unico']=0;

$form['ATIVIDADE2'][8]['campo']="TAREFA";
$form['ATIVIDADE2'][8]['tipo']="texto";
$form['ATIVIDADE2'][8]['null']=0;
$form['ATIVIDADE2'][8]['tam']=40;
$form['ATIVIDADE2'][8]['max']=150;
$form['ATIVIDADE2'][8]['unico']=0;

$form['ATIVIDADE2'][9]['campo']="INSTRUCAO_DE_TRABALHO";
$form['ATIVIDADE2'][9]['tipo']="texto";
$form['ATIVIDADE2'][9]['null']=1;
$form['ATIVIDADE2'][9]['tam']=40;
$form['ATIVIDADE2'][9]['max']=150;
$form['ATIVIDADE2'][9]['unico']=0;

$form['ATIVIDADE2'][10]['campo']="OBSERVACAO";
$form['ATIVIDADE2'][10]['tipo']="texto";
$form['ATIVIDADE2'][10]['tam']=40;
$form['ATIVIDADE2'][10]['max']=150;
$form['ATIVIDADE2'][10]['null']=1;
$form['ATIVIDADE2'][10]['unico']=0;

$form['ATIVIDADE2'][11]['campo']="MAQUINA_PARADA";
$form['ATIVIDADE2'][11]['tipo']="select_rela";
$form['ATIVIDADE2'][11]['null']=0;
$form['ATIVIDADE2'][11]['unico']=0;

$form['ATIVIDADE2'][11]['fecha_fieldset']=1;

$form['ATIVIDADE2'][12]['abre_fieldset']=$ling['plano_prev_mo'];

$form['ATIVIDADE2'][12]['campo']="ESPECIALIDADE";
$form['ATIVIDADE2'][12]['tipo']="select_rela";
$form['ATIVIDADE2'][12]['null']=0;
$form['ATIVIDADE2'][12]['unico']=0;
$form['ATIVIDADE2'][12]['sql']=$filtro_emp;

$form['ATIVIDADE2'][13]['campo']="QUANTIDADE_MO";
$form['ATIVIDADE2'][13]['tipo']="texto_int";
$form['ATIVIDADE2'][13]['null']=0;
$form['ATIVIDADE2'][13]['tam']=5;
$form['ATIVIDADE2'][13]['nao_negativo']=1;
$form['ATIVIDADE2'][13]['nao_zero']=1;

$form['ATIVIDADE2'][14]['campo']="TEMPO_PREVISTO";
$form['ATIVIDADE2'][14]['tipo']="texto_float";
$form['ATIVIDADE2'][14]['null']=0;
$form['ATIVIDADE2'][14]['tam']=5;
$form['ATIVIDADE2'][14]['nao_negativo']=1;
$form['ATIVIDADE2'][14]['nao_zero']=1;

$form['ATIVIDADE2'][15]['campo']="MID_PLANO_PADRAO";
$form['ATIVIDADE2'][15]['tipo']="recb_valor";
$form['ATIVIDADE2'][15]['valor']=$form_recb_valor;
$form['ATIVIDADE2'][15]['null']=0;

$form['ATIVIDADE2'][15]['fecha_fieldset']=1;

$form['ATIVIDADE2'][16]['abre_fieldset']=$ling['plano_prev_mat'];

$form['ATIVIDADE2'][16]['campo']="MID_MATERIAL";
$form['ATIVIDADE2'][16]['tipo']="select_material";
$form['ATIVIDADE2'][16]['null']=1;
$form['ATIVIDADE2'][16]['tam']=0;
$form['ATIVIDADE2'][16]['max']=0;
$form['ATIVIDADE2'][16]['unico']=0;

$form['ATIVIDADE2'][17]['campo']="QUANTIDADE";
$form['ATIVIDADE2'][17]['tipo']="texto_unidade";
$form['ATIVIDADE2'][17]['null']=1;
$form['ATIVIDADE2'][17]['tam']=5;
$form['ATIVIDADE2'][17]['max']=5;
$form['ATIVIDADE2'][17]['nao_negativo']=1;
$form['ATIVIDADE2'][17]['nao_zero']=1;
$form['ATIVIDADE2'][17]['campo_material']=16;

$form['ATIVIDADE2'][17]['fecha_fieldset']=1;



// FORMULARIO LUBRIFICACAO
$form['PLANO_ROTA'][0]['nome']=PLANO_ROTAS;
$form['PLANO_ROTA'][0]['titulo']=$tdb[PLANO_ROTAS]['DESC'];
$form['PLANO_ROTA'][0]['obs']="";
$form['PLANO_ROTA'][0]['dir']=1;
$form['PLANO_ROTA'][0]['largura']=530;
$form['PLANO_ROTA'][0]['altura']=250;
$form['PLANO_ROTA'][0]['chave']="MID";

$form['PLANO_ROTA'][1]['abre_fieldset']=$tdb[PLANO_ROTAS]['DESC'];

$form['PLANO_ROTA'][1]['campo']="MID_EMPRESA";
$form['PLANO_ROTA'][1]['tipo']="select_rela";
$form['PLANO_ROTA'][1]['null']=0;
$form['PLANO_ROTA'][1]['tam']=40;

$form['PLANO_ROTA'][2]['campo']="DESCRICAO";
$form['PLANO_ROTA'][2]['tipo']="texto";
$form['PLANO_ROTA'][2]['null']=0;
$form['PLANO_ROTA'][2]['tam']=40;
$form['PLANO_ROTA'][2]['max']=150;
$form['PLANO_ROTA'][2]['unico']=1;

$form['PLANO_ROTA'][3]['campo']="OBSERVACAO";
$form['PLANO_ROTA'][3]['tipo']="blog";
$form['PLANO_ROTA'][3]['null']=1;
$form['PLANO_ROTA'][3]['tam']=40;
$form['PLANO_ROTA'][3]['max']=4;
$form['PLANO_ROTA'][3]['unico']=0;

$form['PLANO_ROTA'][3]['fecha_fieldset']=1;

// FORMULARIO LINK DA LUBRIFICACAO NO PLANO
$iform = 0;
$form['LINK_LUBRIFICACAO'][$iform]['nome']=LINK_ROTAS;
$form['LINK_LUBRIFICACAO'][$iform]['titulo']=$tdb[LINK_ROTAS]['DESC'];
$form['LINK_LUBRIFICACAO'][$iform]['obs']="";
$form['LINK_LUBRIFICACAO'][$iform]['largura']=670;
$form['LINK_LUBRIFICACAO'][$iform]['altura']=380;
$form['LINK_LUBRIFICACAO'][$iform]['chave']="MID";

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$tdb[ATIVIDADES]['DESC'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="NUMERO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=4;
$form['LINK_LUBRIFICACAO'][$iform]['max']=4;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="TAREFA";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=50;
$form['LINK_LUBRIFICACAO'][$iform]['max']=150;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['prog_material'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_MATERIAL";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_material";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=0;
$form['LINK_LUBRIFICACAO'][$iform]['max']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="QUANTIDADE";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_unidade";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=5;
$form['LINK_LUBRIFICACAO'][$iform]['max']=5;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;
$form['LINK_LUBRIFICACAO'][$iform]['nao_negativo']=1;
$form['LINK_LUBRIFICACAO'][$iform]['nao_zero']=1;
$form['LINK_LUBRIFICACAO'][$iform]['campo_material']=($iform - 1);

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['plano_peri1'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="PERIODICIDADE";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_rela";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;
$form['LINK_LUBRIFICACAO'][$iform]['sql']="WHERE MID != 4";

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="FREQUENCIA";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_int";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=4;
$form['LINK_LUBRIFICACAO'][$iform]['max']=4;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;
$form['LINK_LUBRIFICACAO'][$iform]['nao_negativo']=1;
$form['LINK_LUBRIFICACAO'][$iform]['nao_zero']=1;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['plano_prev_mo'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="ESPECIALIDADE";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_rela";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="QUANTIDADE_MO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_int";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=5;
$form['LINK_LUBRIFICACAO'][$iform]['nao_negativo']=1;
$form['LINK_LUBRIFICACAO'][$iform]['nao_zero']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="TEMPO_PREVISTO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="texto_float";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['tam']=5;
$form['LINK_LUBRIFICACAO'][$iform]['nao_negativo']=1;
$form['LINK_LUBRIFICACAO'][$iform]['nao_zero']=1;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['abre_fieldset']=$ling['plano_ponto'];

$form['LINK_LUBRIFICACAO'][$iform]['campo']="TIPO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="recb_valor";
$form['LINK_LUBRIFICACAO'][$iform]['null']=1;
$form['LINK_LUBRIFICACAO'][$iform]['valor']=1;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_MAQUINA";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_maquina";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_PONTO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="select_ponto_lub";
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_LUBRIFICACAO'][$iform]['campo']="MID_PLANO";
$form['LINK_LUBRIFICACAO'][$iform]['tipo']="recb_valor";
$form['LINK_LUBRIFICACAO'][$iform]['valor']=$form_recb_valor;
$form['LINK_LUBRIFICACAO'][$iform]['null']=0;
$form['LINK_LUBRIFICACAO'][$iform]['unico']=0;

$form['LINK_LUBRIFICACAO'][$iform]['fecha_fieldset']=1;




// FORMULARIO LINK DA INSPECAO NO PLANO
$iform = 0;
$form['LINK_INSPECAO'][$iform]['nome']=LINK_ROTAS;
$form['LINK_INSPECAO'][$iform]['titulo']=$tdb[LINK_ROTAS]['DESC'];
$form['LINK_INSPECAO'][$iform]['obs']="";
$form['LINK_INSPECAO'][$iform]['largura']=670;
$form['LINK_INSPECAO'][$iform]['altura']=380;
$form['LINK_INSPECAO'][$iform]['chave']="MID";

$iform++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['atividade_a'];

$form['LINK_INSPECAO'][$iform]['campo']="NUMERO";
$form['LINK_INSPECAO'][$iform]['tipo']="texto";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=4;
$form['LINK_INSPECAO'][$iform]['max']=4;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform++;
$form['LINK_INSPECAO'][$iform]['campo']="TAREFA";
$form['LINK_INSPECAO'][$iform]['tipo']="texto";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=50;
$form['LINK_INSPECAO'][$iform]['max']=150;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['periodicidade_'];

$form['LINK_INSPECAO'][$iform]['campo']="PERIODICIDADE";
$form['LINK_INSPECAO'][$iform]['tipo']="select_rela";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['sql']="WHERE MID != 4";

$iform++;
$form['LINK_INSPECAO'][$iform]['campo']="FREQUENCIA";
$form['LINK_INSPECAO'][$iform]['tipo']="texto_int";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=4;
$form['LINK_INSPECAO'][$iform]['max']=4;
$form['LINK_INSPECAO'][$iform]['nao_negativo']=1;
$form['LINK_INSPECAO'][$iform]['nao_zero']=1;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['plano_prev_mo'];

$form['LINK_INSPECAO'][$iform]['campo']="ESPECIALIDADE";
$form['LINK_INSPECAO'][$iform]['tipo']="select_rela";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="QUANTIDADE_MO";
$form['LINK_INSPECAO'][$iform]['tipo']="texto_int";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=5;
$form['LINK_INSPECAO'][$iform]['nao_negativo']=1;
$form['LINK_INSPECAO'][$iform]['nao_zero']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="TEMPO_PREVISTO";
$form['LINK_INSPECAO'][$iform]['tipo']="texto_float";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['tam']=5;
$form['LINK_INSPECAO'][$iform]['nao_negativo']=1;
$form['LINK_INSPECAO'][$iform]['nao_zero']=1;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;

$iform ++;
$form['LINK_INSPECAO'][$iform]['abre_fieldset']=$ling['plano_ponto'];

$form['LINK_INSPECAO'][$iform]['campo']="TIPO";
$form['LINK_INSPECAO'][$iform]['tipo']="recb_valor";
$form['LINK_INSPECAO'][$iform]['null']=1;
$form['LINK_INSPECAO'][$iform]['valor']=2;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="MID_MAQUINA";
$form['LINK_INSPECAO'][$iform]['tipo']="select_maquina";
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="MID_CONJUNTO";
$form['LINK_INSPECAO'][$iform]['tipo']="select_conjunto_filtrado";
$form['LINK_INSPECAO'][$iform]['null']=1;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$iform ++;
$form['LINK_INSPECAO'][$iform]['campo']="MID_PLANO";
$form['LINK_INSPECAO'][$iform]['tipo']="recb_valor";
$form['LINK_INSPECAO'][$iform]['valor']=$form_recb_valor;
$form['LINK_INSPECAO'][$iform]['null']=0;
$form['LINK_INSPECAO'][$iform]['unico']=0;

$form['LINK_INSPECAO'][$iform]['fecha_fieldset']=1;



// FORMULARIO PROGRAMA&Ccedil;&Atilde;O GERAL
$form['PROGRAMACAO'][0]['nome']=PROGRAMACAO_DATA;
$form['PROGRAMACAO'][0]['titulo']=$tdb[PROGRAMACAO_DATA]['DESC'];
$form['PROGRAMACAO'][0]['obs']="";
$form['PROGRAMACAO'][0]['largura']=450;
$form['PROGRAMACAO'][0]['altura']=250;
$form['PROGRAMACAO'][0]['chave']="MID";

$form['PROGRAMACAO'][1]['abre_fieldset']=$ling['prog_ajuste'];

$form['PROGRAMACAO'][1]['campo']="DATA";
$form['PROGRAMACAO'][1]['tipo']="data";
$form['PROGRAMACAO'][1]['null']=0;
$form['PROGRAMACAO'][1]['unico']=0;

$form['PROGRAMACAO'][2]['campo']="TEXTO";
$form['PROGRAMACAO'][2]['tipo']="texto";
$form['PROGRAMACAO'][2]['tam']=40;
$form['PROGRAMACAO'][2]['max']=255;
$form['PROGRAMACAO'][2]['null']=0;
$form['PROGRAMACAO'][2]['unico']=0;

$form['PROGRAMACAO'][2]['fecha_fieldset']=1;

$form['PROGRAMACAO_MATERIAL'][0]['nome']=PROGRAMACAO_MATERIAIS;
$form['PROGRAMACAO_MATERIAL'][0]['titulo']=$tdb[PROGRAMACAO_MATERIAIS]['DESC'];
$form['PROGRAMACAO_MATERIAL'][0]['obs']="";
$form['PROGRAMACAO_MATERIAL'][0]['largura']=600;
$form['PROGRAMACAO_MATERIAL'][0]['altura']=300;
$form['PROGRAMACAO_MATERIAL'][0]['chave']="MID";

$form['PROGRAMACAO_MATERIAL'][1]['abre_fieldset']=$tdb[PROGRAMACAO_MATERIAIS]['DESC'];

$form['PROGRAMACAO_MATERIAL'][1]['campo']="MID_MATERIAL";
$form['PROGRAMACAO_MATERIAL'][1]['tipo']="select_material";
$form['PROGRAMACAO_MATERIAL'][1]['null']=0;
$form['PROGRAMACAO_MATERIAL'][1]['tam']=10;
$form['PROGRAMACAO_MATERIAL'][1]['max']=10;
$form['PROGRAMACAO_MATERIAL'][1]['unico']=0;

$form['PROGRAMACAO_MATERIAL'][2]['campo']="QUANTIDADE";
$form['PROGRAMACAO_MATERIAL'][2]['tipo']="texto_unidade";
$form['PROGRAMACAO_MATERIAL'][2]['null']=0;
$form['PROGRAMACAO_MATERIAL'][2]['tam']=5;
$form['PROGRAMACAO_MATERIAL'][2]['max']=5;
$form['PROGRAMACAO_MATERIAL'][2]['unico']=0;
$form['PROGRAMACAO_MATERIAL'][2]['campo_material'] = 1;

$form['PROGRAMACAO_MATERIAL'][3]['campo']="MID_PROGRAMACAO";
$form['PROGRAMACAO_MATERIAL'][3]['tipo']="recb_valor";
$form['PROGRAMACAO_MATERIAL'][3]['valor']=$form_recb_valor;
$form['PROGRAMACAO_MATERIAL'][3]['null']=0;
$form['PROGRAMACAO_MATERIAL'][3]['tam']=0;
$form['PROGRAMACAO_MATERIAL'][3]['max']=0;
$form['PROGRAMACAO_MATERIAL'][3]['unico']=0;

$form['PROGRAMACAO_MATERIAL'][3]['fecha_fieldset']=1;

// FORMULARIO MAO DE OBRA
$form['PROGRAMACAO_MAODEOBRA'][0]['nome']=PROGRAMACAO_MAODEOBRA;
$form['PROGRAMACAO_MAODEOBRA'][0]['titulo']=$tdb[PROGRAMACAO_MAODEOBRA]['DESC'];
$form['PROGRAMACAO_MAODEOBRA'][0]['obs']="";
$form['PROGRAMACAO_MAODEOBRA'][0]['largura']=400;
$form['PROGRAMACAO_MAODEOBRA'][0]['altura']=300;
$form['PROGRAMACAO_MAODEOBRA'][0]['chave']="MID";

$form['PROGRAMACAO_MAODEOBRA'][1]['abre_fieldset']=$tdb[PROGRAMACAO_MAODEOBRA]['DESC'];

$form['PROGRAMACAO_MAODEOBRA'][1]['campo']="TEMPO";
$form['PROGRAMACAO_MAODEOBRA'][1]['tipo']="hora";
$form['PROGRAMACAO_MAODEOBRA'][1]['null']=0;
$form['PROGRAMACAO_MAODEOBRA'][1]['tam']=8;
$form['PROGRAMACAO_MAODEOBRA'][1]['max']=8;
$form['PROGRAMACAO_MAODEOBRA'][1]['unico']=0;

$form['PROGRAMACAO_MAODEOBRA'][2]['campo']="MID_EQUIPE";
$form['PROGRAMACAO_MAODEOBRA'][2]['tipo']="select_rela";
$form['PROGRAMACAO_MAODEOBRA'][2]['null']=0;
$form['PROGRAMACAO_MAODEOBRA'][2]['tam']=10;
$form['PROGRAMACAO_MAODEOBRA'][2]['max']=10;
$form['PROGRAMACAO_MAODEOBRA'][2]['unico']=0;

$form['PROGRAMACAO_MAODEOBRA'][3]['campo']="MID_PROGRAMACAO";
$form['PROGRAMACAO_MAODEOBRA'][3]['tipo']="recb_valor";
$form['PROGRAMACAO_MAODEOBRA'][3]['valor']=$form_recb_valor;
$form['PROGRAMACAO_MAODEOBRA'][3]['null']=0;
$form['PROGRAMACAO_MAODEOBRA'][3]['tam']=0;
$form['PROGRAMACAO_MAODEOBRA'][3]['max']=0;
$form['PROGRAMACAO_MAODEOBRA'][3]['unico']=0;

$form['PROGRAMACAO_MAODEOBRA'][3]['fecha_fieldset']=1;


// FORMULARIO FERIADOS
$form['FERIADO'][0]['nome']=FERIADOS;
$form['FERIADO'][0]['titulo']="CADASTRO DE FERIADOS";
$form['FERIADO'][0]['obs']="";
$form['FERIADO'][0]['largura']=500;
$form['FERIADO'][0]['altura']=180;
$form['FERIADO'][0]['chave']="MID";

$form['FERIADO'][1]['abre_fieldset']=$tdb[FERIADOS]['DESC'];

$form['FERIADO'][1]['campo']="DATA";
$form['FERIADO'][1]['tipo']="data";
$form['FERIADO'][1]['null']=0;
$form['FERIADO'][1]['tam']=30;
$form['FERIADO'][1]['max']=250;
$form['FERIADO'][1]['unico']=0;

$form['FERIADO'][2]['campo']="DESCRICAO";
$form['FERIADO'][2]['tipo']="texto";
$form['FERIADO'][2]['null']=0;
$form['FERIADO'][2]['tam']=40;
$form['FERIADO'][2]['max']=250;
$form['FERIADO'][2]['unico']=0;
$form['FERIADO'][2]['fecha_fieldset']=1;

$form['SOL_SEGURANCA'][0]['nome']=SOLICITACAO_SITUACAO_SEGURANCA;
$form['SOL_SEGURANCA'][0]['titulo']=$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESC'];
$form['SOL_SEGURANCA'][0]['obs']="";
$form['SOL_SEGURANCA'][0]['largura']=500;
$form['SOL_SEGURANCA'][0]['altura']=180;
$form['SOL_SEGURANCA'][0]['chave']="MID";
$form['SOL_SEGURANCA'][1]['abre_fieldset']=$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESC'];

$form['SOL_SEGURANCA'][1]['campo']="DESCRICAO";
$form['SOL_SEGURANCA'][1]['tipo']="texto";
$form['SOL_SEGURANCA'][1]['null']=0;
$form['SOL_SEGURANCA'][1]['tam']=30;
$form['SOL_SEGURANCA'][1]['max']=250;
$form['SOL_SEGURANCA'][1]['unico']=1;

$form['SOL_SEGURANCA'][2]['campo']="NIVEL";
$form['SOL_SEGURANCA'][2]['tipo']="texto";
$form['SOL_SEGURANCA'][2]['null']=0;
$form['SOL_SEGURANCA'][2]['tam']=2;
$form['SOL_SEGURANCA'][2]['max']=2;
$form['SOL_SEGURANCA'][2]['unico']=0;

$form['SOL_SEGURANCA'][2]['fecha_fieldset']=1;

$form['SOL_PRODUCAO'][0]['nome']=SOLICITACAO_SITUACAO_PRODUCAO;
$form['SOL_PRODUCAO'][0]['titulo']=$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESC'];
$form['SOL_PRODUCAO'][0]['obs']="";
$form['SOL_PRODUCAO'][0]['largura']=500;
$form['SOL_PRODUCAO'][0]['altura']=180;
$form['SOL_PRODUCAO'][0]['chave']="MID";
$form['SOL_PRODUCAO'][1]['abre_fieldset']=$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESC'];

$form['SOL_PRODUCAO'][1]['campo']="DESCRICAO";
$form['SOL_PRODUCAO'][1]['tipo']="texto";
$form['SOL_PRODUCAO'][1]['null']=0;
$form['SOL_PRODUCAO'][1]['tam']=30;
$form['SOL_PRODUCAO'][1]['max']=250;
$form['SOL_PRODUCAO'][1]['unico']=1;

$form['SOL_PRODUCAO'][2]['campo']="NIVEL";
$form['SOL_PRODUCAO'][2]['tipo']="texto";
$form['SOL_PRODUCAO'][2]['null']=0;
$form['SOL_PRODUCAO'][2]['tam']=2;
$form['SOL_PRODUCAO'][2]['max']=2;
$form['SOL_PRODUCAO'][2]['unico']=0;

$form['SOL_PRODUCAO'][2]['fecha_fieldset']=1;

// FORMULARIO PROGRAMACAO ROTA
$form['SOLICITACAO'][0]['nome']=SOLICITACOES;
$form['SOLICITACAO'][0]['titulo']=$tdb[SOLICITACOES]['DESC'];
$form['SOLICITACAO'][0]['obs']="";
$form['SOLICITACAO'][0]['largura']=550;
$form['SOLICITACAO'][0]['altura']=400;
$form['SOLICITACAO'][0]['chave']="MID";

$form['SOLICITACAO'][1]['abre_fieldset']=$tdb[SOLICITACOES]['DESC'];

$form['SOLICITACAO'][1]['campo']="MID_MAQUINA";
$form['SOLICITACAO'][1]['tipo']="select_rela";
$form['SOLICITACAO'][1]['null']=0;
$form['SOLICITACAO'][1]['tam']=0;
$form['SOLICITACAO'][1]['max']=0;
$form['SOLICITACAO'][1]['unico']=0;
$form['SOLICITACAO'][1]['sql']="WHERE STATUS = 1";
$form['SOLICITACAO'][1]['js']="atualiza_area2('select_conj', 'parametros.php?id=filtra_conj&campo_id=cc[" . SOLICITACOES . "][2]&campo_nome=cc[" . SOLICITACOES . "][2]&null=1&mid_maq=' + this.value);";


$form['SOLICITACAO'][2]['abre_div']="select_conj";

$form['SOLICITACAO'][2]['campo']="MID_CONJUNTO";
$form['SOLICITACAO'][2]['tipo']="select_rela";
$form['SOLICITACAO'][2]['null']=1;
$form['SOLICITACAO'][2]['tam']=0;
$form['SOLICITACAO'][2]['max']=0;
$form['SOLICITACAO'][2]['unico']=0;
$form['SOLICITACAO'][2]['sql']='WHERE MID_MAQUINA=0';

$form['SOLICITACAO'][2]['fecha_div']="select_conj";

$form['SOLICITACAO'][3]['campo']="DESCRICAO";
$form['SOLICITACAO'][3]['tipo']="texto";
$form['SOLICITACAO'][3]['null']=0;
$form['SOLICITACAO'][3]['tam']=50;
$form['SOLICITACAO'][3]['max']=38;
$form['SOLICITACAO'][3]['unico']=0;

$form['SOLICITACAO'][4]['campo']="TEXTO";
$form['SOLICITACAO'][4]['tipo']="blog";
$form['SOLICITACAO'][4]['null']=0;
$form['SOLICITACAO'][4]['tam']=50;
$form['SOLICITACAO'][4]['max']=8;
$form['SOLICITACAO'][4]['unico']=0;

$form['SOLICITACAO'][5]['campo']="DATA";
$form['SOLICITACAO'][5]['tipo']="datafixa";
$form['SOLICITACAO'][5]['null']=0;
$form['SOLICITACAO'][5]['tam']=10;
$form['SOLICITACAO'][5]['max']=10;
$form['SOLICITACAO'][5]['unico']=0;

$form['SOLICITACAO'][6]['campo']="HORA";
$form['SOLICITACAO'][6]['tipo']="horafixa";
$form['SOLICITACAO'][6]['null']=0;
$form['SOLICITACAO'][6]['tam']=10;
$form['SOLICITACAO'][6]['max']=10;
$form['SOLICITACAO'][6]['unico']=0;

$form['SOLICITACAO'][7]['campo']="USUARIO";
$form['SOLICITACAO'][7]['tipo']="recb_valor";
$form['SOLICITACAO'][7]['valor']=(int)$_SESSION[ManuSess]['user']['MID'];
$form['SOLICITACAO'][7]['null']=0;
$form['SOLICITACAO'][7]['tam']=0;
$form['SOLICITACAO'][7]['max']=0;
$form['SOLICITACAO'][7]['unico']=0;

$form['SOLICITACAO'][7]['fecha_fieldset']=1;

$form['SOLICITACAO'][8]['abre_fieldset']=$ling['sol_criticidade'];

$form['SOLICITACAO'][8]['campo']="PRODUCAO";
$form['SOLICITACAO'][8]['tipo']="select_rela";
$form['SOLICITACAO'][8]['null']=1;
$form['SOLICITACAO'][8]['tam']=0;
$form['SOLICITACAO'][8]['max']=0;
$form['SOLICITACAO'][8]['unico']=0;

$form['SOLICITACAO'][9]['campo']="SEGURANCA";
$form['SOLICITACAO'][9]['tipo']="select_rela";
$form['SOLICITACAO'][9]['null']=1;
$form['SOLICITACAO'][9]['tam']=0;
$form['SOLICITACAO'][9]['max']=0;
$form['SOLICITACAO'][9]['unico']=0;

$form['SOLICITACAO'][10]['campo']="STATUS";
$form['SOLICITACAO'][10]['tipo']="recb_valor";
$form['SOLICITACAO'][10]['null']=0;
$form['SOLICITACAO'][10]['tam']=0;
$form['SOLICITACAO'][10]['max']=0;
$form['SOLICITACAO'][10]['unico']=0;

$form['SOLICITACAO'][10]['fecha_fieldset']=1;


// FORMULARIO PROGRAMACAO ROTA
$form['REQUISICAO'][0]['nome']=REQUISICAO;
$form['REQUISICAO'][0]['titulo']=$tdb[REQUISICAO]['DESC'];
$form['REQUISICAO'][0]['obs']="";
$form['REQUISICAO'][0]['largura']=500;
$form['REQUISICAO'][0]['altura']=340;
$form['REQUISICAO'][0]['chave']="MID";

$form['REQUISICAO'][1]['abre_fieldset']=$tdb[REQUISICAO]['DESC'];

$form['REQUISICAO'][1]['campo']="USUARIO";
$form['REQUISICAO'][1]['tipo']="recb_valor";
$form['REQUISICAO'][1]['valor']=$_SESSION[ManuSess]['user']['MID'];
$form['REQUISICAO'][1]['null']=0;
$form['REQUISICAO'][1]['tam']=0;
$form['REQUISICAO'][1]['max']=0;
$form['REQUISICAO'][1]['unico']=0;

$form['REQUISICAO'][2]['campo']="FUNCIONARIO";
$form['REQUISICAO'][2]['tipo']="select_rela";
$form['REQUISICAO'][2]['null']=1;
$form['REQUISICAO'][2]['tam']=0;
$form['REQUISICAO'][2]['max']=0;
$form['REQUISICAO'][2]['unico']=0;

$form['REQUISICAO'][3]['campo']="MID_ALMOXARIFADO";
$form['REQUISICAO'][3]['tipo']="select_rela";
$form['REQUISICAO'][3]['null']=0;
$form['REQUISICAO'][3]['tam']=0;
$form['REQUISICAO'][3]['max']=0;
$form['REQUISICAO'][3]['unico']=0;

$form['REQUISICAO'][4]['campo']="MOTIVO";
$form['REQUISICAO'][4]['tipo']="texto";
$form['REQUISICAO'][4]['null']=0;
$form['REQUISICAO'][4]['tam']=40;
$form['REQUISICAO'][4]['max']=200;
$form['REQUISICAO'][4]['unico']=0;

$form['REQUISICAO'][5]['campo']="ORDEM";
$form['REQUISICAO'][5]['tipo']="texto";
$form['REQUISICAO'][5]['null']=1;
$form['REQUISICAO'][5]['tam']=10;
$form['REQUISICAO'][5]['max']=30;
$form['REQUISICAO'][5]['unico']=0;

$form['REQUISICAO'][6]['campo']="PRAZO";
$form['REQUISICAO'][6]['tipo']="data";
$form['REQUISICAO'][6]['valor']=0;
$form['REQUISICAO'][6]['null']=0;
$form['REQUISICAO'][6]['tam']=10;
$form['REQUISICAO'][6]['max']=10;
$form['REQUISICAO'][6]['unico']=0;

$form['REQUISICAO'][7]['campo']="MATERIAIS";
$form['REQUISICAO'][7]['tipo']="blog";
$form['REQUISICAO'][7]['valor']=0;
$form['REQUISICAO'][7]['null']=0;
$form['REQUISICAO'][7]['tam']=40;
$form['REQUISICAO'][7]['max']=6;
$form['REQUISICAO'][7]['unico']=0;

$form['REQUISICAO'][8]['campo']="STATUS";
$form['REQUISICAO'][8]['tipo']="recb_valor";
$form['REQUISICAO'][8]['valor']=0;
$form['REQUISICAO'][8]['null']=0;
$form['REQUISICAO'][8]['tam']=0;
$form['REQUISICAO'][8]['max']=0;
$form['REQUISICAO'][8]['unico']=0;

$form['REQUISICAO'][9]['campo']="DATA";
$form['REQUISICAO'][9]['tipo']="datafixa";
$form['REQUISICAO'][9]['null']=0;
$form['REQUISICAO'][9]['tam']=10;
$form['REQUISICAO'][9]['max']=10;
$form['REQUISICAO'][9]['unico']=0;

$form['REQUISICAO'][10]['campo']="HORA";
$form['REQUISICAO'][10]['tipo']="horafixa";
$form['REQUISICAO'][10]['tam']=10;
$form['REQUISICAO'][10]['max']=10;
$form['REQUISICAO'][10]['null']=0;
$form['REQUISICAO'][10]['unico']=0;

$form['REQUISICAO'][10]['fecha_fieldset']=1;

$form['REQUISICAO_RESP'][0]['nome']=REQUISICAO_RESPOSTA;
$form['REQUISICAO_RESP'][0]['titulo']=$tdb[REQUISICAO_RESPOSTA]['DESC'];
$form['REQUISICAO_RESP'][0]['obs']="";
$form['REQUISICAO_RESP'][0]['largura']=500;
$form['REQUISICAO_RESP'][0]['altura']=300;
$form['REQUISICAO_RESP'][0]['chave']="MID";

$form['REQUISICAO_RESP'][1]['abre_fieldset']=$tdb[REQUISICAO_RESPOSTA]['DESC'];

$form['REQUISICAO_RESP'][1]['campo']="DATA";
$form['REQUISICAO_RESP'][1]['tipo']="datafixa";
$form['REQUISICAO_RESP'][1]['tam']=10;
$form['REQUISICAO_RESP'][1]['max']=10;
$form['REQUISICAO_RESP'][1]['null']=0;
$form['REQUISICAO_RESP'][1]['unico']=0;

$form['REQUISICAO_RESP'][2]['campo']="TEXTO";
$form['REQUISICAO_RESP'][2]['tipo']="blog";
$form['REQUISICAO_RESP'][2]['null']=0;
$form['REQUISICAO_RESP'][2]['tam']=40;
$form['REQUISICAO_RESP'][2]['max']=8;
$form['REQUISICAO_RESP'][2]['unico']=0;

$form['REQUISICAO_RESP'][3]['campo']="MID_REQUISICAO";
$form['REQUISICAO_RESP'][3]['null']=0;
$form['REQUISICAO_RESP'][3]['tipo']="recb_valor";
$form['REQUISICAO_RESP'][3]['valor']=$form_recb_valor;
$form['REQUISICAO_RESP'][3]['unico']=1;

$form['REQUISICAO_RESP'][4]['campo']="USUARIO";
$form['REQUISICAO_RESP'][4]['null']=0;
$form['REQUISICAO_RESP'][4]['tipo']="recb_valor";
$form['REQUISICAO_RESP'][4]['valor']=$_SESSION[ManuSess]['user']['MID'];
$form['REQUISICAO_RESP'][4]['unico']=0;

$form['REQUISICAO_RESP'][5]['campo']="HORA";
$form['REQUISICAO_RESP'][5]['tipo']="horafixa";
$form['REQUISICAO_RESP'][5]['tam']=10;
$form['REQUISICAO_RESP'][5]['max']=10;
$form['REQUISICAO_RESP'][5]['null']=0;
$form['REQUISICAO_RESP'][5]['unico']=0;

$form['REQUISICAO_RESP'][5]['fecha_fieldset']=1;


$form['SOLICITACAO_RESP'][0]['nome']=SOLICITACAO_RESPOSTA;
$form['SOLICITACAO_RESP'][0]['titulo']=$tdb[SOLICITACAO_RESPOSTA]['DESC'];
$form['SOLICITACAO_RESP'][0]['obs']="";
$form['SOLICITACAO_RESP'][0]['largura']=600;
$form['SOLICITACAO_RESP'][0]['altura']=260;
$form['SOLICITACAO_RESP'][0]['chave']="MID";

$form['SOLICITACAO_RESP'][1]['abre_fieldset']=$tdb[SOLICITACAO_RESPOSTA]['DESC'];

$form['SOLICITACAO_RESP'][1]['campo']="DATA";
$form['SOLICITACAO_RESP'][1]['tipo']="recb_valor";
$form['SOLICITACAO_RESP'][1]['valor']=date("Y-m-d");
$form['SOLICITACAO_RESP'][1]['null']=1;
$form['SOLICITACAO_RESP'][1]['unico']=0;

$form['SOLICITACAO_RESP'][5]['campo']="HORA";
$form['SOLICITACAO_RESP'][5]['tipo']="horafixa";
$form['SOLICITACAO_RESP'][5]['valor']=date("H:i:s");
$form['SOLICITACAO_RESP'][5]['null']=1;
$form['SOLICITACAO_RESP'][5]['unico']=0;

$form['SOLICITACAO_RESP'][2]['campo']="TEXTO";
$form['SOLICITACAO_RESP'][2]['tipo']="blog";
$form['SOLICITACAO_RESP'][2]['null']=0;
$form['SOLICITACAO_RESP'][2]['tam']=40;
$form['SOLICITACAO_RESP'][2]['max']=8;
$form['SOLICITACAO_RESP'][2]['unico']=0;

$form['SOLICITACAO_RESP'][3]['campo']="MID_SOLICITACAO";
$form['SOLICITACAO_RESP'][3]['null']=0;
$form['SOLICITACAO_RESP'][3]['tipo']="recb_valor";
$form['SOLICITACAO_RESP'][3]['valor']=$form_recb_valor;
$form['SOLICITACAO_RESP'][3]['unico']=0;

$form['SOLICITACAO_RESP'][4]['campo']="USUARIO";
$form['SOLICITACAO_RESP'][4]['null']=0;
$form['SOLICITACAO_RESP'][4]['tipo']="recb_valor";
$form['SOLICITACAO_RESP'][4]['valor']=$_SESSION[ManuSess]['user']['MID'];
$form['SOLICITACAO_RESP'][4]['unico']=0;

$form['SOLICITACAO_RESP'][4]['fecha_fieldset']=1;



$form['NATUREZA'][0]['nome']=NATUREZA_SERVICOS;
$form['NATUREZA'][0]['titulo']=$tdb[NATUREZA_SERVICOS]['DESC'];
$form['NATUREZA'][0]['obs']="";
$form['NATUREZA'][0]['largura']=500;
$form['NATUREZA'][0]['altura']=160;
$form['NATUREZA'][0]['chave']="MID";

$form['NATUREZA'][1]['abre_fieldset']=$tdb[NATUREZA_SERVICOS]['DESC'];

$form['NATUREZA'][1]['campo']="MID_EMPRESA";
$form['NATUREZA'][1]['tipo']="select_rela";
$form['NATUREZA'][1]['null']=1;
$form['NATUREZA'][1]['tam']=15;
$form['NATUREZA'][1]['max']=30;

$form['NATUREZA'][2]['campo']="DESCRICAO";
$form['NATUREZA'][2]['tipo']="texto";
$form['NATUREZA'][2]['null']=0;
$form['NATUREZA'][2]['tam']=30;
$form['NATUREZA'][2]['max']=250;
$form['NATUREZA'][2]['unico']=1;

$form['NATUREZA'][2]['fecha_fieldset']=1;

$form['TIPO_SERVICO'][0]['nome']=TIPOS_SERVICOS;
$form['TIPO_SERVICO'][0]['titulo']=$tdb[TIPOS_SERVICOS]['DESC'];
$form['TIPO_SERVICO'][0]['obs']="";
$form['TIPO_SERVICO'][0]['largura']=500;
$form['TIPO_SERVICO'][0]['altura']=180;
$form['TIPO_SERVICO'][0]['chave']="MID";

$form['TIPO_SERVICO'][1]['abre_fieldset']=$tdb[TIPOS_SERVICOS]['DESC'];

$form['TIPO_SERVICO'][1]['campo']="MID_EMPRESA";
$form['TIPO_SERVICO'][1]['tipo']="select_rela";
$form['TIPO_SERVICO'][1]['null']=1;
$form['TIPO_SERVICO'][1]['tam']=15;
$form['TIPO_SERVICO'][1]['max']=30;

$form['TIPO_SERVICO'][2]['campo']="DESCRICAO";
$form['TIPO_SERVICO'][2]['tipo']="texto";
$form['TIPO_SERVICO'][2]['null']=0;
$form['TIPO_SERVICO'][2]['tam']=30;
$form['TIPO_SERVICO'][2]['max']=250;
$form['TIPO_SERVICO'][2]['unico']=1;

$form['TIPO_SERVICO'][2]['fecha_fieldset']=1;

$form['CAUSA'][0]['nome']=CAUSA;
$form['CAUSA'][0]['titulo']=$tdb[CAUSA]['DESC'];
$form['CAUSA'][0]['obs']="";
$form['CAUSA'][0]['largura']=500;
$form['CAUSA'][0]['altura']=160;
$form['CAUSA'][0]['chave']="MID";

$form['CAUSA'][1]['abre_fieldset']=$tdb[CAUSA]['DESC'];

$form['CAUSA'][1]['campo']="MID_EMPRESA";
$form['CAUSA'][1]['tipo']="select_rela";
$form['CAUSA'][1]['null']=1;
$form['CAUSA'][1]['tam']=15;
$form['CAUSA'][1]['max']=30;

$form['CAUSA'][2]['campo']="DESCRICAO";
$form['CAUSA'][2]['tipo']="texto";
$form['CAUSA'][2]['null']=0;
$form['CAUSA'][2]['tam']=30;
$form['CAUSA'][2]['max']=250;
$form['CAUSA'][2]['unico']=1;

$form['CAUSA'][2]['fecha_fieldset']=1;


$form['DEFEITO'][0]['nome']=DEFEITO;
$form['DEFEITO'][0]['titulo']=$tdb[DEFEITO]['DESC'];
$form['DEFEITO'][0]['obs']="";
$form['DEFEITO'][0]['largura']=500;
$form['DEFEITO'][0]['altura']=160;
$form['DEFEITO'][0]['chave']="MID";

$form['DEFEITO'][1]['abre_fieldset']=$tdb[DEFEITO]['DESC'];

$form['DEFEITO'][1]['campo']="MID_EMPRESA";
$form['DEFEITO'][1]['tipo']="select_rela";
$form['DEFEITO'][1]['null']=1;
$form['DEFEITO'][1]['tam']=15;
$form['DEFEITO'][1]['max']=30;

$form['DEFEITO'][2]['campo']="DESCRICAO";
$form['DEFEITO'][2]['tipo']="texto";
$form['DEFEITO'][2]['null']=0;
$form['DEFEITO'][2]['tam']=30;
$form['DEFEITO'][2]['max']=250;
$form['DEFEITO'][2]['unico']=1;

$form['DEFEITO'][2]['fecha_fieldset']=1;

$form['SOLUCAO'][0]['nome']=SOLUCAO;
$form['SOLUCAO'][0]['titulo']=$tdb[SOLUCAO]['DESC'];
$form['SOLUCAO'][0]['obs']="";
$form['SOLUCAO'][0]['largura']=500;
$form['SOLUCAO'][0]['altura']=160;
$form['SOLUCAO'][0]['chave']="MID";

$form['SOLUCAO'][1]['abre_fieldset']=$tdb[SOLUCAO]['DESC'];

$form['SOLUCAO'][1]['campo']="MID_EMPRESA";
$form['SOLUCAO'][1]['tipo']="select_rela";
$form['SOLUCAO'][1]['null']=1;
$form['SOLUCAO'][1]['tam']=15;
$form['SOLUCAO'][1]['max']=30;

$form['SOLUCAO'][2]['campo']="DESCRICAO";
$form['SOLUCAO'][2]['tipo']="texto";
$form['SOLUCAO'][2]['null']=0;
$form['SOLUCAO'][2]['tam']=30;
$form['SOLUCAO'][2]['max']=250;
$form['SOLUCAO'][2]['unico']=1;

$form['SOLUCAO'][2]['fecha_fieldset']=1;




$form['PENDENCIAS'][0]['nome']=PENDENCIAS;
$form['PENDENCIAS'][0]['titulo']=$tdb[PENDENCIAS]['DESC'];
$form['PENDENCIAS'][0]['obs']="";
$form['PENDENCIAS'][0]['largura']=600;
$form['PENDENCIAS'][0]['altura']=280;
$form['PENDENCIAS'][0]['chave']="MID";
$form['PENDENCIAS'][1]['abre_fieldset']=$tdb[PENDENCIAS]['DESC'];

$form['PENDENCIAS'][1]['campo']="MID_MAQUINA";
$form['PENDENCIAS'][1]['tipo']="select_rela";
$form['PENDENCIAS'][1]['null']=0;
$form['PENDENCIAS'][1]['tam']=0;
$form['PENDENCIAS'][1]['max']=0;
$form['PENDENCIAS'][1]['unico']=0;
$form['PENDENCIAS'][1]['js']="atualiza_area2('conj_os', 'parametros.php?id=filtro_pend&conj[campo_id]=cc[" . PENDENCIAS . "][2]&conj[campo_nome]=cc[" . PENDENCIAS . "][2]&conj[null]=1&os[campo_id]=cc[" . PENDENCIAS . "][3]&os[campo_nome]=cc[" . PENDENCIAS . "][3]&os[null]=1&mid_maq=' + this.value);";

$form['PENDENCIAS'][2]['abre_div']="conj_os";

$form['PENDENCIAS'][2]['campo']="MID_CONJUNTO";
$form['PENDENCIAS'][2]['tipo']="select_rela";
$form['PENDENCIAS'][2]['null']=1;
$form['PENDENCIAS'][2]['tam']=0;
$form['PENDENCIAS'][2]['max']=0;
$form['PENDENCIAS'][2]['unico']=0;

if (($act == 2) or $_POST['cc']) {
    if (($form_recb_valor != "") and ($form_recb_valor != "undefined")) {
        $form['PENDENCIAS'][3]['campo']="MID_ORDEM";
        $form['PENDENCIAS'][3]['tipo']="recb_valor";
        $form['PENDENCIAS'][3]['valor']=$form_recb_valor;
        $form['PENDENCIAS'][3]['null']=0;
        $form['PENDENCIAS'][3]['tam']=30;
        $form['PENDENCIAS'][3]['max']=3;
        $form['PENDENCIAS'][3]['unico']=0;
    }
    else{
        $maq = ($act == 2) ? VoltaValor(PENDENCIAS,'MID_MAQUINA','MID',(int)$_GET['foq'],$tdb[PENDENCIAS]['dba']) : $_POST['cc'][PENDENCIAS][1];
        $form['PENDENCIAS'][3]['campo']="MID_ORDEM";
        $form['PENDENCIAS'][3]['tipo']="select_rela";
        $form['PENDENCIAS'][3]['null']=1;
        $form['PENDENCIAS'][3]['tam']=10;
        $form['PENDENCIAS'][3]['max']=10;
        $form['PENDENCIAS'][3]['unico']=0;
        $form['PENDENCIAS'][3]['autofiltro']='N';
        $form['PENDENCIAS'][3]['sql']="WHERE STATUS = 1 AND (MID_MAQUINA = '$maq' OR MID IN (SELECT DISTINCT MID_ORDEM FROM ".ORDEM_LUB." WHERE MID_MAQUINA = '$maq'))";
    }
}
else {
    $form['PENDENCIAS'][3]['campo']="MID_ORDEM";
    $form['PENDENCIAS'][3]['tipo']="recb_valor";
    $form['PENDENCIAS'][3]['valor']='';
    $form['PENDENCIAS'][3]['null']=0;
    $form['PENDENCIAS'][3]['tam']=30;
    $form['PENDENCIAS'][3]['max']=3;
    $form['PENDENCIAS'][3]['unico']=0;

}

$form['PENDENCIAS'][3]['fecha_div']=1;

$form['PENDENCIAS'][4]['campo']="DATA";
$form['PENDENCIAS'][4]['tipo']="data";
$form['PENDENCIAS'][4]['null']=0;
$form['PENDENCIAS'][4]['tam']=30;
$form['PENDENCIAS'][4]['max']=3;
$form['PENDENCIAS'][4]['unico']=0;

$form['PENDENCIAS'][5]['campo']="DESCRICAO";
$form['PENDENCIAS'][5]['tipo']="blog";
$form['PENDENCIAS'][5]['null']=0;
$form['PENDENCIAS'][5]['tam']=50;
$form['PENDENCIAS'][5]['max']=3;
$form['PENDENCIAS'][5]['unico']=0;

$form['PENDENCIAS'][5]['fecha_fieldset']=1;


// FORMULARIO CHECKLIST
$form['PLANO_CHECKLIST'][0]['nome']=PLANO_CHECKLIST;
$form['PLANO_CHECKLIST'][0]['titulo']=$tdb[PLANO_CHECKLIST]['DESC'];
$form['PLANO_CHECKLIST'][0]['obs']="";
$form['PLANO_CHECKLIST'][0]['dir']=1;
$form['PLANO_CHECKLIST'][0]['largura']=530;
$form['PLANO_CHECKLIST'][0]['altura']=280;
$form['PLANO_CHECKLIST'][0]['chave']="MID";

$form['PLANO_CHECKLIST'][1]['abre_fieldset']=$tdb[PLANO_CHECKLIST]['DESC'];

$form['PLANO_CHECKLIST'][1]['campo']="DESCRICAO";
$form['PLANO_CHECKLIST'][1]['tipo']="texto";
$form['PLANO_CHECKLIST'][1]['null']=0;
$form['PLANO_CHECKLIST'][1]['tam']=30;
$form['PLANO_CHECKLIST'][1]['max']=150;
$form['PLANO_CHECKLIST'][1]['unico']=1;

$form['PLANO_CHECKLIST'][2]['campo']="USAR_TURNOS";
$form['PLANO_CHECKLIST'][2]['tipo']="select_rela";
$form['PLANO_CHECKLIST'][2]['null']=0;
$form['PLANO_CHECKLIST'][2]['unico']=0;


$form['PLANO_CHECKLIST'][3]['campo']="LAYOUT";
$form['PLANO_CHECKLIST'][3]['tipo']="select_rela";
$form['PLANO_CHECKLIST'][3]['null']=0;
$form['PLANO_CHECKLIST'][3]['unico']=0;


$form['PLANO_CHECKLIST'][4]['campo']="FREQUENCIA";
$form['PLANO_CHECKLIST'][4]['tipo']="texto";
$form['PLANO_CHECKLIST'][4]['null']=0;
$form['PLANO_CHECKLIST'][4]['tam']=4;
$form['PLANO_CHECKLIST'][4]['max']=3;
$form['PLANO_CHECKLIST'][4]['unico']=0;

$form['PLANO_CHECKLIST'][4]['fecha_fieldset']=1;

// FORMULARIO CHECKLIST ATIVIDADE
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['nome']=PLANO_CHECKLIST_ATIVIDADES;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['titulo']=$tdb[PLANO_CHECKLIST_ATIVIDADES]['DESC'];
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['obs']="";
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['dir']=1;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['largura']=530;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['altura']=280;
$form['PLANO_CHECKLIST_ATIVIDADES'][0]['chave']="MID";

$form['PLANO_CHECKLIST_ATIVIDADES'][1]['abre_fieldset']=$tdb[PLANO_CHECKLIST_ATIVIDADES]['DESC'];

$form['PLANO_CHECKLIST_ATIVIDADES'][1]['campo']="MID_CHECKLIST";
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['tipo']="recb_valor";
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['valor']=$form_recb_valor;
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['null']=0;
$form['PLANO_CHECKLIST_ATIVIDADES'][1]['unico']=0;

$form['PLANO_CHECKLIST_ATIVIDADES'][2]['campo']="NUMERO";
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['tipo']="texto";
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['null']=0;
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['tam']=5;
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['max']=5;
$form['PLANO_CHECKLIST_ATIVIDADES'][2]['unico']=0;

$form['PLANO_CHECKLIST_ATIVIDADES'][3]['campo']="TAREFA";
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['tipo']="texto";
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['null']=0;
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['tam']=40;
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['max']=150;
$form['PLANO_CHECKLIST_ATIVIDADES'][3]['unico']=0;

$form['PLANO_CHECKLIST_ATIVIDADES'][3]['fecha_fieldset']=1;


// FORMULARIO CHECKLIST PROGRAMACAO
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['nome']=PLANO_CHECKLIST_PROGRAMACAO;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['titulo']=$tdb[PLANO_CHECKLIST_PROGRAMACAO]['DESC'];
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['obs']="";
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['dir']=1;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['largura']=530;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['altura']=280;
$form['PLANO_CHECKLIST_PROGRAMACAO'][0]['chave']="MID";

$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['abre_fieldset']=$tdb[PLANO_CHECKLIST_PROGRAMACAO]['DESC'];

$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['campo']="MID_CHECKLIST";
$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['tipo']="select_rela";
$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['null']=0;
$form['PLANO_CHECKLIST_PROGRAMACAO'][1]['unico']=0;

$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['campo']="MID_MAQUINA";
$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['tipo']="select_rela";
$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['null']=0;
$form['PLANO_CHECKLIST_PROGRAMACAO'][2]['unico']=0;

$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['campo']="DATA_INICIAL";
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['tipo']="data";
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['null']=0;
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['tam']=30;
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['max']=10;
$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['unico']=0;

$form['PLANO_CHECKLIST_PROGRAMACAO'][3]['fecha_fieldset']=1;


// FORMULARIO ALMOXARIFADO
$form['ALMOXARIFADO'][0]['nome']=ALMOXARIFADO;
$form['ALMOXARIFADO'][0]['titulo']=$tdb[ALMOXARIFADO]['DESC'];
$form['ALMOXARIFADO'][0]['obs']="";
$form['ALMOXARIFADO'][0]['dir']=1;
$form['ALMOXARIFADO'][0]['largura']=530;
$form['ALMOXARIFADO'][0]['altura']=280;
$form['ALMOXARIFADO'][0]['chave']="MID";

$form['ALMOXARIFADO'][1]['abre_fieldset']=$tdb[ALMOXARIFADO]['DESC'];

$form['ALMOXARIFADO'][1]['campo']="MID_EMPRESA";
$form['ALMOXARIFADO'][1]['tipo']="select_rela";
$form['ALMOXARIFADO'][1]['null']=0;
$form['ALMOXARIFADO'][1]['unico']=0;

$form['ALMOXARIFADO'][2]['campo']="COD";
$form['ALMOXARIFADO'][2]['tipo']="texto";
$form['ALMOXARIFADO'][2]['null']=0;
$form['ALMOXARIFADO'][2]['tam']=20;
$form['ALMOXARIFADO'][2]['max']=50;
$form['ALMOXARIFADO'][2]['unico']=1;

$form['ALMOXARIFADO'][3]['campo']="DESCRICAO";
$form['ALMOXARIFADO'][3]['tipo']="texto";
$form['ALMOXARIFADO'][3]['null']=0;
$form['ALMOXARIFADO'][3]['tam']=40;
$form['ALMOXARIFADO'][3]['max']=200;

$form['ALMOXARIFADO'][3]['fecha_fieldset']=1;

// GRUPOS DE GR�FICO
$form['GRUPO_GRAFICO'][0]['nome']=GRUPO_GRAFICO;
$form['GRUPO_GRAFICO'][0]['titulo']=$tdb[GRUPO_GRAFICO]['DESC'];
$form['GRUPO_GRAFICO'][0]['obs']="";
$form['GRUPO_GRAFICO'][0]['largura']=500;
$form['GRUPO_GRAFICO'][0]['altura']=160;
$form['GRUPO_GRAFICO'][0]['chave']="MID";

$form['GRUPO_GRAFICO'][1]['abre_fieldset']=$tdb[GRUPO_GRAFICO]['DESC'];

$form['GRUPO_GRAFICO'][1]['campo']="DESCRICAO";
$form['GRUPO_GRAFICO'][1]['tipo']="texto";
$form['GRUPO_GRAFICO'][1]['null']=0;
$form['GRUPO_GRAFICO'][1]['tam']=50;
$form['GRUPO_GRAFICO'][1]['max']=250;
$form['GRUPO_GRAFICO'][1]['unico']=1;

$form['GRUPO_GRAFICO'][1]['fecha_fieldset']=1;

$iform = 0;
$form['PRODUTOS_ACABADOS'][$iform]['nome'] = PRODUTOS_ACABADOS;
$form['PRODUTOS_ACABADOS'][$iform]['titulo'] = $tdb[PRODUTOS_ACABADOS]['DESC'];
$form['PRODUTOS_ACABADOS'][$iform]['obs'] = '';
$form['PRODUTOS_ACABADOS'][$iform]['largura'] = 500;
$form['PRODUTOS_ACABADOS'][$iform]['altura'] = 250;
$form['PRODUTOS_ACABADOS'][$iform]['chave'] = 'MID';

$iform ++;
$form['PRODUTOS_ACABADOS'][$iform]['abre_fieldset'] = $tdb[PRODUTOS_ACABADOS]['DESC'];

$form['PRODUTOS_ACABADOS'][$iform]['campo'] = 'COD';
$form['PRODUTOS_ACABADOS'][$iform]['tipo'] = 'texto';
$form['PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUTOS_ACABADOS'][$iform]['tam'] = 10;
$form['PRODUTOS_ACABADOS'][$iform]['max'] = 100;
$form['PRODUTOS_ACABADOS'][$iform]['unico'] = 1;

$iform++;
$form['PRODUTOS_ACABADOS'][$iform]['campo'] = 'DESCRICAO';
$form['PRODUTOS_ACABADOS'][$iform]['tipo'] = 'texto';
$form['PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUTOS_ACABADOS'][$iform]['tam'] = 50;
$form['PRODUTOS_ACABADOS'][$iform]['max'] = 400;
$form['PRODUTOS_ACABADOS'][$iform]['unico'] = 0;

// Caso esteja editando e este campo j� esteja preenchido n�o pode ser editado
if(($act == 1) OR ($act == 2 and VoltaValor(PRODUTOS_ACABADOS, 'MID_MAQUINA', 'MID', $_GET['foq']) == 0)){
    $iform++;
    $form['PRODUTOS_ACABADOS'][$iform]['campo'] = 'MID_MAQUINA';
    $form['PRODUTOS_ACABADOS'][$iform]['tipo'] = 'select_rela';
    $form['PRODUTOS_ACABADOS'][$iform]['null'] = 0;
    $form['PRODUTOS_ACABADOS'][$iform]['unico'] = 0;
}
elseif($act == 2 and VoltaValor(PRODUTOS_ACABADOS, 'MID_MAQUINA', 'MID', $_GET['foq']) != 0){
    $iform++;
    $form['PRODUTOS_ACABADOS'][$iform]['campo'] = 'MID_MAQUINA';
    $form['PRODUTOS_ACABADOS'][$iform]['tipo'] = 'texto';
    $form['PRODUTOS_ACABADOS'][$iform]['null'] = 0;
    $form['PRODUTOS_ACABADOS'][$iform]['unico'] = 0;
    $form['PRODUTOS_ACABADOS'][$iform]['readonly'] = 1;
    $form['PRODUTOS_ACABADOS'][$iform]['tam'] = 50;
}
$form['PRODUTOS_ACABADOS'][$iform]['fecha_fieldset'] = 1;


// Formul�rio no detalhamento do objeto de manuten��o.
$iform = 0;
$form['PRODUTOS_ACABADOS2'][$iform]['nome'] = PRODUTOS_ACABADOS;
$form['PRODUTOS_ACABADOS2'][$iform]['titulo'] = $tdb[PRODUTOS_ACABADOS]['DESC'];
$form['PRODUTOS_ACABADOS2'][$iform]['obs'] = '';
$form['PRODUTOS_ACABADOS2'][$iform]['largura'] = 500;
$form['PRODUTOS_ACABADOS2'][$iform]['altura'] = 250;
$form['PRODUTOS_ACABADOS2'][$iform]['chave'] = 'MID';

$iform ++;
$form['PRODUTOS_ACABADOS2'][$iform]['abre_fieldset'] = $tdb[PRODUTOS_ACABADOS]['DESC'];

$form['PRODUTOS_ACABADOS2'][$iform]['campo'] = 'COD';
$form['PRODUTOS_ACABADOS2'][$iform]['tipo'] = 'texto';
$form['PRODUTOS_ACABADOS2'][$iform]['null'] = 0;
$form['PRODUTOS_ACABADOS2'][$iform]['tam'] = 10;
$form['PRODUTOS_ACABADOS2'][$iform]['max'] = 100;
$form['PRODUTOS_ACABADOS2'][$iform]['unico'] = 1;

$iform++;
$form['PRODUTOS_ACABADOS2'][$iform]['campo'] = 'DESCRICAO';
$form['PRODUTOS_ACABADOS2'][$iform]['tipo'] = 'texto';
$form['PRODUTOS_ACABADOS2'][$iform]['null'] = 0;
$form['PRODUTOS_ACABADOS2'][$iform]['tam'] = 50;
$form['PRODUTOS_ACABADOS2'][$iform]['max'] = 400;
$form['PRODUTOS_ACABADOS2'][$iform]['unico'] = 0;

$iform++;
$form['PRODUTOS_ACABADOS2'][$iform]['campo'] = 'MID_MAQUINA';
$form['PRODUTOS_ACABADOS2'][$iform]['tipo'] = 'recb_valor';
$form['PRODUTOS_ACABADOS2'][$iform]['null'] = 0;
$form['PRODUTOS_ACABADOS2'][$iform]['tam'] = 3;
$form['PRODUTOS_ACABADOS2'][$iform]['max'] = 11;
$form['PRODUTOS_ACABADOS2'][$iform]['valor'] = $form_recb_valor;
$form['PRODUTOS_ACABADOS2'][$iform]['unico'] = 0;

$form['PRODUTOS_ACABADOS2'][$iform]['fecha_fieldset'] = 1;

// Formul�rio no detalhamento do objeto de manuten��o.
$iform = 0;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['nome'] = PRODUCAO_PRODUTOS_ACABADOS;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['titulo'] = $tdb[PRODUCAO_PRODUTOS_ACABADOS]['DESC'];
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['obs'] = '';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['largura'] = 500;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['altura'] = 250;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['chave'] = 'MID';

$iform ++;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['abre_fieldset'] = $tdb[PRODUCAO_PRODUTOS_ACABADOS]['DESC'];

$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['campo'] = 'MID_PRODUTO_ACABADO';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tipo'] = 'recb_valor';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tam'] = 3;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['max'] = 11;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['valor'] = $form_recb_valor;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['unico'] = 0;

$iform++;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['campo'] = 'DATA_INICIO';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tipo'] = 'data';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tam'] = 10;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['max'] = 400;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['unico'] = 0;

$iform++;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['campo'] = 'DATA_FINAL';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tipo'] = 'data';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tam'] = 10;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['max'] = 400;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['unico'] = 0;
$iform++;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['campo'] = 'QUANT_PROD';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tipo'] = 'texto_int';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tam'] = 4;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['max'] = 11;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['unico'] = 0;
$iform++;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['campo'] = 'QUANT_PERDA';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tipo'] = 'texto_int';
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['null'] = 0;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['tam'] = 4;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['max'] = 11;
$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['unico'] = 0;

$form['PRODUCAO_PRODUTOS_ACABADOS'][$iform]['fecha_fieldset'] = 1;

?>
