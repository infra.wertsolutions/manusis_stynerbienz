<?php
// Fun��es de Estrutura
if (!require("lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);


// IGNORAR ESSAS TABELAS
$tabelas_ignoradas   = array();
$tabelas_ignoradas[] = EMPRESAS;
$tabelas_ignoradas[] = LOGS;


// ARQUIVOS E SUAS RESPECTIVAS TABELAS
$arquivos['maquinas']     = MAQUINAS;
$arquivos['equipamentos'] = EQUIPAMENTOS;
$arquivos['planopadrao']  = PLANO_PADRAO;
$arquivos['inspecao']     = LINK_ROTAS;
$arquivos['lubrificacao'] = LINK_ROTAS;
$arquivos['materiais']    = MATERIAIS;
$arquivos['ordens']       = ORDEM;


########################################################################
$doc = "";

// Comparativo entre as tabelas
$sql = "SHOW TABLES";

// ORIGEM
$rs_v3 = $dba[0]->Execute($sql);
$tab_v3_tmp = $rs_v3->getrows();

$tab_v3 = array();
foreach ($tab_v3_tmp as $k => $v) {
    
    if (in_array($v['Tables_in_'. $manusis['db'][0]['base']], $tabelas_ignoradas)) {
        continue;
    }
    
    $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][0]['base']]} FROM {$manusis['db'][0]['base']}";
    $rs_col_v3 = $dba[0]->Execute($sql_tmp);
    
    $col_tmp = $rs_col_v3->getrows();
    $col = array();
    
    foreach ($col_tmp as $k2 => $v2) {
        $col[$v2["Field"]] = $v2;
    }

    $tab_v3[$v['Tables_in_'. $manusis['db'][0]['base']]] = $col;
}

    
// Passando por todas as tabelas
$cont_tab = 0;
foreach ($tab_v3 as $tab => $cols) {   
    $doc .= "\n\n";
    $doc .= "---Importando tabela: $tab \n\n";

    
    // Tratamento de erros
    $erro = 0;
    
    // Buscando na base de origem apenas as tabelas encontradas na nova base
    $select_origem = "SELECT * FROM " . $tab . " ORDER BY MID ASC";
    $rs_origem = $dba[0] -> Execute($select_origem);

    if (! $rs_origem) {
        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $select_origem);
        $erro = 1;
    }
    
    // Recuperando os dados
    $dados_origem = $rs_origem -> GetRows();
    
    $ii = 0;
    foreach ($dados_origem as $dado) {
        // Montando o INSERT
        $insert_dest = "INSERT INTO " . mb_strtoupper($tab) . " (" . implode(", ", array_keys($dado)) . ")  VALUES (" . parse_insert_values($tab, $cols, $dado) . ")";
                        
        $doc .= $insert_dest . ";\n";
        
        $ii ++;
    }

    $cont_tab ++;
}    

// FINALIZANDO
$doc .= "\n\n";
$doc .= "--- PARABENS!!!! MIGRA��O CONCLUIDA, AGORA SE VIRA COM O RESTO!!!!!";
$doc .= "\n\n";


// PRINTANDO O RESULTADO
echo $doc;




function parse_insert_values($tab_mysql, $def_tab, $dados) {
    global $dba, $manusis;
    
    // Salvando o value montado
    $value = "";
    
    
    // Passando por todos os campos
    $i = 0;
    foreach ($dados as $campo => $valor) {
        $value .= ($i != 0)? ", " : "";
        
        // Buscando o tipo de dado
        list($tipo) = explode('(', $def_tab[$campo]['Type']);
        
        // For�ando MAISCULO
        $tipo = mb_strtoupper($tipo);
        
        // Data
        if (($tipo == "DATE") and ($valor == "0000-00-00")) {
            $valor = "";
        }
        // Data e Hora
        elseif (($tipo == "DATETIME") and (strpos($valor, "0000-00-00") !== FALSE)) {
            $valor = "";
        }
        
        
        // Vazio
        if ($valor == "") {
            $value .= "NULL";
        }
        // Hora
        elseif ($tipo == "TIME") {
            $value .= "to_date('$valor', 'hh24:mi:ss')";
        }
        // Data
        elseif ($tipo == "DATE") {
            $value .= "to_date('$valor', 'yyyy-mm-dd')";
        }
        // Data e Hora
        elseif ($tipo == "DATETIME") {
            $value .= "to_date('$valor', 'yyyy-mm-dd hh24:mi:ss')";
        }
        // NUM�RICO
        elseif (($tipo == "INT") or ($tipo == "FLOAT") or ($tipo == "DECIMAL") or ($tipo == "DOUBLE") or ($tipo == "TINYINT")) {
            $value .= str_replace(',', '.', $valor);
        }
        else {
            $value .= "'". addslashes($valor) . "'";
        }
        
        $i ++;
    }
    
    return $value;
    
}

?>
