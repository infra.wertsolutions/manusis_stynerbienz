<?php

// Fun��es de Estrutura
if (!require("lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);


// TABELAS QUE N�O DEVEM SER CITADAS
$tabelas_ignoradas[] = EMPRESAS;
$tabelas_ignoradas[] = USUARIOS_MODULOS;
$tabelas_ignoradas[] = USUARIOS_PERMISSAO_TIPO;
$tabelas_ignoradas[] = USUARIOS_PERMISSAO;
$tabelas_ignoradas[] = MAQUINAS_PARADA;
$tabelas_ignoradas[] = MAQUINAS_CONTADOR_TIPO;
$tabelas_ignoradas[] = MAQUINAS_STATUS; 
$tabelas_ignoradas[] = LOGS_TIPOS;
$tabelas_ignoradas[] = LOGS;
$tabelas_ignoradas[] = PROGRAMACAO_TIPO;
$tabelas_ignoradas[] = PROGRAMACAO_PERIODICIDADE;
$tabelas_ignoradas[] = TIPO_PLANOS;
$tabelas_ignoradas[] = TIPO_ROTAS;
$tabelas_ignoradas[] = ORDEM_STATUS;
$tabelas_ignoradas[] = EQUIPAMENTOS_STATUS;
$tabelas_ignoradas[] = FUNCIONARIOS_STATUS;
$tabelas_ignoradas[] = FUNCIONARIOS_SITUACAO;
$tabelas_ignoradas[] = INTEGRACAO_TIPOS;
$tabelas_ignoradas[] = VERSAO;

// Conex�o com a outra base
$manusis['db'][1]['driver'] = 'mysql';
$manusis['db'][1]['host']   = 'localhost';
$manusis['db'][1]['user']   = 'manusis_manusis';
$manusis['db'][1]['senha']  = '123123';

// Buscando as cargas realizadas
$sql1 = "SELECT * FROM cargas";
$rs1 = $dba[0] -> Execute($sql1);

// Passando por todas as cargas
while (! $rs1 -> EOF) {
    $carga = $rs1->fields;
    
    // MOSTRANDO A TABELA
    echo "<h2>Migra��o da base {$carga['BASE_ORIGEM']} para a empresa " . VoltaValor(EMPRESAS, 'NOME', 'MID', $carga['MID_EMPRESA']) . "</h2>\n";
    echo "<table border=\"1\" width=\"100%\">
    <tr><th>Tabela</th><th>Registros base original</th><th>Registros base atual</th></tr>\n";
    
    $i = 1;
    $manusis['db'][$i]['base']  = $carga['BASE_ORIGEM'];

    $dba[$i] = ADONewConnection($manusis['db'][$i]['driver']);
    $dba[$i]->SetFetchMode(ADODB_FETCH_ASSOC);
    if (!$dba[$i]->Connect($manusis['db'][$i]['host'], $manusis['db'][$i]['user'], $manusis['db'][$i]['senha'], $manusis['db'][$i]['base'])) {
        errofatal($ling['bd02']);
    }
    
    // Passando por todas as tabelas
    foreach ($tdb as $tab => $def) {
        if(array_search($tab, $tabelas_ignoradas) !== FALSE) {
            continue;
        }
               
        // Para algumas tabelas � melhor detalhar
        if ($tab == ORDEM) {
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']}";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>{$def['DESC']}</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
            
            // Preventivas
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE TIPO = 1";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']} AND TIPO = 1";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>&nbsp;&nbsp;&nbsp;{$def['DESC']} - Preventivas</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
            
            // Rotas
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE TIPO = 2";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']} AND TIPO = 2";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>&nbsp;&nbsp;&nbsp;{$def['DESC']} - Rotas</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
            
            // N�o Sistem�ticas
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE (TIPO != 1 AND TIPO != 2) OR (TIPO IS NULL)";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']} AND ((TIPO != 1 AND TIPO != 2) OR TIPO IS NULL)";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>&nbsp;&nbsp;&nbsp;{$def['DESC']} - N�o sistem�ticas</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
        }
        elseif ($tab == PROGRAMACAO) {
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE TIPO != 4";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']} AND TIPO != 4";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>{$def['DESC']}</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
            
            // Preventivas
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE TIPO = 1";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']} AND TIPO = 1";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>&nbsp;&nbsp;&nbsp;{$def['DESC']} - Preventivas</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
            
            // Rotas
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE TIPO = 2";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            $total_orig = $rs_orig -> fields['TOTAL'];
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']} AND TIPO = 2";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>&nbsp;&nbsp;&nbsp;{$def['DESC']} - Rotas</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
        }
        else {
            // Registros na origem
            $sql_orig = "SELECT COUNT(*) AS TOTAL FROM $tab";
            $rs_orig = $dba[1] -> Execute($sql_orig);
            if (! $rs_orig) {
                $total_orig = "N.A.";
            }
            else {
                $total_orig = $rs_orig -> fields['TOTAL'];
            }
            
            // Registros na ATUAL
            $sql_atual = "SELECT COUNT(*) AS TOTAL FROM $tab WHERE MID_CARGA = {$carga['MID']}";
            $rs_atual = $dba[0] -> Execute($sql_atual);
            $total_atual = $rs_atual -> fields['TOTAL'];
            
            // Mostrando
            echo "<tr><td>{$def['DESC']}</td><td>&nbsp;$total_orig</td><td>&nbsp;$total_atual</td></tr>\n";
        }
    }
    
    echo "</table>";

    $rs1 -> MoveNext();
}

?>
