<?
/**
* Buscar Ocorr�ncias
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package ordens
*/
// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) {
	die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configura��es
elseif (!require("conf/manusis.conf.php")) {
	die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
	die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
	die ($ling['bd01']);
}
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) {
	die ($ling['bd01']);
}
// Autentifica��o
elseif (!require("lib/autent.php")) {
	die ($ling['autent01']);
}
// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) {
	$manusis['tema']="padrao";
}




// Variaveis de direcionamento
$texto=LimpaTexto($_GET['texto']);

$por="%";
$cond="LIKE";
$texto=str_replace(" ",$por,$texto);



$filtro=(int)$_GET['filtro'];
$filtrostatus=$_GET['filtrostatus'];
if ($_GET['filtrostatus'] != "") {
	if ($filtrostatus == 0) {
		$filtro_status =" AND O.STATUS = '2'";
	}
	elseif ($filtrostatus != 0) {
		$filtro_status =" AND O.STATUS = '$filtrostatus'";
	}
}
$filtromaq=(int)$_GET['filtromaq'];
if ($filtromaq != 0) {
	$filtro_status =" AND O.MID_MAQUINA = '$filtromaq'";
}
$qtdpor=(int)$_GET['qtdpor'];
if ($qtdpor == 0) {
	$qtdpor=30;
}


$filtroEmpresaOrdem = VoltaFiltroEmpresa(ORDEM, 1, false);

if(count($filtroEmpresaOrdem)){
	$filtroEmpresaOrdem = " AND O.{$filtroEmpresaOrdem['campo']} IN (".implode(",", $filtroEmpresaOrdem['mid']).")";
}
else{
	$filtroEmpresaOrdem = "";
}


$datai=$_GET['datai'];
$dataf=$_GET['dataf'];

$di=explode("/",$datai);
$df=explode("/",$dataf);
if ((int)$di[0] == 0) {
	$datai="01/01/".date("Y");
}
if ((int)$df[0] == 0) {
	$dataf="31/12/".date("Y");
}
if ($datai != "") {
	$di=explode("/",$datai);
	$df=explode("/",$dataf);
	if (!checkdate ((int)$di[1], (int)$di[0], (int)$di[2])) {
		$erromsg.="<strong><font color=\"red\">".$ling['data_inicial_invalida']." ($datai)</font></strong><br>";
	}
	if (!checkdate ((int)$df[1], (int)$df[0], (int)$df[2])) {
		$erromsg.="<strong><font color=\"red\">".$ling['data_final_invalida']." ($dataf)</font></strong><br>";
	}
	$data_sqli=$di[2]."-".$di[1]."-".$di[0];
	$data_sqlf=$df[2]."-".$df[1]."-".$df[0];
}


echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']} - {$ling['busca_histo_servi']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>
<script type=\"text/javascript\">
function mostrafiltro(){
	filtro=document.getElementById('dfiltro');
	if (filtro.style.visibility == 'visible') {
		filtro.style.visibility = 'hidden';
		filtro.style.display = 'none';
	}
	else {
		filtro.style.visibility='visible';
		filtro.style.display='block';
	}
}
</script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>".$ling['busca_histo_servi']."
</div>
<div id=\"corpo_relatorio\" align=\"center\">
<form action=\"buscar_ocorrencias.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<br /> 
<div align=\"center\" style=\"text-align:center;font-size:13px;\" />
".$ling['sistema_busca_somente_ordens_realizada_prog']."<br /> <br />
</div>
<div align=\"center\" style=\"text-align:center;font-size:13px; width:500px;border:1px solid gray;padding:5px\" />
<input style=\"font-size:12px;\" size=\"50\" id=\"texto\" name=\"texto\" value=\"".stripslashes($_GET['texto'])."\" class=\"campo_text\" /> <a style=\"font-size:10px; font-color:gray; text-decoration:none;	cursor: hand;\" href=\"javascript:return\" onclick=\"mostrafiltro()\">{$ling['filtros']} >> </a><br />
<div align=\"center\">";
if (($filtro == 0) and ($_GET['datai'] == "")){
	echo "<div id=\"dfiltro\" align=\"left\" style=\"padding:3px;margin-top:3px;width:370px;border-top:1px solid gray;visibility:hidden; display:none\">";
}
else {
	echo "<div id=\"dfiltro\" align=\"left\" style=\"padding:3px;margin-top:3px;width:370px;border-top:1px solid gray;visibility=visible\">";
}
echo "
	<label for=\"filtro\" class=\"campo_label\"> {$ling['usar_somente_este_campo']}: &nbsp;&nbsp;</label>
	<select class=\"campo_select\" name=\"filtro\" id=\"filtro\">
	<option value=\"0\">".$ling['todos']."</option>
	<option value=\"1\">".$ling['ativi_preventiva']."</option>
	<option value=\"2\">".$ling['ativi_de_rota']."</option>
	<option value=\"3\">".$ling['plano_preventiva']."</option>
	<option value=\"4\">".$ling['plano_rotas']."</option>
	<option value=\"5\">{$tdb[ORDEM]['MID_MAQUINA']}</option>
	<option value=\"6\">{$tdb[ORDEM]['MID_CONJUNTO']}</option>
	<option value=\"7\">{$tdb[ORDEM]['DEFEITO']}</option>
	<option value=\"8\">{$tdb[ORDEM]['CAUSA']}</option>
	<option value=\"9\">{$tdb[ORDEM]['SOLUCAO']}</option>
	<option value=\"10\">".$ling['prog_material']."</option>
	</select><br clear=\"all\" /><script>
	filtro=document.getElementById('filtro');
	filtro.options[$filtro].selected='selected';
	</script>";

echo "<label for=\"filtrostatus\" class=\"campo_label\"> {$ling['usar_somente_este_campo']}: &nbsp;&nbsp;</label>
	<select class=\"campo_select\" name=\"filtrostatus\" id=\"filtrostatus\">
	<option value=\"0\">".$ling['rel_desc_todas']."</option>
	<option value=\"1\">".$ling['ordens_abertas']."</option>
	<option value=\"2\">".$ling['ordens_fechadas']."</option>
	<option value=\"3\">".$ling['ordens_canceladas']."</option>


	</select><script>
	filtro=document.getElementById('filtrostatus');
	filtro.options[$filtrostatus].selected='selected';
	</script><br clear=\"all\" />";
echo "<label for=\"qtdpor\" class=\"campo_label\">{$ling['qtd_reg']}: &nbsp;&nbsp;</label>
	<input type=\"text\" size=\"2\" class=\"campo_text\" name=\"qtdpor\" id=\"qtdpor\" value=\"$qtdpor\">
<br clear=\"all\" />";
FormData("{$ling['data_inicio']}:","datai",$datai,"campo_label2");
echo "<br clear=\"all\" />";
FormData("{$ling['data_fim']}:","dataf",$dataf,"campo_label2");
echo "
	</div></div>


</div><br />
<input style=\"font-size:12px;\" class=\"botao\" type=\"submit\" name=\"botao\" value=\" {$ling['busca_histo_servi']} \" />
</form><br /><br />
";

// ATIVIDADE PREVENTIVA
if ($filtro == 1) {
	$sql="select O.NUMERO,
O.TIPO,
O.TIPO_SERVICO,
O.MID_MAQUINA,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O,ordem_prev OP
WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
OP.MID_ORDEM = O.MID AND OP.TAREFA $cond '$por{$texto}$por'
$filtro_status 
$filtroEmpresaOrdem
ORDER BY O.DATA_PROG DESC";

}
// ATIVIDADE DE ROTA
elseif ($filtro == 2) {
	$sql="select O.NUMERO,
O.TIPO,
O.TIPO_SERVICO,
ordem_lub.MID_MAQUINA,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O,ordem_lub OL
WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
OL.MID_ORDEM = O.MID AND OL.TAREFA $cond '$por{$texto}$por'
$filtro_status 
$filtroEmpresaOrdem  
ORDER BY O.DATA_PROG DESC";
}
// PLANO PADRAO
elseif ($filtro == 3) {
	$sql="select O.NUMERO,
O.TIPO,
O.TIPO_SERVICO,
O.MID_MAQUINA,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O,programacao P INNER JOIN plano_padrao PP ON (P.MID_PLANO = PP.MID)
WHERE P.MID = O.MID_PROGRAMACAO AND P.TIPO = 1 AND O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
PP.DESCRICAO $cond '$por{$texto}$por'
$filtro_status 
$filtroEmpresaOrdem 
ORDER BY O.DATA_PROG DESC";

}

// PLANO ROTAS
elseif ($filtro == 4) {
	$sql="select O.NUMERO,
O.TIPO,
O.TIPO_SERVICO,
O.MID_MAQUINA,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O, programacao P INNER JOIN plano_rotas PR ON (P.MID_PLANO = PR.MID)
WHERE P.MID = O.MID_PROGRAMACAO AND programacao.TIPO = 2 AND O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
PR.DESCRICAO $cond '$por{$texto}$por'
$filtro_status 
$filtroEmpresaOrdem 
ORDER BY O.DATA_PROG DESC";

}

// MAQUINAS
elseif ($filtro == 5) {
	$sql="select O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O INNER JOIN maquinas M ON (M.MID = O.MID_MAQUINA)

WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
(M.COD $cond '$por{$texto}$por' OR M.DESCRICAO $cond '$por{$texto}$por')

$filtro_status 
$filtroEmpresaOrdem
ORDER BY O.DATA_PROG DESC";
}
// CONTJUNO
elseif ($filtro == 6) {
	$sql="select O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O INNER JOIN maquinas_conjunto MC ON (MC.MID = O.MID_CONJUNTO)

WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
(MC.TAG $cond '$por{$texto}$por' OR MC.DESCRICAO $cond '$por{$texto}$por')
$filtroEmpresaOrdem  
$filtro_status
ORDER BY O.DATA_PROG DESC";
}

elseif ($filtro == 7) {
	$sql="select O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O INNER JOIN defeito D ON (D.MID = O.DEFEITO)

WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
D.DESCRICAO $cond '$por{$texto}$por'
$filtroEmpresaOrdem  
$filtro_status
ORDER BY O.DATA_PROG DESC";
}

elseif ($filtro == 8) {
	$sql="select O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O INNER JOIN causa C ON (C.MID = O.DEFEITO)

WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
C.DESCRICAO $cond '$por{$texto}$por'
$filtroEmpresaOrdem 
$filtro_status
ORDER BY O.DATA_PROG DESC";
}

elseif ($filtro == 9) {
	$sql="select O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O INNER JOIN solucao S ON (S.MID = O.SOLUCAO)

WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
S.DESCRICAO $cond '$por{$texto}$por'
$filtroEmpresaOrdem  
$filtro_status
ORDER BY O.DATA_PROG DESC";
}
// MATERIAL
elseif ($filtro == 10) {
	$sql="select O.NUMERO,
O.TIPO,
O.TIPO_SERVICO,
O.MID_MAQUINA,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O,ordem_material OM INNER JOIN materiais M ON (M.MID = OM.MID_MATERIAL)
WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND
OM.MID_ORDEM = O.MID AND (M.DESCRICAO $cond '$por{$texto}$por' OR M.COD $cond '$por{$texto}$por')
$filtro_status $filtroEmpresaOrdem 
ORDER BY O.DATA_PROG DESC";
}
else {
	$sql="select O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID
FROM ordem O INNER JOIN maquinas M ON (M.MID = O.MID_MAQUINA)

WHERE O.DATA_PROG >= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqli'" : "TO_DATE('".$data_sqli."', 'YYYY-MM-DD')")."  AND O.DATA_PROG <= ".($manusis['db'][$tdb[ORDEM]['dba']]['driver'] != 'oci8' ? "'$data_sqlf'" : "TO_DATE('".$data_sqlf."', 'YYYY-MM-DD')")." AND (
(M.COD $cond '$por{$texto}$por' OR M.DESCRICAO $cond '$por{$texto}$por') OR

O.DEFEITO_TEXTO $cond '$por{$texto}$por' OR
O.CAUSA_TEXTO $cond '$por{$texto}$por' OR
O.SOLUCAO_TEXTO $cond '$por{$texto}$por' OR
O.NUMERO $cond '$por{$texto}$por' OR
O.TEXTO $cond '$por{$texto}$por'
)
$filtro_status
$filtroEmpresaOrdem 
ORDER BY O.NUMERO,
O.MID_MAQUINA,
O.TIPO,
O.TIPO_SERVICO,
O.NUMERO,
O.DATA_PROG,
O.DATA_INICIO,
O.DATA_FINAL,
O.STATUS,
O.MID DESC";
}
//echo $sql;
//echo "<br /> Quant $qtdpor";
$resultado=$dba[0] -> SelectLimit($sql,1,$qtdpor);
//$resultado=$dba[0] -> execute($sql);
if (!$resultado) {
	erromsg("Erro ao efetuar consulta em:<br />
	    Arquivo: ".__FILE__." <br />
	    Linha: ".__LINE__." <br />
	    Erro: ".$dba[0] ->ErrorMsg()." <br />
	    SQL: $sql
	");
}
else {
	echo "<p align=\"center\" style=\"font-size:12px\">{$ling['clic_filtrar_obj']}</p>";

	echo "<div id=\"lt_tabela\"><table width=\"100%\">
	<tr>
	<th></th>
	<th>{$ling['numero']}</th>
	<th>{$ling['tipo']}</th>
	<th>{$ling['rel_desc_obj2_min']}</th>
	<th>{$ling['rel_desc_dt_prog_min']}</th>
	<th>{$ling['data_inicio']}</th>
	<th>{$tdb[ORDEM]['DATA_FINAL']}</th>
	<th>{$ling['ver']}</th>
	</tr>
	";
	$i=0;
	while (!$resultado->EOF) {
		$campo=$resultado->fields;
		if ((int)$campo['TIPO'] == 0) {
			$stipo=VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],0);
		}
		else {
			$stipo=VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],0);
		}
		if ((int)$campo['STATUS'] == 1) {
			$smig="osabertas.png";
		}
		if ((int)$campo['STATUS'] == 2) {
			$smig="osfechadas.png";
		}
		if ((int)$campo['STATUS'] == 3) {
			$smig="oscancel.png";
		}
		$dataprog=NossaData($campo['DATA_PROG']);
		if ($campo['DATA_INICIO'] != "") {
			$datainicial=NossaData($campo['DATA_INICIO']);
		}
		if ($campo['DATA_FINAL'] != "") {
			$datafinal=NossaData($campo['DATA_FINAL']);
		}
		echo "<tr>
		<td><img src=\"imagens/icones/22x22/$smig\" alt=\"$smig\" /></td>
		<td>{$campo['NUMERO']}</td>
		<td>$stipo</td>
		<td><a href=\"buscar_ocorrencias.php?qtdpor=$qtdpor&texto=$texto&filtro=$filtro&filtrostatus=$filtrostatus&datai=$datai&dataf=$dataf&botao=1&filtromaq={$campo['MID_MAQUINA']}\">".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],0)."</a></td>
		<td>$dataprog</td>
		<td>$datainicial</td>
		<td>$datafinal</td>
		<td><a href=\"javascript:janela('detalha_ord.php?busca=".$campo['MID']."', 'parm', 500,400)\"><img src=\"imagens/icones/22x22/visualizar.png\" border=\"0\" title=\" {$ling['ord_visualizar_os']} \" alt=\" \" /></a></td>
		</tr>";
		unset($dataprog);
		unset($datainicial);
		unset($datafinal);
		unset($stipo);
		$resultado->MoveNext();
		$i++;
	}

	echo "</table></div>";
	if ($i == 0){
		echo "<p align=\"center\" style=\"font-size:12px\"><strong>".$ling['nenhum_reg_encontrado']."</strong></p>";
	}
}
echo "


</div>

</div>
</body>
</html>";


?>
