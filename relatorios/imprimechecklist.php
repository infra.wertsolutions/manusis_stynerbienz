<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino
* Nota: Relatorio
*/
$currdir = '..';

// caso o checklist n�o tenha imagem anexada:
// 1= poe imagem padr�o
// 0= n�o poe nada e economiza espa�o
$usa_foto_padrao=1;

// Fun��es do Sistema
if (!require("$currdir/lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("$currdir/conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("$currdir/lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("$currdir/lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("$currdir/lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("$currdir/lib/autent.php")) die ($ling['autent01']);

// Caso n�o exista um padr�o definido
if (!file_exists("$currdir/temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";
$ano=$_GET['ano'];
if ($ano == "") $ano=date("Y");


// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}

if ($_GET['mid']){
	$mid_check = (int)$_GET['mid'];
	$mid_maq = (int)$_GET['maq'];
	$checklist_desc = VoltaValor(PLANO_CHECKLIST,'DESCRICAO','MID',$mid_check,$tdb[PLANO_CHECKLIST]['dba']);
	
	if ($mid_maq) {
		$maq = VoltaValor(MAQUINAS,'DESCRICAO','MID',$mid_maq,$tdb[MAQUINAS]['dba']);
		$mid_setor = (int)VoltaValor(MAQUINAS,'MID_SETOR','MID',$mid_maq,$tdb[MAQUINAS]['dba']);
		$setor = VoltaValor(SETORES,'DESCRICAO','MID',$mid_setor,$tdb[SETORES]['dba']);
		$mid_area = (int)VoltaValor(SETORES,'MID_AREA','MID',$mid_setor,$tdb[SETORES]['dba']);
		$area = VoltaValor(AREAS,'DESCRICAO','MID',$mid_area,$tdb[AREAS]['dba']);
		$mid_fam = (int)VoltaValor(MAQUINAS,'FAMILIA','MID',$mid_maq,$tdb[MAQUINAS]['dba']);
		$fam = VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$mid_fam,$tdb[MAQUINAS_FAMILIA]['dba']);
		$mid_cc = (int)VoltaValor(MAQUINAS,'CENTRO_DE_CUSTO','MID',$mid_maq,$tdb[MAQUINAS]['dba']);
		$cc = VoltaValor(CENTRO_DE_CUSTO,'DESCRICAO','MID',$mid_cc,$tdb[CENTRO_DE_CUSTO]['dba']);
	}
	else {
		$maq=$setor=$area=$fam=$cc='___________________________';
	}
	
	$usarturnos = (int)VoltaValor(PLANO_CHECKLIST,'USAR_TURNOS','MID',$mid_check,$tdb[PLANO_CHECKLIST]['dba']);
	$layout = (int)VoltaValor(PLANO_CHECKLIST,'LAYOUT','MID',$mid_check,$tdb[PLANO_CHECKLIST]['dba']);

	######## valores temporarios
	//$usarturnos = 2; // 1=SIM, 2=NAO
	//$layout = 3; //1=SEMANAL, 2=QUINZENAL, 3=MENSAL
	$imagem_anexa = "temas/padrao/imagens/checklist_sem_anexo.jpg";
	########
	
	
	if ($usarturnos == 1) {
		$numturnos = 0;
		$sql = "SELECT * FROM ".TURNOS." ORDER BY DESCRICAO ASC";
		$tmp=$dba[0] ->Execute($sql);
		while (!$tmp->EOF) {
			$campo = $tmp->fields;
			$numturnos++;
			$turnos[$numturnos]=Abreviate($campo['DESCRICAO'],1,3,true,true);
			$tmp->MoveNext();
		}		
	}
	if (($usarturnos == 1) and ($numturnos == 0)) {
		$numturnos = 1;
		$turnos = array(1 => '-');
	}

	if ($layout == 1) { // semanal
		$numdias = 7;
	}
	elseif ($layout == 2) { // quinzenal
		$numdias = 15;
	}
	elseif ($layout == 3) { // mensal
		$numdias = 31;
	}
	else errofatal($ling['err19']);
	
	// constante: style dos TD
	$tdstyle = "style=\"border-left: 1px solid black; border-top: 1px solid black\"";

	$album='';
	$album_cnt=0;
	$table = "<table cellspacing=0 border=0 style=\"border-right: 1px solid black; border-bottom: 1px solid black; font-size:10px\" width=\"100%\">
	<thead><tr><th rowspan=\"2\" $tdstyle>{$ling['atividades']}</th>";
	if ($usarturnos == 1) $table .= "<th rowspan=\"2\" $tdstyle width=20>".substr($ling['turnos'],0,3)."</th>";
	$table .= "<th colspan=\"$numdias\" align=\"center\" $tdstyle>{$ling['dias']}</th></tr>
	<tr>";
	$largura_das_celulas = "12";
	for($i=1; $i<=$numdias; $i++) {
		if ($layout == 1) { // semanal
			$j = $i+1;
			if ($j == 8) $j=1;
			$table .= "<th $tdstyle>{$ling['semana'][$j]}</th>";
		}
		if ($layout == 2) { // quinzenal
			$j = $i;
			if ($j < 10) $j = '0'.(int)$j;
			$table .= "<th $tdstyle width=\"$largura_das_celulas\">$j</th>";
		}
		if ($layout == 3) { // mensal
			$j = $i;
			if ($j < 10) $j = '0'.(int)$j;
			$table .= "<th $tdstyle width=\"$largura_das_celulas\">$j</th>";
		}
	}
	$table .= "</tr></thead>";
	
	$sql = "SELECT * FROM ".PLANO_CHECKLIST_ATIVIDADES." WHERE MID_CHECKLIST = '$mid_check' ORDER BY NUMERO ASC";
	$tmp=$dba[0] ->Execute($sql);
	while (!$tmp->EOF) {
		$campo = $tmp->fields;
		$table .= "<tr>";
		if ($usarturnos == 1) {
			$table .= "<td rowspan=\"$numturnos\" $tdstyle>{$campo['NUMERO']} - {$campo['TAREFA']}</td>";
			$target = $numturnos;
		}
		else {
			$table .= "<td $tdstyle>{$campo['NUMERO']} - {$campo['TAREFA']}</td>";
			$target = 1;
		} // $target � quantas linhas ser�o colocadas POR TAREFA
		for ($i=1; $i<=$target; $i++) {
			if ($i != 1) $table .= '<tr>'; // n�o abre linha porque a primeira linha ja foi aberta
			if ($usarturnos == 1) $table .= "<td $tdstyle>{$turnos[$i]}</td>";

			## dias
			for($j=1; $j<=$numdias; $j++) {
				$table .= "<td $tdstyle>&nbsp;</td>";
			} // fim dias
			$table .= "</tr>";
		} // fim turnos	
		
		// anexos a esta atividade
		$arquivos = ListaImagensAtividade($campo['MID']);
		$dir_i=$manusis['dir']['checklist']."/{$campo['MID']}";
		if ($arquivos) foreach ($arquivos as $earq) {
			// verifica se ANTES dessa foto,
			// a quantidade de fotos que j� foram colocadas � multiplo de 3
			// se for, inicia nova linha
			$album_cnt_last = $album_cnt / 3;
			if ($album_cnt_last == round($album_cnt_last)) {
				// � multiplo de 3
				if ($album_cnt) $album .= "<tr>";
			}

			// aumenta para contar foto atual
			$album_cnt++;
			
			// coloca esta imagem:
			$album .= "<td align=center><center>{$ling['atividade']} {$campo['NUMERO']}:<br>
			<img src=\"../i.php?i=$dir_i/$earq&s=200\" border=1 hspace=5 align=\"middle\" \></center></td>";
			
			// verifica se DEPOIS dessa foto,
			// a quantidade de fotos que j� foram colocadas � multiplo de 3
			// se for, fecha essa linha linha
			// (duas checagens s�o necess�rias porque esse loop pode n�o repetir)
			$album_cnt_last = $album_cnt / 3;
			if ($album_cnt_last == round($album_cnt_last)) {
				// � multiplo de 3
				if ($album_cnt) $album .= "</tr>";
			}
			
		}
		$tmp->MoveNext();
	}
	
	$table .= "<tr><td colspan=\"".($numdias+2)."\" $tdstyle>
	{$ling['observacoes']}:<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;
	</td></tr></table>";
	// tabela horizontal.
	// celula da esquerda, tabela interna vertical.
	//		- tabela interna vertical, primeira celula possui tabela interna horizontal
	//		- esquerda logoempresa, direita titulo
	// celula da direita, imagem anexada ou logotipo caso nao tenha
	echo "<table cellspacing=0 cellpadding=0 border=0 style=\"font-size:10px\" width=\"100%\">
	<tr><td>
		<table style=\"font-size:10px\" width=100% cellspacing=0 cellpadding=0 >
		<tr><td style=\"border: 1px solid black\">
			<table width=100%>
			<tr><td><img src=\"".$manusis['url']."temas/padrao/imagens/logoempresa.png\" border=0 vspace=2 hspace=2 align=left></td>
			<td valign=bottom><h3>{$ling['plan_checklist']}: $checklist_desc
			<br>$maq</h3></td></tr>
			</table>
		</td></tr>
		<tr><td style=\"border-left: 1px solid black; border-right: 1px solid black\">
			<table width=100% style=\"font-size:10px\">
			<tr><td>{$tdb[AREAS]['DESC']}: $area</td><td>{$tdb[MAQUINAS_FAMILIA]['DESC']}: $fam</td><td rowspan=2>{$ling['mes_label']}: __________________</td><td rowspan=2>{$ling['ano']}: _______</td></tr>
			<tr><td>{$tdb[SETORES]['DESC']}: $setor</td><td>{$tdb[CENTRO_DE_CUSTO]['DESC']}: $cc</td></tr>
			</table>
		</td></tr>
		</table>
	
	</td>";

	echo "</tr></table>";
	echo $table;
	if ($album) echo "<br><table style=\"border: 1px solid black; font-size:10px\" width=100%>$album</table>";
}

function ListaImagensAtividade($mid_atv) {
	global $manusis;
	
	$dir="../".$manusis['dir']['checklist']."/$mid_atv";
	$dir_i=$manusis['dir']['checklist']."/$mid_atv";

	$arquivos = array();
	$tem_arquivos = false;
	
	if (file_exists($dir)) {
		$od = opendir($dir);
		$od2 = opendir($dir);
		$cont_verarq = 0;
		while (false !== ($arq_v = readdir($od2))) {
			$cont_verarq++;
		}
		if ($cont_verarq > 2){
			$i=0;
			while (false !== ($arq = readdir($od))) { //mostra
				$dd=explode(".",$arq);
				if (($dd[1] == "jpg") or ($dd[1] == "png")) {
					$arquivos[] = $arq;
					if (!$tem_arquivos) $tem_arquivos = true;
				}
			}
		}
	}
	if (!$tem_arquivos) return false;
	else return $arquivos;
}
?>