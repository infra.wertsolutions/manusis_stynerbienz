<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$tb=LimpaTexto($_GET['tb']);
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
$qto=(int)$_GET['qto'];
$cc=$_GET['cc'];
$cris=(int)$_GET['cris'];
$criv=LimpaTexto($_GET['criv']);
$cric=LimpaTexto($_GET['cric']);
$cris2=(int)$_GET['cris2'];
$criv2=LimpaTexto($_GET['criv2']);
$cric2=LimpaTexto($_GET['cric2']);
$or=$_GET['or'];
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
$obj=LimpaTexto($_GET['obj']);
$tipo=(int)$_GET['tipo'];
$msg = "";
if (($relatorio != "") or ($exword != "")){
    
     if($_GET['datai'] == ""){
        $msg .= "<li><b>{$ling['data_inicio']}:</b> Campo de preenchimento obrigat&oacute;rio.</li>";
    }
    
    if($_GET['dataf'] == ""){
        $msg .= "<li><b>{$ling['data_fim']}:</b> Campo de preenchimento obrigat&oacute;rio.</li>";
    }
    
    if($msg != ""){
        erromsg("<ul>$msg</ul>");
        exit();
    }
    
    $datai=explode("/",$_GET['datai']);
	$datai=$datai[2]."-".$datai[1]."-".$datai[0];
	$dataf=explode("/",$_GET['dataf']);
	$dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
	if ($tipo == 1) $fil="AND MID_AREA = '$obj'";
	if ($tipo == 2) $fil="AND MID_SETOR = '$obj'";
	if ($tipo == 3) $fil="AND MID_MAQUINA = '$obj'";
	if ($tipo == 4) $fil="AND CENTRO_DE_CUSTO = '$obj'";
	if ($tipo == 5) $fil="";
	if ($tipo == 6) $sql="SELECT * FROM ".ORDEM_PLANEJADO." WHERE NUMERO = '$obj'";
	else $sql="SELECT * FROM ".ORDEM_PLANEJADO." WHERE DATA_INICIO >= '$datai' AND DATA_INICIO <= '$dataf' $fil ORDER BY NUMERO ASC";
	if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
		$err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
		erromsg("SQL ERROR .<br>$err<br><br>$sql");
		exit;
	}
	$iii=0;
	while (!$resultado->EOF) {
		$campo=$resultado -> fields;
		$txt.="
<table cellpadding=\"2\" border=0\" width=\"100%\"><tr>
<td align=\"left\"><font size=\"2\"><strong>{$ling['rel_desc_num_os2']}: ".$campo['NUMERO']."</strong></font> </td>
<td align=\"left\"><strong>{$ling['tipo']}</strong>: ".VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba'])." </td>
<td align=\"left\"><strong>{$ling['responsavel']}</strong>: ".VoltaValor(FUNCIONARIOS,"NOME","MID",$campo['RESPONSAVEL'],$tdb[FUNCIONARIOS]['dba'])." </td>
<td align=\"left\"><strong>{$ling['data_abre']}</strong>: ".$resultado -> UserDate($campo['DATA_ABRE'],'d/m/Y')." </td>
<td align=\"left\"><strong>{$ling['data_inicial']}</strong>: ".$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y')." </td>
<td align=\"left\"><strong>{$ling['data_fim']}</strong>: ".$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y')." </td>
</tr>
<tr>
<td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc1_min']}</strong>: ".VoltaValor(AREAS,"DESCRICAO","MID",$campo['MID_AREA'],$tdb[AREAS]['dba'])." </td>
<td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_loc2_min']}</strong>: ".VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."  </td>
<td align=\"left\" colspan=\"2\"><strong>{$ling['rel_desc_cc2']}</strong>: ".VoltaValor(CENTRO_DE_CUSTO,"DESCRICAO","MID",$campo['CENTRO_DE_CUSTO'],$tdb[CENTRO_DE_CUSTO]['dba'])." </td>
</tr><tr>
<td align=\"left\" colspan=\"5\"><strong>{$ling['sol_objeto']}</strong>: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])." 
<strong>{$ling['sol_posicao']}</strong>:".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])." </td>
<td align=\"left\" ><strong>{$ling['solicitante']}: </strong>".$campo['SOLICITANTE']."
</td></tr></table>";
		$txt.="<table cellpadding=\"0\" cellspacing=\"0\"  border=\"1\" bordercolor=\"black\" id=\"dados_processados\">
<tr><td colspan=\"6\" align=\"right\"><font size=\"2\"><strong>{$ling['rel_desc_mo']}</strong></font></td></tr>
<tr>
<th>{$ling['nome']}</th>
<th>{$ling['equipe']}</th>
<th>{$ling['data_inicial']}</th>
<th>{$ling['data_fim']}</th>
<th>{$ling['rel_desc_extra']}</th>
<th>{$ling['rel_desc_custo_total']}</th>
</tr>";	

// Tratamento para os campos de hora 
if($manusis['db'][$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]['driver'] == "oci8"){
    $sqlOPM = "SELECT OP.*, TO_CHAR(HORA_INICIO, 'HH24:MI:SS') AS HORA_INICIO, TO_CHAR(HORA_FINAL, 'HH24:MI:SS') AS HORA_FINAL FROM ".ORDEM_PLANEJADO_MADODEOBRA." OP WHERE OP.MID_ORDEM = '".$campo['MID']."'";
}
else{
    $sqlOPM = "SELECT * FROM ".ORDEM_PLANEJADO_MADODEOBRA." WHERE MID_ORDEM = '".$campo['MID']."'";
}
$tmp=$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> Execute($sqlOPM);
if(!$tmp){ 

    erromsg ("Erro ao buscar por ".ORDEM_PLANEJADO_MADODEOBRA." em <br /> 
        Linha ".__LINE__." <br /> 
        ".$dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']] -> ErrorMsg()."<br /> $sqlOPM
    ");
}
while (!$tmp->EOF) {
    $ca=$tmp->fields;
    $mo_total=(float)$mo_total + $ca['CUSTO'];
    $txt.= "<tr class=\"cor2\">
<td>".VoltaValor(FUNCIONARIOS,"NOME","MID",$ca['MID_FUNCIONARIO'],$tdb[FUNCIONARIOS]['dba'])."</td>
<td>".VoltaValor(EQUIPES,"DESCRICAO","MID",$ca['EQUIPE'],$tdb[EQUIPES]['dba'])."</td>
<td>".NossaData($ca['DATA_INICIO'])." ".$ca['HORA_INICIO']."</td>
<td>".NossaData($ca['DATA_FINAL'])." ".$ca['HORA_FINAL']."</td>
<td>".number_format((float)$ca['CUSTO_EXTRA'],2,",",".")."</td>
<td>".number_format((float)$ca['CUSTO'],2,",",".")."</td>
</td></tr>";
			$tmp->MoveNext();
		}
		$txt.="<tr><td colspan=\"6\" align=\"center\">{$ling['rel_desc_toal']}: ".number_format((float)$mo_total,2,",",".")."</td></tr></table>";

		$txt.="<table cellpadding=\"0\" cellspacing=\"0\"  border=\"1\" bordercolor=\"black\" id=\"dados_processados\">
<tr><td colspan=\"4\" align=\"right\"><font size=\"2\"><strong>{$ling['rel_desc_mat']}</strong></font></td></tr>
<tr>
<th>{$ling['rel_desc_material']}</th>
<th>{$ling['qtd_qtd']}</th>
<th>{$ling['rel_desc_custo_uni']}</th>
<th>{$ling['rel_desc_custo_total']}</th>
</tr>";	
		$tmp=$dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$campo['MID']."'");
		while (!$tmp->EOF) {
			$ca=$tmp->fields;
			$mtotal=(float)$mtotal + $ca['CUSTO_TOTAL'];
			$txt.="<tr class=\"cor2\">
<td>".VoltaValor(MATERIAIS,"DESCRICAO","MID",$ca['MID_MATERIAL'],$tdb[MATERIAIS]['dba'])."</td>
<td>".$ca['QUANTIDADE']."</td>
<td>".number_format((float)$ca['CUSTO_UNITARIO'],2,",",".")."</td>
<td>".number_format((float)$ca['CUSTO_TOTAL'],2,",",".")."</td>
</td></tr>";
			$tmp->MoveNext();
		}
		$txt.="<tr><td colspan=\"4\" align=\"center\">{$ling['rel_desc_total']}: ".number_format((float)$mtotal,2,",",".")."</td></tr></table>";

		$txt.="<table cellpadding=\"0\" cellspacing=\"0\"  border=\"1\" bordercolor=\"black\" id=\"dados_processados\">
<tr><td colspan=\"2\" align=\"right\"><font size=\"2\"><strong>{$ling['rel_desc_outros_custos']}</strong></font></td></tr>
<tr>
<th>{$ling['descricao']}</th>
<th>{$ling['rel_desc_custo']}</th>
</tr>";	
		$tmp=$dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_CUSTOS." WHERE MID_ORDEM = '".$campo['MID']."'");
		while (!$tmp->EOF) {
			$ca=$tmp->fields;
			$ototal=(float)$ototal + $ca['CUSTO'];
			$txt.="<tr class=\"cor2\">
<td>".$ca['DESCRICAO']."</td>
<td>".number_format((float)$ca['CUSTO'],2,",",".")."</td>
</td></tr>";
			$tmp->MoveNext();
		}
		$total=(float)$mo_total+$mtotal+$ototal;
		$total2=$mo_total+$total2;
		$total3=$mtotal+$total3;
		$total4=$ototal+$total4;
		$txt.="<tr><td colspan=\"2\" align=\"center\">{$ling['rel_desc_total']}: ".number_format((float)$ototal,2,",",".")."</td></tr></table><table id=\"dados_processados\" border=0 width=\"100%\"><tr><td aling=\"left\"><strong>{$ling['rel_desc_custo_total_os']} ".$campo['NUMERO']." ({$ling['rel_desc_sifrao']}): ".number_format((float)$total,2,",",".")."</strong></td></tr></table><hr />";
		unset($ototal);
		unset($mtotal);
		unset($mo_total);
		$iii++;
		$stotal=(float)$total+$stotal;
		$resultado->MoveNext();
	}
	$iii="$iii <br /> <strong>{$ling['rel_desc_custo_total_mo']}: ".number_format((float)$total2,2,",",".")."</strong> <br />
	<strong>{$ling['rel_desc_custo_total_mat']}: ".number_format((float)$total3,2,",",".")."</strong><br />
	<strong>{$ling['rel_desc_custo_total_outros']}: ".number_format((float)$total4,2,",",".")."</strong><br /><br/>
	<strong><font size=\"2\">{$ling['rel_desc_custo_total2']}: ".number_format((float)$stotal,2,",",".")."</font></strong>";
	if ($tipo == 1) $filtro=$tdb[AREAS]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
	if ($tipo == 2) $filtro=$tdb[SETORES]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
	if ($tipo == 3) $filtro=$tdb[MAQUINAS]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
	if ($tipo == 4) $filtro=$tdb[CENTRO_DE_CUSTO]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
	if ($tipo == 5) $filtro="{$ling['todos2']} (".$_GET['datai']." a ".$_GET['dataf'].")";
	if ($tipo == 6) $filtro="{$ling['rel_desc_num_ord']}";
	
	if ($relatorio != "") relatorio_padrao($ling['rel_custo_os'],$filtro,$iii,$txt,1);
	else exportar_word($ling['rel_custo_os'],$filtro,$iii,$txt,$_GET['papel_orientacao']);
}


else {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../lib/movediv.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['rel_custo_os']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"relatorio_custoos.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>".$ling['filtros']."</legend>";
	if ($tipo == 1) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t1\" value=\"1\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t1\" value=\"1\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t1\">".$tdb[AREAS]['DESC']."</label><br clear=\"all\" />";

	if ($tipo == 2) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t2\" value=\"2\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t2\" value=\"2\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t2\">".$tdb[SETORES]['DESC']."</label><br clear=\"all\" />";


	if ($tipo == 3) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t3\" value=\"3\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t3\" value=\"3\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t3\">".$tdb[MAQUINAS]['DESC']."</label><br clear=\"all\" />";


	if ($tipo == 4) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t4\" value=\"4\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t4\" value=\"4\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t4\">".$tdb[CENTRO_DE_CUSTO]['DESC']."</label><br clear=\"all\" />";
	
	if ($tipo == 6) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t6\" value=\"6\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t6\" value=\"6\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t6\">{$ling['rel_desc_num_ord']}</label><br clear=\"all\" />";

	if ($tipo == 5) echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t5\" value=\"5\" onclick=\"document.forms[0].submit();\" checked=\"checked\" />";
	else echo "<input class=\"campo_check\" type=\"radio\" name=\"tipo\" id=\"t5\" value=\"5\" onclick=\"document.forms[0].submit();\" />";
	echo "<label for=\"t5\">{$ling['rel_desc_todos']}</label><br clear=\"all\" />";
	if ($tipo == 1) FormSelect("obj",AREAS,$_GET['obj'],"DESCRICAO","MID",$tdb[AREAS]['dba'],0);
	if ($tipo == 2) FormSelect("obj",SETORES,$_GET['obj'],"DESCRICAO","MID",$tdb[SETORES]['dba'],0);
	if ($tipo == 3) FormSelect("obj",MAQUINAS,$_GET['obj'],"DESCRICAO","MID",$tdb[MAQUINAS]['dba'],0);
	if ($tipo == 4) FormSelect("obj",CENTRO_DE_CUSTO,$_GET['obj'],"DESCRICAO","MID",$tdb[CENTRO_DE_CUSTO]['dba'],0);
	if ($tipo == 6) echo "<input type=\"text\" id=\"obj\" class=\"campo_text\" value=\"".$_GET['obj']."\" name=\"obj\" size=\"10\" maxlength=\"10\" />";

	echo "</fieldset>";
	echo "<fieldset><legend>{$ling['rel_desc_periodo']}</legend>
<label for=\"datai\">{$ling['data_inicio']}</label>
<input onkeypress=\"return ajustar_data(this, event)\" type=\"text\" id=\"datai\" class=\"campo_text\" value=\"".$_GET['datai']."\" name=\"datai\" size=\"10\" maxlength=\"10\" /> 
<br clear=\"all\" />
<label for=\"dataf\">{$ling['data_fim']}</label>
<input onkeypress=\"return ajustar_data(this, event)\" type=\"text\" id=\"dataf\" class=\"campo_text\" value=\"".$_GET['dataf']."\" name=\"dataf\" size=\"10\" maxlength=\"10\" /> 

	</fieldset>";
	echo "<fieldset>
<legend>".$ling['papel_orientacao']."</legend>
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
<label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
<label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
</fieldset>
<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
<input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
</form><br />
</div>
</div>
</body>
</html>";
}
?>