<?

/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Nota: Relatorio
 */
// Funções do Sistema
if (!require("lib/mfuncoes.php"))
    die($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php"))
    die($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/" . $manusis['idioma'][0] . ".php"))
    die($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php"))
    die($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php"))
    die($ling['bd01']);
// Formulários
elseif (!require("lib/forms.php"))
    die($ling['bd01']);
// Autentificação
elseif (!require("lib/autent.php"))
    die($ling['autent01']);

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"" . $ling['xml'] . "\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"temas/" . $manusis['tema'] . "/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"" . $manusis['tema'] . "\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE")
    echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">";

echo "<div id=\"lt_tabela\">
<h2>" . $ling['ATUALIZANDO_PROG'] . "</h2>
<br />\n";

$init = true;
$msg = "";
// Recuperando dados
$pro = ($_POST['aprog']) ? $_POST['aprog'] : $_GET['aprog'];

$ano = "";

if(isset($_POST['ano']) OR isset($_GET['ano'])){
    $ano = ($_POST['ano']) ? $_POST['ano'] : $_GET['ano'];
    $dtFim = explode('/', $ano);
}

if(is_null($pro)){
    $init = false;
    $msg .= "<li>&Eacute; obrigat&oacute;rio selecionar pelo menos uma programa&ccedil;&atilde;o</li>\n";
}

if(trim($ano) == ''){
    $init = false;
    $msg .= "<li>&Eacute; obrigat&oacute;rio informar uma data v&aacute;lida</li>\n";
}

// Caso não exista passou data 
elseif(!isset($dtFim[0]) OR !isset($dtFim[1]) OR !isset($dtFim[2])){
    $init = false;
    $msg .= "<li>A data inform&aacute;da &eacute; inv&aacute;lida ({$ano})</li>\n";
}

elseif(!is_numeric($dtFim[0]) OR !is_numeric($dtFim[1]) OR !is_numeric($dtFim[2])){
    $init = false;
    $msg .= "<li>A data inform&aacute;da &eacute; inv&aacute;lida ({$ano})</li>\n";
}

elseif(!checkdate($dtFim[1], $dtFim[0], $dtFim[2])){
    $init = false;
    $msg .= "<li>A data inform&aacute;da &eacute; inv&aacute;lida ({$ano})</li>\n";
}

else{
    $ano = ($_POST['ano']) ? DataSQL($_POST['ano']) : DataSQL($_GET['ano']);
}
if ($init == true) {

    $pag_ini = (int) $_GET['pag_ini'];
    $pag_tam = (int) $_GET['pag_tam'];

    if ($pag_tam == 0) {
        $pag_tam = 1;
    }


    $progs = array();
    $dt_ini = array();
    $dt_fim = array();

    if (is_array($pro)) {
        foreach ($pro as $prog2 => $key) {
            $sql = "SELECT * FROM " . PROGRAMACAO . " WHERE MID = '$prog2' AND STATUS != 3";
            $tmp_prog = $dba[0]->Execute($sql);

            $campo_prog = $tmp_prog->fields;
            $tipo = $campo_prog['TIPO'];
            $plano = $campo_prog['MID_PLANO'];
            $data_final = $ano;
            $prog_mid = (int) $campo_prog['MID'];

            $print = 1;
            $thcor = 'cor1';

            if ($prog_mid != 0) {

                $tmp6 = $dba[0]->SelectLimit("SELECT MID, DATA_PROG_ORIGINAL FROM " . ORDEM_PLANEJADO . " WHERE MID_PROGRAMACAO = $prog_mid AND TIPO = $tipo AND STATUS = 2 ORDER BY DATA_PROG_ORIGINAL DESC", 1, 0);
                $cao = $tmp6->fields;

                if ($cao['DATA_PROG_ORIGINAL'] == "") {
                    $data_inicial = $campo_prog['DATA_INICIAL'];
                }
                else {
                    $data_inicial = $cao['DATA_PROG_ORIGINAL'];
                }

                echo "<table id=\"lt_tabela\" class=\"tabela\">
			<tr>
			<td align=\"left\">\n";

                if ($tipo == 1) {
                    echo "{$ling['PLANO_M']}: " . VoltaValor(PLANO_PADRAO, "DESCRICAO", "MID", $plano, 0) . " <br />
                    {$ling['OBJ_MANUTENCAO']}: " . VoltaValor(MAQUINAS, "DESCRICAO", "MID", $campo_prog['MID_MAQUINA'], 0) . " <br />
                    {$ling['DATA_INICIO_M']}: " . NossaData($campo_prog['DATA_INICIAL']) . " <br />
                    {$ling['data_ultima_os']}: " . NossaData($data_inicial) . " <br />
                    {$ling['DATA_FINAL_M']}: " . NossaData($ano);
                }
                elseif ($tipo == 2) {
                    echo "{$ling['ROTA_M']}: " . VoltaValor(PLANO_ROTAS, "DESCRICAO", "MID", $plano, 0) . " <br />
                    {$ling['DATA_INICIO_M']}: " . NossaData($campo_prog['DATA_INICIAL']) . " <br />
                    {$ling['data_ultima_os']}: " . NossaData($data_inicial) . " <br />
                    {$ling['DATA_FINAL_M']}: " . NossaData($ano);
                }

                echo "</th>
			</tr>\n";

                if (VoltaTime("00:00:00", NossaData($campo_prog['DATA_FINAL'])) >= VoltaTime("00:00:00", NossaData($ano)) and $pag_ini == 0) {
                    echo "<tr><td style=\"color:red\"><h3>{$ling['ord_data_final_programada_maior_estabelecida']}</h3></td></tr>\n";
                }
                else {
                    $progs[] = $prog_mid;
                    $dt_ini[] = $data_inicial;
                    $dt_fim[] = $data_final;
                }

                echo "</table>
            <br clear=\"all\">\n";
            } //fim desta programação
        }
    }
    if (count($progs) > 0) {
        // Rodando atualização paginada
        $pag = grava_programacao_data($dt_ini, $dt_fim, 0, $tipo, $plano, $progs, 1, $pag_tam, $pag_ini, false, true, false);


        // Ainda tem o que rodar
        if ($pag !== TRUE) {
            $prog_url = "";
            foreach ($pro as $prog2 => $key) {
                $prog_url .= "&aprog[$prog2]=$key";
            }

            $pag_tam = 1000;

            echo "<script>
            mostraCarregando (true);
            self.window.location = 'atualizar_programacao.php?ano=" . NossaData($ano) . "&pag_tam=$pag_tam&pag_ini=$pag$prog_url';
            </script>\n";
            exit();
        }
        else {
            echo "<br /><br /><h3>{$ling['PROG_ATUALI_COM_SUCESSO']}</h3>\n";
            echo "<script>
            mostraCarregando (false);
            </script>\n";
        }
    }

    echo "</div>";
}
else{
    echo "<div style='width:97%; background-color:#E7EAEF; padding:10px; margin:10px;'>";
    
    erromsg("<strong>
                N&atilde; foi poss&iacute;vel atualizar esta programa&ccedil;&atilde;o
                <br />
            </strong>
            <br />
            <ul>{$msg}</ul>");
    echo "</div>";
}
?>
