<?
/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Autor: Fernando Cosentino
 * Nota: Relatorio
 */
// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}


$busca=LimpaTexto($_GET['busca']);
$campo=$_GET['cc'];

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body>
<div id=\"corpo_relatorio\">
<script> window.resizeTo(500,500)</script>
<div id=\"lt_tabela\">
<table widht=\"100%\" cellpadding=\"3\" cellspacing=\"1\">";


$campos_sql = "" . ORDEM . ".*";
	
// Oracle
if ($manusis['db'][$tdb[ORDEM]['dba']]['driver'] == 'oci8') {
	// Campos TIME
	if (is_array($tdb_datas[ORDEM]['T'])) {
		foreach ($tdb_datas[ORDEM]['T'] as $campo) {
			$campos_sql .= ", TO_CHAR(" . ORDEM . ".{$campo}, 'HH24:MI:SS') AS $campo";
		}
	}
	// Campos DATETIME
	if (is_array($tdb_datas[ORDEM]['DT'])) {
		foreach ($tdb_datas[ORDEM]['DT'] as $campo) {
			$campos_sql .= ", TO_CHAR(" . ORDEM . ".{$campo}, 'YYYY-MM-DD HH24:MI:SS') AS $campo";
		}
	}
}


$resultado = $dba[0] -> Execute("SELECT {$campos_sql} FROM ".ORDEM_PLANEJADO." WHERE MID = '$busca' ");
if (!$resultado) echo $dba[0] -> ErrorMsg();
$ca=$resultado->fields;
$osmid = (int) $ca['MID'];

if ($ca['STATUS'] == 1) $st="ABERTO";
if ($ca['STATUS'] == 2) $st="FECHADA";
if ($ca['STATUS'] == 3) $st="CANCELADA";

echo "<tr class=\"white\">
<td><p align=center><b>".$ling['ordem_n']." ".$ca['NUMERO']." ($st)</b></p><p align=left><BR />
<b>".$ling['DATA_DE_ABERTURA']." </b>".NossaData($ca['DATA_ABRE'])." <br>
<b>{$ling['prazo_m']}: </b>".NossaData($ca['DATA_PROG'])." <br>";

if ($ca['STATUS'] == 2) echo "<b>".$ling['data_de_termino']." </b>".NossaData($ca['DATA_FINAL'])."<BR>";

echo "<b>{$ling['rel_desc_resp']}: </b>".htmlentities(VoltaValor(FUNCIONARIOS,"NOME","MID",$ca['RESPONSAVEL'],$tdb[FUNCIONARIOS]['dba']))."<br>";
echo "<b>{$ling['texto_m']}: </b>".$ca['TEXTO']."<br><br>";

if ($ca['TIPO'] != 2) echo "<b>{$ling['rel_desc_obj2']}: </b>".htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$ca['MID_MAQUINA'],$cm))."<br>
<b>{$ling['rel_desc_pos']}: </b>".htmlentities(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$ca['MID_CONJUNTO'],$cc))."<br>";

if ($ca['MID_EQUIPAMENTO']) echo "<b>{$ling['componente']}:</b> ".htmlentities(VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$ca['MID_EQUIPAMENTO'],0))."<br>";

echo "<br>";

if (($ca['TIPO'] == 4) or ($ca['TIPO'] == 2) or ($ca['TIPO'] == 1)) {
    echo "<b>{$ling['tipo_m']}: </b>".htmlentities(VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$ca['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba']))."<br>";
}

if (((int)$ca['TIPO'] == 0) or ($campo['TIPO'] == 4)) {
    echo "<b>{$ling['rel_desc_tp_serv']}: </b>".htmlentities(VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$ca['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba']))."<br>";
    echo "<b>{$ling['solicitante_m']}: </b>".htmlentities(VoltaValor(ORDEM_PLANEJADO,"SOLICITANTE","MID",$ca['MID'],$tdb[ORDEM_PLANEJADO]['dba']))."<br>";
    echo "<b>{$ling['natureza_m']}: </b>".htmlentities(VoltaValor(NATUREZA_SERVICOS,"DESCRICAO","MID",$ca['NATUREZA'],$tdb[NATUREZA]['dba']))."<br><br>";
    echo "<b>{$ling['rel_desc_def']}: </b>".htmlentities(VoltaValor(DEFEITO,"DESCRICAO","MID",$ca['DEFEITO'],$tdb[DEFEITO]['dba']))."<br>";
    echo "".htmlentities($ca['DEFEITO_TEXTO'])."<br>";
    echo "<b>{$ling['rel_desc_cau']}: </b>".htmlentities(VoltaValor(CAUSA,"DESCRICAO","MID",$ca['CAUSA'],$tdb[CAUSA]['dba']))."<br>";
    echo "".htmlentities($ca['CAUSA_TEXTO'])."<br>";
    echo "<b>{$ling['rel_desc_solu']}: </b>".htmlentities(VoltaValor(SOLUCAO,"DESCRICAO","MID",$ca['SOLUCAO'],$tdb[SOLUCAO]['dba']))."<br>";
    echo "".htmlentities($ca['SOLUCAO_TEXTO'])."<br><br>";
}

// ALOCA��O DE M�O-DE-OBRA
echo "<br />";
echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">
<tr><td colspan=\"5\" aling=\"center\"><strong><center>{$tdb[ORDEM_MO_ALOC]['DESC']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[ORDEM_MO_ALOC]['MID_FUNCIONARIO'] . "</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[ORDEM_MO_ALOC]['MID_ESPECIALIDADE'] . "</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[FUNCIONARIOS]['EQUIPE'] . "</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[ORDEM_MO_ALOC]['TEMPO'] . "</center></strong></span></td>
</tr>";

$sql = "SELECT * FROM ".ORDEM_MO_ALOC." WHERE MID_ORDEM = '".$ca['MID']."' ORDER BY MID_ESPECIALIDADE ASC";
if(! $obj = $dba[0] -> Execute($sql)) {
    erromsg("{$ling['arquivo']}: " . __FILE__ . "<br />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> ErrorMsg() . "<br />" . $sql);
}
while (! $obj->EOF) {
    $obj_campo = $obj->fields;

    // EQUIPE
    $equip = VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $obj_campo['MID_FUNCIONARIO']);

    // ESPECIALIDADE
    $esp = VoltaValor(ESPECIALIDADES, "DESCRICAO", "MID", $obj_campo['MID_ESPECIALIDADE'], 0);
    $esp = ($esp != "")? $esp : $ling['ord_sem_esp'];

    echo "<tr>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . htmlentities(VoltaValor(FUNCIONARIOS, 'NOME', 'MID', $obj_campo['MID_FUNCIONARIO'])) . "</td>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . htmlentities($esp) . "</td>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . htmlentities(VoltaValor(EQUIPES, 'DESCRICAO', 'MID', $equip)) . "</td>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . $obj_campo['TEMPO'] . "</td>
    </tr>";

    $obj->MoveNext();
}

echo "</table>";


// ALOCA��O DE M�O-DE-OBRA RESUMO
echo "<br />";
echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">
<tr><td colspan=\"5\" aling=\"center\"><strong><center>{$tdb[ORDEM_MO_PREVISTO]['DESC']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[ORDEM_MO_PREVISTO]['MID_ESPECIALIDADE'] . "</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[ORDEM_MO_PREVISTO]['QUANTIDADE'] . "</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>" . $tdb[ORDEM_MO_PREVISTO]['TEMPO'] . " (h)</center></strong></span></td>
</tr>";

$sql = "SELECT * FROM ".ORDEM_MO_PREVISTO." WHERE MID_ORDEM = '".$ca['MID']."' ORDER BY MID_ESPECIALIDADE ASC";
if(! $obj = $dba[0] -> Execute($sql)) {
    erromsg("{$ling['arquivo']}: " . __FILE__ . "<br />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> ErrorMsg() . "<br />" . $sql);
}
while (! $obj->EOF) {
    $obj_campo = $obj->fields;

    echo "<tr>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $obj_campo['MID_ESPECIALIDADE'])) . "</td>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . $obj_campo['QUANTIDADE'] . "</td>
    <td style='border: 1px solid black;'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . $obj_campo['TEMPO'] . "</td>
    </tr>";

    $obj->MoveNext();
}

echo "</table>";

// Material Previsto
echo "<br><table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
echo "<tr><td colspan=\"5\" aling=\"center\"><strong><center>{$tdb[ORDEM_MAT_PREVISTO]['DESC']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>{$tdb[ORDEM_MAT_PREVISTO]['MID_MATERIAL']}</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>{$tdb[ORDEM_MAT_PREVISTO]['QUANTIDADE']}</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>{$tdb[ORDEM_MAT_PREVISTO]['CUSTO']} (R$)</center></strong></span></td>
</tr>";
$obj=$dba[0] -> Execute("SELECT * FROM ".ORDEM_MAT_PREVISTO." WHERE MID_ORDEM = '".$ca['MID']."'");

while (!$obj->EOF) {
    $obj_campo = $obj -> fields;

    // Unidade
    $uni = VoltaValor(MATERIAIS, "UNIDADE", "MID", $obj_campo['MID_MATERIAL'], 0);

    echo "<tr>";
    echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;" . htmlentities(VoltaValor(MATERIAIS, "DESCRICAO", "MID", $obj_campo['MID_MATERIAL'], 0)) . "</span></td>";
    echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['QUANTIDADE']." " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", $uni, 0)) . "</span></td>";
    echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".number_format($obj_campo['CUSTO'], 2, ',', '.')."</span></td>";
    echo "</tr>\n";
    $obj->MoveNext();
}
echo "</table>";
echo "<br>";

// Oracle
if ($manusis['db'][$tdb[ORDEM_MAQ_PARADA]['dba']]['driver'] == 'oci8') {
	$sql = "SELECT " . ORDEM_MAQ_PARADA . ".*, TO_CHAR(" . ORDEM_MAQ_PARADA . ".HORA_INICIO, 'HH24:MI:SS') AS HORA_INICIO, TO_CHAR(" . ORDEM_MAQ_PARADA . ".HORA_FINAL, 'HH24:MI:SS') AS HORA_FINAL FROM " . ORDEM_MAQ_PARADA . " WHERE " . ORDEM_MAQ_PARADA . ".MID_ORDEM = $osmid ORDER BY " . ORDEM_MAQ_PARADA . ".DATA_INICIO, " . ORDEM_MAQ_PARADA . ".HORA_INICIO DESC";
}
else {
	$sql = "SELECT * FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = '$osmid' ORDER BY DATA_INICIO,HORA_INICIO DESC";
}
$obj = $dba[0] -> Execute($sql);

if (! $obj->EOF) {
    echo "<br><table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
    echo "<tr><td colspan=\"5\" aling=\"center\"><strong><center>".$ling['TEMPO_PARADA_OBJ_MANUTENCAO']."</center></strong></td></tr>
    <tr>
    <td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['DATA_INICIO_M']."</center></strong></span></td>
    <td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['DATA_FINAL_M']."</center></strong></span></td>
    <td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['HORA_INICIO_M']."</center></strong></span></td>
    <td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['HORA_FINAL_M']."</center></strong></span></td>
    <td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['TEMPO_M']."</center></strong></span></td>
    </tr>";
    

    while (! $obj->EOF) {
        $obj_campo_parada = $obj -> fields;
        if ($ordexec == 0) $txt .= "<tr>";
        else echo "<tr bgcolor=\"gray\">";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".NossaData($obj_campo_parada['DATA_INICIO'])."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".NossaData($obj_campo_parada['DATA_FINAL'])."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_parada['HORA_INICIO']."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_parada['HORA_FINAL']."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_parada['TEMPO']."</span></td>";
        echo "</tr>\n";
        $obj->MoveNext();
    }
    echo "</table>";
}
echo "<br>";

echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
echo "<tr><td colspan=\"4\" aling=\"center\"><strong><center>{$ling['MAO_DE_OBRA_APONTA']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black;background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><b><center>".$ling['FUNCIONARIO_M']."</b></b></span></td>
<td style='border: 1px solid black;background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><b><center>".$ling['EQUIPE_M']."</center></b></span></td>
<td style='border: 1px solid black;background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><b><center>".$ling['DATA_INICIO_M']."</center></b></span></td>
<td style='border: 1px solid black;background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><b><center>".$ling['DATA_FINAL_M']."</center></b></span></td>
</tr>";

// Oracle
if ($manusis['db'][$tdb[ORDEM_MAODEOBRA]['dba']]['driver'] == 'oci8') {
	$sql = "SELECT " . ORDEM_MAODEOBRA . ".*, TO_CHAR(" . ORDEM_MAODEOBRA . ".HORA_INICIO, 'HH24:MI:SS') AS HORA_INICIO, TO_CHAR(" . ORDEM_MAODEOBRA . ".HORA_FINAL, 'HH24:MI:SS') AS HORA_FINAL FROM " . ORDEM_MAODEOBRA . " WHERE " . ORDEM_MAODEOBRA . ".MID_ORDEM = $osmid ORDER BY " . ORDEM_MAODEOBRA . ".DATA_INICIO, " . ORDEM_MAODEOBRA . ".HORA_INICIO DESC";
}
else {
	$sql = "SELECT * FROM " . ORDEM_MAODEOBRA . " WHERE MID_ORDEM = $osmid ORDER BY DATA_INICIO, HORA_INICIO DESC";
}

$obj = $dba[0] -> Execute($sql);
while (!$obj->EOF) {
    $obj_campo = $obj -> fields;
    echo  "<tr>";
    echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(FUNCIONARIOS,"NOME","MID",$obj_campo['MID_FUNCIONARIO'],$tdb[FUNCIONARIOS]['dba'])."</span></td>";
    echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(EQUIPES,"DESCRICAO","MID",$obj_campo['EQUIPE'],$tdb[EQUIPES]['dba'])."</span></td>";
    echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".NossaData($obj_campo['DATA_INICIO'])." ".$obj_campo['HORA_INICIO']."</span></td>";
    echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".NossaData($obj_campo['DATA_FINAL'])." ".$obj_campo['HORA_FINAL']."</span></td>";
    echo  "</tr>\n";
    $obj->MoveNext();
}
echo "</table><br>";

$obj=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT MID FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '".$ca['MID']."'");
$obj_campo = $obj -> fields;
$objl=$i=count($obj -> getrows());
if ($objl != 0) {
    echo "<table id=\"dados_processados\" cellpadding=\"2\" width=\"100%\"><tr>
    <tr><td colspan=\"3\" aling=\"center\" style='border: 1px solid black'><strong><center><b>".$tdb[ORDEM_REPROGRAMA]['DESC']."</b></center></strong></td></tr>
    <tr>
    <td style='border: 1px solid black;background-color: #cccccc'><strong><center>".$tdb[ORDEM_REPROGRAMA]['MOTIVO']."</center></strong></td>
    <td style='border: 1px solid black;background-color: #cccccc'><strong><center>".$tdb[ORDEM_REPROGRAMA]['DATA']."</center></strong></td>
    <td style='border: 1px solid black;background-color: #cccccc'><strong><center>".$tdb[ORDEM_REPROGRAMA]['DATA_ORIGINAL']."</center></strong></td>
    </tr>"; 
    $tmp=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '".$ca['MID']."'");
    while (!$tmp->EOF) {
        $obj_campo=$tmp->fields;
        echo "<tr>
        <td style='border: 1px solid black'>".$obj_campo['MOTIVO']."</td>
        <td style='border: 1px solid black'>".$tmp -> UserDate($obj_campo['DATA'],'d/m/Y')."</td>
        <td style='border: 1px solid black'>".$tmp -> UserDate($obj_campo['DATA_ORIGINAL'],'d/m/Y')."</td>
        </tr>";
        $tmp->MoveNext();
    }
    echo "</table><br />";
}
////

if (($ca['TIPO'] == 1) or ($ca['TIPO'] == 2)) {
    $tipo=$ca['TIPO'];
    $plano_mid=VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$ca['MID_PROGRAMACAO'],$tdb[PROGRAMACAO]['dba']);
    if (($tipo == 1) and ($plano_mid == 0) and ($campo['CHECKLIST'] != "")) {
        $txt.= "<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">";
        $ec=explode(",",$campo['CHECKLIST']);

        $txt.= "<tr><td align=\"center\"><strong>{$ling['checklist_m']}</strong></td></tr>";
        for ($f=0; $ec[$f] != ""; $f++) {
            $txt.= "<tr><td>";
            $txt.=VoltaValor(PLANO_INDIVIDUAL_ATIVIDADES,"DESCRICAO","MID",$ec[$f],0);
            $txt.="</td></tr>\n";
        }
        $txt.= "</table>";
    }
    ################ - PREVENTIVA - ################
    elseif ($tipo == 1) {

        $plano_mid=VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$ca['MID_PROGRAMACAO'],$tdb[PROGRAMACAO]['dba']);

        echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
        $plano_texto="PLANO: ".VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$plano_mid,0)."";
        if ($plano_texto == "") $plano_texto="PLANO REMOVIDO";
        echo "
        <tr>
        <td style='border: 0px solid black' colspan=\"7\"><strong><center>$plano_texto</center></strong></td>
        </tr>
        <tr>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['N']."</span></center></td>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['DESCRICAO_M']."</span></center></td>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['I_M']."</span></center></td>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['PARTE_POSICAO']."</span></center></td>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['ESP']."</span></center></td>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['TEMPO_PREVISTO_M']."</span></center></td>
        <td style='border: 1px solid black;background-color: #CCCCCC'><center><span style='font-size:7.5pt;'>".$ling['DIAGNOSTICO']."</span></center></td>
        </tr>";

        $obj=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_PREV." WHERE MID_ORDEM = '".$ca['MID']."' AND MID_PLANO_PADRAO = $plano_mid ORDER BY NUMERO ASC");
        if (!$obj) erromsg($dba[0]->ErrorMsg());
        while (!$obj->EOF) {
            $obj_campo = $obj -> fields;
            $esp=VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$obj_campo['ESPECIALIDADE'],0);
                
            $tmp_mat[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat[$obj_campo['MID_MATERIAL']];
            $tmp_mat_total[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat_total[$obj_campo['MID_MATERIAL']];
                
            $te=explode(":",$obj_campo['TEMPO_PREVISTO']);
            $tmp_mk=mktime((int)$te[0],(int)$te[1],(int)$te[2],0,0,0);
            $tmp_esp[$espm][]=$tt;
            $tmp_esp_total[$espm][]=$tt;
                
            if ($obj_campo['DIAGNOSTICO']==1) {
                $diag = $ling['ok'];
            }
            if ($obj_campo['DIAGNOSTICO']==2) {
                $diag = $ling['nao_ok'];
            }

            echo "<tr>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['NUMERO']."</span></td>";
            echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['TAREFA']."</span></td>";
            echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['INSTRUCAO_DE_TRABALHO']."</span></td>";
            if ($obj_campo['MID_CONJUNTO'] != 0) echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0)."</span></td>";
            else echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['PARTE']."</span></td>";
            echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;$esp</span></td>";
            echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['TEMPO_PREVISTO']."</span></td>";
            echo  "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;$diag</span></td>";
            echo  "</tr>";
            if ($obj_campo['MID_MATERIAL'] != 0) {
                echo  "<tr>\n";
                echo  "<td colspan=5>&nbsp;".VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0)."</td>";
                echo  "<td>&nbsp;".$obj_campo['QUANTIDADE']."</td>";
                echo  "</tr>\n";
            }

            $obj->MoveNext();
        }
        echo  "</table>";
    }
    ################
    ################ - ROTA - ################
    elseif ($tipo == 2) {

        $plano_mid=VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$ca['MID_PROGRAMACAO'],$tdb[PROGRAMACAO]['dba']);

        echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
        
        $plano_texto="{$ling['PLANO_M']}: ".VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$plano_mid,0)."";
        if ($plano_texto == "") $plano_texto=$ling['rel_desc_plano_removido'];
        
        echo "<tr>
        <td style='border: 0px solid black' colspan=\"8\"><strong><center>$plano_texto</center></strong></td>
        </tr>";
        
        $sql2 = "SELECT a.*,c.POSICAO FROM ".ORDEM_LUB." a, ".ROTEIRO_ROTAS." c WHERE a.MID_MAQUINA = c.MID_MAQUINA and a.MID_PLANO = c.MID_PLANO and a.MID_ORDEM = '".$ca['MID']."' ORDER BY c.POSICAO ASC";
        $ultimo_obj ='';
        if(!$obj=$dba[$tdb[ORDEM_LUB]['dba']] -> Execute($sql2)){
            erromsg("Erro ao buscar por ".ORDEM_LUB." em:<br />
            Linha: ".__LINE__."<br />
            Erro: ".$dba[$tdb[ORDEM_LUB]['dba']]->ErrorMsg()."<br />
            SQL: $sql2");
        }
        while (!$obj->EOF) {
            //die($campo['MID']);
            $obj_campo = $obj -> fields;
                
                
            $esp=VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$obj_campo['ESPECIALIDADE'],0);
                
                
            $tmp_mat[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat[$obj_campo['MID_MATERIAL']];
            $tmp_mat_total[$obj_campo['MID_MATERIAL']]=$obj_campo['QUANTIDADE'] + $tmp_mat_total[$obj_campo['MID_MATERIAL']];
                
            $te=explode(":", $obj_campo['TEMPO_PREVISTO']);
            //$tmp_mk=mktime($te[0],$te[1],$te[2],0,0,0);
            $tmp_esp[$espm][] = $obj_campo['TEMPO_PREVISTO'];
            $tmp_esp_total[$espm][]=$obj_campo['TEMPO_PREVISTO'];
            if ($obj_campo['DIAGNOSTICO'] == 1) {
                $diag = $ling['ok'];
            }
            elseif ($obj_campo['DIAGNOSTICO'] == 2) {
                $diag = $ling['nao_ok'];
            }
            else {
                $diag = '-';
            }
            
            if ($obj_campo['TIPO'] == 1) {
                $tipoimg = 'lubrificacao.png';
            }
            if ($obj_campo['TIPO'] == 2) {
                $tipoimg = 'inspecao.png';
            }
            // verifica se come�ou outra m�quina e caso positivo mostra novo header
            if ($ultimo_obj != $obj_campo['MID_MAQUINA']) {
                $objeto = htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_MAQUINA'],0));
                echo "<tr><td colspan=8 style='border: 1px solid black; background-color: #999'><span style='font-size:7pt'><strong>$objeto</strong><span></td></tr>
                <tr>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['TIPO']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['MID_PONTO'].'/'.$tdb[LINK_ROTAS]['MID_CONJUNTO']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['TAREFA']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['ESPECIALIDADE']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['TEMPO_PREVISTO']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['MID_MATERIAL']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$tdb[LINK_ROTAS]['QUANTIDADE']."</center></strong></span></td>
                <td style='border: 1px solid black; background-color: #CCCCCC'><span style='font-size:7.5pt;font-family:Arial'><strong><center>{$ling['diagnostico_m']}</center></strong></span></td>
                </tr>";
            }

            echo "<tr>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><img src=\"".$manusis['url']."imagens/icones/22x22/$tipoimg\" border=0></span></td>";
            
            if ($obj_campo['MID_CONJUNTO']) echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0))."</span></td>";
            else echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(PONTOS_LUBRIFICACAO,"PONTO","MID",$obj_campo['MID_PONTO'],0))."</span></td>";
            
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities($obj_campo['TAREFA'])."</span></td>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;$esp</span></td>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['TEMPO_PREVISTO']."</span></td>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0))."</span></td>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo['QUANTIDADE']."</span></td>";
            echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;$diag</span></td>";
            echo "</tr>";

            $ultimo_obj = $obj_campo['MID_MAQUINA'];
            $obj->MoveNext();
        }
        echo "</table>";
    }
    ################


}

////
$obj=$dba[0] -> Execute("SELECT MID FROM ".PENDENCIAS." WHERE MID_ORDEM = '".$ca['MID']."' OR MID_ORDEM_EXC = '".$ca['MID']."' ORDER BY NUMERO ASC");
$obj_campo = $obj -> fields;
$objl=$i=count($obj -> getrows());
if ($objl != 0) {
    echo "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
    echo "<tr><td colspan=\"6\" aling=\"center\"><strong><center>{$ling['pendencias_m']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['Nordem']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['OBJ_MANUTENCAO']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['POSICAO_M']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['DESCRICAO_M']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['ORDEM_ORIGEM']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['ORDEM_EXECUTADA']."</center></strong></span></td>
</tr>";
    $obj=$dba[0] -> Execute("SELECT * FROM ".PENDENCIAS." WHERE MID_ORDEM = '".$ca['MID']."' OR MID_ORDEM_EXC = '".$ca['MID']."' ORDER BY NUMERO ASC");
    while (!$obj->EOF) {
        $obj_campo = $obj -> fields;
        $ordexec=(int)$obj_campo['MID_ORDEM_EXC'];
        if ($ordexec == 0) $txt .= "<tr>";
        else echo "<tr bgcolor=\"gray\">";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities($obj_campo['NUMERO'])."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_MAQUINA'],0))."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0))."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities($obj_campo['DESCRICAO'])."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(ORDEM_PLANEJADO,"NUMERO","MID",$obj_campo['MID_ORDEM'],0))."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(ORDEM_PLANEJADO,"NUMERO","MID",$obj_campo['MID_ORDEM_EXC'],0))."</span></td>";
        echo "</tr>\n";
        $obj->MoveNext();
    }
    echo "</table>";
}

$obj_mat=$dba[0] -> Execute("SELECT MID FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$ca['MID']."' ORDER BY CUSTO_TOTAL ASC");
$obj_campo = $obj_mat -> fields;
$obj3=$i=count($obj_mat -> getrows());
if ($obj3 != 0) {
    echo "<br><table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
    echo "<tr><td colspan=\"4\" aling=\"center\"><strong><center>{$ling['ord_material_apontado']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['ALMOXARIFADO_M']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['MATERIAL']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['QUANTIDADE']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['CUSTO_UNITARIO_RS']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['CUSTO_TOTAL_RS']."</center></strong></span></td>
</tr>";
    $obj_mat=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MATERIAL." WHERE MID_ORDEM = '".$ca['MID']."' ORDER BY CUSTO_TOTAL ASC");
    $vlr_inicial = 0;
    while (!$obj_mat->EOF) {
        $obj_campo_mat = $obj_mat -> fields;
        if ($ordexec == 0) $txt .= "<tr>";
        else echo "<tr bgcolor=\"gray\">";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(ALMOXARIFADO,"DESCRICAO","MID",$obj_campo_mat['MID_ALMOXARIFADO'],$tdb[ALMOXARIFADO]['dba']))."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".htmlentities(VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo_mat['MID_MATERIAL'],$tdb[MATERIAIS]['dba']))."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_mat['QUANTIDADE']."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_mat['CUSTO_UNITARIO']."</span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_mat['CUSTO_TOTAL']."</span></td>";
        echo "</tr>\n";
        $soma_total = $soma_total + $obj_campo_mat['CUSTO_TOTAL'];
        $obj_mat->MoveNext();
    }
    echo "<tr>";
    echo "<td colspan=\"4\" style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><center>".$ling['SOMA_TOTAL']."</center></span></td>";
    echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".number_format($soma_total,2)."</span></td>";
    echo "</tr>\n";
    echo "</table>";
}

$obj_custo=$dba[0] -> Execute("SELECT MID FROM ".ORDEM_PLANEJADO_CUSTOS." WHERE MID_ORDEM = '".$ca['MID']."' ORDER BY CUSTO ASC");
$obj4=$i=count($obj_custo -> getrows());
if ($obj4 != 0) {
    echo "<br><table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">";
    echo "<tr><td colspan=\"2\" aling=\"center\"><strong><center>{$ling['rel_desc_outros_custos']}</center></strong></td></tr>
<tr>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['DESCRICAO_M']."</center></strong></span></td>
<td style='border: 1px solid black; background-color: #cccccc'><span style='font-size:7.5pt;font-family:Arial'><strong><center>".$ling['CUSTO_M']."</center></strong></span></td>
</tr>";
    $obj_custo=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_CUSTOS." WHERE MID_ORDEM = '".$ca['MID']."' ORDER BY CUSTO ASC");
    $vlr_inicial = 0;
    while (!$obj_custo->EOF) {
        $obj_campo_custo = $obj_custo -> fields;
        if ($ordexec == 0) $txt .= "<tr>";
        else echo "<tr bgcolor=\"gray\">";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><center>&nbsp;".htmlentities($obj_campo_custo['DESCRICAO'])."</center></span></td>";
        echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>&nbsp;".$obj_campo_custo['CUSTO']."</span></td>";
        echo "</tr>\n";
        $soma_total_outros = $soma_total_outros + $obj_campo_custo['CUSTO'];
        $obj_custo->MoveNext();
    }
    echo "<tr>";
    echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><center>".$ling['SOMA_TOTAL']."</center></span></td>";
    echo "<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><b>&nbsp;".number_format($soma_total_outros,2)."</b></span></td>";
    echo "</tr>\n";
    echo "</table>";
}
###/// anexos
$anexotxt.= "<table id=\"dados_processados\" style=\"border: 1px solid black\" width=\"100%\">
<tr><td colspan=\"2\" aling=\"center\"><strong>{$ling['anexos_m']}</strong></td></tr>";
//// lista arquivos
$dir=$manusis['dir']['ordens']."/".$ca['MID'];
$ifiles=0;
if (is_dir($dir)) {
    $anexotxt.= "<tr>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['ARQUIVO']."</strong></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><strong>".$ling['TAMANHO_M']."</strong></span></td>
</tr>";
    $odir = opendir($dir);
    while (false !== ($arq = readdir($odir))) {
        $dd=explode(".",$arq);
        if ($dd[1]) {
            $anexotxt.= "<tr>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'><a style=\"link\" href=\"$dir/$arq\" target=\"_blank\">$arq</a></span></td>
<td style='border: 1px solid black'><span style='font-size:7.5pt;font-family:Arial'>".de_bytes(filesize("$dir/$arq"))."</span></td>
</tr>\n";
            $ifiles++;
        }
    }
    closedir($odir);


}
//// fim lista arquivos
$anexotxt.="</table>";

if ($ifiles) echo $anexotxt;
/// fim anexos

echo "</td></tr></table><Br><Br> <a href=javascript:print()>  {$ling['imprimir']} </a>
    </form><br />
    </div>
    </body>
    </html>";


?>
