<?
/** 
* Modulo principal, alertas
* 
* @author  Fernando Cosentino
* @version  3.0
* @package manusis
* @subpackage Principal
*/

echo "<style>
.link2 {
    font-size:11px;
    text-align:left;
    color:black;
    text-decoration:none;
}
.link3 {
    font-size:10px;
    text-align:left;
    color:black;
    text-decoration:none;
}
</style>";



// FILTRO POR EMPRESA
$fil_emp = VoltaFiltroEmpresa(SOLICITACOES, 2);
$fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";

### permissão de usuario
	$perm_cc = array();
	// Apenas para usuários
	if ($_SESSION[ManuSess]['user']['MID'] != 'ROOT') {
		// BUSCANDO AS MAQUINAS NOS CENTRO DE CUSTOS COM PERMISSÃO
		$sql = "SELECT CENTRO_DE_CUSTO FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}'";
		if (! $tmp = $dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> Execute($sql)) {
			erromsg("Arquivo: " . __FILE__ . " <br />Linha: " . __LINE__ . " <br />" .$dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> ErrorMsg() . "<br />" . $sql);
		}
		while (! $tmp->EOF) {
			$perm_cc[] = $tmp -> fields['CENTRO_DE_CUSTO'];
			$tmp -> MoveNext();
		}
		
		// Caso tenha permissão por Centro de Custo
		if (count($perm_cc) > 0) {
			$fil_emp .= " AND M.CENTRO_DE_CUSTO IN (" . implode(",", $perm_cc) . ")";
		}		
	}
    
	$sql="SELECT MID_MAQUINA FROM ".SOLICITACOES." S, ".MAQUINAS." M WHERE S.MID_MAQUINA = M.MID AND S.STATUS = 0 $fil_emp ORDER BY S.MID_MAQUINA ASC";

if (!$ressolic= $dba[$tdb[SOLICITACOES]['dba']] -> Execute($sql)){
    $err = $dba[$tdb[SOLICITACOESS]['dba']] -> ErrorMsg();
    erromsg("SQL ERROR .<br>$err<br><br>$sql");
    exit;
}
if (count($ressolic-> getrows())) {
    echo "<div id=\"solicitacoes\">
    <a class=\"link2\" ondblclick=\"return false;\" href=\"javascript:janela('modulos/principal/alertas_ajax.php?ajax=solicitacoes')\">
    <strong><img src=\"imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_solicitacoes']}</a><br /></strong>
    </div><br />";
}
else {
    echo "<strong><img src=\"imagens/icones/icon_yel_light.gif\" border=\"0\" /> {$ling['prin_solicitacoes']}<br /><br /></strong>";
}

// FILTRO POR EMPRESA
$fil_emp = VoltaFiltroEmpresa(ORDEM, 2);
$fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";

### permissão de usuario
	$perm_cc = array();
	// Apenas para usuários
	if ($_SESSION[ManuSess]['user']['MID'] != 'ROOT') {
		// BUSCANDO AS MAQUINAS NOS CENTRO DE CUSTOS COM PERMISSÃO
		$sql = "SELECT CENTRO_DE_CUSTO FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}'";
		if (! $tmp = $dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> Execute($sql)) {
			erromsg("Arquivo: " . __FILE__ . " <br />Linha: " . __LINE__ . " <br />" .$dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> ErrorMsg() . "<br />" . $sql);
		}
		while (! $tmp->EOF) {
			$perm_cc[] = $tmp -> fields['CENTRO_DE_CUSTO'];
			$tmp -> MoveNext();
		}
		
		// Caso tenha permissão por Centro de Custo
		if (count($perm_cc) > 0) {
			$fil_emp .= " AND CENTRO_DE_CUSTO IN (" . implode(",", $perm_cc) . ")";
		}		
	}

$sql="SELECT MID_MAQUINA FROM ".ORDEM." WHERE STATUS = 1 AND DATA_PROG < '".date('Y-m-d')."' $fil_emp ORDER BY MID_MAQUINA ASC";
if (!$resordens= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
    $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
    erromsg("SQL ERROR .<br>$err<br><br>$sql");
    exit;
}
if (count($resordens-> getrows())) {
    echo "<div id=\"ordens\">
    <a class=\"link2\" ondblclick=\"return false;\" href=\"javascript:janela('modulos/principal/alertas_ajax.php?ajax=ordens')\">
    <strong><img src=\"imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_orndes_atraso']}</a><br /></strong>
    </div><br />";
}
else {
    echo "<img src=\"imagens/icones/icon_yel_light.gif\" border=\"0\" /><strong> {$ling['prin_orndes_atraso']}<br /></strong><br />";
}

?>
