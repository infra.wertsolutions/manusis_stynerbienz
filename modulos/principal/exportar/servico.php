#!/usr/bin/php
<?php
chdir(dirname(realpath($_SERVER['SCRIPT_FILENAME'])));
// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);

$modelosa_dir[1] = "../../../{$manusis['dir']['imodelos']}";
$modelosa_dir[2] = "../../../{$manusis['dir']['emodelos']}";

$script[1]='importar_dados.php';
$script[2]='exportar_dados.php';

file_put_contents('servico_status.txt','1');

//echo "start\n";
$estedir = dirname(realpath($_SERVER['SCRIPT_FILENAME'])); // se necessario, mude este para o diretorio onde est�o exportar_dados  importar_dados
while (true) {  
    // parada de emergencia: coloque um arquivo chamado "parar_servico.txt" neste diretorio para encerrar este while
    // por praticidade, basta renomear o "parar_servico_.txt" que ja esta na pasta para remover o "_"
    // verificada a cada 1 segundo, segurando o script por 10 segundos
    for ($timer=0; $timer<10; $timer++) {
        sleep(1);
        if (file_exists('parar_servico.txt')) break;
    }
    if (file_exists('parar_servico.txt')) break;
    
    //echo "run\n";
    
    $tmp=$dba[$tdb[INTEGRACAO_ESQUEMAS]['dba']] -> Execute("SELECT *, to_char(ULTIMO_EVENTO, 'yyyy-mm-dd hh24:mi:ss') as ULTIMO_EVENTO, to_char(DATA_INICIO, 'yyyy-mm-dd hh24:mi:ss') as DATA_INICIO FROM ".INTEGRACAO_ESQUEMAS." WHERE STATUS = 1");
    while (!$tmp->EOF) {
        $campo=$tmp->fields;
        $modelos_dir = $modelosa_dir[$campo['TIPO']];
        $modelo_xml = file_get_contents("{$modelos_dir}/{$campo['ESQUEMA']}");
        if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
        $modelo = new SimpleXMLElement($modelo_xml);
        
        if ((string)$modelo->auto_periodicidade == '1') {
            $fator = 60*60; // horas -> fator = 1 hora
        }
        elseif ((string)$modelo->auto_periodicidade == '2') {
            $fator = 60*60*24; // dias -> fator = 1 dia
        }
        elseif ((string)$modelo->auto_periodicidade == '3') {
            $fator = 60*60*24*7; // semanas -> fator = 7 dias
        }
        // Minutos
        elseif ((string)$modelo->auto_periodicidade == '4') {
            $fator = 60; // minutos -> fator = 1 minuto
        }
        else $fator = 0;
    
        if ($fator) {
            // intervalo = numero de segundos entre uma execu��o e outra
            $intervalo = (int)$modelo->auto_frequencia * $fator;
            
            $ultimo_evento = split(' ',$campo['ULTIMO_EVENTO']);
            $ultimo_evento = VoltaTime($ultimo_evento[1],NossaData($ultimo_evento[0]));
            $proximo_evento = $ultimo_evento + $intervalo;
            
            if ($proximo_evento < time()) {
                // eh pra executar!
                //$esq = urlencode($campo['ESQUEMA']);
                $esq = $campo['ESQUEMA'];
                file_put_contents("{$modelos_dir}/{$esq}.txt",'0');
                execInBackground("php-cli \"{$estedir}/{$script[$campo['TIPO']]}\" m={$esq}");
                //echo "exec {$campo['TIPO']} $esq\n";
                $sql = "UPDATE ".INTEGRACAO_ESQUEMAS." SET ULTIMO_EVENTO = to_date('" . date("Y-m-d H:i:s") . "', 'yyyy-mm-dd hh24:mi:ss') WHERE MID = '{$campo['MID']}'";
                $dba[$tdb[INTEGRACAO_ESQUEMAS]['dba']] -> Execute($sql);
            }
        }
        $tmp->MoveNext();
    }
    //echo "end run\n";
}

file_put_contents('servico_status.txt','0');
//echo "quit\n";

?>
