<?php
/**
 * Para implementa��o futura:
 * Neste array apenas � checado se o valor � n�o-nulo. Ou seja, ao inv�s de '1',
 * pode ser usado no lugar um valor �til, como o nome de um campo ou um sub-array
 * Ex:
 * $importar_eventos_tabelas[NOME_DA_TABELA]=1;
 */

$importar_eventos_tabelas = array(PRODUCAO_PRODUTOS_ACABADOS => 1);


// Variavel persistente dos eventos
$importar_eventos_globais=array();

/**
 * Fun��o chamada a cada registro importado, SE E SOMENTE SE a tabela estiver no array $importar_eventos_tabelas
 * Esta fun��o � chamada ANTES de importar, ou seja, modifica��es no array $campos SER�O APLICADAS!!!
 * 
 * ATEN��O!!! Se em algum caso esta fun��o adicionar uma chave no array $campos,
 * essa chave deve ser adicionada em TODOS os registros!! (se n�o d� pau no numero de campos)
 * 
 * @param string $tabela tabela sendo importada
 * @param array  $campos array com os campos que v�o ser inseridos na tabela (pela fun��o que chamou esta)
 */
function Importar_Evento($tabela, &$campos) {
    global $tdb, $dba, $importar_eventos_globais;
    
	switch ($tabela) {
        
    }
    
    return true;
}

/**
 * Fun��o chamada para cada registro importado, SE E SOMENTE SE a tabela estiver no array $importar_eventos_tabelas
 * Esta fun��o � chamada DEPOIS de importar, e recebe somente o MID do registro j� importado
 *
 * @param string $tabela
 * @param int $mid
 */
function Importar_Evento_Apos($tabela, $mid) {
    global $tdb, $dba, $importar_eventos_globais, $msg, $ling;
    
    switch ($tabela) {
        
        // Gerando lan�amento de contador atrav�s dos dados importados
        case PRODUCAO_PRODUTOS_ACABADOS:
            
            
            $dataLancamento = NossaData(VoltaValor($tabela, 'DATA_FINAL', 'MID', $mid));
            $valor = (VoltaValor($tabela, 'QUANT_PROD', 'MID', $mid) + VoltaValor($tabela, 'QUANT_PERDA', 'MID', $mid));
            
            $produto = VoltaValor($tabela, 'MID_PRODUTO_ACABADO', 'MID', $mid);
            
            if($produto !== false){
                /**
                * O Lan�amento deve ser executado no contador da m�quina em qual o produto estava alocado 
                * no dia do lan�amento.
                * Por isso fazemos a busca na tabela de movimenta��o para descobrirmos 
                * Caso a busca abaixo retorne o mid_maquina_destino, signofoca que ela est� alicada nesta m�quina
                * no dia do lan�amento. Se n�o retornar nada, deve-se buscar verificando onde a data de lan�amento
                * Fique dentro das datas inicial e final da movimenta��o. 
                * 
                */
                $sql = "SELECT MID_MAQUINA_DESTINO FROM ".MOV_PRODUTO." WHERE MID_PRODUTO = {$produto} AND DATA_INICIAL <= '".DataSql($dataLancamento)."' AND DATA_FINAL IS NULL";
                if(!$rs = $dba[$tdb[MOV_PRODUTO]['dba']]->Execute($sql)){
                    erromsg("Erro ao localizar {$tdb[MOV_PRODUTO]['DESC']} em:<br />
                    Arquivo: ".__FILE__."<br />
                    Fun��o: ".__FUNCTION__."<br />
                    Linha: ".__LINE__."<br />
                    Erro:{$dba[$tdb[MOV_PRODUTO]['dba']]->ErrorMsg()}
                    SQL:{$sql}
                    ");
                    exit();
                }
                // Encontrou? ent�o salva o mid m�quina para localizarmos o contador dela que � do tipo integra��o.
                elseif(!$rs->EOF){

                    $midMaquina = $rs->fields['MID_MAQUINA_DESTINO'];

                    $contador = VoltaValor(MAQUINAS_CONTADOR, 'MID', array('MID_MAQUINA', 'INTEGRACAO'), array($midMaquina, 1));

                }
                // Localizando a m�quina para o dia do dan�amento.
                else{

                    $sql = "SELECT MID_MAQUINA_DESTINO FROM ".MOV_PRODUTO." WHERE MID_PRODUTO = {$produto} AND DATA_INICIAL >= '".DataSql($dataLancamento)."' AND DATA_FINAL <= '".DataSql($dataLancamento)."' AND DATA_FINAL IS NOT NULL";
                    if(!$rs = $dba[$tdb[$tabela]['dba']]->Execute($sql)){

                        erromsg("Erro ao localizar {$tdb[MOV_PRODUTO]['DESC']} em:<br />
                        Arquivo: ".__FILE__."<br />
                        Fun��o: ".__FUNCTION__."<br />
                        Linha: ".__LINE__."<br />
                        Erro:{$dba[$tdb[MOV_PRODUTO]['dba']]->ErrorMsg()}
                        SQL:{$sql}
                        ");
                        exit();
                    }
                    elseif(!$rs->EOF){

                        $midMaquina = $rs->fields['MID_MAQUINA_DESTINO'];

                        $contador = VoltaValor(MAQUINAS_CONTADOR, 'MID', array('MID_MAQUINA', 'INTEGRACAO'), array($midMaquina, 1));
                        
                    }
                    // Se n�o encontrou aqui n�o lan�a nada porque o produto n�o est� alocado.
                    else{
                        $contador = false;
                    }

                }
                 
                $codProduto = VoltaValor(PRODUTOS_ACABADOS, 'COD', 'MID', $produto);
                $msg .= "\nProcessando produto: {$codProduto}";
                // Caso encontre o contador executa o lan�amento.
                if($contador !== false){
                    $msg .= "\n{$ling['realizando_lancamento']}: ".VoltaValor(MAQUINAS_CONTADOR, 'DESCRICAO', 'MID', $contador)."";
                    $msg .= "\n Resultado: ".LancaContador($contador, $valor, $dataLancamento);
                    $msg .= "\n --------------------";
                }
                else{
                   $msg .= " - Produdo n&atilde;o vinculado a um contador de integra&ccedil;o"; 
                   $msg .= "\n --------------------"; 
                }
            }
            
            break;
    }
    
    return true;
}

function Importar_Evento_Sincronizar($tabela, $campo, $valores) {
    global $tdb, $dba, $importar_eventos_globais;
    
    switch ($tabela) {
    	
        default:
    		$fil = "";
    		// O oracle da erro com mais de 1000 no IN
    		$i = ceil(count($valores) / 1000);
    		for (; $i >= 0; $i--) {
    			// Evitando problemas
    			if (count($valores) == 0) break;
    			
    			// Colocando no in
    			$fil .= ($fil != '')? " OR " : "";
    			$fil .= "$campo NOT IN ('" . implode("', '", array_slice($valores, 0, 1000)) . "')";
    			array_splice($valores, 0, 1000);
    		}
        	
            $sql_sinc = "DELETE FROM $tabela WHERE $fil";
    }
    
    $sql_status = $dba[$tdb[$tabela]['dba']] -> Execute($sql_sinc);
    
    return $sql_status;
}

?>
