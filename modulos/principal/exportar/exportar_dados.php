<?
chdir(dirname(realpath($_SERVER['SCRIPT_FILENAME'])));
$is_windows = (strpos(strtoupper($_SERVER["OS"]),'WINDOWS') !== false) ? true : false;
$barra = ($is_windows ? '\\' : '/');

$time_inicio = time();

// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);

$separador_csv = ';';
$modelos_dir = "..{$barra}..{$barra}..{$barra}{$manusis['dir']['emodelos']}";
$arquivos_dir = "..{$barra}..{$barra}..{$barra}{$manusis['dir']['temporarios']}";
$exportar_dir = "..{$barra}..{$barra}..{$barra}{$manusis['dir']['exportar']}";
$exportar_url = "{$manusis['url']}{$manusis['dir']['exportar']}";

function Operacao($id, $valor) {
    if ($id == 1) return "= '$valor'";
    if ($id == 2) return "!= '$valor'";
    if ($id == 3) return "> '$valor'";
    if ($id == 4) return "< '$valor'";
    if ($id == 5) return "LIKE '%$valor%'";
    return "= '$valor'";
}

function FormataArquivo($arq) {
    if (strpos($arq,'%ds')) $arq = str_replace('%ds',date('w')+1,$arq);
    if (strpos($arq,'%d')) $arq = str_replace('%d',date('d'),$arq);
    if (strpos($arq,'%m')) $arq = str_replace('%m',date('m'),$arq);
    if (strpos($arq,'%a')) $arq = str_replace('%a',date('Y'),$arq);
    if (strpos($arq,'%s')) $arq = str_replace('%s',date('W'),$arq);
    if (strpos($arq,'%h')) $arq = str_replace('%h',date('H-i-s'),$arq);
    return $arq;
}

$conds = array(
    1 => $ling['igual'],
    2 => $ling['diferente'],
    3 => $ling['maior'],
    4 => $ling['menor'],
    5 => $ling['contem']
);
$floats = array('double','decimal');


// Variaveis de direcionamento
if ($argv[1]) parse_str($argv[1],$_GET);
if ($_GET['m']) $arq_xml=LimpaTexto($_GET['m']);

file_put_contents("{$modelos_dir}{$barra}{$arq_xml}.txt",'0');
$modelo_xml = file_get_contents("{$modelos_dir}{$barra}{$arq_xml}");
if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
$_SESSION[ManuSess]['exportar_modelo_xml'] = $modelo_xml;

$modelo = new SimpleXMLElement($modelo_xml);
$tb = "" . $modelo->tabela . "";

if (!$tb) die($ling['err10']);

$mostrar_titulos = (int)$modelo->mostrar_titulos;
$virgula = (string)$modelo->virgula;

foreach ($modelo->campo as $evalor) $tbcampos[(string)$evalor['nome']] = (string)$evalor[0];

$ncampos = VoltaCampos($tb);

$campos=$campos_tipos=array();
foreach ($ncampos as $ecampo) {
	
    if ($tbcampos[$ecampo['nome']] != '0') {
        // Campo DATA
        if (($tdb_ora[$tb]['D']) and (in_array($ecampo['nome'], $tdb_ora[$tb]['D']))) {
            $campos[] = "to_char(" . $ecampo['nome'] . ", 'dd/mm/yyyy') as " . $ecampo['nome'];
        }
        // Campo HORA
        elseif (($tdb_ora[$tb]['T']) and (in_array($ecampo['nome'], $tdb_ora[$tb]['T']))) {
            $campos[] = "to_char(" . $ecampo['nome'] . ", 'hh24:mi:ss') as " . $ecampo['nome'];
        }
        // Campo DATA e HORA
        elseif (($tdb_ora[$tb]['DT']) and (in_array($ecampo['nome'], $tdb_ora[$tb]['DT']))) {
            $campos[] = "to_char(" . $ecampo['nome'] . ", 'dd/mm/yyyy hh24:mi:ss') as " . $ecampo['nome'];
        }
        else {        
            $campos[] = $ecampo['nome'];
        }
        $campos_tipos[$ecampo['nome']] = $ecampo['tipo'];
    }
}

$filtros=$filtros_texto='';
$satis = ((string)$modelo->satisfazer ? (string)$modelo->satisfazer : "AND");
foreach ($modelo->filtro as $efiltro) {
    $rtb=VoltaRelacao($tb,$efiltro['campo']);
    if ($rtb) $val = VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$fil,$rtb['dba']); else $val = (string)$efiltro[0];
    AddStr($filtros," $satis ",(string)$efiltro['campo'].' '.Operacao((int)$efiltro['condicao'],(string)$efiltro[0]));
    AddStr($filtros_texto,"<br>","{$tdb[$tb][(string)$efiltro['campo']]} {$conds[(int)$efiltro['condicao']]} {$val}");
}
if ($filtros) $filtros = "WHERE $filtros";
if (!$filtros_texto) $filtros_texto = "(Nenhum)";
$ord = (string)$modelo->ordenar;
if ($ord) $ord = "ORDER BY $ord ASC";

#####################################
## COME�A A TRABALHAR OS DADOS

$timest = time();
$arq_temp = "{$exportar_dir}{$barra}log{$barra}{$arq_xml}-{$timest}.tmp.csv";
$arq_temp_zip = "{$exportar_dir}{$barra}{$arq_xml}-{$timest}.tmp.zip";
$arq_log = "{$modelos_dir}{$barra}{$arq_xml}.txt";

file_put_contents($arq_temp,'');



$sql = "SELECT ".join(', ', $campos)." FROM $tb $filtros $ord";

$tmp=$dba[$tdb[$tb]['dba']] -> Execute($sql);
$numero_registros = $tmp->NumRows();
$j=0;
$tfile = fopen($arq_temp,"w");
while (!$tmp->EOF) {
    $campo=$tmp->fields;
    $linha=$linha_titulo='';
    if (!$j) {
        // primeira vez do loop
        $titulos = array_keys($campo);
        foreach ($titulos as $eitem) {
            // adiciona primeira linha com titulos
            if ($mostrar_titulos) AddStr($linha_titulo,$separador_csv,'"'.addslashes(unhtmlentities($tdb[$tb][$eitem])).'"');
            // verifica se vai precisar da tabela e se precisar j� pega em array
            $rtb=VoltaRelacao($tb,$eitem);
            if ($rtb) {
                $sql2 = "SELECT ".($rtb['cod'] ? "{$rtb['cod']},":'')."{$rtb['campo']}, {$rtb['mid']} FROM {$rtb['tb']}";
                $tmp2=$dba[$tdb[$tb]['dba']] -> Execute($sql2);
                while (!$tmp2->EOF) {
                    $campo2=$tmp2->fields;
                    $relacoes[$rtb['tb']][$campo2[$rtb['mid']]] = ($rtb['cod'] ? $campo2[$rtb['cod']] . '-':'').$campo2[$rtb['campo']];
                    $tmp2->MoveNext();
                }
                $tem_relacao[$eitem]=$rtb['tb'];
            }
            else $tem_relacao[$eitem]=0;
        }
    }
    // todas as vezes do loop
    foreach ($campo as $ek=>$ecampo) {
        if ($tem_relacao[$ek]) $campo[$ek] = $relacoes[$tem_relacao[$ek]][$ecampo];
        if (in_array($campos_tipos[$ek],$floats)) {
            $campo[$ek] = str_replace(',',$virgula,str_replace('.',$virgula,$campo[$ek]));
        }
    }

    foreach ($campo as $eitem) {
        if (is_numeric($eitem) and (strpos($eitem,$separador_csv) === false)) AddStr($linha,$separador_csv,$eitem);
        else AddStr($linha,$separador_csv,'"'.addslashes($eitem).'"');
    }
    
    if ($linha_titulo) {
        $linha = $linha_titulo."\n".$linha;
        $linha_titulo='';
    }
    
    if ($j) $linha = "\n".$linha;
    fwrite($tfile,$linha);

    $j++;
    
    $perc = round(($j/$numero_registros)*100);
    file_put_contents($arq_log,"$perc/$j/$numero_registros/{$ling['expo_tempo_esperado']}: ".gmdate('H:i:s',time()-$time_inicio));
    
    $tmp->MoveNext();
}
fclose($tfile);
$num_linhas=$j;

##### SALVAR
$msg='';
$status=1;
if ((int)$modelo->mandar_email) {
    $email_arq = $arquivos_dir."{$barra}exportar.csv";
    copy($arq_temp,$email_arq);
    $email_de = 'suporte@manusis.com.br';
    $email_para = (string)$modelo->email;
    $email_assunto = $ling['expo_dados_exportados']." ".unhtmlentities($tdb[$tb]['DESC']);
    $email_texto = "{$ling['expo_anexo_dados_exportados']} {$tdb[$tb]['DESC']}, {$ling['expo_exportados_em']} ".date('d/m/Y')." as ".date('H:i:s')."<br>
    {$ling['filtros']}:<br>
    $filtros_texto<br>
    <br>
    {$ling['expo_esquema']}: $arq_xml";
    if (MandaMailAnexo($email_para,$email_de,$email_assunto,$email_texto,realpath($email_arq))) {
        AddStr($msg,"\n",$ling['expo_email_sucesso']);
    }
    else {
        AddStr($msg,"\n",$ling['err11']);
        $status = 0;
    }
}

if ((int)$modelo->mandar_unidade) {
    $edir = "{$exportar_dir}{$barra}$tb";
    $eurl = "{$exportar_url}/$tb";
    if (!file_exists($edir)) {
        mkdir($edir);
        chmod($edir,0777);
    }
    $arq_nome_arq=FormataArquivo((string)$modelo->unidade);
    $arq_nome=$edir."{$barra}".$arq_nome_arq;
    $arq_url=$eurl."/".$arq_nome_arq;
    if (copy($arq_temp,$arq_nome) === false) {
        AddStr($msg,"\n","");
        $status = 0;
    }
    else {
        AddStr($msg,"\n","{$ling['expo_arquivo_sucesso']}<a href=\"$arq_url\">$arq_url</a>");
    }
}

if ((int)$modelo->mandar_ftp) {
    $ftp_host = (string)$modelo->mandar_ftp_host;
    $ftp_local = (string)$modelo->mandar_ftp_local;
    $ftp_user = (string)$modelo->mandar_ftp_user;
    $ftp_pass = base64_decode((string)$modelo->mandar_ftp_pass);
    
    $ftp_path = (strpos($ftp_local,'/') === false) ? '' : substr($ftp_local,0,strrpos($ftp_local,'/')+1); 
    $ftp_filename = FormataArquivo((strpos($ftp_local,'/') === false) ? $ftp_local : substr($ftp_local,strrpos($ftp_local,'/')+1));
    
    if ($ftp_host and $ftp_local) {
        $conn_id = ftp_connect($ftp_host);
        if (!$conn_id) {
            AddStr($msg,"\n",$ling['expo_servidor_ftp']);
            $status=0;
        }
        else {
            $login_result = ftp_login($conn_id, $ftp_user, $ftp_pass); 
            if (!$login_result) {
                AddStr($msg,"\n",$ling['expo_nao_servidor_ftp']);
                $status=0;
            }
            else {
                // logado
                $temp_arq = realpath($arquivos_dir."{$barra}exportar.csv");

                ftp_pasv($conn_id, true);
                ftp_chdir($conn_id,$ftp_path);
                $upload = ftp_put($conn_id, $ftp_filename, $arq_temp, FTP_BINARY); 
                AddStr($msg,"\n",($upload ? "{$ling['expo_servidor_ftp_envio']}: $ftp_filename".($ftp_path ? " {$ling['expo_diretorio']} $ftp_path" : '') : $ling['expo_nao_servidor_ftp_envio']));
            }
            ftp_close($conn_id); 
        }
    }
}


$texto_xml = str_replace("'", "''", $modelo->asXML());
$texto_csv = str_replace("'", "''", $arq_temp);
$texto_msg = str_replace("'", "''", $msg);

$result = ($status ? (int)$num_linhas : 0);
$nmid = GeraMid(INTEGRADOR,'MID',0);
$sql = "INSERT INTO ".INTEGRADOR." (TIPO, ESQUEMA, ESQUEMA_ARQUIVO, ULTIMO_EVENTO, CSV, RESULTADO, OBS, MID) VALUES (2, '$texto_xml','$arq_xml', to_date('" . date("Y-m-d H:i:s") . "', 'yyyy-mm-dd hh24:mi:ss'),'$texto_csv','$result','$texto_msg','$nmid')";
$tmp=$dba[$tdb[INTEGRADOR]['dba']] -> Execute($sql);
if (!$tmp) {
    AddStr($msg,"\n","{$ling['err13']}: ".$dba[$tdb[INTEGRADOR]['dba']]->ErrorMsg()."\n$sql");
    $status=0;
}

AddStr($msg,"\n","{$ling['expo_tempo_processado']}: ".gmdate('H:i:s',time()-$time_inicio));

file_put_contents($arq_log,"fim/$num_linhas/$num_linhas/$msg");

if ($status) die("msg=".urlencode(nl2br($msg)));
else die("erro=".urlencode(nl2br($msg)));


?>
