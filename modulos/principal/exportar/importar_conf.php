<?
// tabelas e campos que s�o chaves
$importar_modelo[MAQUINAS]=array('COD', 'DESCRICAO', 'NUMERO_DE_SERIE', 'NUMERO_DO_PATRIMONIO');
$importar_modelo[MAQUINAS_CONJUNTO]=array('TAG','DESCRICAO');
$importar_modelo[MATERIAIS]=array('COD','DESCRICAO');
$importar_modelo[MATERIAIS_UNIDADE]=array('COD', 'DESCRICAO');
$importar_modelo[MATERIAIS_FAMILIA]=array('COD', 'DESCRICAO');
$importar_modelo[CENTRO_DE_CUSTO]=array('COD','DESCRICAO');
$importar_modelo[AREAS]=array('COD','DESCRICAO');
$importar_modelo[SETORES]=array('COD','DESCRICAO');
$importar_modelo[ORDEM]=array('NUMERO');
$importar_modelo[FUNCIONARIOS]=array('MATRICULA','NOME');
$importar_modelo[EQUIPAMENTOS]=array('COD','DESCRICAO');
$importar_modelo[MAQUINAS_FAMILIA]=array('COD','DESCRICAO');
$importar_modelo[MAQUINAS_CLASSE]=array('DESCRICAO');
$importar_modelo[MAQUINAS_STATUS]=array('DESCRICAO');
$importar_modelo[FABRICANTES]=array('NOME');
$importar_modelo[FORNECEDORES]=array('NOME');
$importar_modelo[USUARIOS]=array('NOME');
$importar_modelo[ORDEM_STATUS]=array('DESCRICAO');
$importar_modelo[PRODUTOS_ACABADOS]=array('COD');
$importar_modelo[PRODUCAO_PRODUTOS_ACABADOS]=array('MID_PRODUTO_ACABADO');


function VerificaCadastro($tabela, $linha) {
    global $dba, $tdb, $manusis, $importar_modelo;
    
    // Montando os filtros
    $filtro_tmp = "";
    foreach ($importar_modelo[$tabela] as $cc) {
        $filtro_tmp .= ($filtro_tmp != "")? " OR " : "";
        $filtro_tmp .= $cc . " = " . $linha[$cc];
    }
    
    // Buscando o mid
    $sql = "SELECT MID FROM $tabela WHERE $filtro_tmp";  
    echo ". ";
    if (! $rs = $dba[$tdb[$tabela]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$tabela]['dba']] -> ErrorMsg() . "<br />" . $sql);
    }
    
    return (int) $rs -> fields['MID'];
}

?>
