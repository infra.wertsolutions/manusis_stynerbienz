<?
/**
* Cadastro de Checklist da Copagaz
* @autor Fernando Cosentino
* Nota: M�dulo Planejamento.
*/

if (($exe == 1) or ($exe == "")) {
	echo "<div id=\"mod_menu\">	
<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/checklist.png\" border=\"0\" alt=\"".$ling['plan_checklist']."\" />
<span>".$ling['plan_checklist']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=2\">
<img src=\"imagens/icones/22x22/checklist_programacao.png\" border=\"0\" alt=\"{$ling['checklist_programacao']}\" />
<span>{$ling['checklist_programacao']}</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/checklist_aponta.png\" border=\"0\" alt=\"{$ling['checklist_apontar']}\" />
<span>{$ling['checklist_apontar']}</span>
</a>
</div>

<div><h3>".$ling['plan_checklist']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	$campos[0]="DESCRICAO";
	$campos[1]="USAR_TURNOS";
	$campos[2]="LAYOUT";
	$campos[3]="FREQUENCIA";
	
	ListaTabela(PLANO_CHECKLIST,"MID",$campos,"PLANO_CHECKLIST","","",1,1,1,1,0,1,1,1,1,1,"PCHECK");
}
if ($exe == 11) {
	if (!$oq) {
		$oq = (int)$_GET['oq'];
	}

	echo "<div id=\"mod_menu\">	
	<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['plan_checklist']."\" />
<span>".$ling['plan_checklist']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?{$_SERVER['QUERY_STRING']}\">
<img src=\"imagens/icones/22x22/checklist_ativ.png\" border=\"0\" alt=\"".$ling['atividades']."\" />
<span>".$ling['atividades']."</span>
</a>
</div>

<div><h3>".$ling['atividades']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	
	
	$campos[0]="NUMERO";
	$campos[1]="MID_CHECKLIST";
	$campos[2]="TAREFA";
	
	ListaTabela(PLANO_CHECKLIST_ATIVIDADES,"MID",$campos,"PLANO_CHECKLIST_ATIVIDADES","MID_CHECKLIST",$oq,1,1,1,1,0,1,1,1,1,1,"PCHECKATIV");
	echo "</div>";
}

if ($exe == 2) {
	echo "<div id=\"mod_menu\">	
	<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['plan_checklist']."\" />
<span>".$ling['plan_checklist']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?{$_SERVER['QUERY_STRING']}\">
<img src=\"imagens/icones/22x22/checklist_programacao.png\" border=\"0\" alt=\"{$ling['checklist_programacao']}\" />
<span>{$ling['checklist_programacao']}</span>
</a>
</div>

<div><h3>{$ling['checklist_programacao']}</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	

	if (($_GET['cancela']) and ($_GET['confirma']) and ($oq)) {
		DeletaItem(LANCA_CHECKLIST,"DIAGNOSTICO = '0' AND MID_PROGRAMACAO",$oq);
		
		$sql = "UPDATE ".PLANO_CHECKLIST_PROGRAMACAO." SET STATUS='1' WHERE MID='$oq'";
		$dba[$tdb[PLANO_CHECKLIST_PROGRAMACAO]['dba']] ->Execute($sql);
	}
	
	$campos[0]="MID_CHECKLIST";
	$campos[1]="MID_MAQUINA";
	$campos[2]="DATA_INICIAL";
	$campos[3]="STATUS";
	
	ListaTabela(PLANO_CHECKLIST_PROGRAMACAO,"MID",$campos,"PLANO_CHECKLIST_PROGRAMACAO",'','',1,1,1,1,0,1,1,1,1,1,"PCHECKP");
	echo "</div>";
}
if ($exe == 3) {
	echo "<div id=\"mod_menu\">	
	<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['plan_checklist']."\" />
<span>".$ling['plan_checklist']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/checklist_aponta.png\" border=\"0\" alt=\"{$ling['checklist_apontar']}\" />
<span>{$ling['checklist_apontar']}</span>
</a>
</div>

<div><h3>".$ling['checklist_apontar']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	include("modulos/planejamento/checklist_apontamento.php");
}

?>