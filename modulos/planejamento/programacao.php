<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino
 * Nota: M�dulo Cadastro.
 */
$ajaxdestino = 'modulos/planejamento/programacao_ajax.php';

$cont=(int)$_GET['cont'];
$can=$_GET['cancela'];
$oq=(int)$_REQUEST['oq'];
$confirma=(int)$_GET['confirma'];
$msg=$_GET['msg'];
$fstatus = (int)$_REQUEST['status'];
$fam=(int)$_REQUEST['fam'];
$empresa=(int)$_REQUEST['empresa'];
$area=(int)$_REQUEST['area'];
$setor=(int)$_REQUEST['setor'];
$desc=$_REQUEST['desc'];
$destino = $_REQUEST['destino'];
$query = "id=$id&op=$op&exe=$exe";
$ajax = $_REQUEST['ajax'];
$tipoprog = (int)$_REQUEST['tipoprog'];
if (!$tipoprog) $tipoprog = $_SESSION[ManuSess]['lt']['progra']['tipoprog'];
if (!$tipoprog) $tipoprog = 1;


// CANCELANDO PROGRAMA��O
if (($can) and ($confirma != 0) and ($msg != "")){
	// Se der erro n�o voltar a pagina para mostrar o erro
	if(CancelaProg($oq, $msg)) {
		echo "<script> history.back(); </script>";
	}
}

if (($exe == 1) or ($exe == 2) or ($exe == 3) or ($exe == 5) or ($exe == "")) {
	echo "<div id=\"mod_menu\">
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=2\">
    <img src=\"imagens/icones/22x22/programacao.png\" border=\"0\" alt=\"".$ling['tabela_programacao']."\" />
    <span>".$ling['tabela_programacao']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=3\">
    <img src=\"imagens/icones/22x22/cronograma_semanal.png\" border=\"0\" alt=\"".$ling['tabela_programacao2']."\" />
    <span>".$ling['tabela_programacao2']."</span>
    </a>
    </div>";
}

if (($exe == 1) or ($exe == "") or ($exe == 2) or ($exe == 3)) {
	// Titulo conforme sele��o
	if ($exe == 3) {
		echo "<div><h3>".$ling['programacao_titulo2']."</h3></div>";
	}
	else {
		echo "<div><h3>".$ling['programacao_titulo']."</h3></div>";
	}


	echo "</div>
    <br clear=\"all\" />
    <div id=\"lt\">\n
    <div id=\"lt_cab\">\n
    <h3>{$ling['prog_definidas']}</h3>";

	if ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2)) {
		echo "<span id=\"lt_menu\" onmouseover=\"menu_sobre('ltmenu')\" onmouseout=\"menu_fora('ltmenu')\">".$ling['opcoes']."
        <ul id=\"m_ltmenu\">
        <li><a href=\"javascript:abre_janela_prog('0')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['novo']." \" /> ".$ling['novo']." </a></li>
        </ul>
        </span>";
	}

	echo "</div>
    <br clear=\"all\" />";

	// Contador para posicionamento das paginas
	if ($_GET['cont'] != "") {
		$_SESSION[ManuSess]['lt']['progra']['cont'] = (int)$_GET['cont'];
	}
	elseif($_SESSION[ManuSess]['lt']['progra']['cont'] == 0){
		$_SESSION[ManuSess]['lt']['progra']['cont'] = 0;
	}
	$cont = (int)$_SESSION[ManuSess]['lt']['progra']['cont'];


	// Qtos items por pagina
	if ($_GET['buscar_qto']  != "") $_SESSION[ManuSess]['lt']['progra']['buscar_qto']=(int)$_GET['buscar_qto'];
	$buscar_qto=(int)$_SESSION[ManuSess]['lt']['progra']['buscar_qto'];
	if ($buscar_qto == 0) $buscar_qto=50; // se n�o foi definido, usar o padrao;

	// Ano
	if ($_GET['filtro_ano'] != "") {
		$_SESSION[ManuSess]['lt']['progra']['filtro_ano'] = (int)$_GET['filtro_ano'];
	}
	elseif ($_SESSION[ManuSess]['lt']['progra']['filtro_ano'] == ""){
		$_SESSION[ManuSess]['lt']['progra']['filtro_ano'] = date('Y');
	}
	$filtro_ano = $_SESSION[ManuSess]['lt']['progra']['filtro_ano'];

	// Status
	if (isset($_GET['filtro_st'])) {
		$_SESSION[ManuSess]['lt']['progra']['filtro_st'] = (int)$_GET['filtro_st'];
	}
	$filtro_st = (int)$_SESSION[ManuSess]['lt']['progra']['filtro_st'];

	// Empresa
	if (isset($_GET['filtro_emp'])) {
		$_SESSION[ManuSess]['lt']['progra']['filtro_emp'] = (int)$_GET['filtro_emp'];
	}
	elseif ($_SESSION[ManuSess]['lt']['progra']['filtro_emp'] != 0) {
		$_GET['filtro_emp'] = $_SESSION[ManuSess]['lt']['progra']['filtro_emp'];
	}
	$filtro_emp = (int)$_SESSION[ManuSess]['lt']['progra']['filtro_emp'];

	// Local. 1
	if (isset($_GET['filtro_area'])) {
		$_SESSION[ManuSess]['lt']['progra']['filtro_area'] = (int)$_GET['filtro_area'];
	}
	elseif ($_SESSION[ManuSess]['lt']['progra']['filtro_area'] != 0) {
		$_GET['filtro_area'] = $_SESSION[ManuSess]['lt']['progra']['filtro_area'];
	}
	$filtro_area = (int)$_SESSION[ManuSess]['lt']['progra']['filtro_area'];


	// Local. 2
	if (isset($_GET['filtro_setor'])) {
		$_SESSION[ManuSess]['lt']['progra']['filtro_setor'] = (int)$_GET['filtro_setor'];
	}
	elseif ($_SESSION[ManuSess]['lt']['progra']['filtro_setor'] != 0) {
		$_GET['filtro_setor'] = $_SESSION[ManuSess]['lt']['progra']['filtro_setor'];
	}
	$filtro_setor = (int)$_SESSION[ManuSess]['lt']['progra']['filtro_setor'];

	// Destino da manuten��o
	if (isset($_GET['filtro_desc'])) {
		$_SESSION[ManuSess]['lt']['progra']['filtro_desc'] = $_GET['filtro_desc'];
	}
	$filtro_desc = $_SESSION[ManuSess]['lt']['progra']['filtro_desc'];

	// TIPO
	if ($_GET['filtro_tipo'] != "") {
		$_SESSION[ManuSess]['lt']['progra']['filtro_tipo']=(int)$_GET['filtro_tipo'];
	}
	elseif ($_SESSION[ManuSess]['lt']['progra']['filtro_tipo'] == '') {
		$_SESSION[ManuSess]['lt']['progra']['filtro_tipo'] = 1;
	}
	$filtro_tipo = (int)$_SESSION[ManuSess]['lt']['progra']['filtro_tipo'];



	echo "<form class=\"form\" id=\"formularioos\" action=\"manusis.php?id=3&op=3\" method=\"GET\">
    <fieldset><legend>".$ling['filtros']."</legend>
    
    <label for=\"filtro_ano\" class=\"campo_label\">{$ling['ano']}:</label> 
    <select name=\"filtro_ano\" class=\"campo_select_ob\">";
	$ffano = $filtro_ano + 6;
	for ($i = $filtro_ano - 4; $i < $ffano; $i++) {
		if ($i == $filtro_ano) echo "<option value=\"$i\" selected=\"selected\">$i</option>\n";
		else echo "<option value=\"$i\" >$i</option>\n";
	}
	echo "</select>\n";

	echo "<label for=\"filtro_st\">{$ling['situacoes']}:</label>
    <select name=\"filtro_st\" class=\"campo_select\">
    <option value=\"0\"></option>
    <option value=\"1\" " . ($filtro_st == 1 ? "selected=\"selected\"" : "") . ">{$ling['em_andamento']}</option>
    <option value=\"2\" " . ($filtro_st == 2 ? "selected=\"selected\"" : "") . ">{$ling['concluida']}</option>
    <option value=\"3\" " . ($filtro_st == 3 ? "selected=\"selected\"" : "") . ">{$ling['cancelada']}</option>
    
    </select>\n
    <br clear=\"all\" />
    <div id=\"filtro_relatorio\">\n";

	$_GET['dir'] = '.';
	FiltrosRelatorio (1, 1, 1, 0);

	echo "</div>
    <label for=\"filtro_desc\" class=\"campo_label\">{$ling['destino2']}:</label>
    <input type=\"text\" size=\"40\"name=\"filtro_desc\" class=\"campo_text\" value=\"$filtro_desc\" />
    
    <input type=\"hidden\" name=\"id\" value=\"$id\" />
    <input type=\"hidden\" name=\"op\" value=\"$op\" />
    <input type=\"hidden\" name=\"oq\" value=\"$oq\" />
    <input type=\"hidden\" name=\"exe\" value=\"$exe\" />
    <input type=\"hidden\" name=\"filtro_tipo\" value=\"$filtro_tipo\" />
    <!-- ZERANDO A PAGINA��O -->
    <input type=\"hidden\" name=\"cont\" value=\"0\" />

    <input class=\"botao\" type=\"submit\" name=\"localizar\" value=\"{$ling['filtrar']}\" />";

	if ($filtro_tipo == 1) {
		$strong = array('<strong>','</strong>','','');
		$tdprev = "<th><acronym title=\"{$ling['destino']}\">D.M.</acronym></th>";
	}
	if ($filtro_tipo == 2) {
		$strong = array('','','<strong>','</strong>');
	}
	echo "
    </fieldset>
    </div>
    </form>
    <br clear=\"all\" />
    <div id=\"lt_tabela\">\n";
    
    // 
    echo "<form method=\"POST\" action=\"atualizar_programacao.php\" target=\"_blank\">";
    
    echo "<table width=\"100%\" cellspacing=\"0\" style=\"font-size:10px;border:0px;\">
    <tr>
    <td class=\"".(($filtro_tipo == 1)?"aba_sel":"aba")."\" onclick=\"{$ling['sol_aba']}('?$query&filtro_tipo=1&cont=0')\">
    <img src=\"imagens/icones/22x22/prog_prev.png\" border=\"0\" /> ".htmlentities($ling['option_prev'])."
    </td>
    <td class=\"aba_entre\">&nbsp;</td>
    <td class=\"".(($filtro_tipo == 2)?"aba_sel":"aba")."\" onclick=\"{$ling['sol_aba']}('?$query&filtro_tipo=2&cont=0')\">
    <img src=\"imagens/icones/22x22/prog_rota.png\" border=\"0\" /> ".htmlentities($ling['option_rotas'])."
    </td>
    <td class=\"aba_fim\">&nbsp;</td>
    </tr>
    <tr><td colspan=\"20\" class=\"aba_quadro\" valign=\"top\">";


	####//// Filtros SQL:
	$filtro_sql = "";
	$tabela_sql = "";

	// STATUS
	if ($filtro_st != 0) {
			
		if($filtro_st == 1){
			if ($manusis['db'][$tdb[PROGRAMACAO]['dba']]['driver'] == 'oci8') {
				$filtro_sql .= " AND P.STATUS != 2 AND P.MID IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= P.MID AND TO_CHAR(o.DATA_PROG, 'YYYY') = $filtro_ano AND o.STATUS = 1)";
			}
			else {
				$filtro_sql .= " AND P.STATUS != 2 AND P.MID IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= P.MID AND DATE_FORMAT(o.DATA_PROG, '%Y') = $filtro_ano AND o.STATUS = 1)";
			}
		}
		elseif($filtro_st == 2){
			if ($manusis['db'][$tdb[PROGRAMACAO]['dba']]['driver'] == 'oci8') {
				$filtro_sql .= " AND ((P.STATUS = 2) OR (P.STATUS = 1 AND P.MID NOT IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= P.MID AND TO_CHAR(o.DATA_PROG, 'YYYY') = $filtro_ano AND o.STATUS = 1 )))";
			}
			else {
				$filtro_sql .= " AND ((P.STATUS = 2) OR (P.STATUS = 1 AND P.MID NOT IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= P.MID AND DATE_FORMAT(o.DATA_PROG, '%Y') = $filtro_ano AND o.STATUS = 1 )))";
			}
		}
		elseif($filtro_st == 3){
			$filtro_sql .= " AND P.STATUS = $filtro_st";
		}
	}

	// FILTROS POR TIPO
	if ($filtro_tipo == 1) {
		// Permiss�o por empresa
		$fil_emp = VoltaFiltroEmpresa(PLANO_PADRAO, 2, FALSE, "PP");
		// Se tiver filtro
		if ($fil_emp != '') {
			$tabela_sql .= ", ".PLANO_PADRAO." PP";
			$filtro_sql .= " AND P.MID_PLANO = PP.MID AND $fil_emp";
		}

		// Descri��o
		$filtro_tmp = "";
		if (($filtro_desc != '') and ($filtro_emp != 0 or $filtro_area != 0 or $filtro_setor != 0)) {
			$tmp_desc = LimpaTexto($filtro_desc);
			$filtro_tmp = " AND (M.COD LIKE '%$tmp_desc%' OR M.DESCRICAO LIKE '%$tmp_desc%')";
		}
		elseif ($filtro_desc != '') {
			$tmp_desc = LimpaTexto($filtro_desc);
			// Buscando maquinas
			$sql_tmp .= "SELECT M.MID FROM " . MAQUINAS . " M WHERE M.COD LIKE '%$tmp_desc%' OR M.DESCRICAO LIKE '%$tmp_desc%'";
		}
			
		// Local. 2
		if ($filtro_setor != 0) {
			// Buscando maquinas
			$sql_tmp = "SELECT M.MID FROM " . MAQUINAS . " M WHERE M.MID_SETOR = $filtro_setor $filtro_tmp";
		}
		// Local. 1
		elseif ($filtro_area != 0) {
			// Buscando maquinas
			$sql_tmp = "SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S WHERE M.MID_SETOR = S.MID AND S.MID_AREA = $filtro_area $filtro_tmp";
		}
		// Empresa
		elseif ($filtro_emp != 0) {
			// Buscando maquinas
			$sql_tmp .= "SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S, " . AREAS . " A WHERE M.MID_SETOR = S.MID AND S.MID_AREA = A.MID AND A.MID_EMPRESA = $filtro_emp $filtro_tmp";
		}


		// Se houve um filtro por localiza��o ou descri��o entre aqui
		if (($filtro_emp != 0) or ($filtro_area != 0) or ($filtro_setor != 0) or ($filtro_desc != '')) {
			// Rodando o select
			if (! $rs_tmp = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql_tmp)){
				erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
			}
			$filtro_tmp = "";
			$filtro_tmp2 = "";
			while (! $rs_tmp->EOF) {
				$filtro_tmp .= ($filtro_tmp == "")? "" : " OR ";
				$filtro_tmp .= "P.MID_MAQUINA = " . $rs_tmp->fields['MID'];

				$filtro_tmp2 .= ($filtro_tmp2 == "")? "" : " OR ";
				$filtro_tmp2 .= "E.MID_MAQUINA = " . $rs_tmp->fields['MID'];
				$rs_tmp->MoveNext();
			}
			// Encontrou maquinas na localiza��o
			if ($filtro_tmp != '') {
				// Maquinas
				$filtro_sql .= " AND ($filtro_tmp";
			}
			// Sem maquinas n�o mostrar nada
			else {
				$filtro_sql .= " AND P.MID_MAQUINA = 0";
			}

			// COMPONENTES
			if ($filtro_desc == '') {
				$sql_tmp = "SELECT E.MID FROM " . EQUIPAMENTOS . " E WHERE $filtro_tmp2";
			}
			else {
				$tmp_desc = LimpaTexto($filtro_desc);

				if(($filtro_emp != 0) or ($filtro_area != 0) or ($filtro_setor != 0)) {
					$filtro_tmp2 .= ($filtro_tmp2 == '')? "" : " AND ";
					$filtro_tmp2 .= "(E.COD LIKE '%$tmp_desc%' OR E.DESCRICAO LIKE '%$tmp_desc%')";
				}
				else {
					$filtro_tmp2 = "(E.COD LIKE '%$tmp_desc%' OR E.DESCRICAO LIKE '%$tmp_desc%')";
				}

				$sql_tmp = "SELECT E.MID FROM " . EQUIPAMENTOS . " E WHERE $filtro_tmp2";
			}

			if (! $rs_tmp = $dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute($sql_tmp)){
				erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[EQUIPAMENTOS]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
			}
			$filtro_tmp3 = "";
			while (! $rs_tmp->EOF) {
				$filtro_tmp3 .= ($filtro_tmp3 == "")? "" : " OR ";
				$filtro_tmp3 .= "P.MID_EQUIPAMENTO = " . $rs_tmp->fields['MID'];
				$rs_tmp->MoveNext();
			}
			if(($filtro_tmp3 != "") and ($filtro_tmp != "")) {
				$filtro_sql .= " OR $filtro_tmp3)";
			}
			elseif($filtro_tmp3 != "") {
				$filtro_sql .= " AND ($filtro_tmp3)";
			}
			elseif(($filtro_tmp3 == "") and ($filtro_tmp != "")) {
				$filtro_sql .= ") AND P.MID_EQUIPAMENTO = 0";
			}
		}
	}

	if ($filtro_tipo == 2) {
		// PARA BUSCAR AS MAQUINAS PELO ROTEIRO
		$tabela_sql .= ", ".PLANO_ROTAS." PP";
		$filtro_sql .= " AND P.MID_PLANO = PP.MID";

		// Permiss�o por empresa
		$fil_emp = VoltaFiltroEmpresa(PLANO_ROTAS, 2, FALSE, "PP");
		// Se tiver filtro
		if ($fil_emp != '') {
			$filtro_sql .= " AND $fil_emp";
		}

		// Descri��o
		$filtro_tmp = "";
		if (($filtro_desc != '') and ($filtro_emp != 0 or $filtro_area != 0 or $filtro_setor != 0)) {
			$tmp_desc = LimpaTexto($filtro_desc);
			$filtro_tmp = " AND (M.COD LIKE '%$tmp_desc%' OR M.DESCRICAO LIKE '%$tmp_desc%')";
		}
		elseif ($filtro_desc != '') {
			$tmp_desc = LimpaTexto($filtro_desc);
			// Buscando maquinas
			$sql_tmp .= "SELECT M.MID FROM " . MAQUINAS . " M WHERE M.COD LIKE '%$tmp_desc%' OR M.DESCRICAO LIKE '%$tmp_desc%'";
		}
			
		// Local. 2
		if ($filtro_setor != 0) {
			// Buscando maquinas
			$sql_tmp = "SELECT M.MID FROM " . MAQUINAS . " M WHERE M.MID_SETOR = $filtro_setor $filtro_tmp";
		}
		// Local. 1
		elseif ($filtro_area != 0) {
			// Buscando maquinas
			$sql_tmp = "SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S WHERE M.MID_SETOR = S.MID AND S.MID_AREA = $filtro_area $filtro_tmp";
		}
		// Empresa
		elseif ($filtro_emp != 0) {
			// Buscando maquinas
			$sql_tmp .= "SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S, " . AREAS . " A WHERE M.MID_SETOR = S.MID AND S.MID_AREA = A.MID AND A.MID_EMPRESA = $filtro_emp $filtro_tmp";
		}


		// Se houve um filtro por localiza��o ou descri��o entre aqui
		if (($filtro_emp != 0) or ($filtro_area != 0) or ($filtro_setor != 0) or ($filtro_desc != '')) {
			// Rodando o select
			if (! $rs_tmp = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql_tmp)){
				erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
			}
			$filtro_tmp = "";
			while (! $rs_tmp->EOF) {
				$filtro_tmp .= ($filtro_tmp == "")? "" : " OR ";
				$filtro_tmp .= "R.MID_MAQUINA = " . $rs_tmp->fields['MID'];
				$rs_tmp->MoveNext();
			}
			// Encontrou maquinas na localiza��o
			if ($filtro_tmp != '') {
				// Maquinas
				$filtro_sql .= " AND P.MID_PLANO IN (SELECT MID_PLANO FROM " . ROTEIRO_ROTAS . " R WHERE $filtro_tmp)";
			}
			// Sem maquinas n�o mostrar nada
			else {
				$filtro_sql .= " AND P.MID = 0";
			}
		}
	}

	// Para cada PROGRAMACAO
	if ($manusis['db'][$tdb[PROGRAMACAO]['dba']]['driver'] == 'oci8') {
		$sql = "SELECT P.MID, P.TIPO, P.MID_PLANO, P.DATA_INICIAL, P.DATA_FINAL, P.MID_MAQUINA, P.MID_CONJUNTO, P.MID_EQUIPAMENTO, P.MOTIVO, P.STATUS, TO_CHAR(P.DATA_CANCELAMENTO, 'YYYY-MM-DD HH24:MI:SS') AS DATA_CANCELAMENTO FROM ".PROGRAMACAO." P $tabela_sql WHERE  (TO_CHAR(P.DATA_INICIAL, 'YYYY') <= $filtro_ano AND TO_CHAR(P.DATA_FINAL, 'YYYY') >= $filtro_ano) AND P.TIPO = $filtro_tipo $filtro_sql ORDER BY P.DATA_INICIAL";
	}
	else {
		$sql = "SELECT P.* FROM ".PROGRAMACAO." P $tabela_sql WHERE  (DATE_FORMAT(P.DATA_INICIAL, '%Y') <= $filtro_ano AND  DATE_FORMAT(P.DATA_FINAL, '%Y') >= $filtro_ano) AND P.TIPO = $filtro_tipo $filtro_sql ORDER BY P.DATA_INICIAL";
	}

	if (! $resultado = $dba[$tdb[PROGRAMACAO]['dba']] -> SelectLimit($sql, $buscar_qto, $cont)){
		erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
	}

	$nresultado = $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql);
	$nnlinhas = $nresultado -> NumRows();
	$iii = 1;
	if (($nnlinhas != 0) and ($nnlinhas > $buscar_qto)) {
		echo "<span id=\"lt_paginas\">".$ling['paginas'].": \n";
		for ($nlinhas=0; $nlinhas < $nnlinhas; $nlinhas = $nlinhas + $buscar_qto) {
			if ($cont == $nlinhas) echo "<strong>";
			echo "<a href=\"javascript: atualiza_area2('corpo', 'manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&cont=$nlinhas');\">[ $iii ]</a> \n";
			if ($cont == $nlinhas) echo "</strong>\n";
			$iii++;
		}
		echo "</span>\n";
	}

    // Para vis�o em cronograma muda um pouco
	if ($exe == 3) {
		echo "<table id=\"lt_tabela_\">
        <tr style=\"font-size:8px;\">
        <th rowspan=\"2\" width=\"5\"><acronym title=\"{$ling['prog_extender']}\">E.</acronym></th>
        <th rowspan=\"2\">{$ling['destino']}<br />{$tdb[PROGRAMACAO]['MID_PLANO']}</th>
        <th colspan=\"53\">{$ling['plan05']}</th>
        <th rowspan=\"2\">{$ling['opcoes']}</th>
        </tr>";

		// Listando as semanas
		echo "<tr>";
		$mk = mktime(0, 0, 0, 1, 1, $filtro_ano);
		while (date('Y', $mk) == $filtro_ano) {
			$mes = date('n', $mk);
			$dia = date('j', $mk);

			echo "<td width=\"15\" style=\"background-color:#a6bcd9;font-size:7px;\"><strong>" . date('W', $mk) . "</strong></td>";

			$mk = mktime(0, 0, 0, $mes, $dia + 7, $filtro_ano);
		}

		echo "</tr>";
	}
	else {
		echo "<table id=\"lt_tabela_\">
        <tr>
        <th width=\"5\"><acronym title=\"{$ling['prog_extender']}\">Ext.</acronym></th>
        <th>{$ling['destino']}</th>
        <th>{$tdb[PROGRAMACAO]['MID_PLANO']}</th>
        <th>{$tdb[PROGRAMACAO]['DATA_INICIAL']}</th>
        <th>{$tdb[PROGRAMACAO]['DATA_FINAL']}</th>
        <th>% {$ling['concluido']} $filtro_ano</th>
        <th>{$ling['opcoes']}</th>
        </tr>";
	}
	$trcor = 'cor1';
	while (! $resultado->EOF) {
		$campo=$resultado->fields;
		$eordens[0] = 0; // numero de ordens fechadas (STATUS=2)
		$eordens[1] = 0; // numero total de ordens

		// pega as ordens
		if ($manusis['db'][$tdb[PROGRAMACAO]['dba']]['driver'] == 'oci8') {
			$sql = "SELECT * FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO='{$campo['MID']}' AND TO_CHAR(DATA_PROG, 'YYYY') = $filtro_ano AND STATUS <> 3 ORDER BY MID_PROGRAMACAO";
		}
		else {
			$sql = "SELECT * FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO='{$campo['MID']}' AND YEAR(DATA_PROG) = $filtro_ano AND STATUS <> 3 ORDER BY MID_PROGRAMACAO";
		}
		$reordem=$dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
		while (!$reordem->EOF) {
			$caordem=$reordem->fields;
			if ($caordem['STATUS'] == '2') $eordens[0]++;
			$eordens[1]++;
			$reordem->MoveNext();
		}
		if ($eordens[1] > 0) {
			$completo = $eordens[0]/$eordens[1]; // porcentagem decimal (1 significa 100%)
			$completo_str = round($completo*100,2);
		}
		else {
			$completo = -1; // o IF � pra n�o dar division by zero
			$completo_str = '-';
		}

		if($eordens[1] == 0 AND $eordens[0] == 0)
		{
			$eordens[0] = 100;
			$eordens[1] = 100;
			$completo = 1;
			$completo_str = 100;
		}

		if ((!$filtro_st)
		or (($filtro_st == 1) and ($completo != 1))
		or (($filtro_st == 3) and ($completo != 1))
		or (($filtro_st == 2) and ($completo == 1)) or true) { // se n�o foi filtrada por completas, ou se est� completa
			// mostra essa linha na tabela

			$cancela = "";
			$deleta = "";
			$detalha = "";
			// campos que s�o relativos a PLANO ou ROTA
			if ($filtro_tipo == 1) {
				// pega destino
				$tdprev = "";
				if ($campo['MID_MAQUINA']) {
					$maq = htmlentities(strtoupper(VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])));
					$maq_cod = htmlentities(strtoupper(VoltaValor(MAQUINAS,'COD','MID',$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])));
					$tdprev .= "<img src=\"imagens/icones/22x22/local3.png\">";
				}
				if ($campo['MID_EQUIPAMENTO']) {
					$maq = htmlentities(strtoupper(VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$campo['MID_EQUIPAMENTO'],$tdb[EQUIPAMENTOS]['dba'])));
					$maq_cod = htmlentities(strtoupper(VoltaValor(EQUIPAMENTOS,'COD','MID',$campo['MID_EQUIPAMENTO'],$tdb[EQUIPAMENTOS]['dba'])));
					$tdprev .= "<img src=\"imagens/icones/22x22/equip.png\">";
				}

				$plano = htmlentities(strtoupper(VoltaValor(PLANO_PADRAO,'DESCRICAO','MID',$campo['MID_PLANO'],$tdb[PLANO_PADRAO]['dba'])));

				if (VoltaPermissao($id, $op) == 1) {
					if ($campo['STATUS'] != 3) {
						$cancela = "<a href=\"manusis.php?id=$id&op=$op&exe=$exe&oq=".$campo['MID']."&cancela=".PROGRAMACAO."\" onclick=\"return confirma(this, '{$ling['prog_cancela_progra']}', 2)\"><img src=\"imagens/icones/22x22/prog_del.png\" border=\"0\" title=\" {$ling['cancelar']} \" alt=\" \" /></a>";
					}

					$deleta = "<a href=\"javascript: confirma_delete('".PROGRAMACAO."', '".$campo['MID']."', '{$ling['prog_deleta_progra']}')\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" title=\" ".$ling['remover']." \" alt=\" \" /></a>";
				}

				$detalha = "<a href=\"manusis.php?id=$id&op=$op&exe=23&oq=".$campo['MID']."\"><img src=\"imagens/icones/22x22/programacao_plano.png\" border=\"0\" title=\" {$ling['prog_detalhamento']} \" alt=\" \" /></a>";
			}
			if ($filtro_tipo == 2) {
				// lista de maquinas
				// verifica quantas maquinas s�o
				$tdprev = '';
				$sql = "SELECT DISTINCT MID_MAQUINA FROM ".LINK_ROTAS." WHERE MID_PLANO = '{$campo['MID_PLANO']}'";
				$relink=$dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql);
				$imaqs = 0;
				$maqtmp='';
				while (!$relink->EOF) {
					$calink=$relink->fields;
					// so conta esta maq caso ela ainda exista na tabela de maquinas
					if (VoltaValor(MAQUINAS,'MID','MID',$calink['MID_MAQUINA'],0)) {
						$imaqs++;
						if (! $maqtmp) {
							if ($exe == 3) {
								$maqtmp = htmlentities(strtoupper(VoltaValor(MAQUINAS,'COD','MID',$calink['MID_MAQUINA'],0)));
							}
							else {
								$maqtmp = htmlentities(strtoupper(VoltaValor(MAQUINAS,'DESCRICAO','MID',$calink['MID_MAQUINA'],0)));
							}
						}
					}
					$relink->MoveNext();
				}
				// caso seja so 1 maquina, mostra logo ela
				if ($imaqs == 1) $maq = $maqtmp;
				else $maq = "<div id=\"prog{$campo['MID']}\" style=\"text-align: left\"><a href=\"javascript:atualiza_area2('prog{$campo['MID']}','$ajaxdestino?ajax=rotamaq&prog={$campo['MID']}')\"><img src=\"imagens/icones/mais.gif\" border=\"0\" /> $maqtmp</a></div>";

				// outras variaveis
				$plano = htmlentities(strtoupper(VoltaValor(PLANO_ROTAS,'DESCRICAO','MID',$campo['MID_PLANO'],$tdb[PLANO_ROTAS]['dba'])));

				if (VoltaPermissao($id, $op) == 1) {
					if ($campo['STATUS'] != 3) {
						$cancela = "<a href=\"manusis.php?id=$id&op=$op&exe=$exe&oq=".$campo['MID']."&cancela=".PROGRAMACAO."\" onclick=\"return confirma(this, '{$ling['prog_cancela_progra']}', 2)\"><img src=\"imagens/icones/22x22/prog_del.png\" border=\"0\" title=\" {$ling['cancelar']} \" alt=\" \" /></a>";
					}

					$deleta = "<a href=\"javascript: confirma_delete('".PROGRAMACAO."', '".$campo['MID']."', '{$ling['prog_deleta_progra']}')\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" title=\" ".$ling['remover']." \" alt=\" \" /></a>";
				}

				$detalha = "<a href=\"manusis.php?id=$id&op=$op&exe=23&oq=".$campo['MID']."\"><img src=\"imagens/icones/22x22/programacao_rota.png\" border=\"0\" title=\" {$ling['prog_detalhamento']} \" alt=\" \" /></a>";
			}
				
			// Datas
			$datai = NossaData($campo['DATA_INICIAL']);
			$dataf = NossaData($campo['DATA_FINAL']);


			if ($campo['STATUS'] == 3) {
				// cancelada
				$trcor2 = "bgcolor=\"#FFCC99\"";
			}
			elseif ($completo == 1) {
				// completa
				$trcor2 = "bgcolor=\"#FFFF99\"";
			}
			else {
				if ($trcor == 'cor1') $trcor = 'cor2'; else $trcor = 'cor1';
				$trcor2 = "class=\"$trcor\"";
			}

			if ($exe == 3) {
				echo "<tr style=\"font-size:8px;\" $trcor2><td>";
				if($campo['STATUS'] != 3)
				echo "<input type=\"checkbox\" id=\"aprog\" name=\"aprog[".$campo['MID']."]\" value=\"1\" />";
				echo "</td>
                <td>$tdprev ";

				if($filtro_tipo == 1) {
					echo "<acronym title=\"$maq\">$maq_cod</acronym>";
				}
				elseif($filtro_tipo == 2) {
					echo "$maq &nbsp;";
				}

				echo "<br clear=\"all\"/>";

				if(strlen($plano) > 30) {
					echo "<acronym title=\"$plano\">";
				}

				echo substr($plano, 0, 30) . "";


				if(strlen($plano) > 30) {
					echo "</acronym>";
					echo " ...";
				}

				echo "</td>\n";

				// Listando as semanas
				$mk = mktime(0, 0, 0, 1, 1, $filtro_ano);
                $j = 0;
				while (date('Y', $mk) == $filtro_ano) {
					$mes = date('n', $mk);
					$dia = date('j', $mk);
					$semana = date('W', $mk);
					$os_semana = "";
                    
                    /*
                     * Como o statement sql abaixo realiza a busca passando por filtro ano e semana,
                     * o bloco de condicionais abaixo, garante que na semana 1 do ano seguinte n�o sejam
                     * exibidas as ordens da semana 1 do ano vigente ex |1|2.........|52|1|
                     * O mesmo no caso |52|1.........|51|52|
                     */
                    
                    if($semana > 1 and $j == 0){
                        $year = $filtro_ano - 1;
                    }
                    elseif($semana == 1 and $j > 0){
                        $year = $filtro_ano + 1;
                    }
                    else{
                        $year = $filtro_ano;
                    }

					// Buscando OS nessa semana nessa programa��o
					if ($manusis['db'][$tdb[PROGRAMACAO]['dba']]['driver'] == 'oci8') {
						$sql_tmp = "SELECT MID, DATA_PROG, STATUS, NUMERO FROM " . ORDEM . " WHERE MID_PROGRAMACAO = {$campo['MID']} AND TO_CHAR(DATA_PROG, 'YYYY') = $year AND TO_CHAR(DATA_PROG, 'IW') = $semana";
					}
					else {
						$sql_tmp = "SELECT MID, DATA_PROG, STATUS, NUMERO FROM " . ORDEM . " WHERE MID_PROGRAMACAO = {$campo['MID']} AND YEAR(DATA_PROG) = $year AND WEEK(DATA_PROG) = $semana";
					}
						
					if (! $rs_tmp = $dba[$tdb[ORDEM]['dba']] -> Execute($sql_tmp)){
						erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
					}
					// Sem OS
					if($rs_tmp->EOF) {
						$os_semana = "&nbsp;";
					}
					// MOSTRA TODAS AS OS DA SEMANA
					while(! $rs_tmp->EOF) {
						// Tem OS aberta
						if($rs_tmp->fields['STATUS'] == 1) {
							$os_semana .= "<a href=\"javascript:janela('detalha_ord.php?busca={$rs_tmp->fields['MID']}', 'parm', 500,400)\" title=\"{$ling['prog_detalha_ord']} {$rs_tmp->fields['NUMERO']}\"><img src=\"imagens/icones/graydot.jpg\" border=\"0\" width=\"9\" />{$ano}</a>\n";
						}
						// Tem OS FECHADA
						elseif($rs_tmp->fields['STATUS'] == 2) {
							$os_semana .= "<a href=\"javascript:janela('detalha_ord.php?busca={$rs_tmp->fields['MID']}', 'parm', 500,400)\" title=\"{$ling['prog_detalha_ord']} {$rs_tmp->fields['NUMERO']}\"><img src=\"imagens/icones/blackdot.jpg\" border=\"0\" width=\"9\" />{$ano}</a>\n";
						}
						$rs_tmp->MoveNext();
					}

					echo "<td width=\"15\" align=\"center\" valign=\"top\">{$os_semana}</td>";

					$mk = mktime(0, 0, 0, $mes, $dia + 7, $filtro_ano);
                    $j ++;
				}

				echo "<td height=\"55\">$detalha $cancela $deleta</td>
                </tr>";
					
			}
			else {
				echo "<tr style=\"font-size:10px;\" $trcor2>
                <td>";

				if($campo['STATUS'] != 3) {
					echo "<input type=\"checkbox\" id=\"aprog\" name=\"aprog[".$campo['MID']."]\" value=\"1\" />";
				}

				echo "</td>
                <td>$tdprev $maq</td>
                <td>$plano</td>
                <td>$datai</td>
                <td>$dataf</td>
                <td>$completo_str</td>
                <td>$detalha $cancela $deleta</td>
                </tr>\n";
			}

			if ($campo['STATUS'] == 3) {
				$dt = explode(' ', $campo['DATA_CANCELAMENTO']);
				$dtCancela = NossaData($dt[0]) . ' ' . $dt[1];

				echo "<tr $trcor2><td>&nbsp;</td><td colspan=\"" . (($exe == 3)? 55 : 6) . "\"> ->
				{$ling['motivo_cancel']}: " . htmlentities($campo['MOTIVO']) . "&nbsp;&nbsp; Data: " . $dtCancela . "</td></tr>";

			}
		}

		$resultado->MoveNext();
	}


	echo "</table>
    <div id=\"lt_rodape\">{$ling['registros_encontrados']}: $nnlinhas";

	if (VoltaPermissao($id, $op) == 1) {
		echo " <strong><a style=\"color:blue;text-decoration:underline;\" href=\"javascript:janela('logs.php?id=" . PROGRAMACAO . "', 'parm', 500,400)\">".$ling['ultimas_alteracoes']."</a></strong>";
	}

	echo "</div>
    </td>
	</tr>
	</table>
    <br clear=\"all\">\n";

	// Data
	FormData("{$ling['prog_muda_data_progra']}:", "ano", "", "campo_label", "", "campo_text_ob");

    
	echo " &nbsp;&nbsp;<input type=\"submit\" name=\"atribui\" class=\"botao\" value=\"{$ling['prog_atu_progra']}\">
    
    </form></div>";

}


if (($exe == 21) or ($exe == 22) or ($exe == 23) or ($exe == 24) and ($oq != "")) {
	$plano=VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$oq,$tdb[PROGRAMACAO]['dba']);
	$tipo2=VoltaValor(PROGRAMACAO,"TIPO","MID",$oq,$tdb[PROGRAMACAO]['dba']);
	if (($tipo2 == 1) and ($plano == 0)) {
		$plano_desc=htmlentities(strtoupper(VoltaValor(MAQUINAS,"DESCRICAO","MID",VoltaValor(PROGRAMACAO,"MID_MAQUINA","MID",$oq,$tdb[PROGRAMACAO]['dba']),$tdb[MAQUINAS]['dba'])));
	}
	elseif (($tipo2 == 1) and ($plano != 0)) {
		$plano_desc=htmlentities(strtoupper(VoltaValor(MAQUINAS,"DESCRICAO","MID",VoltaValor(PROGRAMACAO,"MID_MAQUINA","MID",$oq,$tdb[PROGRAMACAO]['dba']),$tdb[MAQUINAS]['dba']))." {$ling['plano']}: ".strtoupper(VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$plano,$tdb[PLANO_PADRAO]['dba'])));
	}
	if ($tipo2 == 2) {
		$plano_desc=htmlentities(strtoupper(VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$plano,$tdb[PLANO_PREDITIVA]['dba'])));
	}
	echo "<div id=\"mod_menu\">";
	echo "
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=2\">
    <img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['programacao']."\" />
    <span>".$ling['programacao']."</span>
    </a>
    </div>";

	echo "
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=22&oq=$oq\">
    <img src=\"imagens/icones/22x22/material.png\" border=\"0\" alt=\"".$ling['prog_material']."\" />
    <span>".$ling['prog_material']."</span>
    </a>
    </div>
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=23&oq=$oq\">
    <img src=\"imagens/icones/22x22/crono.png\" border=\"0\" alt=\"{$ling['prog_ordens']}\" />
    <span>{{$ling['prog_ordens']}}</span>
    </a>
    </div>
    
    <div>
    <h3>$plano_desc</h3>
    </div>

    </div>
    <br clear=\"all\" />
    <div>";
}
if (($exe == 21) or ($exe == 22) and ($oq != "")) {
	$campos[0]="MID_MATERIAL";
	$campos[1]="QUANTIDADE";
	ListaTabela (PROGRAMACAO_MATERIAIS,"MID",$campos,"PROGRAMACAO_MATERIAL","MID_PROGRAMACAO",$oq,1,0,1,1,0,1,1,1,1,1,"");
}
if (($exe == 23) and ($oq != "")) {
	$campos[0]="NUMERO";
	$campos[1]="DATA_PROG";
	$campos[2]="STATUS";
	ListaTabela (ORDEM_PLANEJADO,"MID",$campos,"","MID_PROGRAMACAO",$oq,0,0,0,0,0,1,1,0,1,1,"");
}
echo "</div>";

?>
