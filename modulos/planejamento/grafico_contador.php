<?
/**
* Modulo planejamento, preditiva (ajax)
* 
* @author  Fernando Cosentino
* @version  3.0
* @package planejamento
* @subpackage preditiva
*/

$basedir = '../../';
$swfbasedir = '../../';

// Fun��es do Sistema
if (!require("{$basedir}lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("{$basedir}conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("{$basedir}lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("{$basedir}lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("{$basedir}lib/bd.php")) die ($ling['bd01']);


$ponto = (int)$_GET['ponto'];
$last = (int)$_GET['last'];
$queryself = "grafico_contador.php?ponto=$ponto&datai={$_GET['datai']}&dataf={$_GET['dataf']}";
$phpajax = "grafico_contador.php";
$cont_onchange = "location.href='$phpajax?ok=1&ponto='+this.options[this.selectedIndex].value+'&datai='+document.getElementById('datai').value+'&dataf='+document.getElementById('dataf').value";

$ajax = $_GET['ajax'];

if ($ajax == 'area') {
	$area=(int)$_GET['area'];
	if ($area) $sqlcond = "WHERE MID_AREA = '$area'";
	else $sqlcond = '';
	echo "<label class=\"campo_label \" for=\"filtro_setor\">".$tdb[SETORES]['DESC'].":</label>";
	echo " <select name=\"filtro_setor\" id=\"filtro_setor\" class=\"campo_select\" onchange=\"atualiza_area2('maq','$phpajax?ajax=setor&setor=' + this.options[this.selectedIndex].value)\">";
	$tmp=$dba[$tdb[SETORES]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".SETORES." $sqlcond ORDER BY COD ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_setor'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".htmlentities($campo['COD']."-".$campo['DESCRICAO'])."</option>";
		else echo "<option value=\"".$campo['MID']."\">".htmlentities($campo['COD']."-".$campo['DESCRICAO'])."</option>";
		$tmp->MoveNext();
	}
	echo "</select>";
}
elseif ($ajax == 'setor') {
	$setor=(int)$_GET['setor'];
	if ($setor) $sqlcond = "WHERE MID_SETOR = '$setor'";
	else $sqlcond = '';
	echo "<label class=\"campo_label \" for=\"filtro_maq\">".$tdb[MAQUINAS]['DESC'].":</label>";
	echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\" onchange=\"atualiza_area2('contador','$phpajax?ajax=maq&maq=' + this.options[this.selectedIndex].value)\">";
	$tmp=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MODELO,MID FROM ".MAQUINAS." $sqlcond ORDER BY COD ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".htmlentities($campo['COD']."-".$campo['DESCRICAO'])."</option>";
		else echo "<option value=\"".$campo['MID']."\">".htmlentities($campo['COD']."-".$campo['DESCRICAO'])." </option>";
		$tmp->MoveNext();
	}
	echo "</select>";
}
elseif ($ajax == 'maq') {
	$maq=(int)$_GET['maq'];
	if ($maq) $sqlcond = "WHERE MID_MAQUINA = '$maq'";
	else $sqlcond = '';
	echo "<label class=\"campo_label \" for=\"filtro_contador\">".$tdb[MAQUINAS_CONTADOR]['DESC'].":</label>
	<select name=\"filtro_contador\" id=\"filtro_contador\" class=\"campo_select\" onchange=\"$cont_onchange\">";
	$tmp=$dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute("SELECT DESCRICAO,MID FROM ".MAQUINAS_CONTADOR." $sqlcond ORDER BY DESCRICAO ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_contador'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".htmlentities($campo['DESCRICAO'])."</option>";
		else echo "<option value=\"".$campo['MID']."\">".htmlentities($campo['DESCRICAO'])." </option>";
		$tmp->MoveNext();
	}
	echo "</select>";
	
}
elseif ($_GET['ok']) {
	$ponto = (int)$_GET['ponto'];
	$last = (int)$_GET['last'];
	if ($_GET['datai']) $datai = DataSQL($_GET['datai']);
	if ($_GET['dataf']) $dataf = DataSQL($_GET['dataf']);
	
	$mid_maq = (int)VoltaValor(MAQUINAS_CONTADOR,'MID_MAQUINA','MID',$ponto,0);
	$maq_desc = VoltaValor(MAQUINAS,'DESCRICAO','MID',$mid_maq,0);

	$sqldata = '';
	if ($datai) $sqldata .= " AND DATA >= '$datai'";
	if ($dataf) $sqldata .= " AND DATA <= '$dataf'";
	
	$sql = "SELECT * FROM ".LANCA_CONTADOR." WHERE MID_CONTADOR = '$ponto' $sqldata ORDER BY DATA DESC";
	$tmp=$dba[0] ->Execute($sql);
	$num = count($tmp->getrows());
	
	if ($last > ($num-20)) $last = $num-20;
	if ($last < 0) $last = 0;
	
	$linkstyle= "hover{color:white}";
	
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
	<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
	<head>
	 <meta http-equiv=\"pragma\" content=\"no-cache\" />
	<title>{$ling['manusis']}</title>
	<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
	<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
		echo "</head><body><div id=\"central_relatorio\">
	<div id=\"cab_relatorio\">
	<h1>{$ling['grafico_contador']}
	</div>
	<div id=\"corpo_relatorio\"><center><strong>{$tdb[MAQUINAS]['DESC']}: $maq_desc</strong></center><br>";
	
	include("{$basedir}lib/swfcharts/charts.php");
	
	echo "<div align=\"center\">";
	echo InsertChart("{$swfbasedir}lib/swfcharts/charts.swf", "{$swfbasedir}lib/swfcharts/charts_library","{$swfbasedir}modulos/planejamento/cont_graf.php?ponto=$ponto&last=$last", 750, 210, 'FFFFFF' );
	echo "</div>";
	
	###
	// setas e links
	
	echo "<table width=\"700\" id=\"lt\" align=\"center\"><tr><td align=\"left\" width=70>";
	// seta esquerda
	echo "<a class=\"link\" href=\"$queryself&ok=1&last=".($num-20)."\"><img src=\"{$basedir}imagens/icones/22x22/seta_dupla_esquerda.png\" alt=\"\" border=0></a>";
	echo " <a class=\"link\" href=\"$queryself&ok=1&last=".($last+10)."\"><img src=\"{$basedir}imagens/icones/22x22/seta_esquerda.png\" alt=\"\" border=0></a>";
	
	
	echo "</td><td width=\"60%\" align=\"center\">";
	//excel
	//echo "<a class=\"link\" style=\"$linkstyle\" href=\"{$basedir}geragrafico.php?tb=PREDITIVA&ponto=$ponto&last=$last&excel=1\" target=\"_blank\"><img src=\"{$basedir}imagens/icones/22x22/graficos.png\" alt=\"\" border=0> Exportar para MS Excel</a>";
	
	
	echo "</td><td align=\"right\" width=70>";
	// seta direita
	echo "<a class=\"link\" href=\"$queryself&ok=1&last=".($last-10)."\"><img src=\"{$basedir}imagens/icones/22x22/seta_direita.png\" alt=\"\" border=0></a>";
	echo " <a class=\"link\" href=\"$queryself&ok=1&last=0\"><img src=\"{$basedir}imagens/icones/22x22/seta_dupla_direita.png\" alt=\"\" border=0></a>";
	
	echo "</td></tr></table>
	
	<br>
	<center>
	<iframe name=\"cont_lista\" marginWidth=0 marginHeight=0 frameBorder=0 width=200 scrolling=\"auto\" height=400
			src=\"cont_graf.php?ponto=$ponto&last=$last&datai={$_GET['datai']}&dataf={$_GET['dataf']}&print=1\">
	</iframe>
	</center>
	
	</div></body></html>";
}
else {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['grafico_contador']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">";

	echo "<fieldset><legend>{$ling['periodo_opcional']}</legend>";
	FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
	echo "<br clear=\"all\" />";
	FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");
	echo "
	</fieldset>";
	
	echo "<fieldset><legend>{$ling['localizacoes']}</legend>	
	<label class=\"campo_label \" for=\"filtro_area\">".$tdb[AREAS]['DESC'].":</label>";
	echo " <select name=\"filtro_area\" id=\"filtro_area\" class=\"campo_select\" onchange=\"atualiza_area2('setor','$phpajax?ajax=area&area=' + this.options[this.selectedIndex].value)\">";
	$tmp=$dba[$tdb[AREAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".AREAS." ORDER BY COD ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_area'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		$tmp->MoveNext();
	}
	echo "</select><br clear=\"all\" />
	<div id=\"setor\"><label class=\"campo_label \" for=\"filtro_setor\">".$tdb[SETORES]['DESC'].":</label>";
	echo " <select name=\"filtro_setor\" id=\"filtro_setor\" class=\"campo_select\" onchange=\"atualiza_area2('maq','$phpajax?ajax=setor&setor=' + this.options[this.selectedIndex].value)\">";
	$tmp=$dba[$tdb[SETORES]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".SETORES." ORDER BY COD ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_setor'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		$tmp->MoveNext();
	}
	echo "</select></div><br clear=\"all\" />";
	echo "<label class=\"campo_label \" for=\"filtro_maq_fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
	$primeirocampo=strtoupper($ling['todos']);
	FormSelect("filtro_maq_fam",MAQUINAS_FAMILIA,$_GET['filtro_maq_fam'],"DESCRICAO","MID",$tdb[MAQUINAS_FAMILIA]['dba'],0);


	echo "<br clear=\"all\" /><div id=\"maq\"><label class=\"campo_label \" for=\"filtro_maq\">".$tdb[MAQUINAS]['DESC'].":</label>";
	echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\" onchange=\"atualiza_area2('contador','$phpajax?ajax=maq&maq=' + this.options[this.selectedIndex].value)\">";
	$tmp=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".MAQUINAS." ORDER BY COD ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		$tmp->MoveNext();
	}
	echo "</select></div>";
	
	echo "<br clear=\"all\"><div id=\"contador\">
	<label class=\"campo_label \" for=\"filtro_contador\">".$tdb[MAQUINAS_CONTADOR]['DESC'].":</label>
	<select name=\"filtro_contador\" id=\"filtro_contador\" class=\"campo_select\" onchange=\"$cont_onchange\">";
	$tmp=$dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute("SELECT DESCRICAO,MID FROM ".MAQUINAS_CONTADOR." ORDER BY DESCRICAO ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_contador'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".htmlentities($campo['DESCRICAO'])."</option>";
		else echo "<option value=\"".$campo['MID']."\">".htmlentities($campo['DESCRICAO'])." </option>";
		$tmp->MoveNext();
	}
	echo "</select>
	</div>";

	echo "</fieldset>";

	echo "
<br />
</form><br />
</div>
</div>
</body>
</html>";
	
}



?>
