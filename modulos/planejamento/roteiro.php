<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino
* Nota: M�dulo Planejamento
*/

// ok, eu sei que vc n�o aguenta mais ver isso, mas vai tuuudo em array!

// array com as maquinas novas a serem atualizadas,
// no formato $novos[MID_MAQUINA] = MID_MAQUINA
$tmpr=$dba[$tdb[LINK_ROTAS]['dba']] -> Execute("SELECT DISTINCT MID_MAQUINA FROM ".LINK_ROTAS." WHERE MID_PLANO = '$oq'");
while (!$tmpr->EOF) {
	$campor=$tmpr->fields;
	$novos[$campor['MID_MAQUINA']]=$campor['MID_MAQUINA'];
	$tmpr->MoveNext();
}

// array com as posi��es e m�quinas j� cadastradas no roteiro
// no formato $antigos[MID_MAQUINA] = MID_MAQUINA
// aproveita e verifica se a maquina ainda est� na lista nova ($novos[]),
// e se n�o estiver, ja deleta do roteiro
$tmpr=$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("SELECT * FROM ".ROTEIRO_ROTAS." WHERE MID_PLANO = '$oq' ORDER BY POSICAO ASC");
$ultima_posicao = 0;
$deleted = 0;
while (!$tmpr->EOF) {
	$campor=$tmpr->fields;
	$antigos[$campor['MID_MAQUINA']]=$campor['MID_MAQUINA'];
	if ($ultima_posicao < $campor['POSICAO']) {
		$ultima_posicao = $campor['POSICAO'];
	}
	if (!$novos[$campor['MID_MAQUINA']]) {
		// maquina n�o est� na nova lista, deleta ela e avisa que algo foi deletado
		$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("DELETE FROM ".ROTEIRO_ROTAS." WHERE MID = '".$campor['MID']."' LIMIT 1");
		$deleted = 1;
	}
	$tmpr->MoveNext();
}

if ($deleted) {
	// alguma coisa foi deletada, re-organiza o campo POSICAO
	$np = 0; // variavel da nova posi��o de cada item, aumentada ANTES do bloco
	$tmpr=$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("SELECT * FROM ".ROTEIRO_ROTAS." WHERE MID_PLANO = '$oq' ORDER BY POSICAO ASC");
	while (!$tmpr->EOF) {
		$np++;
		$campor=$tmpr->fields;
		if ($campor['POSICAO'] != $np) {
			$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("UPDATE ".ROTEIRO_ROTAS." SET POSICAO='$np' WHERE MID=".$campor['MID']." LIMIT 1");
		}
		$tmpr->MoveNext();
	}
	$ultima_posicao = $np;
}

// a essa altura, $ultima_posicao contem a ultima posicao ocupada, JA ATUALIZADA

// roda foreach adicionando as maquinas novas
$np = $ultima_posicao;
if ($novos) {
	foreach ($novos as $novomid) {
		if (!$antigos[$novomid]) { // maquina ainda nao esta na lista, adiciona
			$np++;
			$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("INSERT INTO ".ROTEIRO_ROTAS." (MID_PLANO,MID_MAQUINA,POSICAO) VALUES ('$oq', '$novomid', '$np')");
		}
	}
}

if (!$naomostraroteiro) { // n�o evita mostrar roteiro
	echo "<div id=\"lt_tabela\">";
	
	include("modulos/planejamento/mostra_roteiro.php");
	
	echo "</div>";
}
?>