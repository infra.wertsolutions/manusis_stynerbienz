<?
/**
* Cadastro de Checklist da Copagaz
* @autor Fernando Cosentino
* Nota: M�dulo Planejamento.
*/

$f_status = (int)$_GET['stat'];
$phpself_nostat = $_SERVER['PHP_SELF']."?id=$id&op=$op&exe=$exe";
$phpself = $_SERVER['PHP_SELF']."?id=$id&op=$op&exe=$exe&stat=$f_status";
$phpajax = 'modulos/planejamento/checklist_apontamento_ajax.php';

// ------------------------------------------------
// Verifica se usuario est� fechando apontamentos
// ------------------------------------------------
$j = 0;
if ($_POST['env']) {
	$fechar = $_POST['fechar'];
	if ($fechar) {
		foreach ($fechar as $mid_lanca => $one) {
			$sql = "UPDATE ".LANCA_CHECKLIST." SET DIAGNOSTICO='1' WHERE MID='$mid_lanca'";
			$dba[0] ->Execute($sql);
			$j++;
		}
		blocomsg($ling['prog_fechados']." ".$j." ".$ling['prog_apontamento'],3);
	}
}


// ------------------------------------------------
// Verifica se falta abrir algum lan�amento novo
// ------------------------------------------------
$sql = "SELECT * FROM ".PLANO_CHECKLIST_PROGRAMACAO." WHERE STATUS = '0' ORDER BY DATA_INICIAL ASC";
$tmp=$dba[0] ->Execute($sql);
while (!$tmp->EOF) {
	$campo = $tmp->fields;
	
	// verifica se n�o tem apontamento aberto. se n�o tiver, abre
	$sql = "SELECT * FROM ".LANCA_CHECKLIST." WHERE MID_CHECKLIST='{$campo['MID_CHECKLIST']}' AND MID_MAQUINA='{$campo['MID_MAQUINA']}' AND MID_PROGRAMACAO='{$campo['MID']}' ORDER BY DATA ASC";
	$tmp2=$dba[0] ->Execute($sql);
	$j=0;
	$ultima_data = '';
	while (!$tmp2->EOF) {
		$campo2 = $tmp2->fields;
		if ($campo2['DIAGNOSTICO'] == '0') $j++;
		if ($campo2['DIAGNOSTICO'] == '1') $ultima_data = $campo2['DATA'];
		$tmp2->MoveNext();
	} // fim de cada apontamento

	if ($j == 0) {
		// n�o tem apontamento aberto, abre um
		if ($ultima_data) {
			// ja tem fechada, calcula a pr�xima data
			$frequencia = VoltaValor(PLANO_CHECKLIST,'FREQUENCIA','MID',$campo['MID_CHECKLIST'],0);
			$ultima_data = VoltaTime('0:0:0',NossaData($ultima_data)); // $ultima_data contem mktime da ultima data
			$ultima_data += $mktime_1_dia*$frequencia;
			$ultima_data = date('Y-m-d',$ultima_data);
		}
		else $ultima_data = $campo['DATA_INICIAL'];
		
		$sql = "INSERT INTO ".LANCA_CHECKLIST." (`MID_CHECKLIST`,`MID_MAQUINA`,`MID_PROGRAMACAO`,`DATA`,`DIAGNOSTICO`) VALUES ('{$campo['MID_CHECKLIST']}', '{$campo['MID_MAQUINA']}', '{$campo['MID']}', '$ultima_data', '0')";
		$dba[0] ->Execute($sql);
	}
	
	$tmp->MoveNext();
} // fim de cada programa��o

// ja est�o todos os apontamentos abertos corretamente


// ------------------------------------------------
// Mostra formularios e tabelas
// ------------------------------------------------
// controle de op��o negritada com pouco c�digo:
if ($f_status == '0') $stat_strong = array('<strong>','</strong>','','');
if ($f_status == '1') $stat_strong = array('','','<strong>','</strong>');
echo "<div id=\"lt\">\n";
echo "<div id=\"lt_cab\"><h3>".$ling['checklist_apontar']."</h3>\n
</div></div><br />\n
{$stat_strong[0]}<a class=\"link\" href=\"$phpself_nostat&stat=0\"><img src=\"imagens/icones/22x22/checklist_aponta_abertas.png\" border=0> {$ling['checklist_aponta_abertas']}</a>{$stat_strong[1]}
&nbsp; &nbsp; &nbsp;
{$stat_strong[2]}<a class=\"link\" href=\"$phpself_nostat&stat=1\"><img src=\"imagens/icones/22x22/checklist_aponta_fechadas.png\" border=0> {$ling['checklist_aponta_fechadas']}</a>{$stat_strong[3]}
<br />";


if ($f_status == 0) echo "<form method=POST action=\"$phpself\">";
$sql = "SELECT * FROM ".LANCA_CHECKLIST." WHERE DIAGNOSTICO=$f_status ORDER BY DATA ASC";
$tmp=$dba[0] ->Execute($sql);
while (!$tmp->EOF) {
	$campo = $tmp->fields;
	$mid_lanca = $campo['MID'];
	$maq_desc = VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo['MID_MAQUINA'],0);
	$check_desc = VoltaValor(PLANO_CHECKLIST,'DESCRICAO','MID',$campo['MID_CHECKLIST'],0);
	echo "<a name=\"$mid_lanca\"></a>
	<table style=\"border: 1px solid black; font-size: 10px\" width=\"100%\"><tr>
	<th>{$tdb[MAQUINAS]['DESC']}: $maq_desc</th><th>{$tdb[PLANO_CHECKLIST]['DESC']}: $check_desc</th><th>{$ling['data']}: ".NossaData($campo['DATA'])
	."</th></tr>
	<tr><td colspan=3>";
	
	// verifica se houve algum erro aqui:
	if (($_POST['add']) and ($mid_lanca_checklist == $mid_lanca) and ($erromsg)) erromsg($erromsg);

	if ($f_status == 0) {
		// mostra select com atividades para este checklist, campo texto observa��o
		// campo para data, e bot�o. botao adiciona pendencia com:
		// mid_maquina e mid_lanca_checklist tirados do lanca_checklist
		// data entrado pelo usuario
		// descricao = tarefa do checklist + observa��o entrado pelo usuario
		// mid_ordem, mid_conjunto e mid_ordem_exc = 0
		// mid por GeraMid
		// e numero automatico
		echo "
		<table><tr><td>{$ling['checklist_pendencias']}</td>
		<td>{$ling['observacoes']}</td><td>{$tdb[PENDENCIAS]['DATA']}</td></tr>
		<tr><td>
		<select name=\"tarefa[$mid_lanca]\" id=\"tarefa$mid_lanca\" class=\"campo_select\">\n";
		
		$sql = "SELECT * FROM ".PLANO_CHECKLIST_ATIVIDADES." WHERE MID_CHECKLIST='{$campo['MID_CHECKLIST']}' ORDER BY NUMERO ASC";
		$tmp2=$dba[0] ->Execute($sql);
		while (!$tmp2->EOF) {
			$campo2 = $tmp2->fields;
			echo "<option value=\"{$campo2['MID']}\">{$campo2['NUMERO']}-{$campo2['TAREFA']}</option>\n";
			$tmp2->MoveNext();
		} // fim de cada atividade
		echo "</select>
		</td><td>
		<input type=text name=\"obs[$mid_lanca]\" id=\"obs$mid_lanca\" size=30 maxlength=100 class=\"campo_text\">
		</td><td>
		<input type=text name=\"data[$mid_lanca]\" id=\"data$mid_lanca\" size=10 maxlength=10 class=\"campo_text_ob\"
		 onkeypress=\"return ajustar_data(this, event)\" value=\"".date("d/m/Y")."\">
		</td><td>
		<input type=button class=\"botao\" value=\"{$ling['adicionar']}\" onclick=\"atualiza_area2('lanca$mid_lanca','$phpajax?ajax=add&mid_lanca_checklist=$mid_lanca&tarefa='+document.getElementById('tarefa$mid_lanca').options[document.getElementById('tarefa$mid_lanca').selectedIndex].value+'&obs='+document.getElementById('obs$mid_lanca').value+'&data='+document.getElementById('data$mid_lanca').value)\">
		</td></tr></table>
		<br clear=\"all\" />";
	}
	
	// pega as pendencias com este checklist
	echo "<div id=\"lt_tabela\"><div id=\"lanca$mid_lanca\">
	<table><tr><th>{$tdb[PENDENCIAS]['DESC']}</th><th>{$tdb[PENDENCIAS]['DATA']}</th>";
	if ($f_status == 0) echo "<th></th>";
	echo "</tr>";
	$sql = "SELECT * FROM ".PENDENCIAS." WHERE MID_LANCA_CHECKLIST='$mid_lanca' ORDER BY NUMERO ASC";
	$tdclass="cor1";
	$tmp2=$dba[0] ->Execute($sql);
	while (!$tmp2->EOF) {
		$campo2 = $tmp2->fields;
		if ($f_status == 0) $dellink = "<td><a href=\"javascript:atualiza_area2('lanca$mid_lanca','$phpajax?ajax=del&mid_pendencia={$campo2['MID']}&mid_lanca_checklist=$mid_lanca')\" onclick=\"return confirm('{$ling['confirma_remover']}')\"><img src=\"imagens/icones/22x22/del.png\" border=0></a></td>";
		else $dellink = '';
		echo "<tr class=\"$tdclass\"><td>{$campo2['NUMERO']} - {$campo2['DESCRICAO']}</td>
		<td>".NossaData($campo2['DATA'])."</td>
		$dellink</tr>\n";
		if ($tdclass == 'cor1') $tdclass = 'cor2';
		else $tdclass = 'cor1';
		$tmp2->MoveNext();
	} // fim de cada atividade
	echo "</td></tr></table></div></div>";
	if ($f_status == 0) echo "<br clear=\"all\" /><br clear=\"all\" />
	<label class=\"campo_label\" for=\"fechar$mid_lanca\">{$ling['prog_fechar_apontamento']}</label>
	<input class=\"campo_check\" type=\"checkbox\" name=\"fechar[$mid_lanca]\" id=\"fechar$mid_lanca\" value=\"1\" />
	<br clear=\"all\" />";

	echo "</td></tr></table><br clear=\"all\" />";
	$tmp->MoveNext();
} // fim de cada apontamento

$aviso = $ling['prog_atencao'];
if ($f_status == 0) echo "<input class=\"botao\" type=submit name=\"env\" value=\"{$ling['prog_fechar_apontamento_m']}\" onclick=\"return confirm('$aviso')\" />
</form></div>";


?>