<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Autor: Fernando Cosentino
* Nota: Ordens de Serviço
*/
//$fecha_os=(int)$_GET['fecha_os'];
$mid_func_aponta = (int) VoltaValor(USUARIOS, "MID_FUNCIONARIO", "MID", (int)$_SESSION[ManuSess]['user']['MID'], 0);
$abre_os=(int)$_GET['abre_os'];
$can_os=(int)$_GET['can_os'];
$confirma=(int)$_GET['confirma'];
$msg = LimpaTexto(strtoupper($_GET['msg']));
$osmid = (int) $_GET['osmid'];
$aponta_func=(int)$_GET['aponta_func'];
$aponta_st=(int)$_GET['aponta_st'];

$ajax = (int)$_GET['ajax'];


if ($exe == "")$exe =1;

if (($abre_os) and ($confirma != 0)){
    $dba[$tdb[ORDEM]['dba']] -> Execute("UPDATE ".ORDEM." SET STATUS = '1' WHERE MID = '$abre_os'");
    logar(4, $ling['ord_reaberta'], ORDEM, 'MID', $osmid);
    
    $tipo = (int)VoltaValor(ORDEM_PLANEJADO,'TIPO','MID',$abre_os,0);
	
	if(($tipo == 1) or ($tipo == 2)){
		$mprog = (int)VoltaValor(ORDEM_PLANEJADO,'MID_PROGRAMACAO','MID',$abre_os,0);
		// BUSCA SE HÁ AINDA ORDENS ABERTAS PARA ESSA PROGAMAÇÃO.
		$up = "UPDATE ".PROGRAMACAO." SET STATUS = 1 WHERE MID = $mprog";
		$obj=$dba[$tdb[PROGRAMACAO]['dba']] -> Execute($up);
			
		if (!$obj) {
			erromsg($dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg());
		}						
	}   
    
    echo "<script> history.back(); </script>";
}
if (($can_os) and ($confirma != 0) and (trim($msg) != '')){
    $dba[$tdb[ORDEM]['dba']] -> Execute("UPDATE ".ORDEM." SET STATUS = '3', TEXTO = 'ORDEM CANCELADA EM ".date("d/m/Y - h:i:s")." PELO USUARIO ".$_SESSION[ManuSess]['user']['USUARIO'].". MOTIVO: $msg.' WHERE MID = '$can_os'");
    logar(4, $ling['ord_cancelamento'], ORDEM, 'MID', $osmid);
    
    $tipo = (int)VoltaValor(ORDEM_PLANEJADO,'TIPO','MID',$can_os,0);
			
	if(($tipo == 1) or ($tipo == 2)){
		// BUSCA SE H� AINDA ORDENS ABERTAS PARA ESSA PROGAMAC�O.
		$mprog = (int)VoltaValor(ORDEM_PLANEJADO,'MID_PROGRAMACAO','MID',$can_os,0);
		$sql_ord = "SELECT COUNT(MID) AS TOTAL FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO = $mprog AND STATUS = 1";
		$tmpord=$dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql_ord);
		
		if (!$tmpord){ 
			erromsg($dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg());
		}
		
		$num = (int)$tmpord->fields['TOTAL'];
		if($num == 0){
			//CASO N�O TENHA MAIS ORDENS, MUDA O STATUS DA PROGRAMAC�O PARA CONCLU�DO.
			$up = "UPDATE ".PROGRAMACAO." SET STATUS = 2 WHERE MID = $mprog";
			$obj=$dba[$tdb[PROGRAMACAO]['dba']] -> Execute($up);
			
			if (!$obj) {
				erromsg($dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg());
			}
		}
	}
     
    echo "<script> history.back(); </script>";
}


// seta responsavel baseado em $_POST['osresp']
if ($_POST['atribui'] and $_POST['osresp'] and $_POST['osresp_select']) {
    $osresp = array_keys($_POST['osresp']);
    $mid_funcionario = $_POST['osresp_select'];
    foreach ($osresp as $estaosresp) {
        $tmp=$dba[$tdb[ORDEM]['dba']] -> Execute("UPDATE ".ORDEM." SET RESPONSAVEL='$mid_funcionario' WHERE MID=$estaosresp");
        logar(4, $ling['ord_resp'], ORDEM, 'MID', $estaosresp);
    }
}

if ($_GET['st'] == '') {
    echo "
    <script>

    // Fun��o usada para abrir as partes da tela e trocar os icones
    icone_mais = 'imagens/icones/32x32/bullet_toggle_plus.png';
    icone_menos = 'imagens/icones/32x32/bullet_toggle_minus.png';

    function abre_tela(id, link) {
        abre_div(id);
        
        idimg = link.getElementsByTagName('IMG')[0];
        
        if (idimg.src.indexOf(icone_mais) !== -1) {
            idimg.src = icone_menos;
        }
        else {
            idimg.src = icone_mais;
        }
    }

    function abre_rota (mid_os, img) {
        // mostrando o div
        abre_div('os_maq_' + mid_os); 
        
        // Preechendo via AJAX e mudando botao para menos
        if(document.getElementById('os_maq_' + mid_os).innerHTML == ''){ 
            atualiza_area2('os_maq_' + mid_os,'parametros.php?id=rotamaq&osmid=' + mid_os);
        }
        
        // Voltando botão para mais
        if (img.src.indexOf('mais.gif') != -1) {
            img.src = 'imagens/icones/menos.gif';
        }
        else {
            img.src = 'imagens/icones/mais.gif';
        }
    }
    
    
    function muda_semana_prog(modo_visao, ano_ant, sem_ant){
        atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&modo_visao=' + modo_visao + '&anoini=' + ano_ant + '&semini=' + sem_ant);
    }
    

    $(function(){
        
        
        
        // Abrir OS
        $('#abre_os').livequery('click', function(){
            abre_janela_apontaosplan('new&idModulo={$id}&op={$op}');
        });
        
        // Filtro por empresa
        $('#filtro_empresa').livequery('change', function(){
            atualiza_area3('area','parametros.php?id=71&empresa=' + $(this).val());
        });
        
        // Filtro por areas
        $('#filtro_area').livequery('change', function(){
            atualiza_area3('setor','parametros.php?id=7&area=' + $(this).val());
        });
        
        // Ver backlog
        $('#abre_backlog').livequery('click', function(){
            abre_tela('tela_backlog', this); 
            ajax_get3('parametros.php?id=seta_ver_backlog');
        });
        
        // Ver programacao
        $('#abre_programacao').livequery('click', function(){
            abre_tela('tela_programacao', this); 
            ajax_get3('parametros.php?id=seta_ver_prog');
        });
        
        // Ver ordens
        $('#abre_ordem').livequery('click', function(){
            abre_tela('tela_os', this);
        });
        
        // ABAS BACKLOG
        $('#backlog_sem').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao2=1');
        });
        
        $('#backlog_mes').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao2=2');
        });
        
        $('#backlog_per').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao2=3');
        });
        
        $('#env_data2').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&datai_bl=' + $('#datai_bl').val() + '&dataf_bl=' + $('#dataf_bl').val());
        });
        
        $('#agregador').livequery('change', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&agregador=' + $(this).val());
        });
        
        // TELA DE PROGRAMAÇÃO SEMANAL
        $('#prog_func').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao=1');
        });
        
        $('#prog_esp').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao=2');
        });
        
        $('#prog_equip').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao=3');
        });
        
        $('#prog_maq').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao=4');
        });
        
        $('#prog_comp').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao=5');
        });
        
        $('#prog_mat').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&oq=$oq&modo_visao=6');
        });
        
        $('#env_data3').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&exe=$exe&anoini=' + $('#anoini').val() + '&semini=' + $('#semini').val());
        });
        
        
        // ABAS TELA DE OS
        $('#os_aberta').livequery('click', function(){
            atualiza_area3('corpo_os', 'manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&sstatus=1&prog=0&cont=0');
        });
        
        $('#os_fechada').livequery('click', function(){
            atualiza_area3('corpo_os', 'manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&sstatus=2&prog=0&cont=0');
        });
        
        $('#os_cancelada').livequery('click', function(){
            atualiza_area3('corpo_os', 'manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&sstatus=3&prog=0&cont=0');
        });
        
        $('#os_programada').livequery('click', function(){
            atualiza_area3('corpo_os', 'manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&sstatus=1&prog=1&cont=0');
        });
        
        
        $('#env_data').livequery('click', function(){
            atualiza_area3('corpo_os', '?st=1&id=$id&op=$op&filtro_datai=' + $('#filtro_datai').val() + '&filtro_dataf=' + $('#filtro_dataf').val());
        });
        
    });

    </script>

    <div id=\"corpo_os\">";
}

echo "
<div id=\"mod_menu\">

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/osabertas.png\" border=\"0\" alt=\"{$ling['ord01']}\" />
<span>" . $ling['carteira_servico'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/pendencias.png\" border=\"0\" alt=\"{$ling['notas_pendencias']}\"  />
<span>" . $ling['notas_pendencias'] . "</span>
</a>
</div>

<div>
<a href=\"geraos.php?tb=p\" target=\"_blank\">
<img src=\"imagens/icones/22x22/imprimir.png\" border=\"0\" alt=\"{$ling['imprimir_ordens_abertas']}\"  />
<span>" . $ling['imprimir_ordens'] . "</span>
</a>
</div>

";


if (($exe == '') or ($exe == 1)){
    
    echo "
    <div><h3>{$ling['ord01']}</h3></div>

    </div>

    <br clear=\"all\" />

    <div id=\"lt\">

    <div id=\"lt_cab\">
    
    <h3>{$ling['ord01']}</h3>\n";

    if ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2)) {
        echo "<span id=\"lt_menu\" onmouseover=\"menu_sobre('ltmenu')\" onmouseout=\"menu_fora('ltmenu')\">".$ling['opcoes']."
                <ul id=\"m_ltmenu\">
                    <li><a  href=\"#\" id=\"abre_os\"><img src=\"imagens/icones/22x22/apontaos.png\" alt=\"\" title=\"OS\" border=\"0\" /> {$ling['ord_abrir']} </a></li>
                </ul>
            </span>\n";
    }

    echo "</div>

    <br clear=\"all\" />
    ";
    
    // MOSTRAR BACKLOG
    $ver_backlog = $_SESSION[ManuSess]['lt']['os']['ver_backlog'];
    
    // MOSTRAR PROGRAMAÇÃO
    $ver_prog = $_SESSION[ManuSess]['lt']['os']['ver_prog'];
    
    
    if ($_POST['env']  != ""){
        $_SESSION[ManuSess]['lt']['os']['filtro_empresa']=(int)$_POST['filtro_empresa'];
        $_SESSION[ManuSess]['lt']['os']['filtro_setor']=(int)$_POST['filtro_setor'];
        $_SESSION[ManuSess]['lt']['os']['filtro_area']=(int)$_POST['filtro_area'];
        $_SESSION[ManuSess]['lt']['os']['filtro_desc']=$_POST['desc'];
        $_SESSION[ManuSess]['lt']['os']['filtro_destino']=$_POST['destino'];
        $_SESSION[ManuSess]['lt']['os']['filtro_origem']=(int)$_POST['filtro_origem'];
        $_SESSION[ManuSess]['lt']['os']['filtro_tipo_serv']=(int)$_POST['filtro_tipo_serv'];
        $_SESSION[ManuSess]['lt']['os']['filtro_natureza']=(int)$_POST['filtro_natureza'];
    }

    $filtro_origem  = $_SESSION[ManuSess]['lt']['os']['filtro_origem'];
    $filtro_empresa = $_SESSION[ManuSess]['lt']['os']['filtro_empresa'];
    $filtro_area    = $_SESSION[ManuSess]['lt']['os']['filtro_area'];
    $filtro_setor   = $_SESSION[ManuSess]['lt']['os']['filtro_setor'];
    $filtro_natureza = $_SESSION[ManuSess]['lt']['os']['filtro_natureza'];
    $desc           = $_SESSION[ManuSess]['lt']['os']['filtro_desc'];
    $filtro_tipo_serv=$_SESSION[ManuSess]['lt']['os']['filtro_tipo_serv'];
    

    if ($_GET['sstatus'] != "") $_SESSION[ManuSess]['lt']['os']['sstatus']=(int)$_GET['sstatus'];
    $sstatus=$_SESSION[ManuSess]['lt']['os']['sstatus'];
    
    if ($sstatus == 0) {
        $sstatus=1;
    }
    
    if ($_GET['prog'] != "") $_SESSION[ManuSess]['lt']['os']['prog']=(int)$_GET['prog'];
    $is_prog=$_SESSION[ManuSess]['lt']['os']['prog'];


    if ($_GET['filtro_datai']  != "") $_SESSION[ManuSess]['lt']['os']['filtro_datai']=$_GET['filtro_datai'];
    $filtro_datai=$_SESSION[ManuSess]['lt']['os']['filtro_datai'];

    if ($_GET['filtro_dataf']  != "") $_SESSION[ManuSess]['lt']['os']['filtro_dataf']=$_GET['filtro_dataf'];
    $filtro_dataf=$_SESSION[ManuSess]['lt']['os']['filtro_dataf'];

    $di=explode("/",$filtro_datai);
    $df=explode("/",$filtro_dataf);
    if ($di[0] == "") {
        $mkt=mktime(0,0,0,date("m"),date("d"),date("Y"));
        $filtro_dataf=date("d/m/Y",$mkt);
        $data_f=date("Y-m-d",$mkt);
        $mkt=mktime(0,0,0,date("m"),date("d")-7,date("Y"));
        $filtro_datai=date("d/m/Y",$mkt);
        $data_i=date("Y-m-d",$mkt);
    }
    if ($di[0] != "") {
        $data_i=$di[2]."-".$di[1]."-".$di[0];
        $data_f=$df[2]."-".$df[1]."-".$df[0];
    }
    $mes=$_GET['mes'];
    if ($mes == "") $mes=$_POST['mes'];
    $tdia=(int) $_GET['tdia'];
    $ano=(int) $_GET['ano'];
    $dia_i=$_GET['dia_i'];
    $dia_f=$_GET['dia_f'];
    if ($ano == "") $ano=(int)$_POST['ano'];
    if (empty($mes)) {
        $mes = date("m");
        $ano = date("Y");
    }
    $co=$tdb[ORDEM]['dba'];
    $cm=$tdb[MAQUINAS]['dba'];
    $cc=$tdb[MAQUINAS_CONJUNTO]['dba'];

    

    echo "
    <form name=\"form\" id=\"formularioos\" method=\"POST\" action=\"?id=$id&op=$op&exe=$exe\">
    <fieldset style=\"font-size:11px;\"><legend>{$ling['filtro']}</legend>
    ";

    FormSelectD('DESCRICAO', '', PROGRAMACAO_TIPO, $filtro_origem, 'filtro_origem', 'filtro_origem', 'MID', '', '', "", '', '', '', "Origem da OS");

    FormSelectD('COD', 'NOME', EMPRESAS, $filtro_empresa, 'filtro_empresa', 'filtro_empresa', 'MID', '', '', "", '', '', '', $tdb[EMPRESAS]['DESC']);

    echo "<br clear=\"all\" />
    <div id=\"area\" style=\"float:left\">";

    FormSelectD('COD', 'DESCRICAO', AREAS, $filtro_area, 'filtro_area', 'filtro_area', 'MID', '', '', "", '', '', '', $tdb[AREAS]['DESC']);

    echo "</div>
    
    <br clear=\"all\" />
    <div id=\"setor\" style=\"float:left\">";

    FormSelectD('COD','DESCRICAO',SETORES,$filtro_setor,'filtro_setor','filtro_setor','MID','','','',"",'A','COD',$tdb[SETORES]['DESC']);

    echo "</div>
       
    <br clear=\"all\" />";
    FormSelectD('DESCRICAO', '', NATUREZA_SERVICOS, $filtro_natureza, 'filtro_natureza', 'filtro_natureza', 'MID','','','','','A','DESCRICAO',$tdb[NATUREZA_SERVICOS]['DESC']);
    
    echo "<br clear=\"all\" />";
    FormSelectD('DESCRICAO', '', TIPOS_SERVICOS, $filtro_tipo_serv, 'filtro_tipo_serv', 'filtro_tipo_serv', 'MID','','','','','A','DESCRICAO',$tdb[TIPOS_SERVICOS]['DESC']);
    
    
    
    echo "
    <br clear=\"all\" />
    <label for=\"desc\">{$ling['destino']} / {$tdb[ORDEM]['TEXTO']}:</label>
    <input type=\"text\" name=\"desc\" id=\"desc\" class=\"campo_text\" size=\"20\" value=\"$desc\" />

    <input type=\"submit\" value=\"{$ling['filtrar']}\" class=\"botao\" name=\"env\" />


    </fieldset>
    </form>
    
    </div>
    
    
    <div id=\"lt\">
    <div id=\"lt_cab\" style=\"font-size:12px;border-right:0px;\">
        <h2 style=\"font-size:12px;cursor:pointer;\" id=\"abre_backlog\" title=\"{$ling['abrir_fechar']} {$ling['calculadora_back']}\">
        <img src=\"" . (($ver_backlog == 1)? "imagens/icones/32x32/bullet_toggle_minus.png" : "imagens/icones/32x32/bullet_toggle_plus.png") . "\" align=\"top\" border=\"0\" />
          {$ling['calculadora_back']}
        </h2>
    </div>
    
    
    
    <div id=\"tela_backlog\" style=\"clear:all;float:none;" . (($ver_backlog == 1)? "display:block;" : "display:none;") . "\">
    
    <div id=\"lt_tabela\">
    ";

    include('calc_backlog.php');

    echo "</div>
    </div>
    </div>
    
    
    <div id=\"lt\">
    <div id=\"lt_cab\" style=\"font-size:12px;border-right:0px;\">
        <h2 style=\"font-size:12px;cursor:pointer;\" id=\"abre_programacao\" title=\"{$ling['abrir_fechar']} {$ling['programacao_mo']}\">
        <img src=\"" . (($ver_prog == 1)? "imagens/icones/32x32/bullet_toggle_minus.png" : "imagens/icones/32x32/bullet_toggle_plus.png") . "\" align=\"top\" border=\"0\" />
          {$ling['programacao_mo']}
        </h2>
    </div>
    
    
    
    <div id=\"tela_programacao\" style=\"clear:all;float:none;" . (($ver_prog == 1)? "display:block;" : "display:none;") . "\">
    
    <div id=\"lt_tabela\">
    ";

    include('programacao.php');

    echo "</div>
    </div>
    </div>
    
    
    <div id=\"lt\">
    <div id=\"lt_cab\" style=\"font-size:12px;border-right:0px;\">
        <h2 style=\"font-size:12px;cursor:pointer;\" id=\"abre_ordem\" title=\"{$ling['abrir_fechar']} {$ling['orndes']}\">
        <img src=\"imagens/icones/32x32/bullet_toggle_minus.png\" align=\"top\" border=\"0\" />
          {$ling['orndes']}
        </h2>
    </div>
    
    <br clear=\"all\" />
    
    <div id=\"tela_os\" style=\"clear:all;float:none;text-align:left;display:block;\">";

    // VALIDA��ES PARA REPROGRAMA��O DE ORDENS
    if( ($_POST['reprog'] != "") and ($_POST['data_reprog'] == "" OR $_POST['osresp'] == ""  OR $_POST['motivo_reprog'] == "") ){

        $msg = "<b>{$ling['impossivel_reprogramar']}</b>";

        // caso n�o selecione nenhuma ordem
        if($_POST['osresp'] == ""){
            $msg .= "<li>{$ling['selecione_ordem']}</li>";
        }

        // caso n�o informe a data para reprograma��o
        if($_POST['data_reprog'] == ""){
            $msg .= "<li>{$ling['informe_data']}</li>";
        }

        // caso n�o informe o motivo
        if($_POST['motivo_reprog'] == ""){
            $msg .= "<li>{$ling['informe_motivo']}</li>";
        }

        erromsg("<ul>{$msg}</ul>");

    }

    // Reprograma OS selecionadas
    if (($_POST['reprog'] != "") and ($_POST['data_reprog'] != "") and ($_POST['motivo_reprog'] != "")) {
        // Coletando os dados
        $os_erro = array();
        $osreprog = array_keys($_POST['osresp']);
        $data = DataSQL($_POST['data_reprog']);
        $data_comp = str_replace('-', '', $data);
        $motivo = strtoupper(LimpaTexto($_POST['motivo_reprog']));
        
        // Passando por todas as OS selecionadas
        foreach ($osreprog as $osmid) {
            // Buscando a data atual
            $data_prog = VoltaValor(ORDEM, 'DATA_PROG', 'MID', $osmid);
            $data_abre = VoltaValor(ORDEM, 'DATA_ABRE', 'MID', $osmid);
            $data_abre_comp = str_replace('-', '', $data_abre);
            
            // Salvando reprogramacao
            if ($data_comp >= $data_abre_comp) {
                $mid = GeraMid(ORDEM_REPROGRAMA, 'MID', $tdb[ORDEM_REPROGRAMA]['dba']);
                $sql = "INSERT INTO " . ORDEM_REPROGRAMA . " (MID, MID_ORDEM, DATA, DATA_ORIGINAL, MOTIVO) VALUES ($mid, $osmid, '$data', '$data_prog', '$motivo')";
                $dba[$tdb[ORDEM_REPROGRAMA]['dba']] -> Execute($sql);
                logar(3, "", ORDEM_REPROGRAMA, 'MID', $mid);

                // Mudando a data programada
                $sql = "UPDATE ".ORDEM." SET DATA_PROG = '$data' WHERE MID = $osmid";
                $dba[$tdb[ORDEM]['dba']] -> Execute($sql);
                logar(4, $ling['ord_rel'], ORDEM, 'MID', $osmid);
            }
            else {
                $os_erro[] = $osmid;
            }
        }
        
        if (count($os_erro) > 0) {
            $msg_erro = "";
            foreach ($os_erro as $osmid) {
                $msg_erro .= ($msg_erro == '')? "" : ", ";
                $msg_erro .= VoltaValor(ORDEM, 'NUMERO', 'MID', $osmid);
            }
            
            echo "<br clear=\"all\" />";
            erromsg($ling['erro_reprog1'] . " $msg_erro.<br />" . $ling['erro_reprog2']);
            echo "<br clear=\"all\" />";
        }
        
    }
    

    echo "<div id=\"lt_tabela\">
    ";
    
    $sqlvar2 = ""; 
    $sqlvar = "";


    // ORDENAÇÃO
    if ($_GET['ordc'] != "") {
        $_SESSION[ManuSess]['lt']['os']['ordc']=$_GET['ordc'];
        
        if($_SESSION[ManuSess]['lt']['os']['ord'] == 'ASC') {
            $_SESSION[ManuSess]['lt']['os']['ord'] = 'DESC';
        }
        else {
            $_SESSION[ManuSess]['lt']['os']['ord'] = 'ASC';
        }
    }
    elseif ($_SESSION[ManuSess]['lt']['os']['ordc'] == '') {
        $_SESSION[ManuSess]['lt']['os']['ordc'] = "O.NUMERO";
        $_SESSION[ManuSess]['lt']['os']['ord'] = "DESC";
    }
    // Campo e sentido da ordenação
    $ordc=$_SESSION[ManuSess]['lt']['os']['ordc'];
    $ord=$_SESSION[ManuSess]['lt']['os']['ord'];
    
    // Icone de ordenação na listagem
    if ($ord == "ASC") {
        $icone_ord = " <img src=\"imagens/icones/asc.png\" border=\"0\" />";
    }
    elseif ($ord == "DESC") {
        $icone_ord = " <img src=\"imagens/icones/desc.png\" border=\"0\" />";
    }

    if ($sstatus != 0) $sqlvar2=" STATUS = '$sstatus' ";
    elseif ($sstatus == 0) $sqlvar2=" STATUS = '1' ";

    if ($sstatus == '1') {
        if ($is_prog) $sqlvar2 .= "AND DATA_PROG > '".date("Y-m-d")."'";
        else $sqlvar2 .= "AND DATA_PROG <= '".date("Y-m-d")."'";
    }

    if (($filtro_origem != 0) and ($filtro_origem != 5))$sqlvar.="AND TIPO = '$filtro_origem' ";
    elseif ($filtro_origem == 5)$sqlvar.="AND TIPO = '0' ";

    if ($filtro_empresa != 0) $sqlvar.="AND MID_EMPRESA = '$filtro_empresa' ";
    if ($filtro_area != 0) $sqlvar.="AND MID_AREA = '$filtro_area' ";
    if ($filtro_setor != 0) $sqlvar.="AND MID_SETOR = '$filtro_setor' ";
    if ($filtro_natureza != 0) $sqlvar .= "AND O.NATUREZA = '$filtro_natureza' ";
    if ($filtro_tipo_serv != 0) $sqlvar.="AND TIPO_SERVICO = '$filtro_tipo_serv' ";


    // FILTRO POR DESCRIÇÃO
    if ($desc != '') {
        $desc_sql = LimpaTexto($desc);

        // Filtro por Empresa
        $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2, false, 'M');
        $fil_emp = ($fil_emp != "")? " AND " . $fil_emp : "";

        // Filtra as maquinas
        $sql = "SELECT M.MID, M.COD, M.DESCRICAO FROM ".MAQUINAS." M WHERE (M.COD LIKE '%$desc_sql%' OR M.DESCRICAO LIKE '%$desc_sql%') $fil_emp";
        $resultado=$dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
        $filsql = '';
        while (! $resultado->EOF) {
            $campo = $resultado->fields;
            $filsql .= ($filsql != "")? " OR " : "";
            $filsql .= "O.MID_MAQUINA = '{$campo['MID']}'";
            $resultado->MoveNext();
        }

        // Filtro por Empresa
        $fil_emp = VoltaFiltroEmpresa(EQUIPAMENTOS, 2);
        $fil_emp = ($fil_emp != "")? " AND " . $fil_emp : "";

        // Filtra equipamentos
        $sql = "SELECT MID FROM ".EQUIPAMENTOS." WHERE (COD LIKE '%$desc_sql%' OR DESCRICAO LIKE '%$desc_sql%')  $fil_emp";
        $resultado=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute($sql);
        while (!$resultado->EOF) {
            $campo=$resultado->fields;
            $filsql .= ($filsql != "")? " OR " : "";
            $filsql .= "O.MID_EQUIPAMENTO = '{$campo['MID']}'";
            $resultado->MoveNext();
        }

        // Busca na descrição do servico
        $filsql .= ($filsql != "")? " OR " : "";
        $filsql .= "O.TEXTO LIKE '%$desc_sql%'";


        $sqlvar .= " AND ($filsql)";
    }

    // Datas
    $sqlvar .= " AND ((DATA_PROG >= '$data_i' AND DATA_PROG <= '$data_f') OR (DATA_INICIO >= '$data_i' AND DATA_INICIO <= '$data_f'))";
    
    // Filtro por empresa
    $filtro_tmp = VoltaFiltroEmpresa(ORDEM, 2);
    $perm_sql = "";
    if($filtro_tmp != '') {
        $perm_sql  = " AND $filtro_tmp";
    }
    
    // FILTRO POR CENTRO DE CUSTO
    if(($_SESSION[ManuSess]['user']['MID'] != 0) and (VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, 'MID', 'USUARIO', (int)$_SESSION[ManuSess]['user']['MID']) != 0)) {
       $perm_sql .= " AND CENTRO_DE_CUSTO IN (SELECT CENTRO_DE_CUSTO FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}')";
    }

    // Buscando
    $sql = "SELECT O.* FROM ".ORDEM." O WHERE $sqlvar2 $sqlvar $perm_sql ORDER BY $ordc $ord";
    //echo "$sql";
    $resultado=$dba[0] -> Execute($sql);
    if (!$resultado) erromsg($dba[0] -> ErrorMsg() . "<br />" . $sql);
    else {
        $osa=$dba[0] -> Execute("SELECT O.MID FROM ".ORDEM." O WHERE STATUS = 1 AND DATA_PROG <= '".date("Y-m-d")."' $sqlvar $perm_sql");
        $total_aberta=count($osa -> getrows());
        $osa=$dba[0] -> Execute("SELECT O.MID FROM ".ORDEM." O WHERE STATUS = 2 $sqlvar $perm_sql");
        $total_fechada=count($osa -> getrows());
        $osa=$dba[0] -> Execute("SELECT O.MID FROM ".ORDEM." O WHERE STATUS = 3 $sqlvar $perm_sql");
        $total_can=count($osa -> getrows());
        $osa=$dba[0] -> Execute("SELECT O.MID FROM ".ORDEM." O WHERE STATUS = 1 AND DATA_PROG > '".date("Y-m-d")."' $sqlvar $perm_sql");
        $total_prog=count($osa -> getrows());
        $i=0;
        $tr_cor=0;

        echo "<form method=\"POST\" action=\"\">
        <table width=\"100%\" cellspacing=\"0\" style=\"font-size:10px;border:0px;\">
        <tr>
        <td class=\"".((($sstatus == 1) and (!$is_prog))?"aba_sel":"aba")."\" id=\"os_aberta\">
        <img src=\"imagens/icones/22x22/osabertas.png\" border=\"0\" align=\"bottom\" /> ".htmlentities($ling['ordens_abertas'])." ($total_aberta)
        </td>
        <td class=\"aba_entre\">&nbsp;</td>
        <td class=\"".(($sstatus == 2)?"aba_sel":"aba")."\" id=\"os_fechada\">
        <img src=\"imagens/icones/22x22/osfechadas.png\" border=\"0\" align=\"bottom\" /> ".htmlentities($ling['ordens_fechadas'])." ($total_fechada)
        </td>
        <td class=\"aba_entre\">&nbsp;</td>
        <td class=\"".(($sstatus == 3)?"aba_sel":"aba")."\" id=\"os_cancelada\">
        <img src=\"imagens/icones/22x22/os_cancel.png\" border=\"0\" align=\"bottom\" /> ".htmlentities($ling['ordens_canceladas'])." ($total_can)
        </td>
        <td class=\"aba_entre\">&nbsp;</td>
        <td class=\"".((($sstatus == 1) and ($is_prog))?"aba_sel":"aba")."\" id=\"os_programada\">
        <img src=\"imagens/icones/22x22/ordem_programada.png\" border=\"0\" align=\"bottom\" /> ".htmlentities($ling['ordens_programadas'])." ($total_prog)
        </td>
        
        <td class=\"aba_entre\">&nbsp;</td>
        
        <td class=\"aba_fim\">";
        
        if ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2)) {
            echo "
            Procurar O.S. N&ordm;: </span><input type=\"text\" id=\"apontaos\" class=\"campo_text\" name=\"apontaos\" size=\"9\" maxlength=\"9\" />
            <a href=\"javascript:abre_janela_apontaosplan(document.getElementById('apontaos').value + '&ap=1&idModulo={$id}&op={$op}');\" class=\"link\"><img src=\"imagens/icones/22x22/apontaos.png\" alt=\"\" title=\"{$ling['ord_ap']}\" border=\"0\" align=\"top\" /></a>
            <br clear=\"all\" />";
        }
            
        FormData("Data:", "filtro_datai", $filtro_datai);
        
        echo " &agrave; ";

        FormData("", "filtro_dataf", $filtro_dataf);
        
        echo "<input type=\"button\" value=\"IR\" class=\"botao\" name=\"env_data\" id=\"env_data\" />";
        
        echo "</td>
        </tr>
        <tr><td colspan=\"20\" class=\"aba_quadro\" valign=\"top\">";

        
        echo "<table width=\"100%\" id=\"lt_tabela_\">
            <tr>
            <th width=\"1\" nowrap=\"nowrap\">&nbsp;</th>
            <th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordc=O.NUMERO')\" title=\"".$ling['definir_ordem']."\">{$tdb[ORDEM]['NUMERO']}" . (($ordc == "O.NUMERO")? $icone_ord : "") . "</a></th>
            <th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordc=O.TIPO')\" title=\"".$ling['definir_ordem']."\">{$tdb[ORDEM]['TIPO']}" . (($ordc == "O.TIPO")? $icone_ord : "") . "</a></th>
            <th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordc=O.MID_MAQUINA')\" title=\"".$ling['definir_ordem']."\">".$ling['destino']."" . (($ordc == "O.MID_MAQUINA")? $icone_ord : "") . "</a></th>
            <th>{$tdb[ORDEM]['TEXTO']} / Plano</th>
            <th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordc=O.DATA_PROG')\" title=\"".$ling['definir_ordem']."\">{$tdb[ORDEM]['DATA_PROG']}" . (($ordc == "O.DATA_PROG")? $icone_ord : "") . "</a></th>
            <th width=\"100px\">{$ling['previsto_vs_executado']}</th>
            <th>{$ling['opcoes']}</th>
            </tr>";

        while (!$resultado->EOF) {
            $campo=$resultado->fields;
            $midordem = $campo['MID'];
            $datacheca = $campo['DATA_PROG'];
            
            $TempoPrevisto = TempoPrevOS($midordem);
            $TempoRealizado = TempoHHOS($midordem);
            
            $porcentagemTempo = Porcentagem($TempoRealizado, $TempoPrevisto);
            

            // Verifica se foi reprogramada
            $resultado2=$dba[0] -> Execute("SELECT * FROM ".ORDEM_REPROGRAMA." WHERE MID_ORDEM = '$midordem' ORDER BY DATA DESC");
            if (!$resultado2) erromsg($dba[0] -> ErrorMsg());
            else {
                $data_reprog = 0;
                if (! $resultado2->EOF) {
                    $campo2=$resultado2->fields;
                    $data_reprog = $campo2['DATA'];
                }
            }
            
            $plano = (int)VoltaValor(PROGRAMACAO,"MID_PLANO","MID",$campo['MID_PROGRAMACAO'],$tdb[PROGRAMACAO]['dba']);

            // Muda cor da linha
            $cor_linha = ($cor_linha == 'cor2')? 'cor1' : 'cor2';

            if ($sstatus == 1) $dias=dias_atrazo($resultado -> UserDate($datacheca,'d-m-Y'),date("d-m-Y"));
            if  ($dias >= 1) echo "<tr style=\"font-size:10px;\" bgcolor=\"#FFFFC0\">\n";
            else echo "<tr style=\"font-size:10px;\" class=\"$cor_linha\">\n";
            
            echo "<td nowrap=\"nowrap\" valign=\"top\">\n";
            
            if ($sstatus == 1) echo "<input type=\"checkbox\" id=\"osresp\" name=\"osresp[".$campo['MID']."]\" value=\"1\" />\n";

            $respons = $campo['RESPONSAVEL'];
            if ($respons) {
                $respons_nome = strtoupper(VoltaValor(FUNCIONARIOS,'NOME','MID',$respons,0));
                echo "<img src=\"imagens/icones/22x22/funcionario.png\" alt=\"$respons_nome\" title=\"$respons_nome\" />\n";
            }
	    //verificando no arquivo se existe algum registro.
	    $arqui = $manusis['dir']['ordens']."/".$campo['MID']."";
	   
	    
	    //Se existir anexos nesta ordem aparece este indicador dizendo que existe anexos.
	    if ( verificar_anexo_existente($arqui) != false ) {
		
		echo "<img src=\"imagens/icones/22x22/solucao.png\" alt=\"{$ling['existem_arquivos_anexados']}\" title=\"{$ling['existem_arquivos_anexados']}\" />";
	    }
            
            $pen=$dba[0] -> Execute("SELECT MID_ORDEM FROM ".PENDENCIAS." WHERE MID_ORDEM = '".$campo['MID']."'");
            $ipen=count($pen -> getrows());

            if ($ipen >= 1) echo "<img src=\"imagens/icones/22x22/pendencias.png\" alt=\"{$ling['pendencias']}\" title=\"{$ling['pendencias']}\" />\n";
            if ($data_reprog != "") echo "<img src=\"imagens/icones/22x22/turnos.png\" alt=\"{$ling['ord_relprogramada']}\" title=\"{$ling['ord_relprogramada']}\"  />\n";

            echo "</td>";

            echo "<td valign=\"top\" nowrap=\"nowrap\">".$campo['NUMERO']."</td>\n";

            // Atrazo
            if  ($dias <= 0) $dias="";
            else $dias="($dias Dias em Atraso)";

            echo "<td valign=\"top\">";

            // no campo serviço, quando for PREVENTIVA, ROTA ou SOLICITACAO, mostra TIPO, do contrario mostra TIPO_SERVICO
            if (($campo['TIPO'] == 1) or ($campo['TIPO'] == 2)){
                echo htmlentities(strtoupper(VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba'])));
            }
            elseif ($campo['TIPO'] == 4){
                echo htmlentities(strtoupper(VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba']).' ('.VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba']).')'));
            }
            else {
                echo htmlentities(strtoupper(VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba'])));
            }

            echo "</td>\n";

            if ($campo['MID_EQUIPAMENTO']) {
                echo "<td align=\"left\" valign=\"top\"><img src=\"imagens/icones/22x22/equip.png\" />\n".htmlentities(strtoupper(VoltaValor(EQUIPAMENTOS,"DESCRICAO","MID",$campo['MID_EQUIPAMENTO'],$cm)))."</td>\n";
            }
            elseif ($campo['MID_MAQUINA']) {
                echo "<td align=\"left\" valign=\"top\"><img src=\"imagens/icones/22x22/local3.png\" />\n".htmlentities(strtoupper(VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$cm)))."</td>\n";
            }
            elseif($campo['TIPO'] == 2) {
                $maq_rota = VoltaValor(ORDEM_LUB, 'MID_MAQUINA', 'MID_ORDEM', $campo['MID']);
                $maq_desc = htmlentities(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $maq_rota));
                
                echo "<td align=\"left\" valign=\"top\" style=\"text-align:left\">\n";
                echo "<img src=\"imagens/icones/mais.gif\" border=\"0\" onclick=\"abre_rota({$campo['MID']}, this);\" title=\"{$ling['ord_ver_maq_rota']}\" style=\"cursor:pointer;\" />\n";
                echo "<img src=\"imagens/icones/22x22/local3.png\" border=\"0\" /> $maq_desc\n";
                echo "<span id=\"os_maq_{$campo['MID']}\" style=\"text-align:left;display:none;\"></span>\n";
                echo "</td>\n";
            }
            else { // nem maquina nem equipamento, mostra em branco
                echo "<td>&nbsp;</td>\n";
            }

            echo "<td valign=\"top\">";
            
            // DESCRICAO DA OS
            if ($campo['TIPO'] == 1) echo htmlentities(strtoupper(VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$plano,0)));
            elseif ($campo['TIPO'] == 2) echo htmlentities(strtoupper(VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$plano,0)));
            else echo nl2br(htmlentities($campo['TEXTO']));

            echo "</td>\n";
            
            echo "<td valign=\"top\">";
            if ($campo['DATA_PROG'] != "0000-00-00") echo $resultado -> UserDate($campo['DATA_PROG'],'d/m/Y')." $dias</td>";
            echo "</td>\n";
            echo "<td valign=\"top\">".BarraProgresso($porcentagemTempo, $ling['previsto_vs_executado'])."<center>({$TempoPrevisto}h/{$TempoRealizado}h)</center></td>\n";

            echo "<td width=\"100\" nowrap=\"nowrap\" valign=\"top\">\n";

            if ($campo['STATUS'] != 3) {
                echo "<a href=\"javascript:janela('detalha_ord.php?busca=".$campo['MID']."', 'parm', 500,400)\"><img src=\"imagens/icones/22x22/visualizar.png\" border=\"0\" title=\" Visualizar OS \" alt=\" \" /></a>\n";

                echo "<a href=\"javascript:abre_janela_arq('ordens', '".$campo['MID']."', $id,'".$tdb[ORDEM]['DESC'].":".$campo['NUMERO']."')\"><img src=\"imagens/icones/22x22/anexar.png\" border=\"0\" title=\" {$ling['anexar_arq']} \" alt=\" {$ling['anexar_arq']} \" /></a>\n";
            }


            if ((VoltaPermissao($id, $op) != 3) and ($campo['STATUS'] == 1)) {
                echo "<a href=\"javascript:abre_janela_apontaosplan('".$campo['MID']."&idModulo={$id}&op={$op}')\"><img src=\"imagens/icones/22x22/apontaos.png\" alt=\"{$ling['ord_ap']}\" title=\"{$ling['ord_ap']}\" border=\"0\" /></a>\n";
            }

            if ((VoltaPermissao($id, $op) != 3) and ($campo['STATUS'] == 2)) {
                echo "<a href=\"manusis.php?id=$id&op=$op&exe=$exe&oq=".$campo['MID']."&abre_os=".$campo['MID']."\" onclick=\"return confirma(this, '{$ling['deseja_reabrir_ordem']}')\"><img src=\"imagens/icones/22x22/reabre.png\" border=0 alt=\" {$ling['reabrir']} \" title=\" {$ling['reabrir']} \" /></a>\n";
            }

            echo "<a href=\"geraos.php?id=3&oq=".$campo['MID']."\" target=\"_blank\"><img src=\"imagens/icones/22x22/imprimir.png\" alt=\"\" title=\"{$ling['imprimir_os']}\" border=\"0\" /></a>\n";

            if ((VoltaPermissao($id, $op) == 1) and (($campo['STATUS'] != 3) and ($campo['STATUS'] != 2))) {
                echo "<a href=\"manusis.php?id=$id&op=$op&exe=$exe&oq=".$campo['MID']."&can_os=".$campo['MID']."\" onclick=\"return confirma(this, '{$ling['ord_confirma_cancela']}', 2)\"><img src=\"imagens/icones/22x22/oscancel.png\" border=\"0\" title=\" Cancelar \" alt=\" \" /></a>\n";
            }

            echo "</td>\n</tr>";
            $resultado -> MoveNext();
            $i++;
        }

        echo "</table>\n
        <div id=\"lt_rodape\" style=\"text-align:left\">{$ling['registros_encontrados']}: $i
        
        <br clear=\"all\" />";

        // Responsavel e reprogramção
        if ($sstatus == 1) {
            echo "<label class=\"campo_label\" style=\"width: 270px\" for=\"data_reprog\">{$ling['ord_reprogramar']} </label>";
            FormData("", "data_reprog", "");

            echo "<label class=\"campo_label\" style=\"width: 270px\" for=\"motivo_reprog\">{$ling['motivo']}: </label>";
            echo "<input type=\"text\" name=\"motivo_reprog\" id=\"motivo_reprog\" class=\"campo_text\" size=\"35\" value=\"\" />\n";

            echo "<input type=\"submit\" name=\"reprog\" class=\"botao\" value=\"{$ling['ord_reprogramar2']}\" />";

            echo "<br clear=\"all\" />
            <label class=\"campo_label\" style=\"width: 270px\" for=\"osresp_select\">{$ling['ord_atribui']}:</label>";
            FormSelectD('NOME', '', FUNCIONARIOS, '', 'osresp_select', 'osresp_select', 'MID', '');
            echo "<input type=\"submit\" name=\"atribui\" class=\"botao\" value=\"{$ling['atribuir']}\" />";
        }
        
        echo "
        </div>
        </td></tr></table>
        </form>
        ";
    }

    echo "
    </div>
    </div>
    </div>";
    
    if ($_GET['st'] == '') {
        echo "</div>";
    }
}

if ($exe == 3) {
    echo " <div>
    <h3>" . $ling['notas_pendencias'] . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";

    $campos = array();
    $campos[] = "NUMERO";
    $campos[] = "MID_MAQUINA";
    $campos[] = "MID_CONJUNTO";
    $campos[] = "MID_ORDEM";
    $campos[] = "DESCRICAO";
    $campos[] = "DATA";
    $campos[] = "MID_ORDEM_EXC";
    
    ListaTabela (PENDENCIAS,"MID",$campos,"PENDENCIAS","","",1,1,1,1,0,1,1,1,1,1,"PEN");
/*
    if (($_POST['gpen'] != "") and ($_POST['ospen'] != "")) {
        $_SESSION[ManuSess]['ospen']=$_POST['ospen'];
        
        echo "<script language=\"javascript\">abre_janela_apontaosplan('new&idModulo={$id}&op={$op}');</script>";
        echo "</div>";
        exit;
    }
    */
}

?>
