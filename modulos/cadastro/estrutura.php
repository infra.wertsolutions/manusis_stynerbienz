<?
/**
* Estrutura Geral do sistema, atalho paras principais funções desse modulo como cadastro de objetos de manutenção, equipamentos, movimentações, localizações etc.
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  cadastro
*/


if (($exe == "") or ($exe == 1) or ($exe == 2) or ($exe == 3) or ($exe == 4) or ($exe == 5) or ($exe == 8) or ($exe == 9) or ($exe == 10) or ($exe == 11) or ($exe == 12)){
    $listaemp=ListaMatriz($tdb[EMPRESAS]['dba']);
    $matrix=$listaemp[0]['nome'];
    $matrix_mid=$listaemp[0]['mid'];
    $matrix_cod=$listaemp[0]['cod'];
    echo "<div id=\"mod_menu\">
    <div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/va.png\" border=\"0\" alt=\"".$ling['visa_geral']."\" />
<span>".$ling['visao_geral']."</span>
</a>
</div>";
    
    if (VoltaPermissao($id, $op) != 3) {
        echo "<div>
        <a href=\"manusis.php?id=$id&op=$op&exe=8\">
        <img src=\"imagens/icones/22x22/equip_mov.png\" border=\"0\" alt=\"".$ling['move_equip']."\" />
        <span>".$ling['move_equip']."</span>
        </a>
        </div>";

        echo "<div>
        <a href=\"manusis.php?id=$id&op=$op&exe=9\">
        <img src=\"imagens/icones/22x22/local3_mov.png\" border=\"0\" alt=\"".$ling['move_local3']."\" />
        <span>".$ling['move_local3']."</span>
        </a>
        </div>
        <div>
            
        <a href=\"manusis.php?id=$id&op=$op&exe=12\">
        <img src=\"imagens/icones/22x22/mov_produto.png\" border=\"0\" alt=\"".$tdb[MOV_PRODUTO]['DESC']."\" />
        <span>".$tdb[MOV_PRODUTO]['DESC']."</span>
        </a>
        </div>";
    }

    if ($_SESSION[ManuSess]['user']['MID'] == "ROOT") echo "<div>
    <a href=\"manusis.php?id=$id&op=$op&exe=10\">
    <img src=\"imagens/icones/22x22/empresa.png\" border=\"0\" alt=\"".$ling['tabela_local1']."\" />
    <span>".$tdb[EMPRESAS]['DESC']."</span>
    </a>
    </div>";
    
    echo "<div>
    <a href=\"manusis.php?id=$id&op=$op&exe=2\">
    <img src=\"imagens/icones/22x22/local1.png\" border=\"0\" alt=\"".$ling['tabela_local1']."\" />
    <span>".$tdb[AREAS]['DESC']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=3\">
    <img src=\"imagens/icones/22x22/local2.png\" border=\"0\" alt=\"".$ling['tabela_local2']."\" />
    <span>".$tdb[SETORES]['DESC']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=4\">
    <img src=\"imagens/icones/22x22/local3.png\" border=\"0\" alt=\"".$ling['tabela_local3']."\" />
    <span>".$tdb[MAQUINAS]['DESC']."</span>
    </a>
    </div>
        
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=5\">
    <img src=\"imagens/icones/22x22/equip.png\" border=\"0\" alt=\"".$ling['tabela_equip']."\" />
    <span>".$tdb[EQUIPAMENTOS]['DESC']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=11\">
    <img src=\"imagens/icones/22x22/acabado.png\" border=\"0\" alt=\"".$tdb[PRODUTOS_ACABADOS]['DESC']."\" />
    <span>".$tdb[PRODUTOS_ACABADOS]['DESC']."</span>
    </a>
    </div>

    <div>";
}
if (($exe == "") or ($exe == 1)) include("modulos/cadastro/va.php");

if ($exe == 10) {
    echo "<h3>".$tdb[EMPRESAS]['DESC']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";

    $res = $dba[0]->Execute("SELECT COUNT(MID) AS NUM FROM ".EMPRESAS);
    $num_empresas = $res->fields('NUM');
    $cad = ($manusis['empresas'] > $num_empresas) ? 1 : 0;
    
    $campos = array ();
    $campos[]="EMP_MATRIZ";
    $campos[]="COD";
    $campos[]="NOME";
    $campos[]="FANTASIA";
    $campos[]="TELEFONE_1";
    $campos[]="ENDERECO";
    $campos[]="NUMERO";
    $campos[]="COMPLEMENTO";
    $campos[]="BAIRRO";
    $campos[]="CIDADE";
    $campos[]="UF";
    ListaTabela(EMPRESAS,"MID",$campos,"MATRIX","","",$cad,1,1,1,1,1,1,1,1,1,"");
    echo "</div>";
}

if ($exe == 2) {
    echo "<h3>".$tdb[AREAS]['DESC']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";
    $campos = array();
    $campos[]="MID_EMPRESA";
    $campos[]="COD";
    $campos[]="DESCRICAO";
    ListaTabela(AREAS,"MID",$campos,"AREA","","",1,1,1,1,1,1,1,1,1,1,"");
    echo "</div>";
}

if ($exe == 3) {
    echo "<h3>".$tdb[SETORES]['DESC']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    $campos[2]="MID_AREA";
    ListaTabela(SETORES,"MID",$campos,"SETOR","","",1,1,1,1,1,1,1,1,1,1,"");
    echo "</div>";
}

if ($exe == 4) {
    echo "<h3>".$tdb[MAQUINAS]['DESC']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";
    
    $campos = array();
    $campos[] = "COD";
    $campos[] = "DESCRICAO";
    $campos[] = "MODELO";
    $campos[] = "NUMERO_DE_SERIE";
    $campos[] = "ANO_DE_FABRICACAO";
    $campos[] = "NUMERO_DO_PATRIMONIO";
    $campos[] = "FAMILIA";
    $campos[] = "CENTRO_DE_CUSTO";
    $campos[] = "FORNECEDOR";
    $campos[] = "CLASSE";
    $campos[] = "STATUS";
    $campos[] = "MID_SETOR";
    
    ListaTabela(MAQUINAS,"MID",$campos,"MAQUINA","","",1,1,1,1,0,1,1,1,1,1,"L3");
    
    echo "</div>";
}

if ($exe == 5) {
    echo "<h3>".$tdb[EQUIPAMENTOS]['DESC']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";
    $campos = array();
    $campos[]="COD";
    $campos[]="DESCRICAO";
    $campos[]="FAMILIA";
    $campos[]="MID_STATUS";
    $campos[]="FABRICANTE";
    $campos[]="FORNECEDOR";
    $campos[]="FICHA_TECNICA";
    $campos[]="MARCA";
    $campos[]="MODELO";
    $campos[]="NSERIE";
    $campos[]="POTENCIA";
    $campos[]="PRESSAO";
    $campos[]="CORRENTE";
    $campos[]="VAZAO";
    $campos[]="TENSAO";
    $campos[]="MID_EMPRESA";
    $campos[]="MID_MAQUINA";
    $campos[]="MID_CONJUNTO";
    ListaTabela(EQUIPAMENTOS,"MID",$campos,"EQUIPAMENTO","","",1,1,1,1,0,1,1,1,1,1,"equip");
    echo "</div>";
}

elseif ($exe == 11) {
    
    echo "<h3>".$tdb[PRODUTOS_ACABADOS]['DESC']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";
    
    $campos = array();
    $campos[]="COD";
    $campos[]="DESCRICAO";
    $campos[]="MID_MAQUINA";

    ListaTabela (PRODUTOS_ACABADOS,"MID",$campos,"PRODUTOS_ACABADOS","","",1,1,1,1,1,1,1,1,1,1,"PA");
}


if (($exe == 6) or ($exe == 61) or ($exe == 62) or ($exe == 63) or ($exe == 64) or ($exe == 65) or ($exe == 66) or ($exe == 67) or ($exe == 68) or ($exe == 69) or ($exe == 70))include("modulos/cadastro/detalha_local3.php");
elseif (($exe == 7) or ($exe == 71) or ($exe == 72) or ($exe == 73) or ($exe == 74) or ($exe == 75))include("modulos/cadastro/detalha_equip.php");
elseif ($exe == 8) include("modulos/cadastro/move_equip.php");
elseif ($exe == 9) include("modulos/cadastro/move_maq.php");
elseif ($exe == 12) include("modulos/cadastro/move_produto.php");
elseif ($exe == 111) {
    
     echo "<div id=\"mod_menu\">
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=11\">
    <img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"" . $ling['voltar'] . "\" />
    </a>
    </div>
    <div>";
    
    $oq = 0;
    
    if(isset($_GET['oq']) and (int)$_GET['oq'] != 0){
        $oq = (int)$_GET['oq'];
    }
    
    $produto = VoltaValor(PRODUTOS_ACABADOS, 'DESCRICAO', 'MID', $oq);
    
    echo "<h3>" . htmlentities($produto) . "</h3>
    </div>
    </div>
    <br clear=\"all\" />
    
    <div>";
    
    $campos = array();
    
    $campos[] = "DATA_INICIO";
    $campos[] = "DATA_FINAL";
    $campos[] = "DATA_CARGA";
    $campos[] = "QUANT_PROD";
    $campos[] = "QUANT_PERDA";
    
    ListaTabela(PRODUCAO_PRODUTOS_ACABADOS, "MID", $campos, "PRODUCAO_PRODUTOS_ACABADOS", "MID_PRODUTO_ACABADO", $oq, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, "");
    echo "</div>";
}
?>
