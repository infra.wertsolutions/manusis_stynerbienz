<?

/**
 * Administra��o da M�o de obra, cadastro de terceiros, equipes, turnos etc.
 * 
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @version  3.0
 * @package cadastro
 * @subpackage  maodeobra
 */
if (($exe == "") or ($exe == 1) or ($exe == 2) or ($exe == 3) or ($exe == 4) or ($exe == 5) or ($exe == 6) or ($exe == 8) or ($exe == 9)) {
    $listaemp = ListaMatriz($tdb[EMPRESAS]['dba']);
    $matrix = $listaemp[0]['nome'];
    $matrix_mid = $listaemp[0]['mid'];
    $matrix_cod = $listaemp[0]['cod'];
    echo "<div id=\"mod_menu\">
	
	<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/funcionario.png\" border=\"0\" alt=\"" . $ling['funcionarios'] . "\" />
<span>" . $ling['funcionarios'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=6\">
<img src=\"imagens/icones/22x22/maodeobra.png\" border=\"0\" alt=\"" . $ling['prestador'] . "\" />
<span>" . $ling['prestador'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=2\">
<img src=\"imagens/icones/22x22/equipe.png\" border=\"0\" alt=\"" . $ling['equipes'] . "\" />
<span>" . $ling['equipes'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/ausencia.png\" border=\"0\" alt=\"" . $ling['ausencia'] . "\" />
<span>" . $ling['ausencia'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=4\">
<img src=\"imagens/icones/22x22/ferias.png\" border=\"0\" alt=\"" . $ling['ferias'] . "\" />
<span>" . $ling['ferias'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=5\">
<img src=\"imagens/icones/22x22/turnos.png\" border=\"0\" alt=\"" . $ling['turnos'] . "\" />
<span>" . $ling['turnos'] . "</span>
</a>
</div>

<div>";
}
if (($exe == "") or ($exe == 1)) {
    echo "<h3>" . $ling['funcionarios'] . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos = array();
    $campos[] = "MID_EMPRESA";
    $campos[] = "NOME";
    $campos[] = "RG";
    $campos[] = "CPF";
    $campos[] = "DATA_NASCIMENTO";
    $campos[] = "MATRICULA";
    $campos[] = "ESPECIALIDADE";
    $campos[] = "EQUIPE";
    $campos[] = "TURNO";
    $campos[] = "CENTRO_DE_CUSTO";
    $campos[] = "VALOR_HORA";
    $campos[] = "ENDERECO";
    $campos[] = "BAIRRO";
    $campos[] = "CEP";
    $campos[] = "CIDADE";
    $campos[] = "UF";
    $campos[] = "TELEFONE";
    $campos[] = "CELULAR";
    $campos[] = "EMAIL";
    $campos[] = "SITUACAO";
    $campos[] = "DATA_ADMISSAO";
    $campos[] = "DATA_DEMISSAO";
    ListaTabela(FUNCIONARIOS, "MID", $campos, "FUNCIONARIO", "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "FUNC");
    echo "</div>";
} elseif ($exe == 2) {
    echo "<h3>" . $ling['equipes'] . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos = array();
    $campos[] = "MID_EMPRESA";
    $campos[] = "DESCRICAO";
    $campos[] = "TIPO";
    $campos[] = "FORNECEDOR";
    $campos[] = "OBSERVACAO";
    ListaTabela(EQUIPES, "MID", $campos, "EQUIPE", "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
} elseif ($exe == 3) {
    echo "<h3>" . $ling['ausencia'] . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos[0] = "MID_FUNCIONARIO";
    $campos[1] = "HORA_INICIAL";
    $campos[2] = "HORA_FINAL";
    $campos[3] = "DATA";
    $campos[4] = "MOTIVO";
    ListaTabela(AUSENCIAS, "MID", $campos, "AUSENCIA", "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
} elseif ($exe == 4) {
    echo "<h3>" . $ling['ferias'] . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos[0] = "MID_FUNCIONARIO";
    $campos[1] = "DATA_INICIAL";
    $campos[2] = "DATA_FINAL";
    $campos[3] = "MOTIVO";
    ListaTabela(AFASTAMENTOS, "MID", $campos, "AFASTAMENTO", "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
} elseif ($exe == 5) {
    // Limpando a sess�o na listagem de funcion�rios
    $_SESSION[ManuSess]['lt'][FUNCIONARIOS] = "";

    // MOSTRANDO A TELA
    echo "<h3>" . $tdb[TURNOS]['DESC'] . "</h3>
    </div>
    </div>
    
    <br clear=\"all\" />
    
    <div>
    <br />
    <table width=\"100%\" cellpadding=\"2\" cellspacing=\"4\" border=\"0\" align=\"center\" class=\"corpo\">
    <tr>
    <td>
    
    Novo Turno: <input class=\"campo_text_ob\" type=\"text\" size=\"35\" name=\"turno\" id=\"turno\" />";
    FormSelectD('COD', 'NOME', EMPRESAS, '', 'emp', 'emp', 'MID', '', (($_SESSION[ManuSess]['user']['MID'] == 'ROOT') ? 'campo_select' : 'campo_select_ob'), '', '', 'A', 'COD', $tdb[TURNOS]['MID_EMPRESA']);
    echo "<input class=\"botao\" type=\"button\" name=\"novoturno\" value=\"" . $ling['cadastrar'] . "\" onclick=\"atualiza_area2('result', 'parametros.php?id=novoTurno&emp=' + document.getElementById('emp').value + '&turno=' + document.getElementById('turno').value +'&modid=$id&op=$op&exe=$exe');\"/>
    
    </td>
    </tr>
    </table>
    <br clear=\"all\" />

    <span id=\"result\">";

    // Filtro por empresa
    $emps = array(0 => '0');
    $filtro_tmp = VoltaFiltroEmpresa(TURNOS);
    if (count($filtro_tmp) > 0) {
        foreach ($filtro_tmp['mid'] as $eemp)
            $emps[$eemp] = $eemp;
        $sqlvar = " WHERE " . $filtro_tmp['campo'] . " IN (" . implode(",", $emps) . ")";
    }
    else
        $sqlvar='';

    $sql = "SELECT * FROM " . TURNOS . " $sqlvar ORDER BY MID_EMPRESA, DESCRICAO ASC";

    $resultado = $dba[$tdb[TURNOS]['dba']]->Execute($sql);
    //echo $sql;
    $i = 0;
    $ch = $_POST['ch'];
    $ccc = $_POST['cc'];
    while (!$resultado->EOF) {
        $campo = $resultado->fields;
        $turno = $campo['DESCRICAO'];
        $tmid = $campo['MID'];
        if (($ch[$tmid] == 1) and ($_POST['hturno'])) {

            if($manusis['db'][$tdb[TURNOS_HORARIOS]['dba']]['driver'] == 'oci8'){

                $sqlHorario = "SELECT TH.*, TO_CHAR(HORA_INICIAL, 'HH24:MI:SS') AS HORA_INICIAL, TO_CHAR(HORA_FINAL, 'HH24:MI:SS') AS HORA_FINAL, TO_CHAR(HORA_TOTAL, 'HH24:MI:SS') AS HORA_TOTAL FROM " . TURNOS_HORARIOS . " TH WHERE MID_TURNO = $tmid ORDER BY DIA ASC";
            }
            else{
                $sqlHorario = "SELECT * FROM " . TURNOS_HORARIOS . " WHERE MID_TURNO = $tmid ORDER BY DIA ASC";
            }

            if(!$rr = $dba[$tdb[TURNOS_HORARIOS]['dba']]->Execute($sqlHorario)){
                erromsg("Erro ao localizar ".TURNOS_HORARIOS." em: <br />
                    Linha: ".__LINE__." <br />
                    ".$dba[$tdb[TURNOS_HORARIOS]['dba']]->ErrorMsg()." <br /> $sqlHorario
                ");
            }
            while (!$rr->EOF) {
                $cc = $rr->fields;
                $hmid = $cc['MID'];

                // Verificando a hora inicio e fim
                $mki = VoltaTime($ccc[$hmid]['hi'], date('d/m/Y'));
                $mkf = VoltaTime($ccc[$hmid]['hf'], date('d/m/Y'));

                if($ccc[$hmid]['hi'] == ""){
                   echo "<font color=\"red\">{$ling['hora_inicial_e_invalida']} ".(($cc['DIA'] == 1 or $cc['DIA'] == 7)?"no":"na")."&nbsp;". $ling['semana'][$cc['DIA']] . " (vazio).</font><br />\n";
                    $error = TRUE;
                }
                if($ccc[$hmid]['hf'] == ""){
                    echo "<font color=\"red\">{$ling['hora_final_e_invalida']}".(($cc['DIA'] == 1 or $cc['DIA'] == 7)?"no":"na")."&nbsp;". $ling['semana'][$cc['DIA']] . " (vazio).</font><br />\n";
                    $error = TRUE;
                }
                if (!ChecaHora($ccc[$hmid]['hi'])) {
                    echo "<font color=\"red\">{$ling['hora_inicial_e_invalida']} ".(($cc['DIA'] == 1 or $cc['DIA'] == 7)?"no":"na")."&nbsp;". $ling['semana'][$cc['DIA']] . " (" . $ccc[$hmid]['hi'] . ").</font><br />\n";
                    $error = TRUE;
                }
                if (!ChecaHora($ccc[$hmid]['hf'])) {
                    echo "<font color=\"red\">{$ling['hora_final_e_invalida']}".(($cc['DIA'] == 1 or $cc['DIA'] == 7)?"no":"na")."&nbsp;". $ling['semana'][$cc['DIA']] . " (" . $ccc[$hmid]['hf'] . ").</font><br />\n";
                    $error = TRUE;
                }
                if (!ChecaHora($ccc[$hmid]['ht'])) {
                    echo "<font color=\"red\">{$ling['horario_total_invalido']} ".(($cc['DIA'] == 1 or $cc['DIA'] == 7)?"no":"na")."&nbsp;". $ling['semana'][$cc['DIA']] . " (" . $ccc[$hmid]['ht'] . ").</font><br />\n";
                    $error = TRUE;
                }
                if (($mki > $mkf) and (!$error)) {
                    echo "<font color=\"red\">Hora inicial maior que hora final ".(($cc['DIA'] == 1 or $cc['DIA'] == 7)?"no":"na")."&nbsp;". $ling['semana'][$cc['DIA']] . " (" . $ccc[$hmid]['hi'] . " e " . $ccc[$hmid]['hf'] . ").</font><br />\n";
                    $error = TRUE;
                }
                // Atualiza apenas se a hora inicial for menor que a final
                if (($mki <= $mkf) and (!$error)) {

                    // TRATAMENTO PARA MODELO ORACLE
                    if($manusis['db'][$tdb[TURNOS_HORARIOS]['dba']]['driver'] == 'oci8'){
                        $sqlUPD = "UPDATE " . TURNOS_HORARIOS . " SET HORA_INICIAL = TO_DATE('{$ccc[$hmid]['hi']}', 'HH24:MI:SS'), HORA_FINAL = TO_DATE('{$ccc[$hmid]['hf']}', 'HH24:MI:SS'), HORA_TOTAL = TO_DATE('{$ccc[$hmid]['ht']}', 'HH24:MI:SS') WHERE MID = $hmid";
                    }
                    else{
                        $sqlUPD = "UPDATE " . TURNOS_HORARIOS . " SET HORA_INICIAL = '" . $ccc[$hmid]['hi'] . "', HORA_FINAL = '" . $ccc[$hmid]['hf'] . "', HORA_TOTAL = '" . $ccc[$hmid]['ht'] . "' WHERE MID = $hmid";
                    }

                    $rrr = $dba[$tdb[TURNOS_HORARIOS]['dba']]->Execute($sqlUPD);

                    if (!$rrr){
                        erromsg("Erro ao atualizar ".TURNOS_HORARIOS." em: <br />
                        Linha: ".__LINE__." <br />
                        ".$dba[$tdb[TURNOS_HORARIOS]['dba']]->ErrorMsg()." <br /> $sqlHorario <br />
                ");
                    }
                }


                $rr->MoveNext();
            }
            unset($rr);
            unset($cc);
        }

        echo "
		<div id=\"lt_tabela\">
        <table>
        <tr>
        <th>{$tdb[TURNOS]['MID_EMPRESA']}: <strong>" . ($campo['MID_EMPRESA'] ? htmlentities(VoltaValor(EMPRESAS, 'COD', 'MID', $campo['MID_EMPRESA'], 0) . '-' . VoltaValor(EMPRESAS, 'NOME', 'MID', $campo['MID_EMPRESA'], 0)) : strtoupper($ling['todas_empresas'])) . "</strong></th>
        <th><strong>$turno</strong></th>
        <th width=\"1\" nowrap=\"nowrap\">";

        $perm = VoltaPermissao($id, $op);
        if ($perm == 1)
            echo "<a href=\"javascript: confirma_delete('" . TURNOS . "', '$tmid');\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" title=\" Remover \" align=\"middle\" alt=\"" . $ling['remover'] . "\" />  </a>\n";
        if (($perm == 1) or ($perm == 2))
            echo "<a href=\"javascript:abre_janela_form('TURNO','$tmid','$oq','2','$id','$exe','$op'," . $form['TURNO'][0]['altura'] . "," . $form['TURNO'][0]['largura'] . ",'','')\"><img src=\"imagens/icones/22x22/editar.png\" border=0 align=\"middle\" alt=\"" . $ling['editar'] . "\" title=\" " . $ling['editar'] . " \" />  </a>\n";

        echo "</th>
        </tr>

        <tr>
        <td width=\"50%\" valign=\"top\">

        <form action=\"manusis.php?id=$id&op=$op&exe=$exe\" method=\"POST\" class=\"form\">
        <table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" align=\"center\">
        <tr>
        <td class=\"cor2\"> </td>
        <td class=\"cor2\"><strong>" . $ling['horario_inicial'] . "</strong></td>
        <td class=\"cor2\"><strong>" . $ling['horario_final'] . "</strong></td>
        <td class=\"cor2\"><strong>" . $ling['horario_total'] . "</strong></td>
        </tr>";

        // tratamento para modelo oracle
        if($manusis['db'][$tdb[TURNOS_HORARIOS]['dba']]['driver'] == 'oci8'){

            $sqlHorario = "SELECT TH.*, TO_CHAR(HORA_INICIAL, 'HH24:MI:SS') AS HORA_INICIAL, TO_CHAR(HORA_FINAL, 'HH24:MI:SS') AS HORA_FINAL, TO_CHAR(HORA_TOTAL, 'HH24:MI:SS') AS HORA_TOTAL FROM " . TURNOS_HORARIOS . " TH WHERE MID_TURNO = $tmid ORDER BY DIA ASC";
        }
        else{
            $sqlHorario = "SELECT * FROM " . TURNOS_HORARIOS . " WHERE MID_TURNO = $tmid ORDER BY DIA ASC";
        }

        if(!$rr = $dba[$tdb[TURNOS_HORARIOS]['dba']]->Execute($sqlHorario)){
            erromsg("Erro ao localizar ".TURNOS_HORARIOS." em: <br />
                Linha: ".__LINE__." <br />
                ".$dba[$tdb[TURNOS_HORARIOS]['dba']]->ErrorMsg()." <br /> $sqlHorario
            ");
        }

        while (!$rr->EOF) {
            $cc = $rr->fields;
            $hmid = $cc['MID'];
            $dia = $ling['semana'][$cc['DIA']];

            echo "<tr>
            <td class=\"cor2\" align=\"left\"><strong>$dia</strong></td>
            <td class=\"cor1\" align=\"center\"><input class=\"campo_text\" type=\"text\" name=\"cc[$hmid][hi]\" value=\"" . $cc['HORA_INICIAL'] . "\" size=\"8\" maxlength=\"8\" " . (($perm == 3) ? "readonly=\"readonly\"" : "onkeypress=\"return ajustar_hora(this, event)\"") . " /></td>
            <td class=\"cor1\" align=\"center\"><input class=\"campo_text\" type=\"text\" name=\"cc[$hmid][hf]\" value=\"" . $cc['HORA_FINAL'] . "\" size=\"8\" maxlength=\"8\" " . (($perm == 3) ? "readonly=\"readonly\"" : "onkeypress=\"return ajustar_hora(this, event)\"") . " /></td>
            <td class=\"cor1\" align=\"center\"><input class=\"campo_text\" type=\"text\" name=\"cc[$hmid][ht]\" value=\"" . $cc['HORA_TOTAL'] . "\" size=\"8\" maxlength=\"8\" " . (($perm == 3) ? "readonly=\"readonly\"" : "onkeypress=\"return ajustar_hora(this, event)\"") . " /></td>
            </tr>";

            $rr->MoveNext();
        }

        echo "<tr>
		<td colspan=\"4\" align=\"center\">
        <input type=\"hidden\" name=\"ch[$tmid]\" value=\"1\" />
        " . (($perm != 3) ? "<input type=\"submit\" name=\"hturno\" class=\"botao\" value=\"" . $ling['atualizar'] . "\" size=\"8\" />" : '') . "
        </td>
        </tr>
        </table>

        </form>
        </td>

        <td width=\"50%\" colspan=\"2\" valign=\"top\">";



        $campos = array();
        $campos[] = "NOME";
        $campos[] = "ESPECIALIDADE";
        ListaTabela(FUNCIONARIOS, "MID", $campos, "FUNCIONARIO", "TURNO", $tmid, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "");

        echo "</td>
		</tr>

		</table>

		</div>

		<br />\n";

        $resultado->MoveNext();
        $i++;
    }

    echo "</span>";
    
} elseif ($exe == 6) {
    echo "<h3>" . $ling['prestador'] . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos = array();
    $campos[] = "MID_EMPRESA";
    $campos[] = "NOME";
    $campos[] = "ESPECIALIDADE";
    $campos[] = "VALOR_HORA";
    $campos[] = "SITUACAO";
    $campos[] = "FORNECEDOR";

    ListaTabela(FUNCIONARIOS, "MID", $campos, "FUNCIONARIO2", "", "", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "PRES");
    echo "</div>";
}

echo "</td>
</td></tr></table>
<br />";
?>
