<?
/**
* Movimentação de Produtos
* 
* @author  Hudson C. Souza <hudson@wolk.com.br>
* @version  3.0
* @package manusis
* @subpackage  cadastro
*/

echo "<h3>".$tdb[MOV_PRODUTO]['DESC']."</h3>
</div>
</div>
<br clear=\"all\" />
<div id=\"lt\"><br />";

$msg = '';

if(isset($_GET['enviar'])){
    
    $motivo = trim(LimpaTexto($_GET['motivo']));
    $destino = (int)$_GET['destino'];
    $origem = (int)$_GET['origem'];
    $dataIni = $_GET['dataIni'];
    $produto = (int)$_GET['produto'];
    // Registro ao qual será efetuado a baixa da movimentação
    $oq = (int)$_GET['oq'];
    
    $dtIni = explode('/', $dataIni);
    
    // Validações
    if($motivo == ''){
        $msg .= "<li><strong>{$tdb[MOV_PRODUTO]['MOTIVO']}:</strong> {$ling['form01']}</li>";
    }
    
    if($dataIni == ''){
        $msg .= "<li><strong>{$tdb[MOV_PRODUTO]['DATA_INICIAL']}:</strong> {$ling['form01']}</li>";
    }
    elseif(!isset($dtIni[0]) or !isset($dtIni[1]) or !isset($dtIni[2])){
        $msg .= "<li><strong>{$tdb[MOV_PRODUTO]['DATA_INICIAL']}:</strong> {$ling['data_invalida']}</li>";
    }
    elseif(strlen($dtIni[2]) < 4){
        $msg .= "<li><strong>{$tdb[MOV_PRODUTO]['DATA_INICIAL']}:</strong> {$ling['ano_invalido']}</li>";
    }
    elseif(!checkdate($dtIni[1], $dtIni[0], $dtIni[2])){
        $msg .= "<li><strong>{$tdb[MOV_PRODUTO]['DATA_INICIAL']}:</strong> {$ling['data_invalida']}</li>";
    }
    else{
       $dataIni = DataSQL($dataIni);
    }
    
    if($origem == $destino){
        $msg .= "<li><strong>{$tdb[MOV_PRODUTO]['MID_MAQUINA_DESTINO']}:</strong> {$ling['erro_mov_prod01']}</li>";
    }
    
    // Sem erros segue a movimentação
    if($msg == ''){
        
        // DADOS PARA INSERIR NA TABELA DE MOVIMENTAÇÃO
        
        $tmpMid = geraMid(MOV_PRODUTO, 'MID', $tdb[MOV_PRODUTO]['dba']);
        $dados['MID'] = $tmpMid; 
        $dados['MID_MAQUINA_ORIGEM'] = $origem; 
        $dados['MID_MAQUINA_DESTINO'] = $destino; 
        $dados['MID_PRODUTO'] = $produto; 
        $dados['DATA_INICIAL'] = "'{$dataIni}'"; 
        $dados['MOTIVO'] = "'{$motivo}'"; 
        
        // Inserindo a movimentação
        $sql = "INSERT INTO ".MOV_PRODUTO." (".implode(',', array_keys($dados)).") VALUES(".implode(',', array_values($dados)).")";
        if(!$dba[$tdb[MOV_PRODUTO]['dba']]->Execute($sql)){
            erromsg("
            Erro ao cadastrar {$tdb[MOV_PRODUTO]['DESC']} em:<br />
            Arquivo: ".__FILE__."<br />
            Linha: ".__LINE__."<br />
            Erro: ".$dba[$tdb[MOV_PRODUTO]['dba']]->ErrorMsg()."<br />
            SQL: $sql    
            ");
        }
        // Atualizando o registro de onde o produto veio
        else{
            
            // Logando a movimentação
            logar(3, '', MOV_PRODUTO, 'MID', $tmpMid);
            
            $dt = explode('-', $dataIni);
            $mkDtFim = mktime(0, 0, 0, $dt[1], ($dt[2] - 1), $dt[0]);
            $dataFinal = date('Y-m-d', $mkDtFim);
            
            $sql = "UPDATE ".MOV_PRODUTO." SET DATA_FINAL = '{$dataFinal}' WHERE MID = {$oq}";
            if(!$dba[$tdb[MOV_PRODUTO]['dba']]->Execute($sql)){
                erromsg("
                Erro ao Atualizar {$tdb[MOV_PRODUTO]['DESC']} em:<br />
                Arquivo: ".__FILE__."<br />
                Linha: ".__LINE__."<br />
                Erro: ".$dba[$tdb[MOV_PRODUTO]['dba']]->ErrorMsg()."<br />
                SQL: $sql    
                ");
            }
            else{
                
                logar(4, "OCORRÊNCIA: ADICIONADO A DATA FINAL DURANTE MOVIMENTAÇÃO", MOV_PRODUTO, 'MID', $oq);
                
                // ATUALIZANDO O CADASTRO DO PRODUTO MOVIMENTADO
                $sql = "UPDATE ".PRODUTOS_ACABADOS." SET MID_MAQUINA = {$destino} WHERE MID = {$produto}";
                if(!$dba[$tdb[PRODUTOS_ACABADOS]['dba']]->Execute($sql)){
                    erromsg("
                    Erro ao Atualizar {$tdb[PRODUTOS_ACABADOS]['DESC']} em:<br />
                    Arquivo: ".__FILE__."<br />
                    Linha: ".__LINE__."<br />
                    Erro: ".$dba[$tdb[PRODUTOS_ACABADOS]['dba']]->ErrorMsg()."<br />
                    SQL: $sql    
                    ");
                }
                else{
                    
                    $txt = '';
                    
                    $txt .= "PRODUTO ATUALIZADO EM MOVIMENTAÇÃO \r\n";
                    
                    // Logando o fechamento da integração
                    logar(4, $txt, PRODUTOS_ACABADOS, 'MID', $produto); 
                    
                    echo "<script>window.location.href='?id={$_GET['id']}&op={$_GET['op']}&exe={$_GET['exe']}&produto={$produto}'</script>";
                }
            }
        }
    }
    
}

if($msg != ''){
    echo "<div style='width:30%'>";
    erromsg("<strong>{$ling['mov_prod_error']}</strong><br /><ul>{$msg}</ul>");
    echo "</div><br />";
}

echo "
<form action=\"manusis.php\" name=\"moveprod\"method=\"get\">
<input type=\"hidden\" name=\"id\" value=\"$id\">
<input type=\"hidden\" name=\"op\" value=\"$op\">
<input type=\"hidden\" name=\"exe\" value=\"$exe\">
<p>".$ling['selecionar_prod'].":<br>";
FormSelectD("COD","DESCRICAO",PRODUTOS_ACABADOS,$_GET['produto'],"produto","produto","MID",0,"campo_select_ob","document.moveprod.submit()","","S","COD","");
echo "</p>";


if ($_GET['produto'] != "") {
    
    // Recuperando a última movimentação para verificar onde o produto está e desde quando
    $sql = "SELECT * FROM ".MOV_PRODUTO." WHERE MID_PRODUTO = ".(int)$_GET['produto']." ORDER BY MID DESC LIMIT 1";
    if(!$rs = $dba[$tdb[MOV_PRODUTO]['dba']]->Execute($sql)){
        erromsg("Erro ao localizar dados de {$tdb[MOV_PRODUTO]['DESC']} em:<br />
            Arquivo: ".__FILE__."<br />
            Linha: ".__LINE__."<br />
            Erro: ".$dba[$tdb[MOV_PRODUTO]['dba']]->ErrorMsg()."<br />
            SQL: $sql
        ");
    }
    elseif(!$rs->EOF){    
        echo "<div id='lt_tabela'>\n";
        
        echo "<br />\n";
        echo "<table>\n";
        echo "<tr>\n";
        echo "<th>{$tdb[MOV_PRODUTO]['MID_PRODUTO']}</th>\n";
        echo "<th>{$tdb[MOV_PRODUTO]['MID_MAQUINA_ORIGEM']}</th>\n";
        echo "<th>{$tdb[MOV_PRODUTO]['DATA_INICIAL']}</th>\n";
        echo "</tr>\n";

        echo "<tr>\n";
        echo "<td>".VoltaValor(PRODUTOS_ACABADOS, 'DESCRICAO', 'MID', $rs->fields['MID_PRODUTO'])."</td>\n";
        echo "<td>".VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', VoltaValor(PRODUTOS_ACABADOS, 'MID_MAQUINA', 'MID', $rs->fields['MID_PRODUTO']))."</td>\n";
        echo "<td>".NossaData($rs->fields['DATA_INICIAL'])."</td>\n";
        echo "</tr>\n";
        
        echo "</table>\n";
        echo "<input type='hidden' name='oq' value='{$rs->fields['MID']}' />";
        echo "<input type='hidden' name='origem' value='".VoltaValor(PRODUTOS_ACABADOS, 'MID_MAQUINA', 'MID', $rs->fields['MID_PRODUTO'])."' />";
    }
    else{
        
        echo "<div id='lt_tabela'>\n";
        
        echo "<br />\n";
        echo "<table>\n";
        echo "<tr>\n";
        echo "<th>{$tdb[MOV_PRODUTO]['MID_PRODUTO']}</th>\n";
        echo "<th>{$tdb[MOV_PRODUTO]['MID_MAQUINA_ORIGEM']}</th>\n";
        echo "<th>{$tdb[MOV_PRODUTO]['DATA_INICIAL']}</th>\n";
        echo "</tr>\n";

        echo "<tr>\n";
        echo "<td colspan='3'>".htmlentities("PRODUTO AINDA NÃO ALOCADO")."</td>\n";
        echo "</tr>\n";
        
        echo "</table>\n";
        echo "<input type='hidden' name='oq' value='0' />";
        echo "<input type='hidden' name='origem' value='0' />";
    }
	echo " <br /><p>".$ling['selecionar_destino'].":<br>";
	FormSelectD("COD","DESCRICAO",MAQUINAS,$_GET['destino'],"destino","destino","MID",0,"campo_select_ob","document.moveprod.submit()","WHERE MID != 0","S","COD");
    echo "</p><br />";
    if($_GET['destino']){
        
        echo "
        ".FormData($tdb[MOV_PRODUTO]['DATA_INICIAL'].":<br />", 'dataIni', $_GET['dataIni'], '', '', 'campo_select_ob')."<br /><br />\n
            
        <p>".$ling['motivo'].":<br />
        <input type=\"text\" class=\"campo_text_ob\" size=\"100\" name=\"motivo\" value='{$_GET['motivo']}'><br /></p><br clear='all'/>\n
        <input type=\"submit\" class=\"botao\" name=\"enviar\" value=\"Movimentar\" /><br><br>";	
    }

	echo "</form>";
	echo "</div>";#lt

}
?>