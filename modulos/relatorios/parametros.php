<?
/**
* Parametros para uso nas requisi��es com AJAX
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/
if (!require ("../../lib/mfuncoes.php"))
    die("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require ("../../conf/manusis.conf.php")) die("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require ("../../lib/idiomas/" . $manusis['idioma'][0] . ".php")) die("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require ("../../lib/adodb/adodb.inc.php")) die($ling['bd01']);
// Informa��es do banco de dados
elseif (!require ("../../lib/bd.php")) die($ling['bd01']);
elseif (!require ("../../lib/delcascata.php")) die($ling['bd01']);
elseif (!require ("funcoes.php")) die("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
elseif (!require ("conf.php")) die("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");

// Opera��o
$op = $_GET['op'];

// ID e OP do ManuSis
$id_man = (int) $_GET['id_man'];
$op_man = (int) $_GET['op_man'];

if ($op == 1) {
    echo "<span id=\"div_formulario_cab\">" . $ling['anexo_titulo'] . "
    <a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"" . $ling['fechar'] . "\" /></a>
    </span>
    <div id=\"div_formulario_corpo\">
    <iframe width=\"100%\" height=\"384\" name=\"frame_form\" id=\"frame_form\" frameborder=\"0\" src=\"modulos/relatorios/arquivos.php?id_man=$id_man&op_man=$op_man\"> </iframe>
    </div>";
}
if ($op == 2) {
    $tb= (int) $_GET['tb'];
    $arq= urldecode($_GET['arq']);
    echo "<span id=\"div_formulario_cab\">" . $ling['anexo_titulo'] . "
    <a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"" . $ling['fechar'] . "\" /></a>
    </span>
    <div id=\"div_formulario_corpo\">
    <iframe width=\"100%\" height=\"384\" name=\"frame_form\" id=\"frame_form\" frameborder=\"0\" src=\"modulos/relatorios/tabelas.php?arq=$arq&tb=$tb\"> </iframe>
    </div>";
}
if ($op == 3) {
    $exe= (int) $_GET['exe'];
    $arq= urldecode($_GET['arq']);
    $arq= LimpaTexto($_GET['arq']);
    $arq= str_replace("..", "", $arq);
    $arq= str_replace("/", "", $arq);
    $dir= "../../" . $manusis['dir']['graficos'];
    if ($arq != "") {
        $xml= new DOMDocument();
        $xml->load("$dir/$arq");
        // Remove tabela
        if ($_GET['del'] == 1) {
            AG_DelTabela($xml, $_GET['oq']);
        }
        // Adiciona Tabela
        if ((int) $_GET['add'] == 1) {
            $add_titulo= LimpaTexto($_GET['titulo']);
            $add_tipo= (int) $_GET['tipo'];
            AG_AddTabela($xml, $add_titulo, $add_tipo);
        }
        $titulo= $xml->getElementsByTagName('descricao');
        $titulo= utf8_decode($titulo->item(0)->nodeValue);
        $arq_envia= urlencode($arq);

        $ger = explode("_", $arq);

        if (($ger[0] == "ge")) {
            $ger = 1;
        }
        else {
            $ger = 0;
        }


        echo "<table id=\"lt_tabela\" width=\"100%\" cellspacing=\"1\" style=\"border:1px solid #ccc;padding:0px;\">
                <tr>
                <th>".$ling['titulo_tabela']."</th>
                <th>".$ling['parametros']."</th>
                <th>".$ling['opcoes']."</th>
                </tr>";
        $params= $xml->getElementsByTagName('tabela');
        $i= 0;
        foreach ($params as $param) {
            $tbtipo= $param->getAttribute('tipo');
            $tbid= $param->getAttribute('id');
            $tbtitulo= $param->getElementsByTagName('titulo');
            $tbtitulo= htmlentities(utf8_decode($tbtitulo->item(0)->nodeValue));
            echo "<tr class=\"cor1\">
                    <td>$tbtitulo</td>
                    <td>";
            if ($tbtipo == 1) {
                echo "<label for=\"$tbid\">Ano:</label><input type=\"text\" size=\"4\" maxlength=\"4\" name=\"ano_$tbid\" id=\"ano_$tbid\" class=\"campo_text\" />";
            }
            elseif ($tbtipo == 2) {
                FormData("Per&iacute;odo de:", "datai_$tbid", "", "campo_label");
                FormData("a", "dataf_$tbid", "", "campo_label");
            }
            echo "<select name=\"tdata_$tbid\" class=\"campo_select\">
                    <option value=\"DATA_INICIO\">Data Inicio</option>
                    <option value=\"DATA_FINAL\">Data Final</option>
                    <option value=\"DATA_PROG\">Data Programado/Abertura</option>
                    <option value=\"DATA_APONTA\">Data Apontamento</option>
                    </select>
                    </td>
                    <td><a href=\"javascript:abre_ag_tabela($tbid,'$arq_envia')\"> <img src=\"imagens/icones/22x22/editar.png\" border=0 align=\"middle\" alt=\"Editar \" title=\"Editar Tabela \" /> </a> 
                    <a href=\"javascript:abre_ag_filtros($tbid,'$arq_envia')\"> <img src=\"imagens/icones/22x22/visualizar.png\" border=0 align=\"middle\" alt=\"Filtros \" title=\" Filtros \" /> </a>"; 

            if ($ger == 1) {
                echo "<a href=\"javascript:abre_ag_gerencia($tbid,'$arq_envia')\"> <img src=\"imagens/icones/22x22/checklist.png\" border=0 align=\"middle\" alt=\"Definir colunas \" title=\"Definir colunas \" /> </a>";
            }
            echo "<a href=\"javascript:atualiza_area('ag_tabelas','modulos/relatorios/parametros.php?op=3&arq=$arq_envia&del=1&oq=$i')\"> <img src=\"imagens/icones/22x22/del.png\" border=0 align=\"middle\" alt=\"Excluir\" title=\"Excluir\" />
                    </td>
                    </tr>";
            $i++;
        }
        echo "</table>";
        unset ($xml);
    } else {
        erromsg("Imposs&iacute;vel abrir o arquivo XML");
    }
}
if ($op == 4) {
    $tb= (int) $_GET['tb'];
    $arq= urldecode($_GET['arq']);
    echo "<span id=\"div_formulario_cab\">" . $ling['anexo_titulo'] . "
    <a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"" . $ling['fechar'] . "\" /></a>
    </span>
    <div id=\"div_formulario_corpo\">
    <iframe width=\"100%\" height=\"384\" name=\"frame_form\" id=\"frame_form\" frameborder=\"0\" src=\"modulos/relatorios/filtros.php?arq=$arq&tb=$tb\"> </iframe>
    </div>";
}
if ($op == 5) {
    $tb= (int) $_GET['tb'];
    $arq= urldecode($_GET['arq']);
    $campo= $_GET['campo'];
    $tmp= VoltaRelacao(ORDEM_PLANEJADO, $campo);
    if ($tmp['cod']) {
        $c1= $tmp['cod'];
        $c2= $tmp['campo'];
    } else {
        $c1= $tmp['campo'];
        $c2= "";
    }
    echo "<label for=\"valor\">Valor:</label>";
    //FormSelectD("valor",$tmp['tb'],0,$tmp['campo'],$tmp['mid'],$tmp['dba'],0,"S");
    FormSelectD($c1, $c2, $tmp['tb'], 0, "valor", "valor", $tmp['mid'], "", $classe= "", $acao= "", $sql_filtro= "", $auto_filtro= "A", $ordem= "", $primeirocampo= "");
    echo "<br clear=\"all\" />";
}
if ($op == 6) {
    $tb= (int) $_GET['tb'];
    $arq= urldecode($_GET['arq']);
    $arq= str_replace("..", "", $arq);
    $arq= str_replace("/", "", $arq);
    $dir= "../../" . $manusis['dir']['graficos'];
    $campo= $_GET['campo'];
    $condicao= $_GET['condicao'];
    $valor= $_GET['valor'];
    $exe= (int) $_GET['exe'];
    $oq= (int) $_GET['oq'];
    if ($arq != "") {
        $xml= new DOMDocument();
        $xml->load("$dir/$arq");
        // Remove Filtro
        if ($_GET['del'] == 1) {
            AG_DelFiltro($xml, $tb, $oq);
        }
        // Adiciona Filtro
        if ($_GET['campo'] != "") {
            AG_AddFiltro($xml, $tb, $campo, $condicao, $valor);
        }
        if ($_GET['ope'] != "") {
            AG_TrocaCondicao($xml, $tb, $oq, (int) $_GET['ope']);
        }
        echo "
                <div id=\"lt_tabela\"><table id=\"lt_tabela\">
                <tr>
                <th>".$ling['operador']."</th>
                <th>".$ling['campo_']."</th>
                <th>".$ling['condicao']."</th>
                <th>".$ling['valor']."</th>      
                <th width=\"22\">".$ling['opcoes']."</th>
                </tr>";
        $tabela= AG_PegaTabela($xml, $tb);
        $params= $tabela->getElementsByTagName('filtro');
        $i= 0;
        foreach ($params as $param) {
            $info_filtro= AG_InfoFiltro($xml, $tb, $i);
            $tmp= VoltaRelacao(ORDEM_PLANEJADO, $info_filtro['campo']);
            if ($tmp) {
                $tmp= htmlentities(VoltaValor($tmp['tb'], $tmp['campo'], $tmp['mid'], $info_filtro['valor'], $tmp['dba']));
            } else {
                $tmp= htmlentities($info_filtro['valor']);
            }
            if ($info_filtro['operador'] == 1) {
                $selected_1= "checked=\"checked\"";
                $selected_2= "";
            }
            elseif ($info_filtro['operador'] == 2) {
                $selected_2= "checked=\"checked\"";
                $selected_1= "";
            }
            echo "<tr class=\"cor1\"><td align=\"left\">";
            if ($i != 0) {
                echo "<input type=\"radio\" name=\"ope$i\" $selected_1 value=\"1\" onclick=\"atualiza_area('tb_filtros','parametros.php?op=6&arq=" . $_GET['arq'] . "&ope=1&tb=$tb&oq=$i')\"> E";
                echo "<input type=\"radio\" name=\"ope$i\" $selected_2 value=\"2\" onclick=\"atualiza_area('tb_filtros','parametros.php?op=6&arq=" . $_GET['arq'] . "&ope=2&tb=$tb&oq=$i')\"> OU";
            }
            echo "</td>
                        <td align=\"left\">" . $AG_filtros[$info_filtro['campo']]['DESCRICAO'] . "</td>
                        <td align=\"left\">" . $AG_cond[$info_filtro['condicao']] . "</td>
                        <td align=\"left\">" . $tmp . "</td>
                        <td><a href=\"javascript:atualiza_area('tb_filtros','parametros.php?op=6&arq=" . $_GET['arq'] . "&del=1&oq=$i&tb=$tb')\"> <img src=\"../../imagens/icones/22x22/del.png\" border=0 align=\"middle\" alt=\"Excluir\" title=\"Excluir\" />
                        </td>
                        </tr>";
            $i++;
        }
        echo "</table></div>";
        unset ($xml);
    } else {
        erromsg("".$ling['imp_abrir_XML']."");
    }
}
if ($op == 7) {
    $dir= "../../" . $manusis['dir']['graficos'];
    $tit= LimpaTexto($_GET['titulo']);
    $arq= LimpaTexto($_GET['arq']);
    $i= LimpaTexto($_GET['i']);
    $xml= new DOMDocument();
    $xml->load("$dir/$arq");
    $xml_grafico= $xml->getElementsByTagName('grafico')->item(0);
    $titulo= $xml_grafico->getElementsByTagName('descricao')->item(0);
    if ($xml_grafico->removeChild($titulo)) {
        $xml->save($xml->documentURI);
    }
    // dentro do <grafico> coloco  a <descricao> e <data_criacao>
    $xml_descricao= $xml->createElement('descricao', utf8_encode($tit));
    $xml_descricao= $xml_grafico->appendChild($xml_descricao);
    $xml->save($xml->documentURI);
    sleep(1);
    echo "<input type=\"text\" size=\"55\" id=\"grafi_$i\" value=\"$tit\" class=\"campo_text\" />";
}
if ($op == 8) {
    $tb= (int) $_GET['tb'];
    $arq= urldecode($_GET['arq']);
    $id= $_GET['id'];
    echo "<span id=\"div_formulario_cab\">" . $ling['anexo_titulo'] . "
    <a href=\"javascript:fecha_janela_form()\"><img src=\"imagens/icones/fecha.gif\" border=\"0\" alt=\" \" title=\"" . $ling['fechar'] . "\" /></a>
    </span>
    <div id=\"div_formulario_corpo\">
    <iframe width=\"100%\" height=\"384\" name=\"frame_form\" id=\"frame_form\" frameborder=\"0\" src=\"modulos/relatorios/gerencia.php?arq=$arq&tb=$tb&id\"> </iframe>
    </div>";
}
if ($op ==9) {
    $tb= (int) $_GET['tb'];
    $arq= urldecode($_GET['arq']);
    $dir= "../../" . $manusis['dir']['graficos'];
    
    $xml= new DOMDocument();
    $xml->load("$dir/$arq");
    
    
}
?>
