<?


// Fun��es do Sistema
if (!require ("lib/mfuncoes.php"))
die("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require ("conf/manusis.conf.php")) die("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require ("lib/idiomas/" . $manusis['idioma'][0] . ".php")) die("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require ("lib/adodb/adodb.inc.php")) die($ling['bd01']);
// Informa��es do banco de dados
elseif (!require ("lib/bd.php")) die($ling['bd01']);
elseif (!require ("lib/delcascata.php")) die($ling['bd01']);



function filtrar($linha, $tb, $campos, $data = "", $hora = "") {
	global $dba, $tdb, $ling, $manusis;

	static $tabela_mostra = array();
	
	$data = (is_array($data))? $data : array();
	$hora = (is_array($hora))? $hora : array();
	$erro = 0;
	$sqlData = array();
	foreach ($campos as $k => $f) {
		$rtb = VoltaRelacao($tb, $f);

		if($rtb != ""){
            
            if($rtb['cod']) {
				$m = (int) VoltaValor($rtb['tb'], $rtb['mid'], $rtb['cod'], LimpaTexto(trim($linha[$k])), $rtb['dba']);
			}
			else {
				$m = (int) VoltaValor($rtb['tb'], $rtb['mid'], $rtb['campo'], LimpaTexto(trim($linha[$k])), $rtb['dba']);
			}

			if (($m == 0) && ($linha[$k] != "")) {
				
				$m= GeraMid($rtb['tb'], $rtb['mid'], 0);
				if ($rtb['cod']) {
					$sql = "INSERT INTO " . $rtb['tb'] . " (MID, " . $rtb['cod'] . ", " . $rtb['campo'] . ") VALUES ('" . $m . "', '" . LimpaTexto(trim($linha[$k])) . "', 'CRIADO DINAMICAMENTE EM CARGA')";
					
					// Verifica se ja mostrei essa lista
					if (! in_array($rtb['tb'], $tabela_mostra)) {
						echo "@;{$rtb['tb']};MID;{$rtb['cod']};{$rtb['campo']}<br />\n";
						$tabela_mostra[] = $rtb['tb'];
					}
					
					// Printando o INSERT
					echo "!;{$rtb['tb']};{$m};'" . LimpaTexto(trim($linha[$k])) . "';'CRIADO DINAMICAMENTE EM CARGA'<br />\n";
				}
				else {
					$sql = "INSERT INTO " . $rtb['tb'] . " (MID, " . $rtb['campo'] . ") VALUES ('" . $m . "', '" . LimpaTexto(trim($linha[$k])) . "')";
					
					// Verifica se ja mostrei essa lista
					if (! in_array($rtb['tb'], $tabela_mostra)) {
						echo "@;{$rtb['tb']};MID;{$rtb['campo']}<br />\n";
						$tabela_mostra[] = $rtb['tb'];
					}
					
					// Printando o INSERT
					echo "!;{$rtb['tb']};{$m};'" . LimpaTexto(trim($linha[$k])) . "'<br />\n";
				}
				
				if (!$dba[0]->Execute($sql)) {
					erromsg("File: cargas_geral.php Line: 47 <br />" . $dba[0]->ErrorMsg() . "<br />" . $sql);
					exit ();
				}
				
				
			}
			if ($m != 0) {
				$linha_bkp[$k]= $linha[$k];
				$linha[$k]= $m;
			}
		}

		// Atribuindo o valor
		if(($f != "") && ($linha[$k] != "")){
			// ORACLE
			if(in_array($f, $data) !== false){
				$sqlData[$f]= "to_date('" . LimpaTexto(trim($linha[$k])) . "', 'YYYY-MM-DD')";
			}
			elseif(in_array($f, $hora) !== false){
				$sqlData[$f]= "to_date('" . LimpaTexto(trim($linha[$k])) . "', 'HH24:MI:SS')";
			}
			elseif ((is_numeric(LimpaTexto(trim($linha[$k]))) !== false) AND ($f != "NUMERO"))  {
				$sqlData[$f]= LimpaTexto(trim($linha[$k]));
			}
			else {
				$sqlData[$f]= "'" . LimpaTexto(trim($linha[$k])) . "'";
			}
		}
		elseif($linha[$k] != "") {
			$erro = 1;
		}
	}

	return $sqlData;
}



if ($_POST['enviar'] != "") {
	$campos_tmp = explode(",", $_POST['campos']);
	$camposD_tmp = explode(",", $_POST['campos_data']);
	$camposH_tmp = explode(",", $_POST['campos_hora']);
	$tabela     = strtolower($_POST['tabela']);
	$erro       = 0;

	// Verifica se a tabela existe
	if(isset($tdb[$tabela])){
		// Verifica se foram passados campos a serem salvos
		if(count($campos_tmp) > 0){
			$campos = array();
			foreach($campos_tmp as $c){
			    $c = trim($c);
				$cc = LimpaTexto(strtoupper($c));

				if((isset($tdb[$tabela][$cc])) || ($cc == "MID")){
					$campos[] = $cc;
				}
				else {
					erromsg("O campo $cc indicado n�o existe na tabela $tabela.");
					$erro = 1;
				}
			}

			foreach($camposD_tmp as $c)
			$camposD[] = LimpaTexto(strtoupper($c));

			foreach($camposH_tmp as $c)
			$camposH[] = LimpaTexto(strtoupper($c));

			if (is_uploaded_file($_FILES['csv']['tmp_name']) && $erro == 0) {

				// Recupera arquivo
				$handle= fopen($_FILES['csv']['tmp_name'], "r");

				// Montando o array com os dados do arquivo
				$sqlData= array ();
				$relData = array();
				while ($linha= fgetcsv($handle, 1000, ";")) {
					$sqlData[]= filtrar($linha, $tabela, $campos, $camposD, $camposH);
					$relData[]= $linha;
				}


				// Inserindo no banco de dados
				$i= 0;
				foreach ($sqlData as $data) {
					
					// Cabe�alho
					if ($i == 0) {
						echo ";;;;<br />\n";
						echo ";;;;<br />\n";
						echo "@;{$tabela};MID;" . implode(";", array_keys($data)) . "<br />\n";
					}
					
					
					$mid = GeraMid($tabela, "MID", 0);
					$tmp_mid = 0;
					// Verifica se o cadastro ja existe
					if ($data['MID'] != "") {
						$tmp_mid = (int) str_replace("'", "", $data['MID']);
						$mid = (int) str_replace("'", "", $data['MID']);
						unset($data['MID']);
					}
					elseif ($data['COD'] != "") {
						$tmp_mid = (int) VoltaValor($tabela, "MID", "COD", str_replace("'", "", $data['COD']), 0);
						if ($tmp_mid != 0){
							$mid = $tmp_mid;
						}
					}
					elseif ($data['TAG'] != "") {
						$tmp_mid = (int) VoltaValor($tabela, "MID", "TAG", str_replace("'", "", $data['TAG']), 0);
						if ($tmp_mid != 0){
							$mid = $tmp_mid;
						}
					}
					elseif ($data['DESCRICAO'] != "") {
						$tmp_mid = (int) VoltaValor($tabela, "MID", "DESCRICAO", str_replace("'", "", $data['DESCRICAO']), 0);
						if ($tmp_mid != 0){
							$mid = $tmp_mid;
						}
					}
					elseif ($data['NUMERO'] != "") {
						$tmp_mid = (int) VoltaValor($tabela, "MID", array("NUMERO", "ATIVO"), array(str_replace("'", "", $data['NUMERO']), str_replace("'", "", $data['ATIVO'])), 0);
						if ($tmp_mid != 0){
							$mid = $tmp_mid;
						}
					}


					$sql = "";
					// N�o encontrou nenhum cadastro
					if ($tmp_mid == 0){
						$sql= "INSERT INTO " . $tabela . " (MID, " . implode(", ", array_keys($data)) . ") VALUES ({$mid}, " . implode(", ", array_values($data)) . ")";
						$logTipo = 3;
						
						// Printando o INSERT
						echo "*;";
					}
					else {
						$cc = "";
						foreach ($data as $k => $v) {
							$cc .= ($cc != "")? ", " : "";
							$cc .= "$k = $v";
						}
						
						$sql = "UPDATE " . $tabela . " SET " . $cc . " WHERE MID = $mid";
						$logTipo = 4;
						
						// Printando o INSERT
						echo "#;";
					}

					if (!$dba[0]->Execute($sql)) {
						erromsg("File: cargas_geral.php Line: " . __LINE__ . "<br />" . $dba[0]->ErrorMsg() . "<br />Registro $i <br />SQL: " . $sql);
						exit ();
					}
					else {
						// Printando o INSERT
						echo "{$tabela};{$mid};\"" . implode("\";\"", array_values($relData[$i])) . "\"<br />\n";
						
						logar($logTipo, "FEITO EM CARGA:", $tabela, "MID", $mid);
						$i++;
					}
				}
				
				echo "<h3>Total de registros: $i</h3>";
			}
			elseif($erro == 0) {
				erromsg("Ocorreu um erro no Upload do arquivo.<br />Tente novamente.");
			}
		}
		else {
			erromsg("Indique os campos a serem salvos usando o campo 'Campos', separe os nomes por v�rgula.");
		}
	}
	else {
		erromsg("A tabela indicada n�o existe, indique uma tabela v�lida.");
	}
}
?>
<html>
<head>
<title>Cargas </title>
</head>
<body>
  <h1>Cargas </h1>
  <form action="cargas_geral.php" method="POST" enctype="multipart/form-data">
        <label for="tabela">Tabela:</label>
        <select id="tabela" class="campo_select" name="tabela" />
        <?php 
        foreach($tdb as $k => $tab)
        echo "<option value=\"$k\">" . $tab['DESC'] . "</option>";

        ?>
        </select>
        
        <br clear="all" />
        <label for="campos">Campos:</label>
        <input id="campos" class="campo_text" type="text" name="campos" size="65" value="<?php echo $_POST['campos'] ?>"/>
        <br clear="all" />
        <label for="file">Arquivo .csv:</label>
        <input id="csv" class="campo_text" type="file" name="csv" size="40"/>
        <fieldset><legend>Oracle</legend>
                <label for="campos_data">Campos data:</label>
                <input id="campos_data" class="campo_text" type="text" name="campos_data" size="65" value="<?php echo $_POST['campos_data'] ?>"/>
                <br clear="all" />
                <label for="campos_hora">Campos hora:</label>
                <input id="campos_hora" class="campo_text" type="text" name="campos_hora" size="65" value="<?php echo $_POST['campos_hora'] ?>"/>
                <br clear="all" />
        </fieldset>
    <br/>
    <br/>
    <input class="botao" type="submit" id="enviar" name="enviar" value="Iniciar carga"/>
  </form>
  
</body>
</html>
