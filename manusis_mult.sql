-- MySQL dump 10.11
--
-- Host: localhost    Database: suporte_v3_corrigido
-- ------------------------------------------------------
-- Server version	5.0.51a-24+lenny4-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `afastamentos`
--

DROP TABLE IF EXISTS `afastamentos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `afastamentos` (
  `MID_FUNCIONARIO` int(11) NOT NULL default '0',
  `DATA_INICIAL` date NOT NULL,
  `DATA_FINAL` date NOT NULL,
  `MOTIVO` varchar(255) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_FUNCIONARIO` (`MID_FUNCIONARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `afastamentos`
--

LOCK TABLES `afastamentos` WRITE;
/*!40000 ALTER TABLE `afastamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `afastamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almoxarifado`
--

DROP TABLE IF EXISTS `almoxarifado`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `almoxarifado` (
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL,
  `DESCRICAO` varchar(200) NOT NULL,
  `COD` varchar(50) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `COD` (`COD`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `almoxarifado`
--

LOCK TABLES `almoxarifado` WRITE;
/*!40000 ALTER TABLE `almoxarifado` DISABLE KEYS */;
/*!40000 ALTER TABLE `almoxarifado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `areas` (
  `COD` varchar(30) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(120) collate latin1_general_ci default NULL,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividades`
--

DROP TABLE IF EXISTS `atividades`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `atividades` (
  `MID_PLANO_PADRAO` int(11) NOT NULL default '0',
  `PERIODICIDADE` int(1) NOT NULL default '1',
  `FREQUENCIA` int(4) NOT NULL default '0',
  `CONTADOR` int(4) NOT NULL default '0',
  `DISPARO` decimal(13,2) NOT NULL default '0.00',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `QUANTIDADE` decimal(11,3) NOT NULL default '0.000',
  `NUMERO` varchar(50) NOT NULL default '0',
  `TAREFA` varchar(250) NOT NULL default '',
  `PARTE` varchar(120) default NULL,
  `INSTRUCAO_DE_TRABALHO` varchar(255) default NULL,
  `TEMPO_PREVISTO` decimal(12,2) NOT NULL default '0.00',
  `OBSERVACAO` varchar(255) default NULL,
  `ESPECIALIDADE` int(11) NOT NULL default '0',
  `MAQUINA_PARADA` int(1) NOT NULL default '1',
  `STATUS` int(1) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `QUANTIDADE_MO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `atividades`
--

LOCK TABLES `atividades` WRITE;
/*!40000 ALTER TABLE `atividades` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ausencia`
--

DROP TABLE IF EXISTS `ausencia`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ausencia` (
  `MID_FUNCIONARIO` int(11) NOT NULL default '0',
  `HORA_INICIAL` time NOT NULL,
  `HORA_FINAL` time NOT NULL,
  `DATA` date NOT NULL,
  `MOTIVO` varchar(255) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  UNIQUE KEY `MID` (`MID`),
  KEY `MID_FUNCIONARIO` (`MID_FUNCIONARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ausencia`
--

LOCK TABLES `ausencia` WRITE;
/*!40000 ALTER TABLE `ausencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `ausencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargas`
--

DROP TABLE IF EXISTS `cargas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cargas` (
  `MID` int(11) NOT NULL,
  `DATA_HORA` datetime NOT NULL,
  `BASE_ORIGEM` varchar(255) NOT NULL,
  `ATUALIZADO` int(11) NOT NULL default '2',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cargas`
--

LOCK TABLES `cargas` WRITE;
/*!40000 ALTER TABLE `cargas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `causa`
--

DROP TABLE IF EXISTS `causa`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `causa` (
  `DESCRICAO` varchar(200) collate latin1_general_ci NOT NULL default '',
  `TEXTO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `causa`
--

LOCK TABLES `causa` WRITE;
/*!40000 ALTER TABLE `causa` DISABLE KEYS */;
/*!40000 ALTER TABLE `causa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `centro_de_custo`
--

DROP TABLE IF EXISTS `centro_de_custo`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `centro_de_custo` (
  `PAI` int(11) NOT NULL default '0',
  `COD` varchar(25) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(125) collate latin1_general_ci NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  UNIQUE KEY `MID` (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `centro_de_custo`
--

LOCK TABLES `centro_de_custo` WRITE;
/*!40000 ALTER TABLE `centro_de_custo` DISABLE KEYS */;
/*!40000 ALTER TABLE `centro_de_custo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contador`
--

DROP TABLE IF EXISTS `contador`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `contador` (
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  `MID_CONTADOR` int(11) default NULL,
  `DATA` date default NULL,
  `VALOR` int(11) default NULL,
  PRIMARY KEY  (`MID`),
  KEY `MID_CONTADOR` (`MID_CONTADOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `contador`
--

LOCK TABLES `contador` WRITE;
/*!40000 ALTER TABLE `contador` DISABLE KEYS */;
/*!40000 ALTER TABLE `contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controle_contador`
--

DROP TABLE IF EXISTS `controle_contador`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `controle_contador` (
  `MID_CONTADOR` int(6) NOT NULL default '0',
  `MID_LANCAMENTO` int(11) NOT NULL default '0',
  `TIPO` int(11) NOT NULL default '0',
  `MID_PROGRAMACAO` int(11) NOT NULL default '0',
  `MID_ATV` int(11) NOT NULL default '0',
  `DATA` date NOT NULL,
  `VALOR` decimal(13,2) NOT NULL default '0.00',
  `DISPAROS` int(11) NOT NULL default '0',
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`),
  KEY `MID_CONTADOR` (`MID_CONTADOR`),
  KEY `MID_PROGRAMACAO` (`MID_PROGRAMACAO`),
  KEY `MID_ATV` (`MID_ATV`),
  KEY `DISPAROS` (`DISPAROS`),
  KEY `MID_LANCAMENTO` (`MID_LANCAMENTO`),
  KEY `DATA` (`DATA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `controle_contador`
--

LOCK TABLES `controle_contador` WRITE;
/*!40000 ALTER TABLE `controle_contador` DISABLE KEYS */;
/*!40000 ALTER TABLE `controle_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defeito`
--

DROP TABLE IF EXISTS `defeito`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `defeito` (
  `DESCRICAO` varchar(200) collate latin1_general_ci NOT NULL default '',
  `TEXTO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `defeito`
--

LOCK TABLES `defeito` WRITE;
/*!40000 ALTER TABLE `defeito` DISABLE KEYS */;
/*!40000 ALTER TABLE `defeito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `empresas` (
  `COD` int(2) unsigned zerofill NOT NULL auto_increment,
  `NOME` varchar(60) collate latin1_general_ci NOT NULL default '',
  `FANTASIA` varchar(60) collate latin1_general_ci default NULL,
  `CNPJ` varchar(14) collate latin1_general_ci NOT NULL default '',
  `IE` varchar(15) collate latin1_general_ci default NULL,
  `ENDERECO` varchar(55) collate latin1_general_ci default NULL,
  `NUMERO` int(11) default NULL,
  `COMPLEMENTO` varchar(25) collate latin1_general_ci default NULL,
  `BAIRRO` varchar(32) collate latin1_general_ci default NULL,
  `CEP` varchar(10) collate latin1_general_ci default NULL,
  `CIDADE` varchar(32) collate latin1_general_ci default NULL,
  `UF` char(2) collate latin1_general_ci default NULL,
  `PAIS` varchar(32) collate latin1_general_ci default NULL,
  `TELEFONE_1` varchar(25) collate latin1_general_ci default NULL,
  `TELEFONE_2` varchar(25) collate latin1_general_ci default NULL,
  `FAX` varchar(25) collate latin1_general_ci default NULL,
  `OBSERVACAO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `EMP_MATRIZ` int(11) NOT NULL default '2',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  UNIQUE KEY `COD` (`COD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamentos`
--

DROP TABLE IF EXISTS `equipamentos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipamentos` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `COD` varchar(25) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(255) collate latin1_general_ci default NULL,
  `FAMILIA` int(11) default '0',
  `FABRICANTE` int(11) default NULL,
  `FORNECEDOR` int(11) default NULL,
  `FICHA_TECNICA` text collate latin1_general_ci,
  `MARCA` varchar(50) collate latin1_general_ci default NULL,
  `MODELO` varchar(50) collate latin1_general_ci default NULL,
  `NSERIE` varchar(50) collate latin1_general_ci default NULL,
  `POTENCIA` varchar(50) collate latin1_general_ci default NULL,
  `PRESSAO` varchar(50) collate latin1_general_ci default NULL,
  `CORRENTE` varchar(50) collate latin1_general_ci default NULL,
  `VAZAO` varchar(50) collate latin1_general_ci default NULL,
  `TENSAO` varchar(50) collate latin1_general_ci default NULL,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_STATUS` int(11) NOT NULL default '1',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_CONJUNTO`,`FAMILIA`,`MID_EMPRESA`,`MID_STATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `equipamentos`
--

LOCK TABLES `equipamentos` WRITE;
/*!40000 ALTER TABLE `equipamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamentos_familia`
--

DROP TABLE IF EXISTS `equipamentos_familia`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipamentos_familia` (
  `COD` varchar(25) collate latin1_general_ci default NULL,
  `DESCRICAO` varchar(120) collate latin1_general_ci default NULL,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `equipamentos_familia`
--

LOCK TABLES `equipamentos_familia` WRITE;
/*!40000 ALTER TABLE `equipamentos_familia` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipamentos_familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamentos_material`
--

DROP TABLE IF EXISTS `equipamentos_material`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipamentos_material` (
  `MID_MATERIAL` int(11) default NULL,
  `QUANTIDADE` decimal(11,2) default NULL,
  `MID_EQUIPAMENTO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_CLASSE` int(11) default NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MATERIAL` (`MID_MATERIAL`,`MID_EQUIPAMENTO`,`MID_CLASSE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `equipamentos_material`
--

LOCK TABLES `equipamentos_material` WRITE;
/*!40000 ALTER TABLE `equipamentos_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipamentos_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamentos_status`
--

DROP TABLE IF EXISTS `equipamentos_status`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipamentos_status` (
  `MID` int(11) NOT NULL,
  `DESCRICAO` varchar(100) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `equipamentos_status`
--

LOCK TABLES `equipamentos_status` WRITE;
/*!40000 ALTER TABLE `equipamentos_status` DISABLE KEYS */;
INSERT INTO `equipamentos_status` VALUES (1,'ATIVO',0,0),(2,'INATIVO',0,0);
/*!40000 ALTER TABLE `equipamentos_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipes`
--

DROP TABLE IF EXISTS `equipes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipes` (
  `DESCRICAO` varchar(100) collate latin1_general_ci NOT NULL default '',
  `TIPO` varchar(50) collate latin1_general_ci default NULL,
  `FORNECEDOR` int(4) default NULL,
  `OBSERVACAO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `FORNECEDOR` (`FORNECEDOR`,`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `equipes`
--

LOCK TABLES `equipes` WRITE;
/*!40000 ALTER TABLE `equipes` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidades`
--

DROP TABLE IF EXISTS `especialidades`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `especialidades` (
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  UNIQUE KEY `MID` (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `especialidades`
--

LOCK TABLES `especialidades` WRITE;
/*!40000 ALTER TABLE `especialidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `especialidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque_entrada`
--

DROP TABLE IF EXISTS `estoque_entrada`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `estoque_entrada` (
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `CENTRO_DE_CUSTO` int(11) NOT NULL default '0',
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `MOTIVO` varchar(255) collate latin1_general_ci default NULL,
  `DOCUMENTO` varchar(150) collate latin1_general_ci default NULL,
  `QUANTIDADE` decimal(12,2) NOT NULL default '0.00',
  `VALOR_UNITARIO` decimal(13,2) NOT NULL default '0.00',
  `VALOR_TOTAL` decimal(13,2) NOT NULL default '0.00',
  `OBSERVACAO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_ALMOXARIFADO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  UNIQUE KEY `MID` (`MID`),
  KEY `MID_MATERIAL` (`MID_MATERIAL`,`MID_ALMOXARIFADO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `estoque_entrada`
--

LOCK TABLES `estoque_entrada` WRITE;
/*!40000 ALTER TABLE `estoque_entrada` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque_entrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque_saida`
--

DROP TABLE IF EXISTS `estoque_saida`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `estoque_saida` (
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `CENTRO_DE_CUSTO` int(11) NOT NULL default '0',
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `MOTIVO` varchar(255) collate latin1_general_ci default NULL,
  `QUANTIDADE` decimal(12,2) NOT NULL default '0.00',
  `ORDEM` varchar(120) collate latin1_general_ci default NULL,
  `OBSERVACAO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_ALMOXARIFADO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MATERIAL` (`MID_MATERIAL`,`MID_ALMOXARIFADO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `estoque_saida`
--

LOCK TABLES `estoque_saida` WRITE;
/*!40000 ALTER TABLE `estoque_saida` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque_saida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabricantes`
--

DROP TABLE IF EXISTS `fabricantes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `fabricantes` (
  `NOME` varchar(150) collate latin1_general_ci default NULL,
  `CNPJ` varchar(50) collate latin1_general_ci default NULL,
  `IE` varchar(50) collate latin1_general_ci default NULL,
  `ENDERECO` varchar(50) collate latin1_general_ci default NULL,
  `BAIRRO` varchar(120) collate latin1_general_ci default NULL,
  `CIDADE` varchar(100) collate latin1_general_ci default NULL,
  `ESTADO` varchar(120) collate latin1_general_ci default NULL,
  `PAIS` varchar(200) collate latin1_general_ci default NULL,
  `CEP` varchar(10) collate latin1_general_ci default NULL,
  `TELEFONE` varchar(16) collate latin1_general_ci default NULL,
  `FAX` varchar(16) collate latin1_general_ci default NULL,
  `SITE` varchar(120) collate latin1_general_ci default NULL,
  `EMAIL` varchar(150) collate latin1_general_ci default NULL,
  `OBSERVACAO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `fabricantes`
--

LOCK TABLES `fabricantes` WRITE;
/*!40000 ALTER TABLE `fabricantes` DISABLE KEYS */;
/*!40000 ALTER TABLE `fabricantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feriados`
--

DROP TABLE IF EXISTS `feriados`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `feriados` (
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `DATA` date NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `feriados`
--

LOCK TABLES `feriados` WRITE;
/*!40000 ALTER TABLE `feriados` DISABLE KEYS */;
/*!40000 ALTER TABLE `feriados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `fornecedores` (
  `MID` int(11) NOT NULL,
  `NOME` varchar(255) collate latin1_general_ci default NULL,
  `FANTASIA` varchar(255) collate latin1_general_ci default NULL,
  `ENDERECO` varchar(255) collate latin1_general_ci default NULL,
  `NUMERO` varchar(255) collate latin1_general_ci default NULL,
  `COMPLEMENTO` varchar(255) collate latin1_general_ci default NULL,
  `BAIRRO` varchar(255) collate latin1_general_ci default NULL,
  `CIDADE` varchar(255) collate latin1_general_ci default NULL,
  `UF` varchar(255) collate latin1_general_ci default NULL,
  `CEP` varchar(255) collate latin1_general_ci default NULL,
  `CNPJ` varchar(255) collate latin1_general_ci default NULL,
  `CGC` varchar(255) collate latin1_general_ci default NULL,
  `TELEFONE_1` varchar(255) collate latin1_general_ci default NULL,
  `TELEFONE_2` varchar(255) collate latin1_general_ci default NULL,
  `TELEFONE_3` varchar(255) collate latin1_general_ci default NULL,
  `E_MAIL` varchar(255) collate latin1_general_ci default NULL,
  `SITE` varchar(255) collate latin1_general_ci default NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  `OBSERVACAO` text collate latin1_general_ci,
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `fornecedores`
--

LOCK TABLES `fornecedores` WRITE;
/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionarios`
--

DROP TABLE IF EXISTS `funcionarios`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `funcionarios` (
  `NOME` varchar(120) collate latin1_general_ci NOT NULL default '',
  `FORNECEDOR` int(11) NOT NULL default '0',
  `RG` varchar(16) collate latin1_general_ci default NULL,
  `CPF` varchar(16) collate latin1_general_ci default NULL,
  `DATA_NASCIMENTO` date default NULL,
  `MATRICULA` varchar(50) collate latin1_general_ci default NULL,
  `ESPECIALIDADE` int(11) NOT NULL default '0',
  `NIVEL` varchar(50) collate latin1_general_ci default NULL,
  `EQUIPE` int(11) NOT NULL default '0',
  `TURNO` int(11) NOT NULL default '0',
  `CENTRO_DE_CUSTO` int(11) NOT NULL default '0',
  `VALOR_HORA` decimal(13,2) default NULL,
  `ENDERECO` varchar(150) collate latin1_general_ci default NULL,
  `BAIRRO` varchar(100) collate latin1_general_ci default NULL,
  `CEP` varchar(12) collate latin1_general_ci default NULL,
  `CIDADE` varchar(100) collate latin1_general_ci default NULL,
  `UF` char(2) collate latin1_general_ci default NULL,
  `TELEFONE` varchar(20) collate latin1_general_ci default NULL,
  `CELULAR` varchar(20) collate latin1_general_ci default NULL,
  `EMAIL` varchar(150) collate latin1_general_ci default NULL,
  `SITUACAO` int(11) default NULL,
  `DATA_ADMISSAO` date default NULL,
  `DATA_DEMISSAO` date default NULL,
  `OBSERVACAO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `FORNECEDOR` (`FORNECEDOR`,`ESPECIALIDADE`,`EQUIPE`,`TURNO`,`CENTRO_DE_CUSTO`,`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `funcionarios`
--

LOCK TABLES `funcionarios` WRITE;
/*!40000 ALTER TABLE `funcionarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `funcionarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionarios_status`
--

DROP TABLE IF EXISTS `funcionarios_status`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `funcionarios_status` (
  `DESCRICAO` varchar(20) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `funcionarios_status`
--

LOCK TABLES `funcionarios_status` WRITE;
/*!40000 ALTER TABLE `funcionarios_status` DISABLE KEYS */;
INSERT INTO `funcionarios_status` VALUES ('LIGADO',1,0,0),('DESLIGADO',2,0,0);
/*!40000 ALTER TABLE `funcionarios_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_grafico`
--

DROP TABLE IF EXISTS `grupo_grafico`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `grupo_grafico` (
  `MID` int(11) NOT NULL,
  `DESCRICAO` varchar(200) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `grupo_grafico`
--

LOCK TABLES `grupo_grafico` WRITE;
/*!40000 ALTER TABLE `grupo_grafico` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo_grafico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integracao_esquemas`
--

DROP TABLE IF EXISTS `integracao_esquemas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `integracao_esquemas` (
  `TIPO` int(1) NOT NULL,
  `ESQUEMA` varchar(64) NOT NULL,
  `DATA_INICIO` datetime NOT NULL,
  `ULTIMO_EVENTO` datetime NOT NULL,
  `STATUS` int(1) NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `TIPO` (`TIPO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `integracao_esquemas`
--

LOCK TABLES `integracao_esquemas` WRITE;
/*!40000 ALTER TABLE `integracao_esquemas` DISABLE KEYS */;
/*!40000 ALTER TABLE `integracao_esquemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integracao_tipos`
--

DROP TABLE IF EXISTS `integracao_tipos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `integracao_tipos` (
  `DESCRICAO` varchar(64) NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `integracao_tipos`
--

LOCK TABLES `integracao_tipos` WRITE;
/*!40000 ALTER TABLE `integracao_tipos` DISABLE KEYS */;
INSERT INTO `integracao_tipos` VALUES ('IMPORTAR',1,0,0),('EXPORTAR',2,0,0);
/*!40000 ALTER TABLE `integracao_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integrador`
--

DROP TABLE IF EXISTS `integrador`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `integrador` (
  `TIPO` int(11) NOT NULL,
  `ESQUEMA` text NOT NULL,
  `ESQUEMA_ARQUIVO` varchar(64) NOT NULL,
  `ULTIMO_EVENTO` datetime default NULL,
  `CSV` text,
  `RESULTADO` int(11) default NULL,
  `OBS` text,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `TIPO` (`TIPO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `integrador`
--

LOCK TABLES `integrador` WRITE;
/*!40000 ALTER TABLE `integrador` DISABLE KEYS */;
/*!40000 ALTER TABLE `integrador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lanca_contador`
--

DROP TABLE IF EXISTS `lanca_contador`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `lanca_contador` (
  `MID_MAQUINA` int(11) NOT NULL,
  `MID_CONTADOR` int(11) default '0',
  `DATA` date default NULL,
  `VALOR` decimal(13,2) default '0.00',
  `MID_USUARIO` int(6) NOT NULL default '0',
  `STATUS` int(2) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_CONTADOR`,`MID_USUARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `lanca_contador`
--

LOCK TABLES `lanca_contador` WRITE;
/*!40000 ALTER TABLE `lanca_contador` DISABLE KEYS */;
/*!40000 ALTER TABLE `lanca_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lanca_preditiva`
--

DROP TABLE IF EXISTS `lanca_preditiva`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `lanca_preditiva` (
  `MID_PONTO` int(11) NOT NULL default '0',
  `DATA` datetime NOT NULL,
  `VALOR` decimal(12,2) NOT NULL default '0.00',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `OBSERVACAO` text collate latin1_general_ci,
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_PONTO` (`MID_PONTO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `lanca_preditiva`
--

LOCK TABLES `lanca_preditiva` WRITE;
/*!40000 ALTER TABLE `lanca_preditiva` DISABLE KEYS */;
/*!40000 ALTER TABLE `lanca_preditiva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link_rotas`
--

DROP TABLE IF EXISTS `link_rotas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `link_rotas` (
  `TIPO` int(1) NOT NULL default '1',
  `MID_PLANO` int(11) NOT NULL default '0',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_PONTO` int(11) NOT NULL default '0',
  `NUMERO` varchar(50) NOT NULL default '0',
  `TAREFA` varchar(150) default NULL,
  `TEMPO_PREVISTO` decimal(12,2) default NULL,
  `MID_MATERIAL` varchar(25) default NULL,
  `QUANTIDADE` decimal(13,2) default NULL,
  `ESPECIALIDADE` int(4) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `PERIODICIDADE` int(1) NOT NULL default '1',
  `FREQUENCIA` int(4) NOT NULL default '0',
  `CONTADOR` int(6) NOT NULL default '0',
  `DISPARO` decimal(13,2) NOT NULL default '0.00',
  `STATUS` int(1) NOT NULL default '0',
  `QUANTIDADE_MO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `TIPO` (`TIPO`,`MID_PLANO`,`MID_MAQUINA`,`MID_PONTO`,`MID_MATERIAL`,`MID_CONJUNTO`,`PERIODICIDADE`,`STATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `link_rotas`
--

LOCK TABLES `link_rotas` WRITE;
/*!40000 ALTER TABLE `link_rotas` DISABLE KEYS */;
/*!40000 ALTER TABLE `link_rotas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `logs` (
  `USUARIO` varchar(75) NOT NULL default '0',
  `IP` varchar(50) NOT NULL default '',
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `TIPO` int(11) NOT NULL default '0',
  `TEXTO` text NOT NULL,
  `TABELA` varchar(120) NOT NULL default '',
  `MID` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs_tipos`
--

DROP TABLE IF EXISTS `logs_tipos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `logs_tipos` (
  `DESCRICAO` varchar(50) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `logs_tipos`
--

LOCK TABLES `logs_tipos` WRITE;
/*!40000 ALTER TABLE `logs_tipos` DISABLE KEYS */;
INSERT INTO `logs_tipos` VALUES ('LOGON',1,0,0),('LOGOFF',2,0,0),('INCLUIU',3,0,0),('ALTEROU',4,0,0),('EXCLUIU',5,0,0);
/*!40000 ALTER TABLE `logs_tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquina_parada`
--

DROP TABLE IF EXISTS `maquina_parada`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquina_parada` (
  `DESCRICAO` varchar(150) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquina_parada`
--

LOCK TABLES `maquina_parada` WRITE;
/*!40000 ALTER TABLE `maquina_parada` DISABLE KEYS */;
INSERT INTO `maquina_parada` VALUES ('SIM',1,0,0),('N�O',2,0,0);
/*!40000 ALTER TABLE `maquina_parada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas`
--

DROP TABLE IF EXISTS `maquinas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas` (
  `COD` varchar(30) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `MODELO` varchar(50) collate latin1_general_ci default NULL,
  `NUMERO_DE_SERIE` text collate latin1_general_ci,
  `ANO_DE_FABRICACAO` date default NULL,
  `NUMERO_DO_PATRIMONIO` varchar(50) collate latin1_general_ci default NULL,
  `GARANTIA` date default NULL,
  `DADOS_TECNICO` text collate latin1_general_ci,
  `LOCALIZACAO_FISICA` varchar(125) collate latin1_general_ci default NULL,
  `FAMILIA` int(11) default NULL,
  `CENTRO_DE_CUSTO` int(11) default NULL,
  `CLASSE` int(11) default NULL,
  `STATUS` int(11) default NULL,
  `FABRICANTE` int(11) default NULL,
  `FORNECEDOR` int(11) default NULL,
  `MID_SETOR` int(11) NOT NULL default '0',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `COD` (`COD`),
  KEY `MID_SETOR` (`MID_SETOR`),
  KEY `STATUS` (`STATUS`),
  KEY `CENTRO_DE_CUSTO` (`CENTRO_DE_CUSTO`),
  KEY `FAMILIA` (`FAMILIA`),
  KEY `FAMILIA_2` (`FAMILIA`,`CENTRO_DE_CUSTO`,`CLASSE`,`STATUS`,`FABRICANTE`,`FORNECEDOR`,`MID_SETOR`,`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas`
--

LOCK TABLES `maquinas` WRITE;
/*!40000 ALTER TABLE `maquinas` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_classe`
--

DROP TABLE IF EXISTS `maquinas_classe`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_classe` (
  `DESCRICAO` varchar(25) collate latin1_general_ci default NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_classe`
--

LOCK TABLES `maquinas_classe` WRITE;
/*!40000 ALTER TABLE `maquinas_classe` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_conjunto`
--

DROP TABLE IF EXISTS `maquinas_conjunto`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_conjunto` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `TAG` varchar(25) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(250) collate latin1_general_ci NOT NULL,
  `COMPLEMENTO` varchar(120) collate latin1_general_ci default NULL,
  `FABRICANTE` int(11) default NULL,
  `FICHA_TECNICA` text collate latin1_general_ci,
  `MARCA` varchar(50) collate latin1_general_ci default NULL,
  `MODELO` varchar(50) collate latin1_general_ci default NULL,
  `NSERIE` varchar(50) collate latin1_general_ci default NULL,
  `POTENCIA` varchar(50) collate latin1_general_ci default NULL,
  `PRESSAO` varchar(50) collate latin1_general_ci default NULL,
  `CORRENTE` varchar(50) collate latin1_general_ci default NULL,
  `VAZAO` varchar(50) collate latin1_general_ci default NULL,
  `TENSAO` varchar(50) collate latin1_general_ci default NULL,
  `MID_CONJUNTO` int(6) default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_CONJUNTO` (`MID_CONJUNTO`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`),
  KEY `FABRICANTE` (`FABRICANTE`),
  KEY `MID_MAQUINA_2` (`MID_MAQUINA`,`FABRICANTE`,`MID_CONJUNTO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_conjunto`
--

LOCK TABLES `maquinas_conjunto` WRITE;
/*!40000 ALTER TABLE `maquinas_conjunto` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_conjunto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_conjunto_material`
--

DROP TABLE IF EXISTS `maquinas_conjunto_material`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_conjunto_material` (
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `QUANTIDADE` decimal(11,2) default NULL,
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_CLASSE` int(11) default NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MATERIAL` (`MID_MATERIAL`,`MID_MAQUINA`,`MID_CONJUNTO`,`MID_CLASSE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_conjunto_material`
--

LOCK TABLES `maquinas_conjunto_material` WRITE;
/*!40000 ALTER TABLE `maquinas_conjunto_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_conjunto_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_contador`
--

DROP TABLE IF EXISTS `maquinas_contador`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_contador` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `DESCRICAO` varchar(60) collate latin1_general_ci NOT NULL default '',
  `TIPO` int(11) NOT NULL default '0',
  `ZERA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `ZERA` (`ZERA`),
  KEY `TIPO` (`TIPO`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_contador`
--

LOCK TABLES `maquinas_contador` WRITE;
/*!40000 ALTER TABLE `maquinas_contador` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_contador_tipo`
--

DROP TABLE IF EXISTS `maquinas_contador_tipo`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_contador_tipo` (
  `DESCRICAO` varchar(60) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_contador_tipo`
--

LOCK TABLES `maquinas_contador_tipo` WRITE;
/*!40000 ALTER TABLE `maquinas_contador_tipo` DISABLE KEYS */;
INSERT INTO `maquinas_contador_tipo` VALUES ('SEQUENCIAL',1,0,0),('INCREMENTAL',2,0,0);
/*!40000 ALTER TABLE `maquinas_contador_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_disponibilidade`
--

DROP TABLE IF EXISTS `maquinas_disponibilidade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_disponibilidade` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MES` int(2) NOT NULL default '0',
  `ANO` int(4) NOT NULL default '0',
  `HORAS` decimal(11,2) NOT NULL default '0.00',
  `MID` int(11) NOT NULL,
  `SEMANA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_disponibilidade`
--

LOCK TABLES `maquinas_disponibilidade` WRITE;
/*!40000 ALTER TABLE `maquinas_disponibilidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_disponibilidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_familia`
--

DROP TABLE IF EXISTS `maquinas_familia`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_familia` (
  `COD` varchar(25) default NULL,
  `DESCRICAO` varchar(120) default NULL,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`),
  KEY `COD` (`COD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_familia`
--

LOCK TABLES `maquinas_familia` WRITE;
/*!40000 ALTER TABLE `maquinas_familia` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_fornecedor`
--

DROP TABLE IF EXISTS `maquinas_fornecedor`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_fornecedor` (
  `MID` int(11) NOT NULL,
  `MID_MAQUINA` int(11) NOT NULL,
  `MID_FORNECEDOR` int(11) NOT NULL default '0',
  `OBS` text,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_FORNECEDOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_fornecedor`
--

LOCK TABLES `maquinas_fornecedor` WRITE;
/*!40000 ALTER TABLE `maquinas_fornecedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `maquinas_fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas_status`
--

DROP TABLE IF EXISTS `maquinas_status`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `maquinas_status` (
  `DESCRICAO` varchar(20) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `maquinas_status`
--

LOCK TABLES `maquinas_status` WRITE;
/*!40000 ALTER TABLE `maquinas_status` DISABLE KEYS */;
INSERT INTO `maquinas_status` VALUES ('INATIVO',0,0,0),('ATIVO',1,0,0);
/*!40000 ALTER TABLE `maquinas_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais`
--

DROP TABLE IF EXISTS `materiais`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais` (
  `COD` varchar(35) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `COMPLEMENTO` varchar(120) collate latin1_general_ci default NULL,
  `UNIDADE` int(11) NOT NULL default '0',
  `FAMILIA` int(11) NOT NULL default '0',
  `MID_SUBFAMILIA` int(11) NOT NULL default '0',
  `LOCALIZACAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `ESTOQUE_MINIMO` decimal(13,2) default '0.00',
  `ESTOQUE_MAXIMO` decimal(13,2) default '0.00',
  `ESTOQUE_ATUAL` decimal(13,2) default '0.00',
  `CUSTO_UNITARIO` double(11,2) NOT NULL default '0.00',
  `FABRICANTE` int(11) NOT NULL default '0',
  `FORNECEDOR` int(11) NOT NULL default '0',
  `OBSERVACAO` text collate latin1_general_ci,
  `CLASSE` varchar(200) collate latin1_general_ci default NULL,
  `MID` int(11) NOT NULL,
  `MID_CRITICIDADE` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `UNIDADE` (`UNIDADE`),
  KEY `FAMILIA` (`FAMILIA`),
  KEY `MID_SUBFAMILIA` (`MID_SUBFAMILIA`,`FABRICANTE`,`FORNECEDOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais`
--

LOCK TABLES `materiais` WRITE;
/*!40000 ALTER TABLE `materiais` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais_almoxarifado`
--

DROP TABLE IF EXISTS `materiais_almoxarifado`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais_almoxarifado` (
  `MID` int(11) NOT NULL auto_increment,
  `MID_MATERIAL` int(11) NOT NULL,
  `MID_ALMOXARIFADO` int(11) NOT NULL,
  `ESTOQUE_ATUAL` float(14,2) NOT NULL,
  `CUSTO_UNITARIO` float(14,2) NOT NULL default '0.00',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MATERIAL` (`MID_MATERIAL`),
  KEY `MID_ALMOXARIFADO` (`MID_ALMOXARIFADO`),
  KEY `MID_MATERIAL_2` (`MID_MATERIAL`,`MID_ALMOXARIFADO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais_almoxarifado`
--

LOCK TABLES `materiais_almoxarifado` WRITE;
/*!40000 ALTER TABLE `materiais_almoxarifado` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais_almoxarifado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais_classe`
--

DROP TABLE IF EXISTS `materiais_classe`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais_classe` (
  `MID` int(11) NOT NULL,
  `DESCRICAO` varchar(50) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais_classe`
--

LOCK TABLES `materiais_classe` WRITE;
/*!40000 ALTER TABLE `materiais_classe` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais_classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais_criticidade`
--

DROP TABLE IF EXISTS `materiais_criticidade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais_criticidade` (
  `DESCRICAO` varchar(64) NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais_criticidade`
--

LOCK TABLES `materiais_criticidade` WRITE;
/*!40000 ALTER TABLE `materiais_criticidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais_criticidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais_familia`
--

DROP TABLE IF EXISTS `materiais_familia`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais_familia` (
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `COD` varchar(120) collate latin1_general_ci NOT NULL default '',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais_familia`
--

LOCK TABLES `materiais_familia` WRITE;
/*!40000 ALTER TABLE `materiais_familia` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais_familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais_subfamilia`
--

DROP TABLE IF EXISTS `materiais_subfamilia`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais_subfamilia` (
  `COD` varchar(25) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `MID_FAMILIA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_FAMILIA` (`MID_FAMILIA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais_subfamilia`
--

LOCK TABLES `materiais_subfamilia` WRITE;
/*!40000 ALTER TABLE `materiais_subfamilia` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais_subfamilia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiais_unidade`
--

DROP TABLE IF EXISTS `materiais_unidade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `materiais_unidade` (
  `DESCRICAO` varchar(45) collate latin1_general_ci NOT NULL default '',
  `COD` varchar(50) collate latin1_general_ci default NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `materiais_unidade`
--

LOCK TABLES `materiais_unidade` WRITE;
/*!40000 ALTER TABLE `materiais_unidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiais_unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mov_equipamento`
--

DROP TABLE IF EXISTS `mov_equipamento`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `mov_equipamento` (
  `MID_EQUIPAMENTO` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_DCONJUNTO` int(11) NOT NULL default '0',
  `MID_DMAQUINA` int(11) NOT NULL default '0',
  `MOTIVO` varchar(255) default NULL,
  `DATA` date NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EQUIPAMENTO` (`MID_EQUIPAMENTO`,`MID_CONJUNTO`,`MID_MAQUINA`,`MID_DCONJUNTO`,`MID_DMAQUINA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `mov_equipamento`
--

LOCK TABLES `mov_equipamento` WRITE;
/*!40000 ALTER TABLE `mov_equipamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `mov_equipamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mov_maquina`
--

DROP TABLE IF EXISTS `mov_maquina`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `mov_maquina` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_SETOR` int(11) NOT NULL default '0',
  `MID_DSETOR` int(11) NOT NULL default '0',
  `DATA` date NOT NULL,
  `MOTIVO` varchar(255) default NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_SETOR`,`MID_DSETOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `mov_maquina`
--

LOCK TABLES `mov_maquina` WRITE;
/*!40000 ALTER TABLE `mov_maquina` DISABLE KEYS */;
/*!40000 ALTER TABLE `mov_maquina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `natureza_servicos`
--

DROP TABLE IF EXISTS `natureza_servicos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `natureza_servicos` (
  `DESCRICAO` varchar(200) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `natureza_servicos`
--

LOCK TABLES `natureza_servicos` WRITE;
/*!40000 ALTER TABLE `natureza_servicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `natureza_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem`
--

DROP TABLE IF EXISTS `ordem`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem` (
  `MID_USUARIO` int(11) default NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_AREA` int(3) default NULL,
  `MID_SETOR` int(3) default NULL,
  `MID_MAQUINA` int(11) default NULL,
  `MID_CONJUNTO` int(11) default NULL,
  `CENTRO_DE_CUSTO` int(3) default NULL,
  `MID_EQUIPAMENTO` int(11) NOT NULL default '0',
  `MID_FORNECEDOR` int(11) NOT NULL default '0',
  `MID_CLASSE` int(11) NOT NULL default '0',
  `MID_FABRICANTE` int(11) NOT NULL default '0',
  `MID_MAQUINA_FAMILIA` int(11) NOT NULL default '0',
  `SOLICITANTE` varchar(120) default NULL,
  `TIPO` int(11) default NULL,
  `MID_PROGRAMACAO` int(11) default '0',
  `NUMERO` varchar(10) NOT NULL default '00-000000',
  `RESPONSAVEL` int(11) default NULL,
  `TEXTO` text,
  `TIPO_SERVICO` int(11) default NULL,
  `NATUREZA` int(11) default NULL,
  `STATUS` int(11) default '1',
  `DATA_PROG` date default NULL,
  `DATA_PROG_ORIGINAL` date default NULL,
  `HORA_ABRE` time NOT NULL,
  `DATA_ABRE` date default NULL,
  `DATA_INICIO` date default NULL,
  `HORA_INICIO` time default NULL,
  `DATA_FINAL` date default NULL,
  `HORA_FINAL` time default NULL,
  `TEMPO_TOTAL` decimal(13,2) default '0.00',
  `CAUSA` int(11) default NULL,
  `CAUSA_TEXTO` text,
  `DEFEITO` int(11) default NULL,
  `DEFEITO_TEXTO` text,
  `SOLUCAO` int(11) default NULL,
  `SOLUCAO_TEXTO` text,
  `MID` int(11) NOT NULL,
  `MID_SOLICITACAO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  UNIQUE KEY `NUMERO` (`NUMERO`),
  KEY `MID_USUARIO` (`MID_USUARIO`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`),
  KEY `MID_AREA` (`MID_AREA`),
  KEY `MID_SETOR` (`MID_SETOR`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`),
  KEY `CENTRO_DE_CUSTO` (`CENTRO_DE_CUSTO`),
  KEY `MID_EQUIPAMENTO` (`MID_EQUIPAMENTO`),
  KEY `MID_FORNECEDOR` (`MID_FORNECEDOR`),
  KEY `DATA_ABRE` (`DATA_ABRE`),
  KEY `DATA_FINAL` (`DATA_FINAL`),
  KEY `MID_PROGRAMACAO` (`MID_PROGRAMACAO`),
  KEY `MID_SOLICITACAO` (`MID_SOLICITACAO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem`
--

LOCK TABLES `ordem` WRITE;
/*!40000 ALTER TABLE `ordem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_custos`
--

DROP TABLE IF EXISTS `ordem_custos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_custos` (
  `MID_ORDEM` int(11) NOT NULL default '0',
  `DESCRICAO` varchar(120) NOT NULL default '',
  `CUSTO` decimal(13,2) default NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_custos`
--

LOCK TABLES `ordem_custos` WRITE;
/*!40000 ALTER TABLE `ordem_custos` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_custos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_lub`
--

DROP TABLE IF EXISTS `ordem_lub`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_lub` (
  `TIPO` int(1) NOT NULL default '1',
  `MID_PLANO` int(11) NOT NULL default '0',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_PONTO` int(11) NOT NULL default '0',
  `NUMERO` varchar(50) NOT NULL default '0',
  `TAREFA` varchar(150) default NULL,
  `TEMPO_PREVISTO` float(12,2) default NULL,
  `MID_MATERIAL` varchar(25) default NULL,
  `QUANTIDADE` decimal(13,2) default NULL,
  `ESPECIALIDADE` int(4) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL auto_increment,
  `PERIODICIDADE` int(1) NOT NULL default '1',
  `FREQUENCIA` int(4) NOT NULL default '0',
  `CONTADOR` int(6) NOT NULL default '0',
  `DISPARO` decimal(13,2) NOT NULL default '0.00',
  `STATUS` int(1) NOT NULL default '0',
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID_ATV` int(11) NOT NULL default '0',
  `DIAGNOSTICO` int(11) NOT NULL default '0',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_AREA` int(11) NOT NULL default '0',
  `CENTRO_DE_CUSTO` int(11) NOT NULL default '0',
  `MID_SETOR` int(11) NOT NULL default '0',
  `QUANTIDADE_MO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_PLANO` (`MID_PLANO`,`MID_MAQUINA`,`MID_PONTO`,`MID_MATERIAL`,`MID_CONJUNTO`,`FREQUENCIA`,`CONTADOR`,`STATUS`,`MID_ORDEM`,`MID_ATV`,`MID_EMPRESA`,`MID_AREA`,`CENTRO_DE_CUSTO`,`MID_SETOR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_lub`
--

LOCK TABLES `ordem_lub` WRITE;
/*!40000 ALTER TABLE `ordem_lub` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_lub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_maodeobra`
--

DROP TABLE IF EXISTS `ordem_maodeobra`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_maodeobra` (
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID_FUNCIONARIO` int(11) NOT NULL default '0',
  `DATA_INICIO` date default NULL,
  `HORA_INICIO` time default NULL,
  `DATA_FINAL` date default NULL,
  `HORA_FINAL` time default NULL,
  `EQUIPE` int(4) default NULL,
  `CUSTO_EXTRA` decimal(11,2) default NULL,
  `CUSTO` decimal(11,2) default NULL,
  `MID` int(11) NOT NULL,
  `MANUTENTOR` varchar(120) character set latin2 default NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`,`MID_FUNCIONARIO`,`EQUIPE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_maodeobra`
--

LOCK TABLES `ordem_maodeobra` WRITE;
/*!40000 ALTER TABLE `ordem_maodeobra` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_maodeobra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_maq_parada`
--

DROP TABLE IF EXISTS `ordem_maq_parada`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_maq_parada` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `DATA_INICIO` date NOT NULL,
  `DATA_FINAL` date NOT NULL,
  `HORA_INICIO` time NOT NULL,
  `HORA_FINAL` time NOT NULL,
  `TEMPO` decimal(11,3) NOT NULL default '0.000',
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_ORDEM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_maq_parada`
--

LOCK TABLES `ordem_maq_parada` WRITE;
/*!40000 ALTER TABLE `ordem_maq_parada` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_maq_parada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_mat_previsto`
--

DROP TABLE IF EXISTS `ordem_mat_previsto`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_mat_previsto` (
  `MID` int(11) NOT NULL auto_increment,
  `MID_ORDEM` int(11) NOT NULL,
  `MID_MATERIAL` int(11) NOT NULL,
  `QUANTIDADE` float(14,2) NOT NULL,
  `CUSTO` float(14,2) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`,`MID_MATERIAL`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_mat_previsto`
--

LOCK TABLES `ordem_mat_previsto` WRITE;
/*!40000 ALTER TABLE `ordem_mat_previsto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_mat_previsto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_material`
--

DROP TABLE IF EXISTS `ordem_material`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_material` (
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `QUANTIDADE` decimal(11,2) NOT NULL default '0.00',
  `CUSTO_UNITARIO` decimal(11,2) default NULL,
  `CUSTO_TOTAL` decimal(11,2) default NULL,
  `MID` int(11) NOT NULL,
  `MID_ALMOXARIFADO` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`,`MID_MATERIAL`,`MID_ALMOXARIFADO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_material`
--

LOCK TABLES `ordem_material` WRITE;
/*!40000 ALTER TABLE `ordem_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_mo_aloc`
--

DROP TABLE IF EXISTS `ordem_mo_aloc`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_mo_aloc` (
  `MID` int(11) NOT NULL auto_increment,
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID_FUNCIONARIO` int(11) NOT NULL default '0',
  `TEMPO` float(12,2) NOT NULL default '0.00',
  `MID_ESPECIALIDADE` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`,`MID_FUNCIONARIO`,`MID_ESPECIALIDADE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_mo_aloc`
--

LOCK TABLES `ordem_mo_aloc` WRITE;
/*!40000 ALTER TABLE `ordem_mo_aloc` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_mo_aloc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_mo_previsto`
--

DROP TABLE IF EXISTS `ordem_mo_previsto`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_mo_previsto` (
  `MID` int(11) NOT NULL auto_increment,
  `MID_ORDEM` int(11) NOT NULL,
  `MID_ESPECIALIDADE` int(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `TEMPO` float(14,2) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`,`MID_ESPECIALIDADE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_mo_previsto`
--

LOCK TABLES `ordem_mo_previsto` WRITE;
/*!40000 ALTER TABLE `ordem_mo_previsto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_mo_previsto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_prev`
--

DROP TABLE IF EXISTS `ordem_prev`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_prev` (
  `MID_PLANO_PADRAO` int(11) NOT NULL default '0',
  `PERIODICIDADE` int(1) NOT NULL default '1',
  `FREQUENCIA` int(4) NOT NULL default '0',
  `CONTADOR` int(4) NOT NULL default '0',
  `DISPARO` decimal(13,2) NOT NULL default '0.00',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `QUANTIDADE` decimal(11,3) NOT NULL default '0.000',
  `NUMERO` varchar(50) NOT NULL default '0',
  `TAREFA` varchar(250) NOT NULL default '',
  `PARTE` varchar(120) default NULL,
  `INSTRUCAO_DE_TRABALHO` varchar(255) default NULL,
  `TEMPO_PREVISTO` float(12,2) NOT NULL default '0.00',
  `OBSERVACAO` varchar(255) default NULL,
  `ESPECIALIDADE` int(11) NOT NULL default '0',
  `MAQUINA_PARADA` int(1) NOT NULL default '1',
  `STATUS` int(1) NOT NULL default '0',
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MID_ATV` int(11) NOT NULL default '0',
  `DIAGNOSTICO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL auto_increment,
  `QUANTIDADE_MO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_PLANO_PADRAO` (`MID_PLANO_PADRAO`,`PERIODICIDADE`,`CONTADOR`,`MID_CONJUNTO`,`MID_MATERIAL`,`STATUS`,`MID_ORDEM`,`MID_ATV`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_prev`
--

LOCK TABLES `ordem_prev` WRITE;
/*!40000 ALTER TABLE `ordem_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_reprograma`
--

DROP TABLE IF EXISTS `ordem_reprograma`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_reprograma` (
  `MID_ORDEM` int(11) NOT NULL default '0',
  `MOTIVO` varchar(120) NOT NULL default '',
  `DATA` date NOT NULL,
  `MID` int(11) NOT NULL,
  `DATA_ORIGINAL` date NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_ORDEM` (`MID_ORDEM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_reprograma`
--

LOCK TABLES `ordem_reprograma` WRITE;
/*!40000 ALTER TABLE `ordem_reprograma` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordem_reprograma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordem_status`
--

DROP TABLE IF EXISTS `ordem_status`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ordem_status` (
  `DESCRICAO` varchar(20) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  UNIQUE KEY `MID` (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ordem_status`
--

LOCK TABLES `ordem_status` WRITE;
/*!40000 ALTER TABLE `ordem_status` DISABLE KEYS */;
INSERT INTO `ordem_status` VALUES ('ABERTO',1,0,0),('FECHADO',2,0,0),('CANCELADO',3,0,0);
/*!40000 ALTER TABLE `ordem_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pendencias`
--

DROP TABLE IF EXISTS `pendencias`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pendencias` (
  `MID_ORDEM` int(11) NOT NULL default '0',
  `NUMERO` varchar(10) NOT NULL default '00-000000',
  `MID_ORDEM_EXC` int(11) NOT NULL default '0',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `DESCRICAO` varchar(250) NOT NULL default '',
  `ESPECIALIDADE` int(11) NOT NULL default '0',
  `DATA` date NOT NULL,
  `STATUS` int(1) NOT NULL default '1',
  `MID_EQUIPAMENTO` int(11) default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  UNIQUE KEY `NUMERO` (`NUMERO`),
  KEY `MID_ORDEM` (`MID_ORDEM`,`MID_ORDEM_EXC`,`MID_MAQUINA`,`MID_CONJUNTO`,`ESPECIALIDADE`,`MID_EQUIPAMENTO`,`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pendencias`
--

LOCK TABLES `pendencias` WRITE;
/*!40000 ALTER TABLE `pendencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `pendencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plano_checklist`
--

DROP TABLE IF EXISTS `plano_checklist`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `plano_checklist` (
  `DESCRICAO` varchar(120) collate latin1_general_ci NOT NULL default '',
  `USAR_TURNOS` int(1) NOT NULL default '0',
  `LAYOUT` int(1) NOT NULL default '0',
  `FREQUENCIA` int(3) NOT NULL default '0',
  `MID` int(11) NOT NULL auto_increment,
  `MID_OLD` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `plano_checklist`
--

LOCK TABLES `plano_checklist` WRITE;
/*!40000 ALTER TABLE `plano_checklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `plano_checklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plano_padrao`
--

DROP TABLE IF EXISTS `plano_padrao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `plano_padrao` (
  `DESCRICAO` varchar(150) NOT NULL default '',
  `OBSERVACAO` text,
  `TIPO` int(11) NOT NULL default '0',
  `MAQUINA_FAMILIA` int(4) NOT NULL default '0',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `EQUIPAMENTO_FAMILIA` int(11) NOT NULL default '0',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MAQUINA_FAMILIA` (`MAQUINA_FAMILIA`,`MID_MAQUINA`,`EQUIPAMENTO_FAMILIA`,`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `plano_padrao`
--

LOCK TABLES `plano_padrao` WRITE;
/*!40000 ALTER TABLE `plano_padrao` DISABLE KEYS */;
/*!40000 ALTER TABLE `plano_padrao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plano_rotas`
--

DROP TABLE IF EXISTS `plano_rotas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `plano_rotas` (
  `DESCRICAO` varchar(150) NOT NULL default '',
  `OBSERVACAO` text,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  `MID_IMPRESSAO` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `plano_rotas`
--

LOCK TABLES `plano_rotas` WRITE;
/*!40000 ALTER TABLE `plano_rotas` DISABLE KEYS */;
/*!40000 ALTER TABLE `plano_rotas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pontos_lubrificacao`
--

DROP TABLE IF EXISTS `pontos_lubrificacao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pontos_lubrificacao` (
  `MID_MAQUINA` int(11) default NULL,
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `PONTO` varchar(255) NOT NULL default '',
  `NPONTO` int(3) NOT NULL default '0',
  `METODO` varchar(50) default NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_CONJUNTO`,`NPONTO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pontos_lubrificacao`
--

LOCK TABLES `pontos_lubrificacao` WRITE;
/*!40000 ALTER TABLE `pontos_lubrificacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `pontos_lubrificacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pontos_preditiva`
--

DROP TABLE IF EXISTS `pontos_preditiva`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pontos_preditiva` (
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) NOT NULL default '0',
  `PONTO` varchar(255) NOT NULL default '',
  `GRANDEZA` varchar(50) NOT NULL,
  `VALOR_MINIMO` decimal(13,2) default NULL,
  `VALOR_MAXIMO` decimal(13,2) default NULL,
  `OBSERVACAO` text,
  `MID_EQUIPAMENTO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`,`MID_CONJUNTO`,`MID_EQUIPAMENTO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pontos_preditiva`
--

LOCK TABLES `pontos_preditiva` WRITE;
/*!40000 ALTER TABLE `pontos_preditiva` DISABLE KEYS */;
/*!40000 ALTER TABLE `pontos_preditiva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacao`
--

DROP TABLE IF EXISTS `programacao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `programacao` (
  `TIPO` int(11) NOT NULL default '0',
  `MID_PLANO` int(11) NOT NULL default '0',
  `DATA_INICIAL` date NOT NULL,
  `DATA_FINAL` date NOT NULL,
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) default '0',
  `STATUS` int(11) NOT NULL default '1',
  `MOTIVO` varchar(255) collate latin1_general_ci default NULL,
  `DATA_CANCELAMENTO` datetime default NULL,
  `MID` int(11) NOT NULL,
  `MID_EQUIPAMENTO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `TIPO` (`TIPO`),
  KEY `MID_PLANO` (`MID_PLANO`),
  KEY `DATA_INICIAL` (`DATA_INICIAL`),
  KEY `MID_MAQUINA` (`MID_MAQUINA`),
  KEY `MID_CONJUNTO` (`MID_CONJUNTO`),
  KEY `STATUS` (`STATUS`),
  KEY `DATA_CANCELAMENTO` (`DATA_CANCELAMENTO`),
  KEY `MID_EQUIPAMENTO` (`MID_EQUIPAMENTO`),
  KEY `TIPO_2` (`TIPO`,`MID_PLANO`,`MID_MAQUINA`,`MID_CONJUNTO`,`STATUS`,`MID_EQUIPAMENTO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `programacao`
--

LOCK TABLES `programacao` WRITE;
/*!40000 ALTER TABLE `programacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacao_maodeobra`
--

DROP TABLE IF EXISTS `programacao_maodeobra`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `programacao_maodeobra` (
  `MID_PROGRAMACAO` int(11) NOT NULL default '0',
  `ESPECIALIDADE` int(11) NOT NULL default '0',
  `TEMPO` time NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `ESPECIALIDADE` (`ESPECIALIDADE`),
  KEY `MID_PROGRAMACAO` (`MID_PROGRAMACAO`),
  KEY `MID_PROGRAMACAO_2` (`MID_PROGRAMACAO`,`ESPECIALIDADE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `programacao_maodeobra`
--

LOCK TABLES `programacao_maodeobra` WRITE;
/*!40000 ALTER TABLE `programacao_maodeobra` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacao_maodeobra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacao_materiais`
--

DROP TABLE IF EXISTS `programacao_materiais`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `programacao_materiais` (
  `MID_PROGRAMACAO` int(11) NOT NULL default '0',
  `MID_MATERIAL` int(11) NOT NULL default '0',
  `QUANTIDADE` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_MATERIAL` (`MID_MATERIAL`),
  KEY `MID_PROGRAMACAO` (`MID_PROGRAMACAO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `programacao_materiais`
--

LOCK TABLES `programacao_materiais` WRITE;
/*!40000 ALTER TABLE `programacao_materiais` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacao_materiais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacao_periodicidade`
--

DROP TABLE IF EXISTS `programacao_periodicidade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `programacao_periodicidade` (
  `DESCRICAO` varchar(50) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `programacao_periodicidade`
--

LOCK TABLES `programacao_periodicidade` WRITE;
/*!40000 ALTER TABLE `programacao_periodicidade` DISABLE KEYS */;
INSERT INTO `programacao_periodicidade` VALUES ('DIAS',1,0,0),('SEMANAS',2,0,0),('MESES',3,0,0),('POR CONTADOR',4,0,0);
/*!40000 ALTER TABLE `programacao_periodicidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacao_tipo`
--

DROP TABLE IF EXISTS `programacao_tipo`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `programacao_tipo` (
  `DESCRICAO` varchar(50) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `programacao_tipo`
--

LOCK TABLES `programacao_tipo` WRITE;
/*!40000 ALTER TABLE `programacao_tipo` DISABLE KEYS */;
INSERT INTO `programacao_tipo` VALUES ('PREVENTIVA',1,0,0),('ROTA',2,0,0),('SOLICITACÃO',4,0,0);
/*!40000 ALTER TABLE `programacao_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requisicao`
--

DROP TABLE IF EXISTS `requisicao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `requisicao` (
  `USUARIO` int(11) NOT NULL default '0',
  `FUNCIONARIO` int(11) default NULL,
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `MOTIVO` varchar(255) default NULL,
  `ORDEM` varchar(50) default NULL,
  `MATERIAIS` text NOT NULL,
  `PRAZO` date NOT NULL,
  `STATUS` int(11) default NULL,
  `MID` int(11) NOT NULL,
  `MID_ALMOXARIFADO` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `USUARIO` (`USUARIO`,`FUNCIONARIO`,`STATUS`,`MID_ALMOXARIFADO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `requisicao`
--

LOCK TABLES `requisicao` WRITE;
/*!40000 ALTER TABLE `requisicao` DISABLE KEYS */;
/*!40000 ALTER TABLE `requisicao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requisicao_resposta`
--

DROP TABLE IF EXISTS `requisicao_resposta`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `requisicao_resposta` (
  `MID_REQUISICAO` int(11) NOT NULL default '0',
  `USUARIO` int(11) default NULL,
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `TEXTO` text NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_REQUISICAO` (`MID_REQUISICAO`,`USUARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `requisicao_resposta`
--

LOCK TABLES `requisicao_resposta` WRITE;
/*!40000 ALTER TABLE `requisicao_resposta` DISABLE KEYS */;
/*!40000 ALTER TABLE `requisicao_resposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roteiro_rotas`
--

DROP TABLE IF EXISTS `roteiro_rotas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `roteiro_rotas` (
  `MID_PLANO` int(11) NOT NULL default '0',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `POSICAO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL auto_increment,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_PLANO` (`MID_PLANO`,`MID_MAQUINA`,`POSICAO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `roteiro_rotas`
--

LOCK TABLES `roteiro_rotas` WRITE;
/*!40000 ALTER TABLE `roteiro_rotas` DISABLE KEYS */;
/*!40000 ALTER TABLE `roteiro_rotas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setores`
--

DROP TABLE IF EXISTS `setores`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `setores` (
  `COD` varchar(30) collate latin1_general_ci NOT NULL default '',
  `DESCRICAO` varchar(120) collate latin1_general_ci default NULL,
  `MID_AREA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_AREA` (`MID_AREA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `setores`
--

LOCK TABLES `setores` WRITE;
/*!40000 ALTER TABLE `setores` DISABLE KEYS */;
/*!40000 ALTER TABLE `setores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacao_resposta`
--

DROP TABLE IF EXISTS `solicitacao_resposta`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `solicitacao_resposta` (
  `MID_SOLICITACAO` int(11) NOT NULL default '0',
  `USUARIO` int(11) NOT NULL default '0',
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `TEXTO` text collate latin1_general_ci NOT NULL,
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_SOLICITACAO` (`MID_SOLICITACAO`,`USUARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `solicitacao_resposta`
--

LOCK TABLES `solicitacao_resposta` WRITE;
/*!40000 ALTER TABLE `solicitacao_resposta` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacao_resposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacao_situacao_producao`
--

DROP TABLE IF EXISTS `solicitacao_situacao_producao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `solicitacao_situacao_producao` (
  `DESCRICAO` varchar(75) NOT NULL default '',
  `NIVEL` int(1) NOT NULL default '1',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `NIVEL` (`NIVEL`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `solicitacao_situacao_producao`
--

LOCK TABLES `solicitacao_situacao_producao` WRITE;
/*!40000 ALTER TABLE `solicitacao_situacao_producao` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacao_situacao_producao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacao_situacao_seguranca`
--

DROP TABLE IF EXISTS `solicitacao_situacao_seguranca`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `solicitacao_situacao_seguranca` (
  `DESCRICAO` varchar(75) NOT NULL default '',
  `NIVEL` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `NIVEL` (`NIVEL`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `solicitacao_situacao_seguranca`
--

LOCK TABLES `solicitacao_situacao_seguranca` WRITE;
/*!40000 ALTER TABLE `solicitacao_situacao_seguranca` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacao_situacao_seguranca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacoes`
--

DROP TABLE IF EXISTS `solicitacoes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `solicitacoes` (
  `NUMERO` varchar(10) collate latin1_general_ci NOT NULL default '00-000000',
  `MID_MAQUINA` int(11) NOT NULL default '0',
  `MID_CONJUNTO` int(11) default '0',
  `DESCRICAO` varchar(150) collate latin1_general_ci NOT NULL default '',
  `TEXTO` text collate latin1_general_ci NOT NULL,
  `DATA` date NOT NULL,
  `HORA` time default NULL,
  `STATUS` int(11) NOT NULL default '0',
  `USUARIO` int(11) NOT NULL default '0',
  `PRODUCAO` int(11) default '0',
  `SEGURANCA` int(11) default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  UNIQUE KEY `NUMERO` (`NUMERO`),
  KEY `NUMERO_2` (`NUMERO`,`MID_MAQUINA`,`MID_CONJUNTO`,`STATUS`,`USUARIO`,`PRODUCAO`,`SEGURANCA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `solicitacoes`
--

LOCK TABLES `solicitacoes` WRITE;
/*!40000 ALTER TABLE `solicitacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solucao`
--

DROP TABLE IF EXISTS `solucao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `solucao` (
  `DESCRICAO` varchar(200) collate latin1_general_ci NOT NULL default '',
  `TEXTO` text collate latin1_general_ci,
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `solucao`
--

LOCK TABLES `solucao` WRITE;
/*!40000 ALTER TABLE `solucao` DISABLE KEYS */;
/*!40000 ALTER TABLE `solucao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_lubrificante`
--

DROP TABLE IF EXISTS `tipo_lubrificante`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tipo_lubrificante` (
  `DESCRICAO` varchar(150) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tipo_lubrificante`
--

LOCK TABLES `tipo_lubrificante` WRITE;
/*!40000 ALTER TABLE `tipo_lubrificante` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_lubrificante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_planos`
--

DROP TABLE IF EXISTS `tipo_planos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tipo_planos` (
  `DESCRICAO` varchar(150) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tipo_planos`
--

LOCK TABLES `tipo_planos` WRITE;
/*!40000 ALTER TABLE `tipo_planos` DISABLE KEYS */;
INSERT INTO `tipo_planos` VALUES ('�NICO OBJETO DE MANUTEN��O',1,0,0),('FAMILIA DE OBJETO DE MANUTEN��O',2,0,0),('QUALQUER OBJETO DE MANUTEN��O',3,0,0),('FAMILIA DE COMPONENTE',4,0,0),('QUALQUER COMPONENTE',5,0,0);
/*!40000 ALTER TABLE `tipo_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_rotas`
--

DROP TABLE IF EXISTS `tipo_rotas`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tipo_rotas` (
  `DESCRICAO` varchar(150) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tipo_rotas`
--

LOCK TABLES `tipo_rotas` WRITE;
/*!40000 ALTER TABLE `tipo_rotas` DISABLE KEYS */;
INSERT INTO `tipo_rotas` VALUES ('LUBRIFICA��O',1,0,0),('INSPE��O',2,0,0);
/*!40000 ALTER TABLE `tipo_rotas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_servicos`
--

DROP TABLE IF EXISTS `tipos_servicos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tipos_servicos` (
  `DESCRICAO` varchar(200) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tipos_servicos`
--

LOCK TABLES `tipos_servicos` WRITE;
/*!40000 ALTER TABLE `tipos_servicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipos_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turnos`
--

DROP TABLE IF EXISTS `turnos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `turnos` (
  `DESCRICAO` varchar(60) collate latin1_general_ci NOT NULL default '',
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_EMPRESA` (`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `turnos`
--

LOCK TABLES `turnos` WRITE;
/*!40000 ALTER TABLE `turnos` DISABLE KEYS */;
/*!40000 ALTER TABLE `turnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turnos_horarios`
--

DROP TABLE IF EXISTS `turnos_horarios`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `turnos_horarios` (
  `MID_TURNO` int(11) NOT NULL default '0',
  `HORA_INICIAL` time NOT NULL,
  `HORA_FINAL` time NOT NULL,
  `HORA_TOTAL` time NOT NULL,
  `DIA` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `MID_TURNO` (`MID_TURNO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `turnos_horarios`
--

LOCK TABLES `turnos_horarios` WRITE;
/*!40000 ALTER TABLE `turnos_horarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `turnos_horarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios` (
  `NOME` varchar(120) collate latin1_general_ci NOT NULL,
  `EMAIL` varchar(120) collate latin1_general_ci NOT NULL,
  `CELULAR` varchar(12) collate latin1_general_ci default NULL,
  `USUARIO` varchar(16) collate latin1_general_ci NOT NULL default '',
  `SENHA` varchar(125) collate latin1_general_ci NOT NULL default '',
  `GRUPO` int(11) NOT NULL default '0',
  `CENTRO_DE_CUSTO` int(11) default '0',
  `MID_FUNCIONARIO` int(11) default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`EMAIL`,`USUARIO`,`MID`),
  KEY `USUARIO` (`USUARIO`,`GRUPO`,`CENTRO_DE_CUSTO`,`MID_FUNCIONARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_grupos`
--

DROP TABLE IF EXISTS `usuarios_grupos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_grupos` (
  `DESCRICAO` varchar(75) collate latin1_general_ci NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_grupos`
--

LOCK TABLES `usuarios_grupos` WRITE;
/*!40000 ALTER TABLE `usuarios_grupos` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_modulos`
--

DROP TABLE IF EXISTS `usuarios_modulos`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_modulos` (
  `DESCRICAO` varchar(50) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `ID` int(11) NOT NULL default '0',
  `OP` int(11) NOT NULL default '0',
  `DIR` varchar(164) NOT NULL default '0',
  `ADMIN` tinyint(1) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `ID` (`ID`,`OP`,`ADMIN`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_modulos`
--

LOCK TABLES `usuarios_modulos` WRITE;
/*!40000 ALTER TABLE `usuarios_modulos` DISABLE KEYS */;
INSERT INTO `usuarios_modulos` VALUES ('PRINCIPAL',0,0,0,'modulos/principal',0,0,0),('CADASTRO',1,1,0,'modulos/cadastro',0,0,0),('ESTOQUE',2,2,0,'modulos/estoque',0,0,0),('PLANEJAMENTO',3,3,0,'modulos/planejamento',0,0,0),('ORDENS',4,4,0,'modulos/ordens',0,0,0),('RELATORIOS',5,5,0,'modulos/relatorios',0,0,0),('AJUDA',6,6,0,'modulos/ajuda',0,0,0),('PLANEJAMENTO',26,5,3,'0',0,0,0),('MATERIAIS',25,5,2,'0',0,0,0),('ESTRUTURAL',24,5,1,'0',0,0,0),('CARTEIRA DE SERVI�OS',23,4,2,'0',0,0,0),('SOLICITAC�ES',22,4,1,'0',0,0,0),('PROGRAMA��O',21,3,5,'0',0,0,0),('MONITORAMENTO',20,3,4,'0',0,0,0),('ROTAS',19,3,2,'0',0,0,0),('PREVENTIVA',18,3,1,'0',0,0,0),('ALMOXARIFADO',17,2,4,'0',0,0,0),('REQUISI��O',16,2,3,'0',0,0,0),('MOVIMENTA��O',15,2,2,'0',0,0,0),('MATERIAIS',14,2,1,'0',0,0,0),('TABELAS',13,1,3,'0',0,0,0),('M�O DE OBRA',12,1,2,'0',0,0,0),('ESTRUTURA GERAL',11,1,1,'0',0,0,0),('REGISTROS',10,0,5,'0',1,0,0),('INTEGRA��O',9,0,3,'0',1,0,0),('ADM. DO SISTEMA',8,0,2,'0',1,0,0),('RESUMO',7,0,1,'0',0,0,0),('ORDENS',27,5,4,'0',0,0,0),('GRAFICOS',28,5,5,'0',0,0,0),('ASS GRAFICOS',29,5,6,'0',0,0,0),('GR�FICOS GERENCIAIS',30,5,7,'0',0,0,0),('TESTE DE VELOCIDADE',31,0,4,'0',1,0,0);
/*!40000 ALTER TABLE `usuarios_modulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_online`
--

DROP TABLE IF EXISTS `usuarios_online`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_online` (
  `NOME` varchar(200) collate latin1_general_ci NOT NULL default '0',
  `USUARIO` int(11) NOT NULL default '0',
  `IP` varchar(50) collate latin1_general_ci NOT NULL default '',
  `TEMPO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `USUARIO` (`USUARIO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_online`
--

LOCK TABLES `usuarios_online` WRITE;
/*!40000 ALTER TABLE `usuarios_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_permisao_cc`
--

DROP TABLE IF EXISTS `usuarios_permisao_cc`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_permisao_cc` (
  `USUARIO` int(11) NOT NULL default '0',
  `CENTRO_DE_CUSTO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `USUARIO` (`USUARIO`,`CENTRO_DE_CUSTO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_permisao_cc`
--

LOCK TABLES `usuarios_permisao_cc` WRITE;
/*!40000 ALTER TABLE `usuarios_permisao_cc` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_permisao_cc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_permisao_sol`
--

DROP TABLE IF EXISTS `usuarios_permisao_sol`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_permisao_sol` (
  `USUARIO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_EMPRESA` int(11) NOT NULL default '0',
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `USUARIO` (`USUARIO`,`MID_EMPRESA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_permisao_sol`
--

LOCK TABLES `usuarios_permisao_sol` WRITE;
/*!40000 ALTER TABLE `usuarios_permisao_sol` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_permisao_sol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_permissao`
--

DROP TABLE IF EXISTS `usuarios_permissao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_permissao` (
  `GRUPO` int(11) NOT NULL default '0',
  `MODULO` int(11) NOT NULL default '0',
  `PERMISSAO` int(11) NOT NULL default '0',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`),
  KEY `GRUPO` (`GRUPO`,`MODULO`,`PERMISSAO`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_permissao`
--

LOCK TABLES `usuarios_permissao` WRITE;
/*!40000 ALTER TABLE `usuarios_permissao` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_permissao_tipo`
--

DROP TABLE IF EXISTS `usuarios_permissao_tipo`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `usuarios_permissao_tipo` (
  `DESCRICAO` varchar(50) NOT NULL default '',
  `MID` int(11) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `usuarios_permissao_tipo`
--

LOCK TABLES `usuarios_permissao_tipo` WRITE;
/*!40000 ALTER TABLE `usuarios_permissao_tipo` DISABLE KEYS */;
INSERT INTO `usuarios_permissao_tipo` VALUES ('ADMINISTRADOR',1,0,0),('SUPERVISOR',2,0,0),('USUÁRIO',3,0,0),('SEM PERMISSÃO',0,0,0);
/*!40000 ALTER TABLE `usuarios_permissao_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versao`
--

DROP TABLE IF EXISTS `versao`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `versao` (
  `MID` int(11) NOT NULL,
  `VERSAO` varchar(10) NOT NULL,
  `MID_OLD` int(11) NOT NULL default '0',
  `MID_CARGA` int(11) NOT NULL default '0',
  PRIMARY KEY  (`MID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `versao`
--

LOCK TABLES `versao` WRITE;
/*!40000 ALTER TABLE `versao` DISABLE KEYS */;
/*!40000 ALTER TABLE `versao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-27 12:10:55
