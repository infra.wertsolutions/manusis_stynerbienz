<?
/** 
* Configuração dos Modulos 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  configuracao
*/

$manusis['modulo'] = array();

$sql = "SELECT * FROM " . USUARIOS_MODULOS . " WHERE OP = 0 ORDER BY ID";

$result = $dba[$tdb[USUARIOS_MODULOS]['dba']] -> Execute($sql);

if(!$result->EOF){
	
	$dataModulos = 	$result->getRows();

	foreach ($dataModulos as $modulo)
	{
		$sql = "SELECT OP, DESCRICAO, ADMIN FROM " . USUARIOS_MODULOS . " WHERE ID = {$modulo['MID']} AND OP <> 0 ORDER BY OP ASC";
		$result = $dba[$tdb[USUARIOS_MODULOS]['dba']] -> Execute($sql);
		if(!$result->EOF)
		{
		   while(!$result->EOF)
		   {
		   	  $campos = $result->fields;
		   	  $modulo['DESCRICAO'] = ucfirst(mb_strtolower($modulo['DESCRICAO']));
		   	  
	   	  
		   	  if($campos['ADMIN'] == 1 && $_SESSION[ManuSess]['user']['MID'] === "ROOT")
		   	  {
		   	  	    $manusis['modulo'][$modulo['ID']]['nome'] = $modulo['DESCRICAO'];
	             	$manusis['modulo'][$modulo['ID']]['dir']  = $modulo['DIR'];
	             	$manusis['modulo'][$modulo['ID']]['menu_admin'][$campos['OP']]  = ucfirst( mb_strtolower($campos['DESCRICAO']));
		   	  }
		   	  elseif(VoltaPermissao($modulo['ID'], $campos['OP']) != 0)
		   	  {
	             	$manusis['modulo'][$modulo['ID']]['nome'] = $modulo['DESCRICAO'];
	             	$manusis['modulo'][$modulo['ID']]['dir']  = $modulo['DIR'];
	             	$manusis['modulo'][$modulo['ID']]['menu'][$campos['OP']]  = ucfirst( mb_strtolower($campos['DESCRICAO']));	             		   	  	
		   	  }		   	  
		   	  
		   	  $result->moveNext();
		   }	
		}
	}
	
}
