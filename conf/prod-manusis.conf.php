<?
/** 
* Configura��o Geral
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  configuracao
*/

ini_set('register_globals','Off');
ini_set('safe_mode','On');
setlocale(LC_ALL, 'en_US');
session_start();
//$manusis['debug'] = 1;
define('CONF_CARREGADO','1');
define('ManuSess','Styner');

$manusis['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/stynerbienz/';

/**
 * Preencha obrigatoriamente o diretorio principal do Manusis!
 */
$manusis['dir']['principal'] = 'manusis/';
$manusis['dir']['temporarios'] = 'arquivos/temp';
$manusis['dir']['maquinas'] = 'arquivos/maquinas';
$manusis['dir']['equipamentos'] = 'arquivos/equipamentos';
$manusis['dir']['planopadrao'] = 'arquivos/planopadrao';
$manusis['dir']['inspecao'] = 'arquivos/inspecao';
$manusis['dir']['lubrificacao'] = 'arquivos/lubrificacao';
$manusis['dir']['materiais'] = 'arquivos/materiais';
$manusis['dir']['graficos'] = 'arquivos/graficos';
$manusis['dir']['ordens'] = 'arquivos/ordens';
$manusis['dir']['emodelos'] = 'arquivos/modelos_exportar';
$manusis['dir']['imodelos'] = 'arquivos/modelos_importar';
$manusis['dir']['exportar'] = 'arquivos/exportar';

$manusis['caixaalta'] = 1;
$manusis['tema'] = 'padrao';

$manusis['idioma'][0] = 'portuguesbr';

$manusis['db'][0]['driver']="mysql";
$manusis['db'][0]['host']="192.168.0.100";
$manusis['db'][0]['user']="manusis";
$manusis['db'][0]['senha']="ma20nu3";
$manusis['db'][0]['base']="manusis_stynerbienz";

/*$manusis['db'][0]['driver']="oci8";
$manusis['db'][0]['host']="192.168.0.1";
$manusis['db'][0]['user']="modelo_v3";
$manusis['db'][0]['senha']="ma21nu3";
$manusis['db'][0]['base']="xe";*/

//$manusis['debug'] = 1;

// N�MERO DE EMPRESAS PERMITIDO NA LICEN�A
$manusis['empresas'] = 1;


$manusis['admin']['nome'] = 'Administrador';
$manusis['admin']['email'] = 'volmir.weiler@advaltech.com';
$manusis['admin']['usuario'] = 'administrador';
$manusis['admin']['senha'] = 'e05146';

$manusis['solicitacao']['informa'] = 'volmir.weiler@advaltech.com';
$manusis['solicitacao']['gravaos'] = 1;

$manusis['ordem']['custo'] = 1;

$manusis['os']['alocacao_recursos'] = 1;
$manusis['os']['impressao_nova'] = 1;

// Linas para da impress�o de ordem por tipo.
$manusis['os']['maodeobra'][0] = 3; // n�o sistem�ticas
$manusis['os']['maodeobra'][1] = 3; // Preventivas
$manusis['os']['maodeobra'][2] = 3; // Rotas
$manusis['os']['maodeobra'][4] = 3; // Solicita��es

$manusis['os']['parada'][0] = 2;
$manusis['os']['parada'][1] = 2;
$manusis['os']['parada'][2] = 2;
$manusis['os']['parada'][4] = 2;

$manusis['os']['materiais'][0] = 3;
$manusis['os']['materiais'][1] = 3;
$manusis['os']['materiais'][2] = 3;
$manusis['os']['materiais'][4] = 3;

$manusis['os']['pendencia'][0] = 5;
$manusis['os']['pendencia'][1] = 5;
$manusis['os']['pendencia'][2] = 5;
$manusis['os']['pendencia'][4] = 5;

$manusis['os']['defeito'][0] = 2;
$manusis['os']['defeito'][1] = 2;
$manusis['os']['defeito'][2] = 2;
$manusis['os']['defeito'][4] = 2;

$manusis['os']['causa'][0] = 2;
$manusis['os']['causa'][1] = 2;
$manusis['os']['causa'][2] = 2;
$manusis['os']['causa'][4] = 2;

$manusis['os']['solucao'][0] = 5;
$manusis['os']['solucao'][1] = 5;
$manusis['os']['solucao'][2] = 5;
$manusis['os']['solucao'][4] = 5;

$manusis['os']['dias_para_alerta'] = 1;

// DIA INICIAL PARA MOSTRAR A PROGRAMA��O SEMANAL
// LEMBRANDO: 1 = SEGUNDA E 7 = DOMINGO
$manusis['os']['dia_ini_prog'] = 1;

$manusis['upload']['tamanho'] = 15360000;

$manusis['erro']['reportar'] = 0;

$manusis['sess']['expira'] = 0;
$manusis['sess']['tempo'] = 99999500;

$manusis['log']['logon'] = 1;
$manusis['log']['logoff'] = 1;
$manusis['log']['delete'] = 1;
$manusis['log']['insert'] = 1;
$manusis['log']['update'] = 1;
$manusis['log']['erros'] = 1;

$_autotag_setor['status'] = 0;
$_autotag_setor['tabela'] = 'areas';
$_autotag_setor['caracter'] = '.';
$_autotag_setor['casasdecimais'] = 3;
$_autotag_setor['incrementa'] = 1;

$_autotag_maq['status'] = 0;
$_autotag_maq['tabela'] = 'maquinas_familia';
$_autotag_maq['caracter'] = '';
$_autotag_maq['casasdecimais'] = 3;
$_autotag_maq['incrementa'] = 1;

$_autotag_conj['status'] = 0;
$_autotag_conj['tabela'] = 'maquinas';
$_autotag_conj['caracter'] = '.';
$_autotag_conj['casasdecimais'] = 2;
$_autotag_conj['incrementa'] = 1;

$_autotag_equip['status'] = 0;
$_autotag_equip['tabela'] = 'equipamentos_familia';
$_autotag_equip['caracter'] = '.';
$_autotag_equip['casasdecimais'] = 3;
$_autotag_equip['incrementa'] = 1;

$_autotag_fammaq = 0;
$_autotag_famequip = 0;

?>
