<?php
// Fun��es de Estrutura
if (!require("lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("lib/bd.php")) die ($ling['bd01']);


$user_oracle = "MODELO_V3";
$table_espace_oracle = "USERS";


$migra_tipo = array (
    'INT'       => 'NUMBER',
    'TINYINT'   => 'NUMBER',
    'FLOAT'     => 'NUMBER',
    'DECIMAL'   => 'NUMBER',
    'DOUBLE'    => 'NUMBER',
    'VARCHAR'   => 'VARCHAR2',
    'CHAR'      => 'VARCHAR2',
    'TEXT'      => 'VARCHAR2(4000)',
    'DATE'      => 'DATE',
    'TIME'      => 'DATE',
    'DATETIME'  => 'TIMESTAMP'
);


function converte_tipo ($tipo) {
    global $migra_tipo;
    
    $tipon = ''; 
    $parametro = '';
    
    // Dividindo em tipo e parametro
    list($tipon, $parametro) = explode('(', mb_strtoupper($tipo));
    
    $novo_tipo = $migra_tipo[$tipon];
    
    if ($parametro != '') {
        $parametro = substr($parametro, 0, strpos($parametro, ')'));
        
        if ($tipo == 'INT') {
            $parametro = "$parametro, 0";
        }
        
        $novo_tipo .= "(" . $parametro . ")";
    }
    
    return $novo_tipo;
}






// Comparativo entre as tabelas
$sql = "SHOW TABLES";

// Modelo
$rs_v3 = $dba[0]->Execute($sql);
$tab_v3_tmp = $rs_v3->getrows();

$tab_v3 = array();
foreach ($tab_v3_tmp as $k => $v) {
    
    $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][0]['base']]} FROM {$manusis['db'][0]['base']}";
    $rs_col_v3 = $dba[0]->Execute($sql_tmp);
    
    $col_tmp = $rs_col_v3->getrows();
    $col = array();
    
    foreach ($col_tmp as $k2 => $v2) {
        $col[] = $v2;
    }

    $tab_v3[$v['Tables_in_'. $manusis['db'][0]['base']]]['colunas'] = $col;
    
    // MOSTRANDO OS INDEXES
    $sql_tmp = "SHOW INDEXES FROM {$v['Tables_in_'. $manusis['db'][0]['base']]} FROM {$manusis['db'][0]['base']}";
    $rs_col_v3 = $dba[0]->Execute($sql_tmp);
    
    $col_tmp = $rs_col_v3->getrows();
    $col = array();
    
    foreach ($col_tmp as $k2 => $v2) {
        $col[] = $v2;
    }

    $tab_v3[$v['Tables_in_'. $manusis['db'][0]['base']]]['index'] = $col;
}

//print_r($tab_v3);


foreach ($tab_v3 as $tabela => $def) {
    
    if (mb_strlen($tabela) > 30) {
        echo "\n--- ERRO AO CRIAR A TABELA " . mb_strtoupper($tabela) . ". O NOME DO OBEJETO N&Atilde;O PODE ULTRAPASSAR 25 CARACTERES.\n";
        continue;
    }
    
    echo "\n--- CRIANDO A TABELA $tabela \n";
    
    // Criando a tabela
    echo "CREATE TABLE \"" . mb_strtoupper($tabela) . "\" (\n";
    
    // Criando os campos
    foreach ($def['colunas'] as $campo) {
        // Nome do campo
        echo " \"" . mb_strtoupper($campo['Field']) . "\" ";
        
        // TIPO DE DADO
        echo converte_tipo($campo['Type']) . " ";
        
        
        // VALOR PADR�O
        if ($campo['Default'] != '') {
            if (is_numeric($campo['Default'])) {
                echo "DEFAULT {$campo['Default']} ";
            }
            elseif (($campo['Type'] != 'date') and ($campo['Type'] != 'time') and ($campo['Type'] != 'datetime')) {
                echo "DEFAULT '{$campo['Default']}' ";
            }
        }
        
        // � OBRIGAT�RIO
        if ($campo['Null'] == 'YES') {
            echo "NULL ";
        }
        else {
            echo "NOT NULL ";
        }
        
        echo ", \n";
    } 
    
    echo " CONSTRAINT \"PK_" . substr(mb_strtoupper($tabela), 0, 27) . "\" PRIMARY KEY (\"MID\") \n";
    echo " USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 \n";
    echo " STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 \n";
    echo " PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) \n";
    echo " TABLESPACE \"USERS\"  ENABLE \n";
    echo ") PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING \n";
    echo "STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 \n";
    echo "PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) \n";
    echo "TABLESPACE \"$table_espace_oracle\";\n\n";
    
    
    // Criando os INDEXES
    foreach ($def['index'] as $index) {
        if (($index['Column_name'] == "MID") or ($index['Key_name'] == "PRIMARY") or ($index['Non_unique'] != 0)) continue;
        
        echo "CREATE UNIQUE INDEX \"UK_" . substr(mb_strtoupper($tabela) . "_" . mb_strtoupper($index['Column_name']), 0, 27) . "\" ON \"" . mb_strtoupper($tabela) . "\" (" . mb_strtoupper($index['Column_name']) . ");\n";
    }

}

?>
