<?php
// Fun��es de Estrutura
if (!require("lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);

// Base de origem
$base_origem = $_GET['base_origem'];

// Empresa destino das mudan�as
$empresa_dest = (int)$_GET['empresa_dest'];

// Nome do arquivo para colocar nos form e links
$nome_arquivo = "compara_base.php";

// Conex�o com a outra base
$manusis['db'][1]['driver'] = 'mysql';
$manusis['db'][1]['host']   = 'localhost';
$manusis['db'][1]['user']   = 'suporte';
$manusis['db'][1]['senha']  = '123123';
$manusis['db'][1]['base']   = $base_origem;


// Informa��es do banco de dados
if (!require("lib/bd.php")) die ($ling['bd01']);


$tabelas_ignoradas[] = EMPRESAS;
$tabelas_ignoradas[] = USUARIOS_MODULOS;
$tabelas_ignoradas[] = USUARIOS_PERMISSAO_TIPO;
$tabelas_ignoradas[] = USUARIOS_PERMISSAO;
$tabelas_ignoradas[] = MAQUINAS_PARADA;
$tabelas_ignoradas[] = MAQUINAS_CONTADOR_TIPO;
$tabelas_ignoradas[] = MAQUINAS_STATUS; 
$tabelas_ignoradas[] = LOGS_TIPOS;
$tabelas_ignoradas[] = LOGS;
$tabelas_ignoradas[] = PROGRAMACAO_TIPO;
$tabelas_ignoradas[] = PROGRAMACAO_PERIODICIDADE;
$tabelas_ignoradas[] = TIPO_PLANOS;
$tabelas_ignoradas[] = TIPO_ROTAS;
$tabelas_ignoradas[] = ORDEM_STATUS;
$tabelas_ignoradas[] = EQUIPAMENTOS_STATUS;
$tabelas_ignoradas[] = FUNCIONARIOS_STATUS;
$tabelas_ignoradas[] = FUNCIONARIOS_SITUACAO;
$tabelas_ignoradas[] = INTEGRACAO_TIPOS;
$tabelas_ignoradas[] = VERSAO;

// ARQUIVOS E SUAS RESPECTIVAS TABELAS
$arquivos['maquinas']     = MAQUINAS;
$arquivos['equipamentos'] = EQUIPAMENTOS;
$arquivos['planopadrao']  = PLANO_PADRAO;
$arquivos['inspecao']     = LINK_ROTAS;
$arquivos['lubrificacao'] = LINK_ROTAS;
$arquivos['materiais']    = MATERIAIS;
$arquivos['ordens']       = ORDEM;


########################################################################

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"Manusis Padr�o\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "
<script>
// Alerta ao fechar a pagina
window.onbeforeunload = function(e) {
    var e = e || window.event;

    var msg = 'Se fizer isso a atualiza��o pode dar errado!!!!';

    // For IE and Firefox
    if (e) {
        e.returnValue = msg;
    }

    // For Safari
    return msg;
}
</script>
</head>
<body>
<div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>Migra��o de Bases</h1>
</div>
<div id=\"corpo_relatorio\">";

// Apenas dar op��o no inicio do processo
if (($base_origem == '') or ($empresa_dest == 0)) {
    // INICIANDO ENTRADA NA SESS�O
    $_SESSION[ManuSess]['migra']      = array();
    $_SESSION[ManuSess]['migra_tipo'] = 0;
    $_SESSION[ManuSess]['migra_cod']  = '';
    $_SESSION[ManuSess]['migra_mid']  = GeraMid('cargas', 'MID', 0);
    $_SESSION[ManuSess]['migra_update'] = array();
    
    echo "<form method=\"GET\" action=\"$nome_arquivo\">

    <fieldset><legend>Bases de dados</legend>
    Base origem: "; 

    echo "<select name=\"base_origem\" id=\"base_origem\" class=\"campo_select_ob\">";



    $sql = "SHOW DATABASES";
    $res = $dba[1]->Execute($sql);
    while (!$res->EOF) {
        $campo = $res->fields;
        echo "<option ".(($base_origem == $campo['Database'])?"selected=\"selected\"":'')." value=\"{$campo['Database']}\">{$campo['Database']}</option>\n"; 
        $res->MoveNext();
    }

    echo "</select>";

    echo " Empresa destino:";
    FormSelectD('COD', 'NOME', EMPRESAS, $empresa_dest, 'empresa_dest', 'empresa_dest', 'MID', '', 'campo_select_ob');


    echo "
    <input type=\"submit\" name=\"env\" value=\"Iniciar migra��o da base selecionada\" class=\"botao\" onclick=\"window.onbeforeunload = null;\" />

    </fieldset>
    </form>
    <br><br>";
}

// REALIZANDO A MIGRA��O
if (($_GET['env'] != '') and ($base_origem != '') and ($empresa_dest != 0)) {
 
    #############
    // Comparativo entre as tabelas
    $sql = "SHOW TABLES";
    
    // Modelo
    $rs_v3 = $dba[1]->Execute($sql);
    $tab_v3_tmp = $rs_v3->getrows();
    
    $tab_v3 = array();
    foreach ($tab_v3_tmp as $k => $v) {
        
        $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][1]['base']]} FROM {$manusis['db'][1]['base']}";
        $rs_col_v3 = $dba[1]->Execute($sql_tmp);
        
        $col_tmp = $rs_col_v3->getrows();
        $col = array();
        
        foreach ($col_tmp as $k2 => $v2) {
            if ($v2["Field"] != "MID") {
                $col[$v2["Field"]] = $v2;
            }
        }
    
        $tab_v3[$v['Tables_in_'. $manusis['db'][1]['base']]] = $col;
    }
    
    // Cliente
    $rs_dest = $dba[0]->Execute($sql);
    $tab_dest_tmp = $rs_dest->getrows();
    
    $tab_dest = array();
    foreach ($tab_dest_tmp as $k => $v) {
        
        $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][0]['base']]} FROM {$manusis['db'][0]['base']}";
        $rs_col_dest = $dba[0]->Execute($sql_tmp);
        
        $col_tmp = $rs_col_dest->getrows();
        
        $col = array();
        foreach ($col_tmp as $k2 => $v2) {
            if ($v2["Field"] != "MID") {
                $col[$v2["Field"]] = $v2;
            }
        }
        
        $tab_dest[$v['Tables_in_'. $manusis['db'][0]['base']]] = $col;
    }
    
    ###################################################################
    echo "<h1>Passo 1: igualar as bases</h1>
    <br />
    <form action=\"$nome_arquivo\" method=\"GET\">
    <input type=\"hidden\" name=\"alterar_tb\" value=\"1\" />
    <input type=\"hidden\" name=\"base_origem\" value=\"$base_origem\" />
    <input type=\"hidden\" name=\"empresa_dest\" value=\"$empresa_dest\" />
    <table width=\"100%\" cellspacing=0 cellpadding=2 border=\"0\">
    
    ";
    
    foreach ($tab_v3 as $tab => $cols) {
        if (array_search($tab, $tabelas_ignoradas) !== FALSE) {
            continue;
        }
        $etab='';   
        foreach ($cols as $col => $def) {
            if ($tab_dest[$tab][$col] != $def) {
                $def_txt=$n_txt='';
                foreach ((array)$def as $ek => $ev) {
                    if ($ev) {
                        AddStr($def_txt,'<br />',Campo($ek,$ev));
                    }
                }
                foreach ((array)$tab_dest[$tab][$col] as $ek => $ev) {
                    if ($ev) {
                        AddStr($n_txt,'<br />',Campo($ek,$ev));
                    }
                }
                
                $etab .= "<tr>
                <td style=\"border-bottom: 1px solid black;border-right: 1px solid black;border-left: 1px solid black\">{$col}</td>
                <td style=\"border-bottom: 1px solid black;border-right: 1px solid black\">$def_txt</td>
                <td style=\"border-bottom: 1px solid black;border-right: 1px solid black\" align=\"center\"><input type=\"checkbox\" name=\"migra_campo[$tab][$col]\" value=\"1\" /></td>
                <td style=\"border-bottom: 1px solid black;border-right: 1px solid black\">" . ($n_txt ? $n_txt : 'N�o tem') . "</td>
                </tr>";
            }
        }
        if ($etab) {
            echo "
            
            <tr>
                <td style=\"border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;border-left: 1px solid black\"><strong>TABELA</strong></td>
                <td width=\"40%\" style=\"border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black\" align=\"center\"><strong>$tab</strong></td>
                <td style=\"border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black\" align=\"center\">&nbsp;</td>
                <td width=\"40%\" style=\"border-top: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black\" align=\"center\"><strong>$tab</strong></td>
            </tr>
            <tr style=\"background-color: #555555; color: #FFFFFF\">
                <th>Coluna</th>
                <th>Defini��o {$manusis['db'][1]['base']}</th>
                <th>Migrar?????</th>
                <th>Defini��o {$manusis['db'][0]['base']}</th>                
            </tr>";
            //echo "<table border=\"1\" width=\"100%\" style=\"border-collapse: collapse\">";
            echo "
            $etab
            
            <tr><td colspan=\"4\" style=\"border:0px;\">&nbsp;</td></tr>
            <tr><td colspan=\"4\" style=\"border:0px;\">&nbsp;</td></tr>
            ";
        }
    }
    
    echo "</table>

    <input type=\"submit\" name=\"migrar\" value=\"Alterar colunas selecionadas\" class=\"botao\" onclick=\"window.onbeforeunload = null;\" />
    
    </form>";
}

/// Processando as mudan�as na base
if (($base_origem != '') and ($empresa_dest != 0) and ($_GET['migrar'] != '')) {
    
    echo "<h1>Passo 1: igualar as bases</h1>
    <hr />
    <h2>Realizando as altera��es selecionadas:</h2>
    <br /><br />";
    
    // O que deve ser migrado???
    $migrar = $_GET['migra_campo'];
    
    // Comparativo entre as tabelas
    $sql = "SHOW TABLES";
    
    // ORIGEM
    $rs_v3 = $dba[1]->Execute($sql);
    $tab_v3_tmp = $rs_v3->getrows();
    
    $tab_v3 = array();
    foreach ($tab_v3_tmp as $k => $v) {
        
        $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][1]['base']]} FROM {$manusis['db'][1]['base']}";
        $rs_col_v3 = $dba[1]->Execute($sql_tmp);
        
        $col_tmp = $rs_col_v3->getrows();
        $col = array();
        
        foreach ($col_tmp as $k2 => $v2) {
            if ($v2["Field"] != "MID") {
                $col[$v2["Field"]] = $v2;
            }
        }
    
        $tab_v3[$v['Tables_in_'. $manusis['db'][1]['base']]] = $col;
    }
    
    // MODELO V3 MULT
    $rs_dest = $dba[0]->Execute($sql);
    $tab_dest_tmp = $rs_dest->getrows();
    
    $tab_dest = array();
    foreach ($tab_dest_tmp as $k => $v) {
        
        $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][0]['base']]} FROM {$manusis['db'][0]['base']}";
        $rs_col_dest = $dba[0]->Execute($sql_tmp);
        
        $col_tmp = $rs_col_dest->getrows();
        
        $col = array();
        foreach ($col_tmp as $k2 => $v2) {
            if ($v2["Field"] != "MID") {
                $col[$v2["Field"]] = $v2;
            }
        }
        
        $tab_dest[$v['Tables_in_'. $manusis['db'][0]['base']]] = $col;
    }
    
    // Salva as tabelas que foram criadas
    $create_log = array();
    
    foreach ($tab_v3 as $tab => $cols) { 
        foreach ($cols as $col => $def) {
            if (($tab_dest[$tab][$col] != $def) and ($migrar[$tab][$col] == 1)) {
                // Iniciando as variaveis
                $alter_sql  = "";
                $create_sql = "";
                
                
                // A TABELA N�O EXISTE NO BANCO
                if ((! $tab_dest[$tab]) and ($create_log[$tab] == '')) {
                    $create_sql = "CREATE TABLE IF NOT EXISTS `$tab` (`MID` INT NOT NULL, `MID_OLD` INT NOT NULL DEFAULT 0, `MID_CARGA` INT NOT NULL DEFAULT 0, PRIMARY KEY ( `MID` )) ENGINE = MYISAM ";
                    $create_log[$tab] = 1;
                }
                
                // INICIANDO O ALTER
                $alter_sql = "ALTER TABLE `$tab` ";
                
                // O CAMPO N�O EXISTE
                if (! $tab_dest[$tab][$col]) {
                    $alter_sql .= " ADD COLUMN `{$def['Field']}` ";
                }
                // APENAS ALTERANDO
                else {
                    $alter_sql .= " CHANGE `{$def['Field']}` `{$def['Field']}` ";
                }
                
                // TIPO DE DADO
                $alter_sql .= mb_strtoupper("{$def['Type']}");
                
                
                // � OBRIGAT�RIO
                if ($def['Null'] == 'YES') {
                    $alter_sql .= " NULL ";
                }
                else {
                    $alter_sql .= " NOT NULL ";
                }
                
                // VALOR PADR�O
                if ($def['Default'] != '') {
                    $alter_sql .= " DEFAULT ";
                    
                    if (is_numeric($def['Default'])) {
                        $alter_sql .= " {$def['Default']} ";
                    }
                    else {
                        $alter_sql .= " '{$def['Default']}' ";
                    }
                }
                
                // EXTRA
                if ($def['Extra'] != '') {
                    $alter_sql .= mb_strtoupper(" {$def['Extra']} ");
                }
                
                // JA TA OK
                $alter_sql .= "";
                
                
                if ($create_sql != '') {
                    if (! $dba[0] -> Execute($create_sql)) {
                        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $create_sql);
                    }
                    echo $create_sql . ";<br />";
                }
                if ($alter_sql != '') {
                    if (! $dba[0] -> Execute($alter_sql)) {
                        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $alter_sql);
                    }
                    echo $alter_sql . ";<br />";
                }
            }
        }
    }
    
    
    echo "<br /><br />
    <input type=\"submit\" name=\"importar\" value=\"Passo 2: importar os dados do BD de origem\" class=\"botao\" onclick=\"window.onbeforeunload = null; window.location = '$nome_arquivo?base_origem=$base_origem&empresa_dest=$empresa_dest&importar_dados=1'\" />";
}


// Migrando os DADOS da base de origem
if (($base_origem != '') and ($empresa_dest != 0) and ($_GET['importar_dados'] == 1)) {
    echo "<h1>Passo 2: importar os dados do BD de origem</h1>
    <hr />";

    
    // Verificando se a empresa de destino n�o � a primeira a dar carga
    if ($_SESSION[ManuSess]['migra_tipo'] == 0) {
        $sql = "SELECT COUNT(*) AS TOTAL FROM " . AREAS . " WHERE MID_EMPRESA != $empresa_dest";
        if (! $rs_dest = $dba[0] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
        }
        // SE ESTA N�O � A PRIMEIRA EMPRESA � PRECISO FAZER UPDATE PRA FINALIZAR
        if ($rs_dest -> fields['TOTAL'] > 0) {
            $_SESSION[ManuSess]['migra_tipo'] = 2;
        }
        // � A PRIMEIRA EMPRESA, APENAS FAZER A CARGA E MANTER OS MIDS
        else {
            $_SESSION[ManuSess]['migra_tipo'] = 1;
        }
        
        // CASO TENHA MAIS DE UMA EMPRESA COLOCA O C�DIGO NA FRENTE
        if ($manusis['empresas'] > 1) {
            $_SESSION[ManuSess]['migra_cod'] = VoltaValor(EMPRESAS, 'COD', 'MID', $empresa_dest);
        }
        else {
            $_SESSION[ManuSess]['migra_cod'] = '';
        }
    }   
    
    
    // Registros por p�gina
    $pag_tam = 5000;
    
    // Comparativo entre as tabelas
    $sql = "SHOW TABLES";
    
    // ORIGEM
    $rs_v3 = $dba[1]->Execute($sql);
    $tab_v3_tmp = $rs_v3->getrows();
    
    $tab_v3 = array();
    foreach ($tab_v3_tmp as $k => $v) {
        
        $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][1]['base']]} FROM {$manusis['db'][1]['base']}";
        $rs_col_v3 = $dba[1]->Execute($sql_tmp);
        
        $col_tmp = $rs_col_v3->getrows();
        $col = array();
        
        foreach ($col_tmp as $k2 => $v2) {
            $col[$v2["Field"]] = $v2;
        }
    
        $tab_v3[$v['Tables_in_'. $manusis['db'][1]['base']]] = $col;
    }
    
    // MODELO V3 MULT
    $rs_dest = $dba[0]->Execute($sql);
    $tab_dest_tmp = $rs_dest->getrows();
    
    $tab_dest = array();
    foreach ($tab_dest_tmp as $k => $v) {
        
        $sql_tmp = "SHOW COLUMNS FROM {$v['Tables_in_'. $manusis['db'][0]['base']]} FROM {$manusis['db'][0]['base']}";
        $rs_col_dest = $dba[0]->Execute($sql_tmp);
        
        $col_tmp = $rs_col_dest->getrows();
        
        $col = array();
        foreach ($col_tmp as $k2 => $v2) {
            $col[$v2["Field"]] = $v2;
        }
        
        $tab_dest[$v['Tables_in_'. $manusis['db'][0]['base']]] = $col;
    }
    
    // Passando por todas as tabelas
    $cont_tab = 0;
    foreach ($tab_dest as $tab => $cols) {
        // Somente se a tabela existir na ORIGEM
        if ($tab_v3[$tab] != '') {
            if (($_SESSION[ManuSess]['migra'][$tab] == 1) or (array_search($tab, $tabelas_ignoradas) !== FALSE)) {
                $cont_tab ++;
                continue;
            }
            else {
                echo "<h2>Importando tabela: $tab</h2>";
                
                // Iniciando Transa��o
                $dba[0] -> StartTrans();
                
                // Recuperando a p�gina atual
                $pag_ini = (int)$_GET['pag_ini'];
            }
            
            // Tratamento de erros
            $erro = 0;
            
            // Buscando apenas os campos que existem nas duas bases
            $campos_select = array();
            foreach ($cols as $col => $def) {
                if ($tab_v3[$tab][$col] != '') {
                    $campos_select[] = $col;
                }
            }
            
            
            // Numero de registros total
            $select_origem = "SELECT COUNT(*) AS TOTAL FROM " . $tab . " ORDER BY MID ASC";
            if (! $rs_origem = $dba[1] -> Execute($select_origem)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[1] -> ErrorMsg() . "<br />" . $select_origem);
            }
            $total_origem = $rs_origem -> fields['TOTAL'];
            
            
            // Buscando na base de origem apenas as tabelas encontradas na nova base
            $select_origem = "SELECT " . implode(", ", $campos_select) . " FROM " . $tab . " ORDER BY MID ASC";
            
            
            // Dependendo da quantidade de registros fazer paginando
            if ($total_origem > $pag_tam) {
                $rs_origem = $dba[1] -> SelectLimit($select_origem, $pag_tam, $pag_ini);
                $pag_ini += $pag_tam;
            }
            else {
                $rs_origem = $dba[1] -> Execute($select_origem);
            }
            
            if (! $rs_origem) {
                erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[1] -> ErrorMsg() . "<br />" . $select_origem);
                $erro = 1;
            }
            
            // Recuperando os dados
            $dados_origem = $rs_origem -> GetRows();
            
            // Campo para salvar o MID antigo
           /* $campos_select[] = "MID_OLD";
            $campos_select[] = "MID_CARGA";*/
                        
            // Verifica se a tabela tem MID_EMPRESA
            if (($tab_dest[$tab]['MID_EMPRESA'] != '') and ($tab_v3[$tab]['MID_EMPRESA'] == '')) {
                $campos_select[] = "MID_EMPRESA";
            }
            
            if (($tab == ATIVIDADES or $tab == LINK_ROTAS) and ($tab_v3[$tab]['QUANTIDADE_MO'] == '')) {
                $campos_select[] = 'QUANTIDADE_MO';
            }
            
            $mid_cont = GeraMid($tab, 'MID', 0);
            $ii = 0;
            foreach ($dados_origem as $dado) {
                // Montando o INSERT
                $insert_dest = "INSERT INTO $tab (" . implode(", ", $campos_select) . ")";
                
                
                // Adicionando os ? para facilitar o insert
                $insert_dest .= " VALUES (";
                
                for ($i = 0; $i < count($campos_select); $i++) {
                    if ($i > 0) $insert_dest .= ", ";
                    $insert_dest .= "?";
                }
                
                $insert_dest .= ")";
                
                //$dado['MID_OLD'] = $dado['MID'];
                //$dado['MID_CARGA'] = $_SESSION[ManuSess]['migra_mid'];
                
                // CASO N�O FOR A SEGUNDA BASE A MIGRAR MUDAR OS MIDS E FAZER UPDATE DEPOIS
                if ($_SESSION[ManuSess]['migra_tipo'] == 2) {
                    $dado['MID'] = $mid_cont;
                }
                
                // Colocar c�digo na frente
                if (($_SESSION[ManuSess]['migra_cod'] != '') and ($manusis['empresas'] > 1)){
                    // REFAZ O NUMERO COM BASE NA EMPRESA
                    if ($tab == ORDEM) {
                        $dado['NUMERO'] = $_SESSION[ManuSess]['migra_cod'] . '-' . $dado['NUMERO'];
                    }
                    
                    // REFAZ O NUMERO COM BASE NA EMPRESA
                    if ($tab == SOLICITACOES) {
                        $dado['NUMERO'] = $_SESSION[ManuSess]['migra_cod'] . '-' . $dado['NUMERO'];
                    }
                    
                    // REFAZ O NUMERO COM BASE NA EMPRESA
                    if ($tab == PENDENCIAS) {
                        $dado['NUMERO'] = $_SESSION[ManuSess]['migra_cod'] . '-' . $dado['NUMERO'];
                    }
                }
                
                
                // Verifica se a tabela tem MID_EMPRESA
                if ($tab_dest[$tab]['MID_EMPRESA'] != '') {
                    $dado['MID_EMPRESA'] = $empresa_dest;
                }
                
                // Tratando os campos TIME que mudaram para FLOAT
                if (($tab == ATIVIDADES) and (strpos($dado['TEMPO_PREVISTO'], ':') !== false)) {
                    $dado['TEMPO_PREVISTO'] = TimeToFloat($dado['TEMPO_PREVISTO']);
                }
                if (($tab == ATIVIDADES) and ($dado['QUANTIDADE_MO'] == 0)) {
                    $dado['QUANTIDADE_MO'] = 1;
                }
                if (($tab == LINK_ROTAS) and (strpos($dado['TEMPO_PREVISTO'], ':') !== false)) {
                    $dado['TEMPO_PREVISTO'] = TimeToFloat($dado['TEMPO_PREVISTO']);
                }
                if (($tab == LINK_ROTAS) and ($dado['QUANTIDADE_MO'] == 0)) {
                    $dado['QUANTIDADE_MO'] = 1;
                }
                
                if (! $dba[0] -> Execute ($insert_dest, array_values($dado))){
                    echo "<pre>";
                    print_r($dado);
                    echo "</pre>";
                    erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $insert_dest);
                    $erro = 1;
                    break;
                }
                else {
                    //echo "# ";
                    $mid_cont ++;
                    $ii ++;
                }
            }
            
            if (($erro == 0) and ($pag_ini == 0 or $pag_ini >= $total_origem)) {
                $_SESSION[ManuSess]['migra'][$tab] = 1;
                $pag_ini = 0;
            }
            
            // Finalizando a Transa��o
            $dba[0] -> CompleteTrans();
            
            break;
        }
        $cont_tab ++;
    }
    
    if ($pag_ini > $pag_tam) {
        $ii += $pag_ini;
    }
    
    echo "<h2>Total de registros na origem: $total_origem / Total de registros importados: $ii</h2><br /><br />";
    
    if ($cont_tab == count($tab_dest)) {
        // CRIANDO REGISTRO DA CARGA
        $ins = "INSERT INTO cargas (MID, DATA_HORA, BASE_ORIGEM, MID_EMPRESA) VALUES ({$_SESSION[ManuSess]['migra_mid']}, '" . date('Y-m-d H:i:s') . "', '$base_origem', $empresa_dest)";
        if (! $dba[0] -> Execute($ins)){
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $ins);
        }
        
        if ($_SESSION[ManuSess]['migra_tipo'] == 2) {
            echo "<input type=\"submit\" name=\"importar\" value=\"Passo 3: atualiza��o das rela��es entre as tabelas\" class=\"botao\" onclick=\"window.onbeforeunload = null; window.location = '$nome_arquivo?base_origem=$base_origem&empresa_dest=$empresa_dest&update_relacao=1'\" />";
        }
        else {
            // ULTIMAS ATUALIZA��ES NOS DADOS
            pos_migracao ();
            
            echo "<h1><center>PARABENS!!!! MIGRA��O CONCLUIDA, AGORA SE VIRA COM O RESTO!!!!!</center></h1>
            <center><a href=\"$nome_arquivo\" onclick=\"window.onbeforeunload = null;\">Migrar outra base de dados</a></center>";
        }
    }
    else {
        if ($pag_ini == 0) {
            echo "<input type=\"submit\" name=\"importar\" value=\"Pr�xima tabela >>>>>\" class=\"botao\" onclick=\"window.onbeforeunload = null; window.location = '$nome_arquivo?base_origem=$base_origem&empresa_dest=$empresa_dest&importar_dados=1'\" />";
        }
        else {
            echo "<input type=\"submit\" name=\"importar\" value=\"Pr�xima p�gina nessa tabela >>>>>\" class=\"botao\" onclick=\"window.onbeforeunload = null; window.location = '$nome_arquivo?base_origem=$base_origem&empresa_dest=$empresa_dest&importar_dados=1&pag_ini=$pag_ini'\" />";
        }
    }
}

// ATUALIZANDO AS RELA��ES ENTRE AS TABELAS
if (($base_origem != '') and ($empresa_dest != 0) and ($_GET['update_relacao'] == 1)) {
    echo "<h1>Passo 3: atualiza��o das rela��es entre as tabelas</h1>
    <hr />\n";
    
    if (VoltaValor('cargas', 'ATUALIZADO', 'MID', $_SESSION[ManuSess]['migra_mid']) == 1) {
        echo "<h2 style=\"color:red;\"><center>CUIDADO ESSA BASE J� PASSOU PELO PROCESSO DE ATUALIZA��O!!!!!</center></h2>\n";
    }
    
    // Passando por todas as tabelas
    foreach ($tdb as $tab => $def) {
        if(array_search($tab, $tabelas_ignoradas) !== FALSE) {
            continue;
        }

        // Formatando a saida
        echo "<br />\n# ATUALIZANDO TABELA $tab<br /><br />\n";
        
        if ($tab == PROGRAMACAO) {
            // Passando PELO CAMPO MID_PLANO
            // Preventivas
            $update = "UPDATE `".PROGRAMACAO."` A, `".PLANO_PADRAO."` B SET A.MID_PLANO = B.MID WHERE A.TIPO = 1 AND A.MID_PLANO = B.MID_OLD AND A.MID_CARGA = B.MID_CARGA AND A.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']};";
            echo $update . "<br />\n";
            
            // Rotas
            $update = "UPDATE `".PROGRAMACAO."` A, `".PLANO_ROTAS."` B SET A.MID_PLANO = B.MID WHERE A.TIPO = 2 AND A.MID_PLANO = B.MID_OLD AND A.MID_CARGA = B.MID_CARGA AND A.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']};";
            echo $update . "<br />\n";
            
            // Solicita��es
            $update = "UPDATE `".PROGRAMACAO."` A, `".SOLICITACOES."` B SET A.MID_PLANO = B.MID WHERE A.TIPO = 4 AND A.MID_PLANO = B.MID_OLD AND A.MID_CARGA = B.MID_CARGA AND A.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']};";
            echo $update . "<br />\n";
        }
        
        // Passando por todos os campos
        foreach ($def as $campo => $desc) {
            if (($campo == 'DESC') or ($campo == 'dba')) continue;
            
            // Esse campo ja foi tratado ali em cima
            if (($tab == PROGRAMACAO) and ($campo == 'MID_PLANO')) continue;
            
            // Rela��es desse campo
            $rtb = VoltaRelacao($tab, $campo);
            if (($rtb != '') and (array_search($rtb['tb'], $tabelas_ignoradas) === FALSE)) {
                $update = "UPDATE `$tab` A, `{$rtb['tb']}` B SET A.$campo = B.MID WHERE  A.$campo = B.MID_OLD AND A.MID_CARGA = B.MID_CARGA AND A.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']};";
                echo $update . "<br />\n";
            }
        }
    }
    
    
    echo "<h2><center>AGORA RODE OS UPDATES ACIMA NO BANCO DE DADOS!!!</center></h2>";
    
    // ULTIMAS ATUALIZA��ES NOS DADOS
    pos_migracao ();
    
    echo "<br />";
    echo "<br />";
    echo "<input type=\"submit\" name=\"importar\" value=\"Passo 4: c�pia da pasta arquivos\" class=\"botao\" onclick=\"window.onbeforeunload = null; window.location = '$nome_arquivo?base_origem=$base_origem&empresa_dest=$empresa_dest&copia_arquivos=1'\" />";
    
    // mudando para ja atualizado o status da carga
    UpdateItem('cargas', "ATUALIZADO = 1", 'MID', $_SESSION[ManuSess]['migra_mid']);
}

// COPIANDO O CONTE�DO DA PASTA ARQUIVOS
if (($base_origem != '') and ($empresa_dest != 0) and ($_GET['copia_arquivos'] == 1)) {
    echo "<h1>Passo 4: c�pia da pasta arquivos</h1>
    <hr />";
    
    $_SESSION[ManuSess]['migra_mid'] = 2;
    
    // Recuperando o caminho informado
    $pasta_arquivos = $_GET['pasta_arquivos'];
    
    // Validando se o valor � um diret�rio
    if (! is_dir($pasta_arquivos)) {
        
        // Mostrando erro
        if ($pasta_arquivos != '') {
            erromsg("O caminho informado n�o � um diret�rio v�lido.");
        }
        
        echo "<form action=\"compara_base.php\" method=\"GET\">
        
        <input type=\"hidden\" name=\"base_origem\" value=\"$base_origem\" />
        <input type=\"hidden\" name=\"empresa_dest\" value=\"$empresa_dest\" />
        <input type=\"hidden\" name=\"copia_arquivos\" value=\"1\" />
        <label for=\"pasta_arquivos\">Informe o caminho da pasta arquivos apartir da raiz (/): </label> 
        <input type=\"text\" class=\"campo_text\" size=\"50\" id=\"pasta_arquivos\" name=\"pasta_arquivos\" />
        <input type=\"submit\" name=\"env2\" class=\"botao\" value=\"Enviar\" onclick=\"window.onbeforeunload = null;\" />
        
        </form>";
    }
    else {
        
        // Verifica se o caminho informado termina em /
        if (substr($pasta_arquivos, -1) != '/') {
            $pasta_arquivos .= '/';
        }
        
        // Pasta arquivos desse modelo v3
        $pasta_destino = realpath('./arquivos/');
        
        // Passando pelos diret�rios configurados de anexo
        foreach($manusis['dir'] as $chave => $pasta_tmp) {
            // ENTRADAS QUE SER�O IGNORADAS NA C�PIA
            if (($chave == 'principal') or ($chave == 'temporarios') or ($chave == 'exportar')) {
                continue;
            }
            
            // Deixando apenas a pasta
            $pasta = str_replace('arquivos/', '', $pasta_tmp);
            
            // Abrindo o diret�rio
            $dir_origem = dir($pasta_arquivos . "" . $pasta);
            while (false !== ($item = $dir_origem->read())) {
                // pulando os desnecess�rios
                if (($item == '.') or ($item == '..') or (substr($item, 0, 1) == '.')) continue;
                
                // Caminho completo do atual
                $item_path = $pasta_arquivos . "" . $pasta . "/" . $item;
                
                // Destino no MODELO V3
                $item_dest = $pasta_destino . "/" . $pasta . "/";
                
                // Se n�o for um diret�rio apenas copie para o novo
                if (! is_dir($item_path)) {
                    $item_dest .= $item;
                    
                    if (! copy($item_path, $item_dest)) {
                        erromsg("Erro ao copiar 1: $item_path para $item_dest");
                    }
                    else {
                        echo "Copiando arquivo $item_path para $item_dest...<br />\n";
                    }
                }
                // Aqui � que o bicho pega
                else {
                    // � um diret�rio que esta configurado, ent�o sei pra onde mandar
                    if ($arquivos[$chave] != '') {
                        // Buscando o novo nome da pasta, ou seja, o novo MID do cadastro
                        $item_novo = $item_dest . "" . VoltaValor($arquivos[$chave], 'MID', array('MID_OLD', 'MID_CARGA'), array($item, $_SESSION[ManuSess]['migra_mid']));
                        // Verifica sen�o existe um igual e cria se preciso
                        if (! is_dir($item_novo)) {
                            // Criando o novo diret�rio
                            if (! mkdir($item_novo)){
                                erromsg("Erro ao criar diret�rio: $item_novo");
                            }
                            else {
                                echo "Criando diret�rio: $item_novo...<br />\n";
                            }
                        }
                    }
                    // Como n�o sei o que fazer uso o mesmo
                    else {
                        $item_novo = $item_dest . "" . $item;
                        
                        // Verifica sen�o existe um igual e cria se preciso
                        if (! is_dir($item_novo)) {
                            // Criando o novo diret�rio
                            if (! mkdir($item_novo)){
                                erromsg("Erro ao criar diret�rio: $item_novo");
                            }
                            else {
                                echo "Criando diret�rio: $item_novo...<br />\n";
                            }
                        }
                    }
                    
                    // Navegando no diret�rio para copiar os arquivos
                    $dir_item = dir($item_path);
                    while (false !== ($item_filho = $dir_item->read())) {
                        // pulando os desnecess�rios
                        if (($item_filho == '.') or ($item_filho == '..') or (substr($item_filho, 0, 1) == '.')) continue;
                        
                        // Caminho completo do filho
                        $item_filho_path = $item_path . "/" . $item_filho;
                        
                        $item_filho_novo = $item_novo . "/" . $item_filho;
                        
                        if (! copy($item_filho_path, $item_filho_novo)) {
                            erromsg("Erro ao copiar 2: $item_filho_path para $item_filho_novo");
                        }
                        else {
                            echo "Copiando arquivo $item_filho_path para $item_filho_novo...<br />\n";
                        }
                    }
                    
                }
            }
            
            $dir_origem->close();
        }
        
        echo "<br />";
        echo "<br />";
        
        echo "<h1><center>PARABENS!!!! MIGRA��O CONCLUIDA, AGORA SE VIRA COM O RESTO!!!!!</center></h1>
        <center><a href=\"$nome_arquivo\" onclick=\"window.onbeforeunload = null;\">Migrar outra base de dados</a></center>";
    }
}

echo "</div>
</body>
</html>";


function Campo($k, $v) {
    $ling_db_yn['YES']='SIM';
    $ling_db_yn['NO']='N�O';
    
    $ling_db_key['UNI']='UNICA';
    $ling_db_key['PRI']='PRIM�RIA';
    
    
    $key = $k;
    switch ($k) {
        case 'Field': $key = 'CAMPO'; return ''; break;
        case 'Type': $key = 'TIPO'; break;
        case 'Null': $key = 'NULO'; break;
        case 'Default': $key = 'VALOR PADR�O'; break;
        case 'Key': $key = 'CHAVE'; break;
        case 'Extra': $key = 'EXTRA'; break;
    }
    if ($k == 'Key') {
        $val = $ling_db_key[$v];
    }
    if ($k == 'Null') {
        $val = $ling_db_yn[$v];
    }
    if (!$val) $val = $v;
    return "$key: $val";
}

function pos_migracao ($mid_carga = 0) {
    global $dba, $manusis;
    
    if ($mid_carga != 0) {
        $_SESSION[ManuSess]['migra_mid'] = $mid_carga;
    }
    
    echo "<br />\n";
    echo "<br />\n";

    echo "<h2><center>PARA FINALIZAR, ALGUMAS MUDAN�AS NAS INFORMA��ES DO SISTEMA. RODE OS SEGUINTES UPDATES NO BANCO DE DADOS!!!</center></h2>";
        

    // ATUALIZANDO O ORDEM_PREV COM OS NOVOS CAMPOS
    echo "# ATUALIZANDO O ORDEM_PREV, AGORA OS CAMPOS DA ATIVIDADE S�O REPLICADOS NELE <br />\n";

    echo "UPDATE  " 
     . ORDEM_PREV . " as a, " 
     . ATIVIDADES . " as b ".
    "SET 
    a.MID_PLANO_PADRAO      = b.MID_PLANO_PADRAO,
    a.PERIODICIDADE         = b.PERIODICIDADE,
    a.FREQUENCIA            = b.FREQUENCIA, 
    a.CONTADOR              = b.CONTADOR, 
    a.DISPARO               = b.DISPARO,
    a.MID_CONJUNTO          = b.MID_CONJUNTO, 
    a.MID_MATERIAL          = b.MID_MATERIAL,
    a.QUANTIDADE            = b.QUANTIDADE,
    a.NUMERO                = b.NUMERO,
    a.TAREFA                = b.TAREFA, 
    a.PARTE                 = b.PARTE,
    a.INSTRUCAO_DE_TRABALHO = b.INSTRUCAO_DE_TRABALHO,
    a.TEMPO_PREVISTO        = b.TEMPO_PREVISTO,
    a.OBSERVACAO            = b.OBSERVACAO,
    a.ESPECIALIDADE         = b.ESPECIALIDADE, 
    a.MAQUINA_PARADA        = b.MAQUINA_PARADA,
    a.QUANTIDADE_MO         = b.QUANTIDADE_MO    
    WHERE 
        a.MID_ATV   = b.MID
    AND a.MID_ORDEM IN (SELECT MID FROM ". ORDEM."  WHERE TIPO = 1 AND MID_CARGA = {$_SESSION[ManuSess]['migra_mid']})
    AND a.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']} 
    AND b.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']};<br />\n";


    echo "<br />\n";
    echo "<br />\n";

    // ATUALIZANDO O ORDEM_LUB COM OS NOVOS CAMPOS
    echo "# ATUALIZANDO O ORDEM_LUB, AGORA OS CAMPOS DO LINK_ROTAS S�O REPLICADOS NELE <br />\n";

    echo "UPDATE  " 
     . ORDEM_LUB . " as a, " 
     . LINK_ROTAS . " as b ".
    "SET 
    a.TIPO            = b.TIPO,
    a.MID_PLANO       = b.MID_PLANO,
    a.PERIODICIDADE   = b.PERIODICIDADE,
    a.FREQUENCIA      = b.FREQUENCIA, 
    a.CONTADOR        = b.CONTADOR, 
    a.DISPARO         = b.DISPARO,
    a.MID_MAQUINA     = b.MID_MAQUINA, 
    a.MID_CONJUNTO    = b.MID_CONJUNTO, 
    a.MID_PONTO       = b.MID_PONTO, 
    a.MID_MATERIAL    = b.MID_MATERIAL,
    a.QUANTIDADE      = b.QUANTIDADE,
    a.NUMERO          = b.NUMERO, 
    a.TAREFA          = b.TAREFA, 
    a.STATUS          = b.STATUS,
    a.TEMPO_PREVISTO  = b.TEMPO_PREVISTO,
    a.ESPECIALIDADE   = b.ESPECIALIDADE, 
    a.QUANTIDADE_MO   = b.QUANTIDADE_MO    
    WHERE 
        a.MID_ATV   = b.MID
    AND a.MID_ORDEM IN (SELECT MID FROM ". ORDEM."  WHERE TIPO = 2 AND MID_CARGA = {$_SESSION[ManuSess]['migra_mid']})
    AND a.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']} 
    AND b.MID_CARGA = {$_SESSION[ManuSess]['migra_mid']}; <br />\n";


    echo "<br />\n";
    echo "<br />\n";

    echo "# ATUALIZANDO A LIGA��O COM AS SOLICITA��ES, AGORA N�O PASSA MAIS PELA PROGRAMA��O <br />\n";

    // ATUALIZANDO O CAMPO MID_SOLICITACAO DAS OS
    echo "UPDATE " . ORDEM . " O SET MID_SOLICITACAO = (SELECT MID_PLANO FROM " . PROGRAMACAO . " WHERE TIPO = 4 AND MID = O.MID_PROGRAMACAO) WHERE O.TIPO = 4 AND MID_PROGRAMACAO != 0 AND MID_SOLICITACAO = 0 AND MID_CARGA = {$_SESSION[ManuSess]['migra_mid']}; <br />\n";

    echo "<br />\n";
    echo "<br />\n";
    
}

?>
