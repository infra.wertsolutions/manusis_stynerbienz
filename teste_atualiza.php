<?php

// Funções do Sistema
if (!require("lib/mfuncoes.php")) die ("".$ling['arq_estrutura_nao_pode_ser_carregado']."");
// Configurações
elseif (!require("conf/manusis.conf.php")) die ("".$ling['arq_configuracao_nao_pode_ser_carregado']."");
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ("".$ling['arq_idioma_nao_pode_ser_carregado']."");
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Autentificação
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Formulários
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);

// gerar contadores.

$sql = " SELECT MID, MID_MAQUINA FROM ".PRODUTOS_ACABADOS." WHERE MID_MAQUINA != 0";

if(!$rs = $dba[0]->Execute($sql)){
    erromsg("Erro ao localizar as maquinas em <br />
    Linha: ".__LINE__." <br />
    Erro: {$dba[0]->ErrorMsg()} <br />
    SQL: $sql

    ");
}
elseif (!$rs->EOF) {

    $i = 0;
    while(!$rs->EOF){
        $row = $rs->fields;
        
//        criaContadorIntegracao((int)$row['MID_MAQUINA'], (int)$row['MID']);
        salvaMovimentacaoInicialProduto($row['MID']);
        
        $rs->MoveNext();
        $i ++;
    }
    
    echo "$i Movimentações geradas";
    
    
}
else{
    echo "No Way to create the counters";
}
?>
