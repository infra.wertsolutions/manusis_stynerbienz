<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Arquivo Base
*/
// Funções de Estrutura
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
elseif (!require("lib/autent.php")) die ($ling['autent01']);

$tb=$_GET['tb'];
$filtro_tipo=(int)$_GET['filtro_tipo'];
$tipo=(int)$_GET['tipo'];
$id=(int)$_GET['id'];
$oq=(int)$_GET['oq'];
$obj=(int)$_GET['obj'];
if ($tb == "DEFEITOS") include ("relatorios/graficodefeitos.php");
elseif ($tb == "SERVICOS") include ("relatorios/graficoservicos.php");
elseif ($tb == "MAQUINAS") include ("relatorios/graficomaquinas.php");
elseif ($tb == "SERVICOS_SETOR") include ("relatorios/graficoservicossetor.php");
elseif ($tb == "SERVICOS_SETOR2") include ("relatorios/graficoservicossetor2.php");
elseif ($tb == "FUNCIONARIOS") include ("relatorios/graficofuncionarios.php");
elseif ($tb == "SERVPEGU") include ("relatorios/graficoservicospeguform.php");
elseif ($tb == "DF1") include ("relatorios/graficodf1.php");
elseif ($tb == "DF2") include ("relatorios/graficodf2.php");
elseif ($tb == "PREVCORR") include ("relatorios/graficoprevcorr.php");
elseif ($tb == "PLANEXEC") include ("relatorios/graficoplanexec.php");
elseif ($tb == "DESOP") include ("relatorios/graficodesop.php");
elseif ($tb == "MAQPARADA") include ("relatorios/graficomaqparada.php");
elseif ($tb == "PARADA") include ("relatorios/graficoparada.php");
elseif ($tb == "PARADA2") include ("relatorios/graficoparada2.php");
elseif ($tb == "TOP10PEGU") include ("relatorios/graficotop10pegu.php");
elseif ($tb == "PARADAMUL") include ("relatorios/graficoparadamultilite.php");
elseif ($tb == "FUNCIONARIOSPERC") include ("relatorios/graficofuncionariosperc.php");
elseif ($tb == "SOLIC_EMERG") include ("relatorios/graficosolxemer.php");
elseif ($tb == "ABERTASFECHADAS") include("relatorios/graficoabertasxfechadas.php");
elseif ($tb == "RANKINGMAQ") include("relatorios/graficorankingmaq.php");
elseif ($tb == "CCON") include("relatorios/conduspar_integrado.php");
elseif ($tb == "PREDITIVA") include("modulos/planejamento/pred_graf.php");
elseif ($tb != "") include ("relatorios/".LimpaTexto($tb).".php");

function  gera_grafico($titulo,$filtros,$ordenado,$plan,$graf){
	global $manusis;
	$doc.="<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
xmlns:o=\"urn:schemas-microsoft-com:office:office\"
xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content=\"Manusis Web\">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author> Manusistem</o:Author>
  <o:LastAuthor> Manusistem</o:LastAuthor>
  <o:LastPrinted>2006-06-17T14:07:58Z</o:LastPrinted>
  <o:Created>2006-06-07T17:44:39Z</o:Created>
  <o:LastSaved>2006-06-17T14:09:43Z</o:LastSaved>
  <o:Company>Manusistem</o:Company>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
  <o:LocationOfComponents HRef=\"file:///D:\\\" />
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:\"\,\";
	mso-displayed-thousand-separator:\"\.\";}
@page
	{margin:.39in .2in .39in .2in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;
	mso-page-orientation:landscape;
	mso-horizontal-page-align:center;}
.font7
	{color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	mso-number-format:\"\@\";}
.xl25
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	border:.5pt solid windowtext;
	background:gray;
	mso-pattern:auto none;}
.xl26
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	border:.5pt solid windowtext;}
.xl27
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	border:.5pt solid windowtext;
	background:gray;
	mso-pattern:auto none;}
.xl28
	{mso-style-parent:style0;
	font-size:8.0pt;
	text-align:center;
	border:.5pt solid windowtext;}
.xl29
	{mso-style-parent:style0;
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;}
.xl30
	{mso-style-parent:style0;
	font-size:14.0pt;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	mso-number-format:\"\@\";
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;}
.xl31
	{mso-style-parent:style0;
	font-size:8.0pt;
	font-weight:700;
	mso-number-format:\"\@\";
	text-align:right;
	vertical-align:top;
	border:.5pt solid windowtext;
	white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Plan1</x:Name>
    <x:WorksheetOptions>
     <x:Print>
      <x:ValidPrinterInfo/>
      <x:PaperSizeIndex>9</x:PaperSizeIndex>
      <x:HorizontalResolution>300</x:HorizontalResolution>
      <x:VerticalResolution>300</x:VerticalResolution>
     </x:Print>
     <x:Selected/>
     <x:TopRowVisible>2</x:TopRowVisible>
     <x:Panes>
      <x:Pane>
       <x:Number>3</x:Number>
       <x:ActiveRow>30</x:ActiveRow>
       <x:ActiveCol>3</x:ActiveCol>
      </x:Pane>
     </x:Panes>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
    <x:Sorting>
     <x:Sort>$ordenado</x:Sort>
     <x:Descending/>
    </x:Sorting>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>8580</x:WindowHeight>
  <x:WindowWidth>11340</x:WindowWidth>
  <x:WindowTopX>480</x:WindowTopX>
  <x:WindowTopY>45</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext=\"edit\" spidmax=\"1028\"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext=\"edit\">
  <o:idmap v:ext=\"edit\" data=\"1\"/>
 </o:shapelayout></xml><![endif]-->
</head><body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 width=987 style='border-collapse: collapse; table-layout:fixed;width:740pt'>
 <col width=120 style='mso-width-source:userset;mso-width-alt:4388;width:90pt'>
 <col width=99 style='mso-width-source:userset;mso-width-alt:3620;width:74pt'>
 <col width=64 span=12 style='width:48pt'>
 <tr height=24 style='mso-height-source:userset;height:18.0pt'>
  <td colspan=2 rowspan=2 height=68 class=xl29 width=219 style='height:51.0pt;  width:164pt'><img src=\"".$manusis['url']."temas/padrao/imagens/logoempresa.png\" border=0 vspace=2 hspace=2 align=center></td>
  <td colspan=12 class=xl30 width=768 style='border-left:none;width:576pt'>$titulo</td>
 </tr>
 <tr class=xl24 height=44 style='mso-height-source:userset;height:33.0pt'>
  <td colspan=12 height=44 class=xl31 width=768 style='height:33.0pt;  border-left:none;width:576pt'>{$ling['filtros_m']}:<font class=font7><br>
  $filtros
  </font></td></tr>
 <tr height=14 style='mso-height-source:userset;height:10.5pt'>
  <td height=14 colspan=14 style='height:10.5pt;mso-ignore:colspan'></td>
 </tr>
$plan
$graf
 <tr height=17 style='height:12.75pt'>
  <td colspan=14 height=17 class=xl31 style='border-right:.5pt solid black; height:12.75pt'>EMITIDO POR ".$_SESSION[ManuSess]['user']['NOME']." ".date("d/m/Y H:i")."<br />
</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=120 style='width:90pt'></td>
  <td width=99 style='width:74pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>
</body>
</html>";
	header("Content-type: application/msexcel");
	header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header("Content-disposition: inline; filename=excel.xls");
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header("Pragma: public");
	echo $doc;
}
?>
